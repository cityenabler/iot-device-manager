# Connect a new IoT Agent

The IoT Device Manager is entirely based on the FIWARE IoT Agents and natively supports a plethora of protocols and data formats that are already provided by the FIWARE ecosystem.


Anyway, if needed, whoever is in the condition of **developing his own agent** thanks to the **[FIWARE IoT Agent Framework](https://iotagent-node-lib.readthedocs.io/en/latest/)**.
Such a framework provides a [library](https://github.com/telefonicaid/iotagent-node-lib) that can be exploited by developers to implement a new IoT Agent ready to be connected to the IoT Device Manager. 


The development of a new Agent is out of the scope of this guide, further instruction and best practices can be found [here](https://github.com/telefonicaid/iotagent-node-lib/blob/master/doc/usermanual.md).


## Prerequisite
In order to add the support for a new agent to the IoT Device Manager the developer must previously:

- checkout the latest version of the IoT Device Manager source code as described in the installing section of the [README](../README.md) file.
- configure an up and running FIWARE IoTStack composed at least by:
  - FIWARE IDM **Keyrock 7**
  - FIWARE **Orion** Context Broker
  - the following **FIWARE IoT Agents**:
    - agent-json
    - agent-ul20
- develop the new IoT Agent
- take note about the new IoT Agent deployment details (e.g. **ip**, **northbound port**, **southbound port**)

Once these prerequisites are satisfied, the developer has to register the new IoT Agent on the IoT Device Manager. This implies some coding both on the Front-End part and in the Back-End part of the tool.  

##### BACK-END

1. Create a **configuration file** for the new agent
   - Open the package it.eng.iot.configuration and create the file configuration_iotaXYZ.properties.
     Here is a template for the configuration_iotXYZ.properties file:


```	
	###########################################
	############# IoTAgent XYZ ################
	###########################################
	
	#Put here your protocol name
	iota.xyz.data.protocol=xyz
	
	#Allowed values are http:// and https://
	iota.xyz.host.protocol=http://
	
	#ip/dns where the new agent is reachable
	iota.xyz.host=127.0.0.1
	
	#the port the agent will receive the device and service metadata through  
	iota.xyz.northbound.port=1234
	
	#the port the agent will receive the device measures through
	iota.xyz.southbound.port=5678
	
	#the public URL the agent will receive the device measures through
	#This will be shown in the infopoint after the publication of a device
	iota.xyz.infopoint.endpoint=http://127.0.0.1:5678
```	

2. Create the **ConfIotaXYZ** class.
   - Open the package it.eng.iot.configuration and define a new Class named ConfIotaXYZ.
     Here is a template for the class:

```    
    public class ConfIotaXYZ {
	    private static final String BUNDLE_NAME = "it.eng.iot.configuration.configuration_iotaXYZ"; //$NON-NLS-1$

	    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	    private ConfIotaXYZ() {}

	    public static String getString(String key) {
			try {
				Optional<String> env = Optional.ofNullable(System.getenv(key));
				return env.orElse(RESOURCE_BUNDLE.getString(key));
			} catch (MissingResourceException e) {
				return '!' + key + '!';
			}
		}
	}
```

>This class prioritize the access to the properties. First it searches in the **Environment variables** and then, if it founds nothing, falls back to the properties file. So the developer can define the properties of the configuration_iotaXYZ.properties file through the homonymous env variables.

3. Extend the **dataformats** enumeration
   - Open the class _it.eng.iot.servlet.model.TransportProtocol.java_ 
   - Add your supported dataformats to the enumeration (e.g. JSON)

4. Extend the **protocols** enumeration
   - Open the class _it.eng.iot.servlet.model.DataformatProtocol.java_ 
   - Add your supported protocols to the enumeration (e.g. XYZ)

5. Create the **FEXYZDevicePublishedConfirmBean** class 
   - open the package it.eng.iot.servlet.model
   - Create a class named **FEXYZDevicePublishedConfirmBean**
     Here is a template for the class:

```
public abstract class FEXYZDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {
	public FEXYZDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.XYZ);
	}
}
```

6. Update the **DeviceConfigManager** class.
   - Open the file _it.eng.iot.utils.DeviceConfigManager.java_ 
   - go to the **getDeviceConfirmantionData** method
   - just before the **return** statement add a new **else if** condition like the following:

```  
       // XYZ
		else if (transportProtocol.toLowerCase().equalsIgnoreCase("XYZ")){
			LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);
			LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);
			LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
			
			FEXYZDevicePublishedConfirmBean devicePublishedConfirm = new FEXYZDevicePublishedConfirmBean();

			devicePublishedConfirm.setApikey(apikey);
			endpoint = ConfIotaXYZ.getString("iota.xyz.infopoint.endpoint") +
							ConfIdas.getString("iota.resource") + 
							"?i=" + deviceId + "&k=" + apikey;
			LOGGER.log(Level.INFO,"IOT Agent Endpoint " + endpoint);

			devicePublishedConfirm.setEndpoint(endpoint);
			//The developer must distinguish in case the new agent is able to handle different dataformats
			devicePublishedConfirm.setDataformat(DataformatProtocol.JSON);
			devicePublishedConfirm.setName(screenId);
			devicePublishedConfirm.setDeviceId(deviceId);
			
			devicePublishedConfirm.setOrganization(orgs);
			devicePublishedConfirm.setServerEndpoint(device.getEndpoint());

			devicePublishedConfirmBean=devicePublishedConfirm;
			LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
			
	    }
```

7. Create the **IOTA_XYZ** class
   - open the package _it.eng.tools_
   - create a new file named **IOTA_XYZ** extending IOTA
     Here is a template for the class

```
public class IOTA_XYZ extends IOTA {

	public IOTA_XYZ() throws Exception {
		
		super(ConfIotaXYZ.getString("iota.services"), 
				ConfIotaXYZ.getString("iota.devices"), 
				ConfIdas.getString("iota.resource"),
				ConfIotaXYZ.getString("iota.xyz.host.protocol"), 
				ConfIotaXYZ.getString("iota.xyz.data.protocol"), 
				"XYZ",
				ConfIotaXYZ.getString("iota.xyz.host"),
				ConfIotaXYZ.getString("iota.xyz.northbound.port"),
				ConfIotaXYZ.getString("iota.xyz.southbound.port"));
	}
}
```

8. Update the **IOTAgentManager**
   - Open the file _it.eng.iot.utils.IOTAgentManager.java_ 
   - in the method **switchIOTA** add a new else if condition just after the other already existing ones

```
    else if (dataformatProtocol.equalsIgnoreCase("XYZ")) {
		LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
		IOTA iota_xyz = new IOTA_XYZ();
		idas = new Idas(iota_xyz);
	} 
```

##### FRONT-END

1. Handle the protocol selection event
   - Open the Javascript file _WebContent/js/newdevices.js_
   - go to the **changeTransportProtocol** function
   - add a new **else if** condition just after the already existing ones
     Here is a template:

```
    else if (transportProt=="XYZ"){
        //If your agent supports both push and pull mode
        $('#pushonlyalert').addClass('hide');
        $("#retrievedatamodeCheck").attr('disabled', false);
		
		//Otherwise disable the pull mode
		//setDevicePullPush(false);
		
		 var select = $("#dataformat_protocol");
		 
		 //Disable all the dataformats that your agent is not able to deal with
		 select.find('option#dataformatsigfox').prop('disabled', true);
		 select.find('option#dataformatlwm2m').prop('disabled', true);
		 
		 //Choose a default dataformat
		 select.val('JSON');
		 
		 //If your agent supports only one dataformat, disable the selection
	     select.attr("disabled", false);
	     
	     select.formSelect();
	     
	     //Put here any other GUI modifications that must be triggered by the selection of the XYZ protocol
	     
     }
```
2. add the XYZ option to the protocol select box
   - Open the file _WEB-INF/view/newdevices.jsp_
   - search for the <select id="transport_protocol">
   - add the following option
   
```
<option value="XYZ">XYZ</option>
```
   - search for <select id="dataformat_protocol">
   - add any dataformat your new IoT Agent is able to deal with


### Share with the community
Now that your new IoT Agent is connected to the IoT Device Manager you can share such an extension with the Digital Enabler community.
By publishing a new topic on the [IoT Device Manager forum](https://issues.digitalenabler.eng.it/projects/digital-enabler/boards/6) the merge process into the master branch will start.

>Please, be sure that the source code of your IoT Agent is publicly available on [github](https://github.com), [gitlab](https://gitlab.com/) or [bitbucket](https://bitbucket.org/).