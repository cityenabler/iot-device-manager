package it.eng.iot.hub;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.hub.model.Dive;
import it.eng.iot.hub.model.HubEntity;
import it.eng.iot.hub.model.kapua.Kapua;
import it.eng.iot.utils.RuntimeTypeAdapterFactory;

public class HubFactory {
	
	private static final Logger LOGGER = LogManager.getLogger(HubFactory.class);
	
	public static HubEntity getHub(String hubString) {
		
		try {
			Type type = new TypeToken<HubEntity>(){}.getType();
			RuntimeTypeAdapterFactory<HubEntity> adapter = RuntimeTypeAdapterFactory.of(HubEntity.class, "type")
				      .registerSubtype(Kapua.class);
			//TODO: add registerSubtype(Dive.class) whenever will be implemented

			Gson gson = new GsonBuilder().registerTypeAdapterFactory(adapter).create();
			HubEntity hub = gson.fromJson(hubString, type);
			return hub;			
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static List<HubEntity> getHubList(String hubListString) {	
		try {
			List<HubEntity> hubs = new ArrayList<HubEntity>();
			Type type = new TypeToken<HubEntity>(){}.getType();
			RuntimeTypeAdapterFactory<HubEntity> adapter = RuntimeTypeAdapterFactory.of(HubEntity.class, "type")
				      .registerSubtype(Kapua.class);
			//TODO: add registerSubtype(Dive.class) whenever will be implemented
			
			JsonArray jsonHubArray = new JsonParser().parse(hubListString).getAsJsonArray();
			Gson gson = new GsonBuilder().registerTypeAdapterFactory(adapter).create();
			for(JsonElement jsonHub : jsonHubArray) {
				HubEntity hub = gson.fromJson(jsonHub, type);
				if (hub instanceof Kapua) {
					hub.setType("kapua");
				}
				if (hub instanceof Dive) {
					hub.setType("dive");
				}
				hubs.add(hub);
			}			
			return hubs;
			
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}

}
