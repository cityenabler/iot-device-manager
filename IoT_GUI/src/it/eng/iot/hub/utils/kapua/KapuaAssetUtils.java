package it.eng.iot.hub.utils.kapua;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.eng.iot.exceptions.AssetConfigurationException;
import it.eng.iot.hub.model.kapua.Component;
import it.eng.iot.hub.model.kapua.Configuration;
import it.eng.iot.hub.model.kapua.InputPortNames;
import it.eng.iot.hub.model.kapua.KuraAssetConfiguration;
import it.eng.iot.hub.model.kapua.KuraAssetProperty;
import it.eng.iot.hub.model.kapua.Keyword;
import it.eng.iot.hub.model.kapua.KuraDeviceAsset;
import it.eng.iot.hub.model.kapua.OutputPortNames;
import it.eng.iot.hub.model.kapua.Position;
import it.eng.iot.hub.model.kapua.RenderingProperties;
import it.eng.iot.hub.model.kapua.ValueWireGraphService;
import it.eng.iot.hub.model.kapua.Wire;

public class KapuaAssetUtils {
	
	private static final Logger LOGGER = LogManager.getLogger(KapuaAssetUtils.class);
	
	private static final int X_POSITION_START = -1740;
    private static final int Y_POSITION_START = -600;
    private static final int Y_POSITION_STEP= 80;
	
	//Search and return a property in the asset configuration by name
	/**
	 * @param config: the Kura configuration
	 * @param name: the name of the property to search
	 * @return the property object inside the configuration
	 */
	public static KuraAssetProperty getPropertyByName(KuraAssetConfiguration config, String name) {
		if (config != null && config.getProperties() != null) {
			List<KuraAssetProperty> property = config.getProperties().getProperty();
			
			if (property != null) {
				for (KuraAssetProperty prop : property) {
					if (name.equalsIgnoreCase(prop.getName())) {
						return prop;
					}		
				}
			}
			LOGGER.log(Level.INFO, "prop \"" + name + "\" not found");
		}
		LOGGER.log(Level.INFO, "Kapua Asset Configuration or its properties field is null");
		return null;
	}
	
	//Set the value of a property in the asset configuration searched by name and return the asset configuration changed
	/**
	 * @param config: the Kura configuration
	 * @param propName: property name to search
	 * @param newPropVal: the value to set the property to
	 * @return the Kura configuration changed
	 */
	public static KuraAssetConfiguration setPropertyValByName(KuraAssetConfiguration config, String propName, String newPropVal) {
		if (config != null && config.getProperties() != null) {
			List<KuraAssetProperty> property = config.getProperties().getProperty();
			
			if (property != null) {
				for (KuraAssetProperty prop : property) {
					if (propName != null && propName.equalsIgnoreCase(prop.getName())) {
						prop.setValue(new ArrayList<String>(Arrays.asList(newPropVal)));	/*The value is always stored as an array*/
						return config;
					}		
				}
			}
		}
		
		// No changes, return original object
		LOGGER.log(Level.INFO, "No changes made, returning the original Kapua Asset Configuration");
		return config;
	}
	
	//Sets the value of a property in the asset configuration searched by name and returns the asset configuration changed. With no changes, returns original configuration.
		/**
		 * @param config: the Kura configuration
		 * @param propNameEnd: suffix to search in property name
		 * @param newPropVal: the value to set the property to
		 * @return the Kura configuration changed
		 */
		public static KuraAssetConfiguration setPropertyValByNameEnd(KuraAssetConfiguration config, String propNameEnd, String newPropVal) {
			if (config != null && config.getProperties() != null) {
				List<KuraAssetProperty> property = config.getProperties().getProperty();
				
				if (property != null) {
					for (KuraAssetProperty prop : property) {
						if (propNameEnd != null && prop.getName().endsWith(propNameEnd)) {
							prop.setValue(new ArrayList<String>(Arrays.asList(newPropVal)));	/*A property's value is always stored as an array*/
							LOGGER.log(Level.INFO, "Property " + prop.getName() + " set with value " + newPropVal);
						}		
					}
				}
			}
			return config;
		}

	//Create the string for the graphic representation of the three components Asset, Timer, Publisher and their interconnections
    /**
     * @param tripletList: a list of triplet with the names of Asset, Timer, Publisher components
     * @return the string to put in the field "value" of property "WireGraph" for the template of Kura component "WireGraphService"
     * @throws JsonProcessingException
     */
    public static String createWiregraphValue(List<Triplet<String, String, String>> tripletList) throws JsonProcessingException {
        List<Component> components = new ArrayList<>();
        List<Wire> wires = new ArrayList<>();
	    ValueWireGraphService value = new ValueWireGraphService();
	    for (Triplet<String, String, String> t : tripletList) {
	    	int idx = 0;
	    	
	        Component cTimer = new Component();
	        cTimer.setPid(t.getValue1());
	        cTimer.setInputPortCount(0);
	        cTimer.setOutputPortCount(1);
	        cTimer.setRenderingProperties(renderingPropertiesBuilder(X_POSITION_START,Y_POSITION_START + Y_POSITION_STEP * idx));
	        components.add(cTimer);
	        
	        Component cAsset = new Component();
	        cAsset.setPid(t.getValue0());
	        cAsset.setInputPortCount(1);
	        cAsset.setOutputPortCount(1);
	        cAsset.setRenderingProperties(renderingPropertiesBuilder(X_POSITION_START +200,Y_POSITION_START + Y_POSITION_STEP * idx));
	        components.add(cAsset);
	        
	        Component cPublisher = new Component();
	        cPublisher.setPid(t.getValue2());
	        cPublisher.setInputPortCount(1);
	        cPublisher.setOutputPortCount(0);
	        cPublisher.setRenderingProperties(renderingPropertiesBuilder(X_POSITION_START +400,Y_POSITION_START + Y_POSITION_STEP * idx));
	        components.add(cPublisher);
	        
	        Wire w2 = new Wire(t.getValue1(), 0, t.getValue0(), 0);
	        wires.add(w2);
	        Wire w1 = new Wire(t.getValue0(), 0, t.getValue2(), 0);
	        wires.add(w1);
	        
	        idx++;
	    }
        value.setComponents(components);
        value.setWires(wires);
        return value.toJSON();
    }

    private static RenderingProperties renderingPropertiesBuilder(int x, int y) {
        RenderingProperties  res = new RenderingProperties();
        res.setPosition(new Position(x,y));
        res.setInputPortNames(new InputPortNames());
        res.setOutputPortNames(new OutputPortNames());
        return res;
    }
    
    //Build a list of triplet<assetName, timerName, publisherName> for every asset in the input list
    /**
     * @param assets: a list of names of Kura asset components
     * @return a list of triplet each one with name for the three components asset, timer, publisher
     */
    public static List<Triplet<String, String, String>> buildTriplet(List<String> assets) {
    	List<Triplet<String, String, String>> assetTripletList = new ArrayList<Triplet<String, String, String>>();
    	for (String asset : assets) {
    		Triplet<String, String, String> t = 
    				new Triplet<String, String, String>(asset, asset.concat("-Timer"), asset.concat("-Publisher"));
    		assetTripletList.add(t);
    	}
    	return assetTripletList;
    }
    
    /**
     * @param input: the string within to replace some placeholders
     * @param keywords: a list of placeholders to replace
     * @param configs: a list of configurations from which to get the new values to replace the placeholders
     * @return the input string with placeholders replaced by values got from configurations
     * @throws AssetConfigurationException
     */
    public static String replaceKeywords(String input, List<Keyword> keywords, List<Configuration> configs) throws AssetConfigurationException {
    	for (Keyword keyword : keywords) {
    		boolean isConfigPresent = false;
			for (Configuration config : configs) {
				String newVal = checkToEncodePassword(config.getName(), config.getValue());
				if (keyword.getName().compareTo(config.getName()) == 0) {
					input = input.replace(keyword.getName(), newVal);
					isConfigPresent = true;
				}
			}
			
			//Configuration not found in device collection, try to use default value
			if (!isConfigPresent) {
				LOGGER.log(Level.INFO, "Device configuration value for \"" + keyword.getName() + "\" not found on DB");
				boolean isDefaultPresent = false;
				if (keyword.getDefaultValue() != null && !keyword.getDefaultValue().isEmpty() && keyword.getDefaultValue().compareToIgnoreCase("CALCULATED") != 0) {
					String newVal = checkToEncodePassword(keyword.getName(), keyword.getDefaultValue());
					LOGGER.log(Level.INFO, "Replacing value \"" + keyword.getName() + "\" with value \"" + keyword.getDefaultValue() + "\"");
	    			input = input.replace(keyword.getName(), newVal);
	    			isDefaultPresent = true;
	    		}
				if (!isDefaultPresent && keyword.getDefaultValue().compareToIgnoreCase("CALCULATED") != 0) {
					LOGGER.log(Level.ERROR, "Device configuration default value for field \"" + keyword.getName() + "\" not found on DB");
					throw new AssetConfigurationException("Device configuration default value for field \"" + keyword.getName() + "\" not found on DB");
				}
    		}
		}
    	return input;
    }
    
    // Returns the encoded string value for the password or the original string value for the others
    public static String checkToEncodePassword(String name, String value) {
    	String ret = "";
    	
    	if (name.compareTo("PASSWORD_VALUE") == 0) {
    		ret = Base64.encodeBase64String(value.getBytes());
		} else ret = value;
    	return ret;
    }
}
