package it.eng.iot.hub.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import it.eng.iot.configuration.ConfServiceHubKapua;
import it.eng.iot.exceptions.AssetConfigurationException;
import it.eng.iot.exceptions.GatewaysException;
import it.eng.iot.exceptions.GetAssetException;
import it.eng.iot.exceptions.TokenException;
import it.eng.iot.hub.model.kapua.Asset;
import it.eng.iot.hub.model.kapua.Device;
import it.eng.iot.hub.model.kapua.Kapua;
import it.eng.iot.hub.model.kapua.KuraAssetConfiguration;
import it.eng.iot.hub.model.kapua.KuraAssetProperty;
import it.eng.iot.hub.model.kapua.KapuaAuthentication;
import it.eng.iot.hub.model.kapua.KapuaGetAssetsListResp;
import it.eng.iot.hub.model.kapua.KapuaGetDevicesListResp;
import it.eng.iot.hub.model.kapua.KuraTemplate;
import it.eng.iot.hub.model.kapua.Keyword;
import it.eng.iot.hub.model.kapua.KuraDevice;
import it.eng.iot.hub.model.kapua.KuraDeviceAsset;
import it.eng.iot.hub.model.kapua.KuraGatewayMap;
import it.eng.iot.hub.utils.kapua.KapuaAssetUtils;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.utils.AssetConfigManager;
import it.eng.iot.utils.AssetTemplateManager;
import it.eng.iot.utils.DeviceUtils;
import it.eng.iot.utils.HubManager;
import it.eng.iot.utils.RestUtils;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceStaticAttribute;

public class KapuaController {
	
	private static final Logger LOGGER = LogManager.getLogger(KapuaController.class);
	
	private static final String deviceStatus = "connected";
	private static final String dataService = "org.eclipse.kura.data.DataService-X";
	private static final String cloudService = "org.eclipse.kura.cloud.CloudService-X";
	private static final String mqttDataTransport = "org.eclipse.kura.core.data.transport.mqtt.MqttDataTransport-X";
	private static final String modbus = "modbus";
	
	
	static String urlPrefix = ConfServiceHubKapua.getString("service.kapua.UrlPrefix");
	static String urlAuthSuffix = ConfServiceHubKapua.getString("service.kapua.UrlAuthSuffix");
	static String urlDevicesListSuffix = ConfServiceHubKapua.getString("service.kapua.UrlDevicesListSuffix");
	static String urlStatusDevicesListSuffix = ConfServiceHubKapua.getString("service.kapua.UrlStatusDevicesListSuffix");
	static String urlTimeoutSuffix = ConfServiceHubKapua.getString("service.kapua.UrlTimeoutSuffix");
	static String urlComponentIdSuffix = ConfServiceHubKapua.getString("service.kapua.UrlComponentIdSuffix");
	static String urlAssetsListSuffix = ConfServiceHubKapua.getString("service.kapua.UrlAssetsListSuffix");
	static String urlDeviceConfigurationsSuffix = ConfServiceHubKapua.getString("service.kapua.UrlDeviceConfigurationsSuffix");
	
	
	private static HashMap<String, KapuaAuthentication> tokenMap = new HashMap<String, KapuaAuthentication>();
	
	//Procedure to get Kura device list for a Kapua hub getting first the token
	public static List<JsonObject> getGatewaysProcedure(String hubName, String status) {
		List<JsonObject> output = new ArrayList<JsonObject>();
		try {
			List<KuraDevice> gateways = getGatewaysAPIRest(hubName, status, getTokenId(hubName));
			
			//Map KuraDevice to a simpler object
			gateways.forEach((g) -> output.add(g.toJsonObj()));
			return output;
		} catch(TokenException ex) {
			LOGGER.log(Level.ERROR, "Error retrieving token for hub \"" + hubName + "\" - " + ex.toString());
			return new ArrayList<JsonObject>();	//No valid token to call Kapua APIs then return empty list
		} catch(GatewaysException ex) {
			LOGGER.log(Level.ERROR, "Error retrieving list of Kura gateways for hub \"" + hubName + "\" - " + ex.toString());
			return output;	//Return empty list
		} catch(Exception ex) {
			LOGGER.log(Level.ERROR, "Generic error for hub \"" + hubName + "\" - " + ex.toString());
			return output;	//Return empty list
		}
	}
	
	//Procedure to call Kapua API for device registration
	/**
	 * @param idasDevice: the device to register also in Kapua-Kura
	 * @param topic: the DeMa address to send to Kura where to publish the data from real device connected to Kura
	 * @return a triplet with:
	 * result ok -> "ok", 200, id of the device created/configurated in Kura,
	 * result no ok -> "error", error code, error message
	 */
	public static Triplet<String, Integer, String> provisioningDeviceProcedure(IdasDevice idasDevice, String fiwareservice, String topic) {
		String hubName = "";
		String gatewayDisplayName = "";
		String gatewayClientId = "";
		String assetName = "";
		String modbusAddr = "";
		String idRet = null;
		
		try {
			assetName = idasDevice.getDevice_id();
			Set<IdasDeviceStaticAttribute> staticAttributes = idasDevice.getStatic_attributes();
			String assetType = DeviceUtils.getAsset(staticAttributes);
			hubName = DeviceUtils.getHubName(staticAttributes);
			gatewayDisplayName = DeviceUtils.getGateway(staticAttributes);
			gatewayClientId = KuraGatewayMap.getInstance().get(DeviceUtils.getGateway(staticAttributes));
			modbusAddr = DeviceUtils.getModbusAddress(staticAttributes);
			
			String deviceEndpoint = DeviceUtils.parseEndpoint(idasDevice.getEndpoint());
			if (deviceEndpoint == null) {
				LOGGER.log(Level.ERROR, "The device endpoint \"" + idasDevice.getEndpoint() + "\" has invalid format");
				return new Triplet<>("error", 510, "invalid device endpoint");
			}
			
			//TODO future implementation:
			//- to update "MODBUS_TCP_UDP_IP_VALUE" value in "devices" collection with deviceEndpoint.split(":")[1] value
			//- to add to "devices" collection the properties related to "access.type" and "modbus.tcp-udp.port" fields of modbus_driver configuration for Kapua, like "MODBUS_TCP_UDP_IP_VALUE"
			//- to update "access.type" value in "devices" collection with deviceEndpoint.split(":")[0] value
			//- to update "modbus.tcp-udp.port" in "devices" collection with deviceEndpoint.split(":")[2] value
			//- to get previous 3 values from "devices" collection and replace them into modbus_driver configuration for Kapua
			
			String driverEndpoint = deviceEndpoint.split(":")[0].concat(deviceEndpoint.split(":")[1].substring(2));
			
			if (hubName == null || hubName.isEmpty() || gatewayClientId == null || gatewayClientId.isEmpty()) {
				LOGGER.log(Level.ERROR, "Hubname or gateway null or empty");
				return new Triplet<>("error", 501, "hubname or gateway null or empty");
			}
			
			String tokenId = getTokenId(hubName);
			LOGGER.log(Level.INFO, "Kapua Token id: " + tokenId);
			String timeout = "10000";
			
			LOGGER.log(Level.INFO, "Creating asset \"" + assetName + "\" on hub \"" + hubName + "\" for gateway \"" + gatewayDisplayName + "\"");
			Kapua hub = (Kapua) HubManager.findHubByName(hubName);
			
			if (null != tokenId) {
				Map<String, String> getHeaders = buildHeaders(tokenId, "*/*");
				
				String createAssetEndpoint = hub.getUrl() + ":" + hub.getPort() 
				+ urlPrefix 
				+ urlDeviceConfigurationsSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", gatewayClientId)
				+ urlTimeoutSuffix.replace("$timeout", timeout);
				
				Asset asset = AssetTemplateManager.findAssetByName(assetType);
				KuraTemplate assetTemplate = asset.getTemplateAsset();
				KuraTemplate timerTemplate = asset.getTemplateTimer();
				KuraTemplate publisherTemplate = asset.getTemplatePublisher();
				String templateConfig = asset.getTemplateConfigurations();
				List<Keyword> templateConfigKeywords = asset.getTemplateConfigurationKeywords();
				Device device = AssetConfigManager.findDeviceById(assetName);
				
				if (assetTemplate != null && timerTemplate != null && publisherTemplate != null && templateConfig != null && !templateConfig.isEmpty()) {
				    
				    String subDataService = dataService.substring(0, dataService.length() - 1);
                    String subCloudService = cloudService.substring(0, cloudService.length() - 1);
                    String subMqttDataTransport = mqttDataTransport.substring(0, mqttDataTransport.length() - 1);
					
					//Modify template configuration for asset, timer, publisher components
					KuraAssetConfiguration assetConfig = assetTemplate.getConfiguration().get(0);	/*We assume a list of only one element*/
					assetConfig.setId(assetName);
					assetConfig = KapuaAssetUtils.setPropertyValByName(assetConfig, "componentId", assetName);
					assetConfig = KapuaAssetUtils.setPropertyValByName(assetConfig, "kura.service.pid", assetName);
					assetConfig = KapuaAssetUtils.setPropertyValByName(assetConfig, "service.pid", assetName);
					String driverPid = KapuaAssetUtils.getPropertyByName(assetConfig, "driver.pid").getValue().get(0);
					String newDriverPid = driverPid.concat("-").concat(driverEndpoint);
					assetConfig = KapuaAssetUtils.setPropertyValByName(assetConfig, "driver.pid", newDriverPid);	/*Diversify the id for devices with same driver, but different IP*/
					if(driverPid.equals(modbus)) {
						assetConfig = KapuaAssetUtils.setPropertyValByNameEnd(assetConfig, "#unit.id", modbusAddr);		/*unit.id = modbus driver address: it's the same for every asset channel*/
					}
					
					KuraAssetConfiguration timerConfig = timerTemplate.getConfiguration().get(0);
					timerConfig.setId(assetName.concat("-Timer"));
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "componentId", assetName.concat("-Timer"));
					
					/*In the future some of these values could be taken from DeMa GUI in "New device" page*/
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "simple.interval", "10");
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "simple.time.unit", "SECONDS");
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "type", "SIMPLE");
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "cron.interval", "0/10 * * * * ?");
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "kura.service.pid", assetName.concat("-Timer"));
					String now = Long.toString(GregorianCalendar.getInstance().getTimeInMillis());
					int random = (int)(Math.random() * (100) + 1);
					timerConfig = KapuaAssetUtils.setPropertyValByName(timerConfig, "service.pid", "org.eclipse.kura.wire.Timer-".concat(now).concat("-").concat(Integer.toString(random)));
					
					KuraAssetConfiguration publisherConfig = publisherTemplate.getConfiguration().get(0);
					publisherConfig.setId(assetName.concat("-Publisher"));
					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "componentId", assetName.concat("-Publisher"));
					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "kura.service.pid", assetName.concat("-Publisher"));
					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "service.pid", "org.eclipse.kura.example.mqttpublisher.DEPublisher-".concat(now).concat("-").concat(Integer.toString(random)));
					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "publish.topic", topic);
//					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "componentName", "DEPublisher");
					String dataServiceTarget = KapuaAssetUtils.getPropertyByName(publisherConfig, "DataService.target").getValue().get(0);
					dataServiceTarget = dataServiceTarget.replace(dataService, subDataService.concat(fiwareservice));
					publisherConfig = KapuaAssetUtils.setPropertyValByName(publisherConfig, "DataService.target", dataServiceTarget);
					
					//Create a list with the three configuration for asset, timer, publisher
					ArrayList<KuraAssetConfiguration> configurationList = new ArrayList<KuraAssetConfiguration>();
					configurationList.add(assetConfig);
					configurationList.add(timerConfig);
					configurationList.add(publisherConfig);
					KuraTemplate config = new KuraTemplate(configurationList);
					
					//Build request body from modified templates for asset, timer, publisher components
					String assetCreateBody = new Gson().toJson(config);
					
					//Build all configurations for all other components in order to have complete asset configuration in Kura
					//1. Retrieve the list of assets present in Kura-gateway
					LOGGER.log(Level.INFO, "Retrieving the list of assets present in gateway \"" + gatewayDisplayName + "\"");
					List<String> kuraAssetNames = getAssetsDevice(hubName, tokenId, gatewayClientId);
					if (kuraAssetNames.contains(assetName)) {
						LOGGER.log(Level.ERROR, "Cannot create asset \"" + assetName + "\": asset with same name already present in gateway \"" + gatewayDisplayName + "\"");
						return new Triplet<>("error", 502, "asset with same name already present in Kura gateway");
					}
					kuraAssetNames.add(assetName);	/*Add also name of new asset we're going to create*/
					
					//2. Build WireGraph value string passing all the assets in Kura, because the graph must be updated with new asset, keeping existing ones
					LOGGER.log(Level.INFO, "Building wiregraph component for configuration of asset \"" + assetName + "\" in gateway \"" + gatewayDisplayName + "\"");
					String wiregraphValue = KapuaAssetUtils.createWiregraphValue(KapuaAssetUtils.buildTriplet(kuraAssetNames));
					
					//3. Replace keywords with actual values
					LOGGER.log(Level.INFO, "Building components' configurations for asset \"" + assetName + "\" in gateway \"" + gatewayDisplayName + "\"");
					templateConfig = templateConfig.replace(dataService, subDataService.concat(fiwareservice)).replace(cloudService, subCloudService.concat(fiwareservice)).replace(mqttDataTransport, subMqttDataTransport.concat(fiwareservice));
					
					String newTemplate = KapuaAssetUtils.replaceKeywords(templateConfig, templateConfigKeywords, (null != device) ? device.getConfiguration() : new ArrayList<>());
					
					//Fields that can be replaced only at runtime (without a value from devices collection or a default in assetTemplates collection)
					newTemplate = newTemplate.replace("MODBUS_DRIVER_ID", newDriverPid).replace("DRIVER_KURA_SERVICE_PID", newDriverPid);
					
					newTemplate = newTemplate.replace("WIREGRAPH_VALUE", wiregraphValue);
					String newTemplateJsonString = new JsonParser().parse(newTemplate).getAsJsonObject().toString();
					
					//ACTUALLY CREATE THE NEW ASSET
					//Call Kapua API to send asset-timer-publisher configurations
					LOGGER.log(Level.INFO, "Sending device registration request to \"" + hubName + "\" hub");
					LOGGER.log(Level.INFO, "Calling endpoint " + createAssetEndpoint);
					String assetCreationResp = RestUtils.consumePut(createAssetEndpoint, assetCreateBody, MediaType.APPLICATION_JSON_TYPE, getHeaders);
					
					//Call Kapua API to send list of other components configurations
					LOGGER.log(Level.INFO, "Sending other components configuration request to \"" + hubName + "\" hub");
					LOGGER.log(Level.INFO, "Calling endpoint " + createAssetEndpoint);
					String assetConfigurationResp = RestUtils.consumePut(createAssetEndpoint, newTemplateJsonString, MediaType.APPLICATION_JSON_TYPE, getHeaders);
					
					//Correct response from Kapua for asset creation hasn't body
					if (assetCreationResp.isEmpty() && assetConfigurationResp.isEmpty()) {
						LOGGER.log(Level.INFO, "Asset \"" + assetName + "\" created on hub \"" + hubName + "\" for gateway \"" + gatewayDisplayName + "\"");
						
						//Get id of asset just created from configuration
						String getConfEndpoint = hub.getUrl() + ":" + hub.getPort() 
						+ urlPrefix 
						+ urlDeviceConfigurationsSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", gatewayClientId)
						+ urlComponentIdSuffix.replace("$componentId", assetName);
						LOGGER.log(Level.INFO, "Getting configuration of asset \"" + assetName + "\" on gateway \"" + gatewayDisplayName + "\" on hub " + hubName);
						LOGGER.log(Level.INFO, "Calling endpoint " + getConfEndpoint);
						
						int maxAttempts = 4;
						int attempt = 1;
						boolean configInResponse = false;
						KuraTemplate assetConfigResp = null;
						
						//Try some times to get configuration from Kapua response
						while(attempt <= maxAttempts && configInResponse == false) {
							Thread.sleep(500);	//Wait a while to make configuration in Kura/Kapua available for the GET response to DeMa
							String getConfigResp = RestUtils.consumeGet(getConfEndpoint, getHeaders);
							LOGGER.log(Level.INFO, "Getting configuration - Attempt # " + attempt);
							//From response we get only the list of configurations and not the root field "type" = "deviceConfiguration"
							assetConfigResp = new Gson().fromJson(getConfigResp, KuraTemplate.class);
							if (assetConfigResp != null && assetConfigResp.getConfiguration() != null && !assetConfigResp.getConfiguration().isEmpty()) {
								configInResponse = true;
								break;
							}
							attempt++;
						}
						if (configInResponse == false) {
							LOGGER.log(Level.ERROR, "Missing configuration in response from \"" + hubName + "\"");
							return new Triplet<>("error", 503, "missing configuration");
						}
						
						//Return id of configuration for component asset created.
						//Request is for single configuration thus response list will have only one element
						idRet = assetConfigResp.getConfiguration().get(0).getId();
						return new Triplet<>("ok", 200, idRet);
					}
				} else {
					LOGGER.log(Level.ERROR, "The configuration template to create the asset \"" + assetName + "\" is empty");
					return new Triplet<>("error", 504, "empty configuration template");
				}
			} else {
				LOGGER.log(Level.ERROR, "Token NULL for request of asset \"" + assetName + "\" creation on hub \"" + hubName + "\" for gateway \"" + gatewayDisplayName + "\"");
				return new Triplet<>("error", 505, "token NULL");
			}
		} catch(TokenException ex) {
			LOGGER.log(Level.ERROR, "Error retrieving token for hub \"" + hubName + "\" - " + ex.toString());
			return new Triplet<>("error", 506, "error retrieving token");
		} catch(AssetConfigurationException ex) {
			LOGGER.log(Level.ERROR, "Error building components' configurations for asset \"" + assetName + "\" in gateway \"" + gatewayDisplayName + "\" - " + ex.toString());
			//Cannot rollback for asset/timer/publisher configurations in Kura because configurations delete doesn't exist
			return new Triplet<>("error", 507, "error building configurations for asset");
		} catch(GetAssetException ex) {
			LOGGER.log(Level.ERROR, "Error getting the assets list of Kura: " + ex.getMessage());
			return new Triplet<>("error", 508, "error getting the assets list");
		} catch(Exception ex) {
			LOGGER.log(Level.ERROR, "Generic error for hub \"" + hubName + "\" - " + ex.toString());
			return new Triplet<>("error", 509, "generic error");
		}
		return new Triplet<>("ok", 200, idRet);
	}
	
	public static String deleteDeviceProcedure(String assetName, String hubName, String fiwareservice, String gatewayDisplayName) {
	    try {
    	    String publisherComponentId = assetName.concat("-Publisher");
    	    
    	    //Comment added after change about unique Dataservice and MqttDataTransport configurations for several device with one common endpoint
//    	    String mqttDataTransportComponentId = "org.eclipse.kura.core.data.transport.mqtt.MqttDataTransport-".concat(fiwareservice);

    	    Kapua hub = (Kapua) HubManager.findHubByName(hubName);
            List<JsonObject> gatewayDeviceList = getGatewaysProcedure(hubName, deviceStatus);   //Get list of Kura
            KuraGatewayMap.getInstance().populate(gatewayDeviceList);   //Build map gatewayClientId-gatewayDisplayName
            String gatewayClientId = KuraGatewayMap.getInstance().get(gatewayDisplayName);
            KuraAssetConfiguration conf;
            
            String ret = null;
            
	        String tokenId = getTokenId(hubName);
            HashMap<String, String> headers = buildHeaders(tokenId, "*/*");
            String endpoint = hub.getUrl() + ":" + hub.getPort() 
            + urlPrefix 
            + urlDeviceConfigurationsSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", gatewayClientId); //Send single configuration in a list without specifying componentId in URL
//            + urlComponentIdSuffix.replace("$componentId", componentId);
            
            ArrayList<KuraAssetConfiguration> configurationList = new ArrayList<KuraAssetConfiguration>();
            
            List<KuraAssetConfiguration> publisherConfList = getDeviceConfigurations(assetName, hubName, gatewayDisplayName, gatewayClientId, publisherComponentId);
            if (publisherConfList != null && !publisherConfList.isEmpty()) {
                conf = publisherConfList.get(0); //get of configurations by component returns a list of only one item
                
                //Empty the property value to "invalidate" publication on DeMa's topic 
                conf = KapuaAssetUtils.setPropertyValByName(conf, "publish.topic", "");
                configurationList.add(conf);
            } else {    //If publisher configuration doesn't exist we assume it's an error situation
                LOGGER.log(Level.ERROR, "Error retrieving list of configurations for componentId \"" + publisherComponentId + "\"");
                return null;
            }
            
          //Comment added after change about unique Dataservice and MqttDataTransport configurations for several device with one common endpoint
//            List<KuraAssetConfiguration> mqttDataTransportConfList = getDeviceConfigurations(assetName, hubName, gatewayDisplayName, gatewayClientId, mqttDataTransportComponentId);
//            if (mqttDataTransportConfList != null && !mqttDataTransportConfList.isEmpty()) {
//                conf = mqttDataTransportConfList.get(0); //get of configurations by component returns a list of only one item
//                
//                //Empty the property "broker-url" value and reset to default property "password" value to "invalidate" publication to DeMa's broker url 
//                conf = KapuaAssetUtils.setPropertyValByName(conf, "broker-url", "");
//                conf = KapuaAssetUtils.setPropertyValByName(conf, "password", "password");
//                configurationList.add(conf);
//                
//            } else {    //If mqttDataTransport configuration doesn't exist we assume it's an error situation
//                LOGGER.log(Level.ERROR, "Error retrieving list of configurations for componentId \"" + mqttDataTransportComponentId + "\"");
//                return null;
//            }
            
            KuraTemplate config = new KuraTemplate(configurationList);
            String body = new Gson().toJson(config);
            
            //Send new configurations for Publisher and MqttDataTransport components to the Kura device
            //LOGGER.log(Level.INFO, "Sending configurations to \"" + hubName + "\" hub for component \"" + publisherComponentId + "\" and \"" + mqttDataTransportComponentId + "\"");
            LOGGER.log(Level.INFO, "Sending configurations to \"" + hubName + "\" hub for component \"" + publisherComponentId);
            LOGGER.log(Level.INFO, "Calling endpoint " + endpoint);
            ret = RestUtils.consumePut(endpoint, body, MediaType.APPLICATION_JSON_TYPE, headers);

            if (ret != null) return "ok";
                
        } catch (TokenException e) {
            LOGGER.log(Level.ERROR, "Error retrieving token for Kapua");
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            LOGGER.log(Level.ERROR, "Error sending configuration of asset \"" + assetName + "\" on gateway \"" + gatewayDisplayName + "\" on hub \"" + hubName);
            e.printStackTrace();
            return null;
        }
	    
	    return null;
	}
	
	public static List<KuraAssetConfiguration> getDeviceConfigurations(String assetName, String hubName, String gatewayDisplayName, String gatewayClientId, String confId) throws TokenException {        
        Kapua hub = (Kapua) HubManager.findHubByName(hubName);
        String tokenId;
        
        try {
            tokenId = getTokenId(hubName);
        } catch (Exception ex) {
            LOGGER.log(Level.ERROR, "Error getting the token for Kapua: " + ex.getMessage());
            throw new TokenException("Error getting the token for Kapua");
        }
        Map<String, String> getHeaders = buildHeaders(tokenId, "application/json");
        
        String endpoint = hub.getUrl() + ":" + hub.getPort() 
        + urlPrefix 
        + urlDeviceConfigurationsSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", gatewayClientId);
        
        //To get single configuration by component: return will be a list of only one item
        if (confId != null && !confId.isEmpty()) {
            endpoint = endpoint.concat(urlComponentIdSuffix.replace("$componentId", confId));
        }
        
        String getConfigResp;
        try {
            getConfigResp = RestUtils.consumeGet(endpoint, getHeaders);
        } catch (Exception ex) {
            LOGGER.log(Level.ERROR, "Error getting the configurations of component \"" + confId + "\" for asset \"" + assetName + "\" on hub \"" + hubName + "\" for gateway \"" + gatewayDisplayName + "\" - " + ex.getMessage());
            return null;
        }
        if (getConfigResp != null && !getConfigResp.isEmpty()) {
            KuraTemplate configResp = new Gson().fromJson(getConfigResp, KuraTemplate.class);
            List<KuraAssetConfiguration> configurations = configResp.getConfiguration();
            return configurations;
        } else return null;
    }
	
	//Authentication to Kapua: get the token for successive requests
	/**
	 * @param hubName: Kapua name
	 * @return the token object obtained by authentication request with username and password
	 * @throws Exception
	 */
	public static KapuaAuthentication getTokenAPIRest(String hubName) throws Exception {
		
		try {
			LOGGER.log(Level.INFO, "Retrieving hub \"" + hubName + "\" by name");
			Kapua hub = (Kapua) HubManager.findHubByName(hubName);
			
			// Login (get token from the response)
			Map<String, String> postHeaders = new HashMap<String, String>();
			postHeaders.put("accept", "application/json");
			postHeaders.put("Context-Type", "application/json");
			
			String username = hub.getParameter().getUsername();
			String password = hub.getParameter().getPassword();
			if(username == null || username.isEmpty() || password == null || password.isEmpty()) {
				LOGGER.log(Level.ERROR, "Empty username or password for hub \"" + hubName);
				return null;
			}
			JsonObject body = new JsonObject();
			body.addProperty("username", username);
			body.addProperty("password", password);
			String authEndpoint = hub.getUrl() + ":" + hub.getPort() + urlPrefix + urlAuthSuffix;
			LOGGER.log(Level.INFO, "Retrieve token for hub \"" + hubName + "\"");
			LOGGER.log(Level.INFO, "Calling endpoint " + authEndpoint);
			String authResp = RestUtils.consumePost(authEndpoint, body.toString(), postHeaders);
			KapuaAuthentication token = new Gson().fromJson(authResp, KapuaAuthentication.class);
			return token;
		} catch(Exception ex) {
			throw new TokenException(ex.getMessage());
		}
	}
	
	//Get Kura-devices list: each "Kura device" is a gateway managed by "Kapua" hub.
	//Each "asset" corresponds to a physical sensor/actuator connected to a Kura.
	//HubDevice is every "asset" in each Kura-device
	//TERMINOLOGY MAPPING between DeMa and Kura-Kapua world
	//Hub -> Kapua
	//Gateway -> Kura-device
	//HubDevice -> (Kura) asset
	/**
	 * @param hubName: name of Kapua
	 * @param status: of the Kura devices to retrieve (for example "connected")
	 * @param tokenId: the token identifier for the Kapua authentication
	 * @return the list of Kura devices registered to Kapua
	 * @throws Exception
	 */
	public static List<KuraDevice> getGatewaysAPIRest(String hubName, String status, String tokenId) throws Exception {
		
		try {
			LOGGER.log(Level.INFO, "Retrieving hub \"" + hubName + "\" by name");
			Kapua hub = (Kapua) HubManager.findHubByName(hubName);
			
			if (null != tokenId) {
			
			//Get list of Kura gateways
			Map<String, String> getHeaders = buildHeaders(tokenId, "application/json");
			String devEndpoint = hub.getUrl() + ":" + hub.getPort() + urlPrefix + urlDevicesListSuffix.replace("$scopeId", hub.getParameter().getScopeId());
			if (status != null && !status.isEmpty()) {
				devEndpoint.concat(urlStatusDevicesListSuffix).replace("$status", status);
			}			
			LOGGER.log(Level.INFO, "Getting list of Kura gateways for hub \"" + hubName + "\"");
			LOGGER.log(Level.INFO, "Calling endpoint " + devEndpoint);
			String getDevicesResp = RestUtils.consumeGet(devEndpoint, getHeaders);
			KapuaGetDevicesListResp kapuaGetDevicesListResp = new Gson().fromJson(getDevicesResp, KapuaGetDevicesListResp.class);
			return kapuaGetDevicesListResp.getItems();
			} else {
				return new ArrayList<KuraDevice>();
			}
		} catch(Exception ex) {
			throw new GatewaysException(ex.getMessage());
		}
	}
	
	//TODO: change logic or move it to elsewhere
	//Get the assets list from each Kura registered for a Kapua
//	public static List<FEDevice> getDevices(String hubName, String tokenId, List<KuraDevice> kuraDevices) throws Exception {
//		
//		LOGGER.log(Level.INFO, "Retrieving hub \"" + hubName + "\" by name");
//		Kapua hub = (Kapua) HubManager.findHubByName(hubName);
//		
//		Map<String, String> getHeaders = buildHeaders(tokenId, "application/json");
//		
//		HashMap<String, List<KuraDeviceAsset>> assetsMap = new HashMap<String, List<KuraDeviceAsset>>();
//		LOGGER.log(Level.INFO, "Getting list of assets for every Kura device of hub \"" + hubName + "\"");
//		for(KuraDevice kuraDevice : kuraDevices) {
//			LOGGER.log(Level.INFO, "Kura device name: " + kuraDevice.getDisplayName() + " - id: " + kuraDevice.getId());
//			String assEndpoint = hub.getUrl() + ":" + hub.getPort() + urlPrefix + urlAssetsListSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", kuraDevice.getId());
//			LOGGER.log(Level.INFO, "Calling endpoint " + assEndpoint);
//			String getAssetsResp = RestUtils.consumeGet(assEndpoint, getHeaders);
//			KapuaGetAssetsListResp kapuaGetAssetsListResp = new Gson().fromJson(getAssetsResp, KapuaGetAssetsListResp.class);
//			List<KuraDeviceAsset> assets = kapuaGetAssetsListResp.getDeviceAsset();
//			
//			// Build a map with all the assets for all Kura devices
//			assetsMap.put(kuraDevice.getId(), assets);
//		}
//		
//		// Map Kura assets to attributes in every HubDevice to return
//		List<FEDevice> hubDeviceList = new ArrayList<FEDevice>();
//		
//		// Loop for each Kura
//		for(String kuraId : assetsMap.keySet()) {
//			
//			// Loop for every asset
//			for(KuraDeviceAsset asset : assetsMap.get(kuraId)) {
//				FEDevice hubDevice = asset2HubDevice(hub, kuraId, asset);
//				hubDeviceList.add(hubDevice);
//			}
//		}
//		
//		return hubDeviceList;
//	}
	
	//Get the assets list from each Kura registered for a Kapua
	/**
	 * @param hubName: the name of Kapua
	 * @param tokenId: the token identifier for the Kapua authentication
	 * @param kuraDevices: a list of Kura registered to Kapua
	 * @return a map with <Kura's name, list of assets registered to Kura> 
	 * @throws Exception
	 */
	public static HashMap<String, List<KuraDeviceAsset>> getAssetsAllDevices(String hubName, String tokenId, List<String> kuraDevices) throws Exception {
		
		LOGGER.log(Level.INFO, "Retrieving hub \"" + hubName + "\" by name");
		Kapua hub = (Kapua) HubManager.findHubByName(hubName);
		
		Map<String, String> getHeaders = buildHeaders(tokenId, "application/json");
		
		HashMap<String, List<KuraDeviceAsset>> assetsMap = new HashMap<String, List<KuraDeviceAsset>>();
		LOGGER.log(Level.INFO, "Getting list of assets for every Kura device of hub \"" + hubName + "\"");
		for(String kuraDevice : kuraDevices) {
			LOGGER.log(Level.INFO, "Kura device id: " + kuraDevice);
			String assEndpoint = hub.getUrl() + ":" + hub.getPort() + urlPrefix + urlAssetsListSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", kuraDevice);
			LOGGER.log(Level.INFO, "Calling endpoint " + assEndpoint);
			String getAssetsResp = RestUtils.consumeGet(assEndpoint, getHeaders);
			KapuaGetAssetsListResp kapuaGetAssetsListResp = new Gson().fromJson(getAssetsResp, KapuaGetAssetsListResp.class);
			List<KuraDeviceAsset> assets = kapuaGetAssetsListResp.getDeviceAsset();
			
			// Build a map with all the assets for all Kura devices
			assetsMap.put(kuraDevice, assets);
		}			
		return assetsMap;
	}
	
	//Get the assets list from a specific Kura registered for a Kapua
		/**
		 * @param hubName: the name of Kapua
		 * @param tokenId: the token identifier for the Kapua authentication
		 * @param kuraDevice: the specific Kura registered to Kapua
		 * @return the list of assets registered to Kura
		 * @throws Exception
		 */
		public static List<String> getAssetsDevice(String hubName, String tokenId, String kuraDevice) throws Exception {
			List<String> assetNameList = new ArrayList<String>();
				
			try {
				LOGGER.log(Level.INFO, "Retrieving hub \"" + hubName + "\" by name");
				Kapua hub = (Kapua) HubManager.findHubByName(hubName);
				
				Map<String, String> getHeaders = buildHeaders(tokenId, "application/json");
				
				LOGGER.log(Level.INFO, "Getting list of assets for every Kura device of hub \"" + hubName + "\"");
				LOGGER.log(Level.INFO, "Kura device id: " + kuraDevice);
				String assEndpoint = hub.getUrl() + ":" + hub.getPort() + urlPrefix + urlAssetsListSuffix.replace("$scopeId", hub.getParameter().getScopeId()).replace("$deviceId", kuraDevice);
				LOGGER.log(Level.INFO, "Calling endpoint " + assEndpoint);
				String getAssetsResp = RestUtils.consumeGet(assEndpoint, getHeaders);
				KapuaGetAssetsListResp kapuaGetAssetsListResp = new Gson().fromJson(getAssetsResp, KapuaGetAssetsListResp.class);
				List<KuraDeviceAsset> assets = kapuaGetAssetsListResp.getDeviceAsset();
				for (KuraDeviceAsset asset : assets) {
					assetNameList.add(asset.getName());
				}
			} catch(Exception ex) {
				throw (new GetAssetException("Error getting the assets list of Kura " + kuraDevice + ": " + ex.getMessage()));
			}
			return assetNameList;
		}

	//Map a Kura asset to a FEDevice
	//TODO: choose if to fix it or to delete it
	private static FEDevice asset2HubDevice(Kapua hub, String kuraId, KuraDeviceAsset asset) {
		FEDevice device = new FEDevice(asset.getName());
		
		device.setName(asset.getName());
		device.setProtocol(hub.getProt());
		device.setHubName(hub.getName());
		device.setHubType(hub.getType());
		device.setGateway(kuraId);
		device.setTransport("MQTT");
		device.setDataformat_protocol("JSON");
		
		return device;
	}

	//Get the Kapua hub token from the tokens map of all Kapua hubs or a new one if the current token has expired
	/**
	 * @param hubName: the name of Kapua
	 * @return the token identifier for the Kapua authentication
	 * @throws Exception
	 */
	private static String getTokenId(String hubName) throws Exception {
		if (tokenMap != null && tokenMap.containsKey(hubName) && tokenMap.get(hubName).getExpiresOn().after(new Date(System.currentTimeMillis()))) {
			return tokenMap.get(hubName).getTokenId();
		} else {
			
			//Get a new token directly from Kapua
			KapuaAuthentication token = getTokenAPIRest(hubName);
			tokenMap.put(hubName, token);
			return token.getTokenId();
		}
	}
	
	private static HashMap<String, String> buildHeaders(String tokenId, String acceptType) {
	    HashMap<String, String> getHeaders = new HashMap<String, String>();
        getHeaders.put("accept", acceptType);
        getHeaders.put("Authorization", "Bearer " + tokenId);
        return getHeaders;
	}
	
}
