package it.eng.iot.hub.model.kapua;

import java.util.Date;

public class KapuaAuthentication {

	private String userId;
	private Date expiresOn;
	private String refreshToken;
	private String tokenId;
	private Date refreshExpiresOn;
	private Date invalidatedOn;
	private String type;
	private Date modifiedOn;
	private String modifiedBy;
	private int optlock;
	private String[] entityAttributes;
	private String[] entityProperties;
	private String scopeId;
	private String id;
	private String createdBy;
	private Date createdOn;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getExpiresOn() {
		return expiresOn;
	}
	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public Date getRefreshExpiresOn() {
		return refreshExpiresOn;
	}
	public void setRefreshExpiresOn(Date refreshExpiresOn) {
		this.refreshExpiresOn = refreshExpiresOn;
	}
	public Date getInvalidatedOn() {
		return invalidatedOn;
	}
	public void setInvalidatedOn(Date invalidatedOn) {
		this.invalidatedOn = invalidatedOn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getOptlock() {
		return optlock;
	}
	public void setOptlock(int optlock) {
		this.optlock = optlock;
	}
	public String[] getEntityAttributes() {
		return entityAttributes;
	}
	public void setEntityAttributes(String[] entityAttributes) {
		this.entityAttributes = entityAttributes;
	}
	public String[] getEntityProperties() {
		return entityProperties;
	}
	public void setEntityProperties(String[] entityProperties) {
		this.entityProperties = entityProperties;
	}
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
