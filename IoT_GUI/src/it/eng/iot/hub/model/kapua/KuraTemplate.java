package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KuraTemplate {
	private List<KuraAssetConfiguration> configuration;

	public KuraTemplate() {
	}
	
	public KuraTemplate(List<KuraAssetConfiguration> configuration) {
		super();
		this.configuration = configuration;
	}

	public List<KuraAssetConfiguration> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(List<KuraAssetConfiguration> configuration) {
		this.configuration = configuration;
	}

	@Override
	public String toString() {
		return "KapuaAssetTemplate [configuration=" + configuration + "]";
	}
	
	
}
