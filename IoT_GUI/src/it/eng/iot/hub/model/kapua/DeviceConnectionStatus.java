package it.eng.iot.hub.model.kapua;

public enum DeviceConnectionStatus {
	CONNECTED("CONNECTED"),
	DISCONNECTED("DISCONNECTED"),
	MISSING("MISSING"),
	NULL("NULL");
	
	private String value;
	
	DeviceConnectionStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
        return value;
    }
}
