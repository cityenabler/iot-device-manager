package it.eng.iot.hub.model.kapua;

public class RenderingProperties {
    Position position;
    InputPortNames inputPortNames;
    OutputPortNames outputPortNames;

    public RenderingProperties() {
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public InputPortNames getInputPortNames() {
        return inputPortNames;
    }

    public void setInputPortNames(InputPortNames inputPortNames) {
        this.inputPortNames = inputPortNames;
    }

    public OutputPortNames getOutputPortNames() {
        return outputPortNames;
    }

    public void setOutputPortNames(OutputPortNames outputPortNames) {
        this.outputPortNames = outputPortNames;
    }
}
