package it.eng.iot.hub.model.kapua;

import java.util.Date;

public class DeviceEvent {

	private String responseCode;
	private KapuaPosition position;
	private String action;
	String resource;
	String deviceId;
	Date receivedOn;
	Date sentOn;
	String eventMessage;
	String type;
	String scopeId;
	String id;
	String createdBy;
	Date createdOn;

}
