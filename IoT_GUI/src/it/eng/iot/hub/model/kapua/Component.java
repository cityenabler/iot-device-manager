package it.eng.iot.hub.model.kapua;

/*
 Example

components":[
      {
         "pid":"K3test-A",
         "inputPortCount":1,
         "outputPortCount":1,
         "renderingProperties":{
            "position":{
               "x":-1540,
               "y":-600
            },
            "inputPortNames":{

            },
            "outputPortNames":{

            }
         }
      },

 */


public class Component {

    private String pid;
    private int inputPortCount;
    private int outputPortCount;
    private RenderingProperties renderingProperties;

    public Component() {
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getInputPortCount() {
        return inputPortCount;
    }

    public void setInputPortCount(int inputPortCount) {
        this.inputPortCount = inputPortCount;
    }

    public int getOutputPortCount() {
        return outputPortCount;
    }

    public void setOutputPortCount(int outputPortCount) {
        this.outputPortCount = outputPortCount;
    }

    public RenderingProperties getRenderingProperties() {
        return renderingProperties;
    }

    public void setRenderingProperties(RenderingProperties renderingProperties) {
        this.renderingProperties = renderingProperties;
    }
}
