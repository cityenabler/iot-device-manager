package it.eng.iot.hub.model.kapua;

import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonObject;

public class KuraGatewayMap extends HashMap<String, String> {
	private static KuraGatewayMap INSTANCE;
    
    private KuraGatewayMap() {
    	super();
    }
    
    public static KuraGatewayMap getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new KuraGatewayMap();
        }
        
        return INSTANCE;
    }
    
    public static void populate(List<JsonObject> list) {
    	for (JsonObject json : list) {
    		INSTANCE.put(json.get("clientId").getAsString(), json.get("id").getAsString());
    	}
    }
}
