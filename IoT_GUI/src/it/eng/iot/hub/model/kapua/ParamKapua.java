package it.eng.iot.hub.model.kapua;

public class ParamKapua {

	private String username;
	private String password;
	private String scopeId;
	
	public ParamKapua(String username, String password, String scopeId) {
		this.username = username;
		this.password = password;
		this.scopeId = scopeId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	
}
