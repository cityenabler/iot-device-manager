package it.eng.iot.hub.model.kapua;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.List;

public class  ValueWireGraphService{
    private List<Component> components;
    private List<Wire> wires;

    public ValueWireGraphService() {
    }

    public List<Component> getComponents() {
        return components;
    }

    public String toJSON() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return mapper.writeValueAsString(this).replace("\"","\\\"");
    }


    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public List<Wire> getWires() {
        return wires;
    }

    public void setWires(List<Wire> wires) {
        this.wires = wires;
    }
}
