package it.eng.iot.hub.model.kapua;

import java.util.Date;

public class KapuaPosition {
	private double precision;
	private Date timestamp;
	private int status;
	private double latitude;
	private double altitude;
	private double heading;
	private double speed;
	private int satellites;
	private double longitude;
	
	public KapuaPosition() {
	}
	
	public KapuaPosition(double precision, Date timestamp, int status, double latitude, double altitude, double heading,
			double speed, int satellites, double longitude) {
		super();
		this.precision = precision;
		this.timestamp = timestamp;
		this.status = status;
		this.latitude = latitude;
		this.altitude = altitude;
		this.heading = heading;
		this.speed = speed;
		this.satellites = satellites;
		this.longitude = longitude;
	}
	public double getPrecision() {
		return precision;
	}
	public void setPrecision(double precision) {
		this.precision = precision;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getAltitude() {
		return altitude;
	}
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	public double getHeading() {
		return heading;
	}
	public void setHeading(double heading) {
		this.heading = heading;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public int getSatellites() {
		return satellites;
	}
	public void setSatellites(int satellites) {
		this.satellites = satellites;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
