package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KapuaGetAssetsListResp {

	private String type;
	private List<KuraDeviceAsset> deviceAsset;
	
	public KapuaGetAssetsListResp() {
	}

	public KapuaGetAssetsListResp(String type, List<KuraDeviceAsset> deviceAsset) {
		super();
		this.type = type;
		this.deviceAsset = deviceAsset;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<KuraDeviceAsset> getDeviceAsset() {
		return deviceAsset;
	}

	public void setDeviceAsset(List<KuraDeviceAsset> deviceAsset) {
		this.deviceAsset = deviceAsset;
	}
	
}
