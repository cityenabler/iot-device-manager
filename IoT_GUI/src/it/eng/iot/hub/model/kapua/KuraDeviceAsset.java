package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KuraDeviceAsset {
	
	private List<DeviceAssetChannel> channels;
	private String name;
	
	public KuraDeviceAsset() {
	}
	
	public KuraDeviceAsset(List<DeviceAssetChannel> channels, String name) {
		super();
		this.channels = channels;
		this.name = name;
	}
	
	public List<DeviceAssetChannel> getChannels() {
		return channels;
	}

	public void setChannels(List<DeviceAssetChannel> channels) {
		this.channels = channels;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
