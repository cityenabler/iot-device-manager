package it.eng.iot.hub.model.kapua;

import java.util.Date;

public class DeviceConnection {

	private String protocol;
	private DeviceConnectionStatus status;
	private String userId;
	private String clientId;
	private String serverIp;
	private boolean allowUserChange;
	private UserCouplingMode userCouplingMode;
	private String reservedUserId;
	private String clientIp;
	private String type;
	private Date modifiedOn;
	private String modifiedBy;
	private int optlock;
	private String entityAttributes;
	private String entityProperties;
	private String scopeId;
	private String id;
	private String createdBy;
	private Date createdOn;

}
