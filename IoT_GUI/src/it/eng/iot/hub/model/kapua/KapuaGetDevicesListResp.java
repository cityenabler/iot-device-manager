package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KapuaGetDevicesListResp {

	private String type;
	private List<KuraDevice> items;
	private boolean limitExceeded;
	private KuraDevice firstItem;
	private boolean empty;
	private int size;
	
	public KapuaGetDevicesListResp() {
	}
	
	public KapuaGetDevicesListResp(String type, List<KuraDevice> items, boolean limitExceeded, KuraDevice firstItem, boolean empty,
			int size) {
		super();
		this.type = type;
		this.items = items;
		this.limitExceeded = limitExceeded;
		this.firstItem = firstItem;
		this.empty = empty;
		this.size = size;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<KuraDevice> getItems() {
		return items;
	}

	public void setItems(List<KuraDevice> items) {
		this.items = items;
	}

	public boolean isLimitExceeded() {
		return limitExceeded;
	}
	
	public void setLimitExceeded(boolean limitExceeded) {
		this.limitExceeded = limitExceeded;
	}
	
	public KuraDevice getFirstItem() {
		return firstItem;
	}
	
	public void setFirstItem(KuraDevice firstItem) {
		this.firstItem = firstItem;
	}
	
	public boolean isEmpty() {
		return empty;
	}
	
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
}
