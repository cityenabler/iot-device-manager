package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KuraAssetProperty {
	private String name;
	private boolean array;
	private boolean encrypted;
	private String type;
	private List<String> value;
	
	public KuraAssetProperty() {
	}

	public KuraAssetProperty(String name, boolean array, boolean encrypted, String type, List<String> value) {
		super();
		this.name = name;
		this.array = array;
		this.encrypted = encrypted;
		this.type = type;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isArray() {
		return array;
	}

	public void setArray(boolean array) {
		this.array = array;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}
	
}
