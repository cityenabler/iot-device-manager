package it.eng.iot.hub.model.kapua;

import it.eng.iot.hub.model.HubEntity;

/* EXAMPLE OF HUB
{​
    name: "Kapua01",​
    type: "Kapua",
    parameter: {
    	username: "user123",
    	password: "password123",
    	scopeId: "_"
    }​
    url: "http://212.215.245.146",
    getDevicesApiRest: "",​
    port: 8080,​
    prot: "http",​
    }​

*/

public class Kapua extends HubEntity {
	
	ParamKapua parameter;
	
	public Kapua(ParamKapua parameter) {
		super();
		this.parameter = parameter;
	}	

	public ParamKapua getParameter() {
		return parameter;
	}

	public void setParameter(ParamKapua parameter) {
		this.parameter = parameter;
	}

}
