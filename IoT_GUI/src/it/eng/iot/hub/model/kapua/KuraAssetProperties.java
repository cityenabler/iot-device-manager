package it.eng.iot.hub.model.kapua;

import java.util.List;

public class KuraAssetProperties {
	private List<KuraAssetProperty> property;

	public KuraAssetProperties() {
	}

	public KuraAssetProperties(List<KuraAssetProperty> property) {
		super();
		this.property = property;
	}

	public List<KuraAssetProperty> getProperty() {
		return property;
	}

	public void setProperty(List<KuraAssetProperty> property) {
		this.property = property;
	}
	
}
