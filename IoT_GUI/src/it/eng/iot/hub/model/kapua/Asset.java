package it.eng.iot.hub.model.kapua;

import java.util.List;

public class Asset {
	private String id;
    private String name;
    private String type;
    private KuraTemplate templateAsset;
    private KuraTemplate templateTimer;
    private KuraTemplate templatePublisher;
    private String templateConfigurations;
    private List<Keyword> templateConfigurationKeywords;
    private String description;
    private List<AttributesDema> attributesDemaList;

    public Asset() {
    }
    
    public Asset(String id, String name, String type, KuraTemplate templateAsset, KuraTemplate templateTimer, KuraTemplate templatePublisher, String templateConfigurations, List<Keyword> templateConfigurationKeywords, String description, List<AttributesDema> attributesDemaList) {
	    super();
		this.id = id;
	    this.name = name;
	    this.type = type;
	    this.templateAsset = templateAsset;
	    this.templateTimer = templateTimer;
	    this.templatePublisher = templatePublisher;
	    this.templateConfigurations = templateConfigurations;
	    this.templateConfigurationKeywords = templateConfigurationKeywords;
	    this.description = description;
	    this.attributesDemaList = attributesDemaList;
	}
	
	public String getId() {
	    return id;
	}
	
	public void setId(String id) {
	    this.id = id;
	}
	
	public String getName() {
	    return name;
	}
	
	public void setName(String name) {
	    this.name = name;
	}
	
	public String getType() {
	    return type;
	}
	
	public void setType(String type) {
	    this.type = type;
	}
	
	public KuraTemplate getTemplateAsset() {
		return templateAsset;
	}

	public void setTemplateAsset(KuraTemplate templateAsset) {
		this.templateAsset = templateAsset;
	}

	public KuraTemplate getTemplateTimer() {
		return templateTimer;
	}

	public void setTemplateTimer(KuraTemplate templateTimer) {
		this.templateTimer = templateTimer;
	}

	public KuraTemplate getTemplatePublisher() {
		return templatePublisher;
	}

	public void setTemplatePublisher(KuraTemplate templatePublisher) {
		this.templatePublisher = templatePublisher;
	}

	public String getTemplateConfigurations() {
		return templateConfigurations;
	}

	public void setTemplateConfigurations(String templateConfigurations) {
		this.templateConfigurations = templateConfigurations;
	}
	
	public List<Keyword> getTemplateConfigurationKeywords() {
		return templateConfigurationKeywords;
	}

	public void setTemplateConfigurationKeywords(List<Keyword> templateConfigurationKeywords) {
		this.templateConfigurationKeywords = templateConfigurationKeywords;
	}

	public String getDescription() {
	    return description;
	}
	
	public void setDescription(String description) {
	    this.description = description;
	}
	
	public List<AttributesDema> getAttributesDemaList() {
	    return attributesDemaList;
	}
	
	public void setAttributesDemaList(List<AttributesDema> attributesDemaList) {
	    this.attributesDemaList = attributesDemaList;
	}
	
	@Override
	public String toString() {
	    StringBuffer paramterToString = new StringBuffer().append("[");
	    for(AttributesDema a : attributesDemaList) {
	        paramterToString.append(a.getName() + " : " + a.getType() + ", ");
	    }
	    paramterToString.deleteCharAt(paramterToString.length() - 1);
	    paramterToString.append("]");
	    
	    StringBuffer keywords = new StringBuffer().append("[");
	    for(Keyword keyword : templateConfigurationKeywords) {
	    	keywords.append(keyword.getName() + " : " + keyword.getType() + " : " + keyword.getDefaultValue() + ", ");
	    }
	    keywords.deleteCharAt(keywords.length() - 1);
	    keywords.append("]");
	    
	    return String.format("Asset {_id='%s', name='%s', type='%s', templateAsset='%s', templateTimer='%s', templatePublisher='%s', templateConfigurations='%s', templateConfigurationKeywords='%s', description='%s' , Attributes='%s'}\n",
	            id, name, type, templateAsset, templateTimer, templatePublisher, templateConfigurations, keywords, description, paramterToString);
	}
	
	public String attrToString() {
	    StringBuffer paramterToString = new StringBuffer().append("[");
	    for(AttributesDema a : attributesDemaList){
	        paramterToString.append(a.getName() + ", ");
	    }
	    paramterToString.delete(paramterToString.length() - 2, paramterToString.length());
	    paramterToString.append("]");
	    return paramterToString.toString();
	}
}
