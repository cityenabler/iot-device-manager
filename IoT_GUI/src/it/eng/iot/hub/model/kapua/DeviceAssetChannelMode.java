package it.eng.iot.hub.model.kapua;

public enum DeviceAssetChannelMode {
	WRITE("WRITE"),
	READ("READ"),
	READ_WRITE("READ_WRITE");
		
	private String value;
	
	DeviceAssetChannelMode(String value) {
		this.value = value;
	}
	
	public String getValue() {
        return value;
    }
}
