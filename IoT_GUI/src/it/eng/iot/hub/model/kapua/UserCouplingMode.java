package it.eng.iot.hub.model.kapua;

public enum UserCouplingMode {
	INHERITED("INHERITED"),
	LOOSE("LOOSE"),
	STRICT("STRICT");
	
	private String value;
	
	UserCouplingMode(String value) {
		this.value = value;
	}
	
	public String getValue() {
        return value;
    }
}
