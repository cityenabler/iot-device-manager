package it.eng.iot.hub.model.kapua;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Position {

    private int x;
    private int y;

    @JsonCreator
    public Position( @JsonProperty("x") int x,
                     @JsonProperty("y") int y) {
        this.x = x;
        this.y= y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
