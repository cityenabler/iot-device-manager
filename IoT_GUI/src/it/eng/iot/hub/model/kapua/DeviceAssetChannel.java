package it.eng.iot.hub.model.kapua;

import java.util.Date;

public class DeviceAssetChannel {

	private DeviceAssetChannelMode mode;
	private Date timestamp;
	private String error;
	private String name;
	private String value;
	
	public DeviceAssetChannel(DeviceAssetChannelMode mode, Date timestamp, String error, String name, String value) {
		super();
		this.mode = mode;
		this.timestamp = timestamp;
		this.error = error;
		this.name = name;
		this.value = value;
	}

	public DeviceAssetChannelMode getMode() {
		return mode;
	}

	public void setMode(DeviceAssetChannelMode mode) {
		this.mode = mode;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
