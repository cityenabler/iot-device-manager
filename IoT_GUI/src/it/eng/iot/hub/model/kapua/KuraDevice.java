package it.eng.iot.hub.model.kapua;

import java.util.Date;
import java.util.List;

import com.google.gson.JsonObject;

public class KuraDevice {

	private String osVersion;
	private String serialNumber;
	private String displayName;
	DeviceEvent lastEvent;
	DeviceConnection connection;
	private DeviceStatus status;
	private String groupId;
	private String clientId;
	private String firmwareVersion;
	private String modelId;
	private String biosVersion;
	private String jvmVersion;
	private String applicationFrameworkVersion;
	private String connectionInterface;
	private String connectionIp;
	private String acceptEncoding;
	private String applicationIdentifiers;
	private String connectionId;
	private String lastEventId;
	private String imei;
	private String imsi;
	private String iccid;
	private String osgiFrameworkVersion;
	private String customAttribute1;
	private List<String> tagIds;
	private String customAttribute2;
	private String customAttribute3;
	private String customAttribute4;
	private String customAttribute5;
	private String type;
	private Date modifiedOn;
	private String modifiedBy;
	private int optlock;
	private String entityAttributes; //{"additionalProp1": "string","additionalProp2": "string","additionalProp3": "string"}
	private String entityProperties; //{"additionalProp1": "string", "additionalProp2": "string","additionalProp3": "string"}
	private String scopeId;
	private String id;
	private String createdBy;
	private Date createdOn;
	
	public KuraDevice() {
		super();
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public DeviceEvent getLastEvent() {
		return lastEvent;
	}
	public void setLastEvent(DeviceEvent lastEvent) {
		this.lastEvent = lastEvent;
	}
	public DeviceConnection getConnection() {
		return connection;
	}
	public void setConnection(DeviceConnection connection) {
		this.connection = connection;
	}
	public DeviceStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceStatus status) {
		this.status = status;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getBiosVersion() {
		return biosVersion;
	}
	public void setBiosVersion(String biosVersion) {
		this.biosVersion = biosVersion;
	}
	public String getJvmVersion() {
		return jvmVersion;
	}
	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}
	public String getApplicationFrameworkVersion() {
		return applicationFrameworkVersion;
	}
	public void setApplicationFrameworkVersion(String applicationFrameworkVersion) {
		this.applicationFrameworkVersion = applicationFrameworkVersion;
	}
	public String getConnectionInterface() {
		return connectionInterface;
	}
	public void setConnectionInterface(String connectionInterface) {
		this.connectionInterface = connectionInterface;
	}
	public String getConnectionIp() {
		return connectionIp;
	}
	public void setConnectionIp(String connectionIp) {
		this.connectionIp = connectionIp;
	}
	public String getAcceptEncoding() {
		return acceptEncoding;
	}
	public void setAcceptEncoding(String acceptEncoding) {
		this.acceptEncoding = acceptEncoding;
	}
	public String getApplicationIdentifiers() {
		return applicationIdentifiers;
	}
	public void setApplicationIdentifiers(String applicationIdentifiers) {
		this.applicationIdentifiers = applicationIdentifiers;
	}
	public String getConnectionId() {
		return connectionId;
	}
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	public String getLastEventId() {
		return lastEventId;
	}
	public void setLastEventId(String lastEventId) {
		this.lastEventId = lastEventId;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getIccid() {
		return iccid;
	}
	public void setIccid(String iccid) {
		this.iccid = iccid;
	}
	public String getOsgiFrameworkVersion() {
		return osgiFrameworkVersion;
	}
	public void setOsgiFrameworkVersion(String osgiFrameworkVersion) {
		this.osgiFrameworkVersion = osgiFrameworkVersion;
	}
	public String getCustomAttribute1() {
		return customAttribute1;
	}
	public void setCustomAttribute1(String customAttribute1) {
		this.customAttribute1 = customAttribute1;
	}
	public List<String> getTagIds() {
		return tagIds;
	}
	public void setTagIds(List<String> tagIds) {
		this.tagIds = tagIds;
	}
	public String getCustomAttribute2() {
		return customAttribute2;
	}
	public void setCustomAttribute2(String customAttribute2) {
		this.customAttribute2 = customAttribute2;
	}
	public String getCustomAttribute3() {
		return customAttribute3;
	}
	public void setCustomAttribute3(String customAttribute3) {
		this.customAttribute3 = customAttribute3;
	}
	public String getCustomAttribute4() {
		return customAttribute4;
	}
	public void setCustomAttribute4(String customAttribute4) {
		this.customAttribute4 = customAttribute4;
	}
	public String getCustomAttribute5() {
		return customAttribute5;
	}
	public void setCustomAttribute5(String customAttribute5) {
		this.customAttribute5 = customAttribute5;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getOptlock() {
		return optlock;
	}
	public void setOptlock(int optlock) {
		this.optlock = optlock;
	}
	public String getEntityAttributes() {
		return entityAttributes;
	}
	public void setEntityAttributes(String entityAttributes) {
		this.entityAttributes = entityAttributes;
	}
	public String getEntityProperties() {
		return entityProperties;
	}
	public void setEntityProperties(String entityProperties) {
		this.entityProperties = entityProperties;
	}
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public JsonObject toJsonObj() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("id", this.getId());
		jsonObject.addProperty("clientId", this.getClientId());
		jsonObject.addProperty("displayName", this.getDisplayName());
		
		return jsonObject;
	}
	
}
