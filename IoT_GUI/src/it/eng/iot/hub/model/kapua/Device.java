package it.eng.iot.hub.model.kapua;

import java.util.List;

public class Device {
	
	private String id;
	private String deviceId;
    private String type;
    private Gateway gateway;
    private List<Configuration> configuration;

    public Device() {
    }
    
	public Device(String id, String deviceId, String type, Gateway gateway, List<Configuration> configuration) {
		super();
		this.id = id;
		this.deviceId = deviceId;
		this.type = type;
		this.gateway = gateway;
		this.configuration = configuration;
	}
	
	public String getId() {
	    return id;
	}
	
	public void setId(String id) {
	    this.id = id;
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Gateway getGateway() {
		return gateway;
	}
	
	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}
	
	public List<Configuration> getConfiguration() {
		return configuration;
	}
	
	public void setConfiguration(List<Configuration> configuration) {
		this.configuration = configuration;
	}
	
	public Configuration getConfigurationItem(String configurationName) {
		List<Configuration> configurations = this.getConfiguration();
		for (Configuration configuration : configurations) {
			if (configuration.getName().compareTo(configurationName) == 0) {
				return configuration;
			}
		}
		return null;
	}
	
	public void setConfigurationItem(Configuration config) {
		List<Configuration> configurations = this.getConfiguration();
		for (Configuration configuration : configurations) {
			if (configuration.getName().compareTo(config.getName()) == 0) {
				configuration.setValue(config.getValue());	/*Keep name and type, replace only the value*/
			}
		}
	}
	
	@Override
	public String toString() {	    
	    StringBuffer keywords = new StringBuffer().append("[");
	    for(Configuration keyword : configuration) {
	    	keywords.append(keyword.toString());
	    }
	    keywords.deleteCharAt(keywords.length() - 1);
	    keywords.append("]");
	    
	    return String.format("Asset {deviceId='%s', type='%s', gateway='%s', configurations='%s'}\n",
	            deviceId, type, gateway, configuration);
	}
	
}
