package it.eng.iot.hub.model.kapua;

public class KuraAssetConfiguration {
	private String id;
	private KuraAssetProperties properties;
	
	public KuraAssetConfiguration() {
	}
	
	public KuraAssetConfiguration(String id, KuraAssetProperties properties) {
		super();
		this.id = id;
		this.properties = properties;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public KuraAssetProperties getProperties() {
		return properties;
	}
	
	public void setProperties(KuraAssetProperties properties) {
		this.properties = properties;
	}
	
}
