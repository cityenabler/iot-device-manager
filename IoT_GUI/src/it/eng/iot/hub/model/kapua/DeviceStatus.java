package it.eng.iot.hub.model.kapua;

public enum DeviceStatus {
	ENABLE("ENBLED"),
	DISABLED("DISABLED");
	
	private String value;
	
	DeviceStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
        return value;
    }
}
