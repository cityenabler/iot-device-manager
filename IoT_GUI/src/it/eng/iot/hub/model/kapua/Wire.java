package it.eng.iot.hub.model.kapua;

/*
Example

      {
         "emitter":"K3test-A",
         "emitterPort":0,
         "receiver":"DE_publisher",
         "receiverPort":0
      },
 */

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Wire {

    private String emitter;
    private int emitterPort;
    private String receiver;
    private int receiverPort;

    @JsonCreator
    public Wire(  @JsonProperty("emitter") String emitter,
                  @JsonProperty("emitterPort")  int emitterPort,
                  @JsonProperty("receiver") String receiver,
                  @JsonProperty("receiverPort") int receiverPort) {
        this.emitter = emitter;
        this.emitterPort = emitterPort;
        this.receiver = receiver;
        this.receiverPort = receiverPort;
    }

    public String getEmitter() {
        return emitter;
    }

    public void setEmitter(String emitter) {
        this.emitter = emitter;
    }

    public int getEmitterPort() {
        return emitterPort;
    }

    public void setEmitterPort(int emitterPort) {
        this.emitterPort = emitterPort;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getReceiverPort() {
        return receiverPort;
    }

    public void setReceiverPort(int receiverPort) {
        this.receiverPort = receiverPort;
    }
}
