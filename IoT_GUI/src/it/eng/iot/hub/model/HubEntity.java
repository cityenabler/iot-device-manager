package it.eng.iot.hub.model;

public abstract class HubEntity {

	private String name;
	private String type;
	private String url;
	private String getDevicesApiRest;
	private String port;
	private String prot;

	public HubEntity() {
	}

	public HubEntity(String name, String type, String url, String getDevicesApiRest, String port, String prot) {
		super();
		this.name = name;
		this.type = type;
		this.url = url;
		this.getDevicesApiRest = getDevicesApiRest;
		this.port = port;
		this.prot = prot;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getGetDevicesApiRest() {
		return getDevicesApiRest;
	}

	public void setGetDevicesApiRest(String getDevicesApiRest) {
		this.getDevicesApiRest = getDevicesApiRest;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProt() {
		return prot;
	}

	public void setProt(String prot) {
		this.prot = prot;
	}

}
