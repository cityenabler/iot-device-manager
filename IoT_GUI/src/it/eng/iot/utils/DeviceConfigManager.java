package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadLocalRandom;

import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfIdas;
import it.eng.iot.configuration.ConfIotaJSON;
import it.eng.iot.configuration.ConfIotaLWM2M;
import it.eng.iot.configuration.ConfIotaLoRa;
import it.eng.iot.configuration.ConfIotaOPCUA;
import it.eng.iot.configuration.ConfIotaSigfox;
import it.eng.iot.configuration.ConfIotaUL20;
import it.eng.iot.configuration.ConfMQTTBroker;
import it.eng.iot.configuration.ConfMQTTSimulator;
import it.eng.iot.configuration.ConfOrionCB;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.DataformatProtocol;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.servlet.model.FEDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEHttpDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEHttpPullDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEHttpPushDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FELoraDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FELwm2mPushDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEMqttPullDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEMqttPushDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FEOpcuaDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.FESigfoxPushDevicePublishedConfirmBean;
import it.eng.iot.servlet.model.MapCenter;
import it.eng.iot.servlet.model.RetrieveDataMode;
import it.eng.iot.servlet.model.TransportProtocol;
import it.eng.tools.Idas;
import it.eng.tools.Orion;
import it.eng.tools.TenantManager;
import it.eng.tools.model.ApplicationServerAttribute;
import it.eng.tools.model.DeviceEntityBean;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceAttribute;
import it.eng.tools.model.IdasDeviceCommand;
import it.eng.tools.model.IdasDeviceInternalAttributeLWM2M;
import it.eng.tools.model.IdasDeviceInternalAttributeLoRa;
import it.eng.tools.model.IdasDeviceInternalAttributeSigfox;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceLoRaLWM2M;
import it.eng.tools.model.IdasDeviceSigfox;
import it.eng.tools.model.IdasDeviceStaticAttribute;
import it.eng.tools.model.LorawanAttribute;
import it.eng.tools.model.ObjectAttribute;
import it.eng.tools.model.ScopeDTO;



public abstract class DeviceConfigManager {

	private static final Logger LOGGER = LogManager.getLogger(DeviceConfigManager.class);
	private static Orion orion;
	private static final String DEVICE_PREFIX ="Device:";
	static String loraMqttEndpoint = ConfIotaLoRa.getString("iota.lora.attrs.mqtt.endpoint");
	static String loraServerProvider = ConfIotaLoRa.getString("iota.loraServer.attrs.provider");
	static String loraTTNProvider = ConfIotaLoRa.getString("iota.loraTTN.attrs.provider");
	static String loraAppEuiDefault = ConfIotaLoRa.getString("iota.lora.attrs.appeui.default");
	static String loraTtnHost = ConfIotaLoRa.getString("iota.lora.ttn.host");
	static String enabledUrlshortener = ConfTools.getString("urlshortener.active");
	static String urlshortenerPattern = ConfTools.getString("urlshortener.url.pattern");
	static final String orionPublicHost = ConfOrionCB.getString("orion.public.host");
	static final String orionFilteringDocumentation = ConfOrionCB.getString("orion.filtering.documentation");
	
	
	
	
	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}

	/*
	 * Delete Service: 
	 *  - Action1 : Delete the device on IDAS 
	 *  - Action2 : Delete the device on ORION 
	 *  - Action3 : Delete all subscriptions linked to the device_id
	 */
	public static boolean deleteDeviceProcess(String headerService, String headerServicePath, String deviceId, String dataformat_protocol, boolean isCamera) throws Exception {
		boolean deletedIotDevice = false;
		boolean deletedOrionDevice = false;
		boolean deletedDeviceSubscription = false;
		boolean simulatorActive = Boolean.parseBoolean(ConfTools.getString("simulator.active"));
		
		try {
			
			LOGGER.log(Level.INFO,"headerService" + headerService);

			boolean isLora = isLora(dataformat_protocol);
			boolean isOpcua = isOpcua(dataformat_protocol);
			
			if (simulatorActive && !isCamera && !isLora && !isOpcua) {
			
				// 0. STOP DEVICE SIMULATIONS
				JSONObject simStopped  = IotsimUtils.stopDeviceSimulation(headerService, headerServicePath, deviceId);
				LOGGER.log(Level.INFO,"IotsimUtils.consumeGet result" + simStopped);
			}
			
			
			
			// 1. DELETE DEVICE ON all the IOTA existing in IDAS
			for (DataformatProtocol dataformat: DataformatProtocol.values()) {
				String dataformatProtocol = dataformat.toString();
				
				
				if (!CommonUtils.isEnabledAgent(dataformatProtocol))
					continue;
				
				
				if (dataformatProtocol.equalsIgnoreCase(dataformat_protocol)){
					
					// SWITCH AGENT // 
					Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
					
					
					if (isLora) {
						
						IdasDevice idasDevice = idas.getDevice(headerService, headerServicePath, deviceId);
						
						boolean isTTN = LoraUtils.isTTN(idasDevice);
						
						if (!isTTN) {
							String devEUI = LoraUtils.getDevEui(idasDevice);
							String jwt = LoraUtils.getJWTSingleton();
							LoraUtils.deleteDeviceOnServer(devEUI, jwt);
						}
					}
					
					
					try{
						deletedIotDevice = idas.deleteDevice(headerService, headerServicePath, deviceId);
						LOGGER.log(Level.INFO,"Deleted Iot Device " + deletedIotDevice);
					}catch (Exception e) {
						deletedIotDevice = false;
						LOGGER.log(Level.ERROR,"Device not deleted because it is not created on this IOTA " + e.getMessage());
					}
				}
			}
			if (deletedIotDevice){
				
				// 2. DELETE the subscription related to the device
				deletedDeviceSubscription = SubscriptionManager.deleteDeviceSubscriptions(headerService, headerServicePath, deviceId);
				
				// 3. UPDATE opType=deleted Of the DEVICE Entity on ORION
				deletedOrionDevice = orion.updateEntityAttribute(headerService, headerServicePath, deviceId, "opType", "deleted");
				
			}
		} 
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			if (deletedIotDevice) {
				// TODO ROOLBACK ON IDAS
			}

			if (deletedOrionDevice) {
				// TODO ROLLBACK ON ORION
			}

			if (deletedDeviceSubscription) {
				// TODO ROLLBACK ON ORION
			}

			throw new Exception("Device deletion failed");
		}

		return deletedIotDevice && deletedOrionDevice && deletedDeviceSubscription;
	}




	/* 
	 * modifyDeviceProcess 
	 * STEP1: Remove device from IDAS	
	 * STEP2: Create on IDAS
	 * STEP3: Remove the delta attribute on Orion
	 */
	public static boolean modifyDeviceProcess(String headerService, String headerServicePath, IdasDeviceList device) throws Exception {
		try {

			if(device==null){
				throw new Exception("No device supplied");
			}

			Set<IdasDevice> devicesList = device.getDevices();
			if(devicesList==null || devicesList.isEmpty()){
				throw new Exception("No device supplied");
			}

			for(IdasDevice dev : devicesList){

				boolean deletedIotDevice = false;
				boolean deletedOrionDevice = false;
				boolean deletedOrionDeltaAttrs = false;

				try{
					String deviceId = dev.getDevice_id();
					String dataformatProtocol = dev.getProtocol();
					// STEP1: Remove from IDAS		
					//Idas idas = new Idas();
					Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
					
					IdasDevice oldDevice = idas.getDevice(headerService, headerServicePath, deviceId);
					

					boolean deleteDeviceCompleted = idas.deleteDevice(headerService, headerServicePath, deviceId);

					if (deleteDeviceCompleted) {
					
						dev = setInternalAttribute(dataformatProtocol, dev, headerService, headerServicePath, true, oldDevice);
						
						// STEP2: Create on IDAS
						JSONObject jResp = idas.registerDevice(headerService, headerServicePath, device);

						if (jResp!=null) {
							// STEP3: Remove the delta attribute on Orion
							// 3.1 Get IDAS entity attributes
							IdasDevice idasDevice = device.getDevices().iterator().next();
							Set<String> idasAttributes = new HashSet<String>();
							Set<IdasDeviceAttribute> listAttributes = idasDevice.getAttributes();

							Iterator<IdasDeviceAttribute> it = listAttributes.iterator();
							while(it.hasNext()) {
								IdasDeviceAttribute attribute = it.next();
								String attributeName = attribute.getName();
								idasAttributes.add(attributeName);
							}

							// FIX: add commands to the attribute list to be compared with those existing on orion
							Set<IdasDeviceCommand> listCommands = idasDevice.getCommands();
							
							if (listCommands!= null) {
							
								Iterator<IdasDeviceCommand> cmds = listCommands.iterator();
								while(cmds.hasNext()) {
									IdasDeviceCommand cmd = cmds.next();
									String cmdName = cmd.getName();
									idasAttributes.add(cmdName);
									idasAttributes.add(cmdName+"_info");
									idasAttributes.add(cmdName+"_status");
								}
							}

							Set<IdasDeviceStaticAttribute> listStaticAttributes = idasDevice.getStatic_attributes();
							
							if (listStaticAttributes!= null) {
								Iterator<IdasDeviceStaticAttribute> its = listStaticAttributes.iterator();
								while(its.hasNext()) {
									IdasDeviceStaticAttribute attribute = its.next();
									String attributeName = attribute.getName();
									idasAttributes.add(attributeName);
								}
							}
							// 3.2 Get ORION entity attributes
							Orion orion = new Orion();

							String orionEntityAttrs = orion.getEntityAttributes(headerService, headerServicePath, deviceId);
							JSONObject jsonObj = new JSONObject(orionEntityAttrs);

							Set<String> orionAttributes = new HashSet<String>();
							Iterator<String> iterator = jsonObj.keySet().iterator();

							while (iterator.hasNext()){
								String elem = iterator.next();
								if ( elem.equalsIgnoreCase("id") || elem.equalsIgnoreCase("type") || elem.equalsIgnoreCase("TimeInstant")){
									continue;
								}
								orionAttributes.add(elem);
							}
							Collection<String> exludedAttributes =  CollectionUtils.subtract(orionAttributes, idasAttributes);
							for(String removedAttr: exludedAttributes){
								try{
									orion.deleteEntityAttribute(headerService, headerServicePath, deviceId, removedAttr);
								}
								catch(Exception attr_e){
									LOGGER.log(Level.ERROR, attr_e.getMessage(), attr_e);
								}
							}
							deletedOrionDeltaAttrs = true;
						}

					}

				}
				catch(Exception inner_e){
					LOGGER.log(Level.ERROR, inner_e.getMessage(), inner_e);

					if (deletedIotDevice) {
						// TODO ROOLBACK ON IDAS
					}

					if (deletedOrionDevice) {
						// TODO ROLLBACK ON ORION
					}

					if(deletedOrionDeltaAttrs){
						//TODO ROLLBACK of the attrs
					}
				}

			}
		} 
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			throw new Exception("Device deletion failed: " + e.getMessage());
		}

		return true;
	}


	public static IdasDevice setInternalAttributeLWM2M(String dataformatProtocol, IdasDevice device) {
		
		IdasDeviceLoRaLWM2M lwm2mDevice = new IdasDeviceLoRaLWM2M(device.getDevice_id(), device.getEntity_name(), 
				device.getEntity_type(), device.getTransport(), device.getProtocol(), device.getEndpoint(), device.getLoraDeviceProfileId());
		lwm2mDevice.setAttributes(device.getAttributes());
		lwm2mDevice.setStatic_attributes(device.getStatic_attributes());
		lwm2mDevice.setCommands(device.getCommands());
		
		if (dataformatProtocol.toLowerCase().equalsIgnoreCase("lwm2m")){
			try {
			// get attribute
			int min = 0;
			int max = 999999;
			int randomInt = ThreadLocalRandom.current().nextInt(min, max + 1);
			//int objectType = 633830;
			// TODO TO UNDENRSTAND BETTER THE MEANING OF objectType
			int objectType = randomInt;
			int objectInstance = 0;
			int objectResource = 0;
			Set<IdasDeviceAttribute> attrs = device.getAttributes();
			
			Map<String, ObjectAttribute> lwm2mResourceMapping = new HashMap<String, ObjectAttribute>();
			for(IdasDeviceAttribute attr : attrs){
				String attrName= attr.getName();
				// set internal attribute
				objectResource = objectResource + 1;
				ObjectAttribute objectAttribute = new ObjectAttribute(objectType, objectInstance, objectResource);
				lwm2mResourceMapping.put(attrName, objectAttribute);
			}	
			
			IdasDeviceInternalAttributeLWM2M attrLwm2m = new IdasDeviceInternalAttributeLWM2M();
			attrLwm2m.setLwm2mResourceMapping(lwm2mResourceMapping);
			LOGGER.log(Level.INFO, "lwm2mResourceMapping " + new Gson().toJson(attrLwm2m)); 
			// {"lwm2mResourceMapping":{"status":{"objectType":1111,"objectInstance":0,"objectResource":1}}}
			lwm2mDevice.setInternal_attributes(attrLwm2m);
			
			LOGGER.log(Level.INFO, "device " + new Gson().toJson(device));
			} catch (Exception e){
				LOGGER.log(Level.ERROR,"Internal attribute not configured " + e.getMessage());
			}					
		}	
		
		String XLOG = new Gson().toJson(device);
		LOGGER.log(Level.INFO, XLOG);
		
		return lwm2mDevice;
	}

	
public static IdasDevice setInternalAttributeSIGFOX(String dataformatProtocol, IdasDevice device) {
		
		IdasDeviceSigfox sigfoxDevice = new IdasDeviceSigfox(device.getDevice_id(), device.getEntity_name(), 
				device.getEntity_type(), device.getTransport(), device.getProtocol(), device.getEndpoint(), device.getLoraDeviceProfileId());
		sigfoxDevice.setAttributes(device.getAttributes());
		sigfoxDevice.setStatic_attributes(device.getStatic_attributes());
		sigfoxDevice.setCommands(device.getCommands());
		
		if (dataformatProtocol.toLowerCase().equalsIgnoreCase("sigfox")){
			try {
			// get attribute
			Set<IdasDeviceAttribute> attrs = device.getAttributes();
			Set<IdasDeviceInternalAttributeSigfox> internal_attributes = new HashSet<IdasDeviceInternalAttributeSigfox>();
			/*
			"internal_attributes": [
		        {
		          "mapping": "theCounter::uint:32  theParam1::uint:32 param2::uint:8 tempDegreesCelsius::uint:8  voltage::uint:16"
		        }
		      ]
			*/
			String values = "";
			for(IdasDeviceAttribute attr : attrs){
				String attrName = attr.getName();
				// set internal attribute - attribute list
				// tempDegreesCelsius::uint:8  voltage::uint:16
				String val = " " + attrName + "::uint:32";
				values =  values + val;
			}	
			IdasDeviceInternalAttributeSigfox mapping = new IdasDeviceInternalAttributeSigfox(values.trim());
			Set<IdasDeviceInternalAttributeSigfox> mappingSet = new HashSet<IdasDeviceInternalAttributeSigfox>();
			mappingSet.add(mapping);
			sigfoxDevice.setInternal_attributes (mappingSet);
			
			} catch (Exception e){
				LOGGER.log(Level.ERROR,"Internal attribute not configured " + e.getMessage());
			}					
		}	
		String XLOG = new Gson().toJson(device);
		LOGGER.log(Level.INFO,XLOG);
		return sigfoxDevice;
	}


	public static IdasDevice setInternalAttribute(String dataformatProtocol, IdasDevice device, String fiwareservice, String fiwareservicepath, boolean isUpdate, IdasDevice oldDevice) {
	
		if (dataformatProtocol.toLowerCase().equalsIgnoreCase("lwm2m")){
			device = setInternalAttributeLWM2M(dataformatProtocol, device);
		} else if (dataformatProtocol.toLowerCase().equalsIgnoreCase("sigfox")){
			device = setInternalAttributeSIGFOX(dataformatProtocol, device);
		}else if (device.getProtocol().equalsIgnoreCase("CAYENNELPP") || device.getProtocol().equalsIgnoreCase("CBOR") || device.getProtocol().equalsIgnoreCase("APPLICATION_SERVER")) {//LORA
			device = setInternalAttributeLoraAndPublishOnAppServer(device, fiwareservice, fiwareservicepath,  isUpdate, oldDevice);
		}
		
		return device;
	}
	

	/* Return the device location */
	public static String getDeviceLocation(String headerService, String headerServicePath, String deviceId){

		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "Device"));
		params.add(new BasicNameValuePair("id", deviceId));

		String resp =  orion.getEntities(headerService, headerServicePath, params);
		LOGGER.log(Level.INFO, "resp: " + resp);

		Type type = new TypeToken<Set<DeviceEntityBean>>(){}.getType();
		Set<DeviceEntityBean> apiResp = new Gson().fromJson(resp, type);

		String location = "0,0";
		for (DeviceEntityBean device : apiResp){
			location = device.getLocation().getValue();
		}
		LOGGER.log(Level.INFO, "Device Location:" + location);
		return location;
	}


	/* Prepare confirmation data  */
	public static FEDevicePublishedConfirmBean getDeviceConfirmantionData (String transportProtocol, String dataformatProtocol, IdasDevice device, String fiwareservice, String fiwareservicepath){

		/* PREPARING THE CONFIRMATION DATA */	
		FEDevicePublishedConfirmBean devicePublishedConfirmBean = null;

		String deviceId = device.getDevice_id();
		Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();

		String orgs = "";
		String endpoint = "";
		String retrieveDataMode = "";
		String screenId = "";

		for(IdasDeviceStaticAttribute attr : staticAttrs){
			LOGGER.log(Level.INFO,"attributi IDAS " + attr.getName() + " - " + attr.getValue());
			if ("organization".equalsIgnoreCase(attr.getName()) ){
				orgs = attr.getValue();
				LOGGER.log(Level.INFO,"orgs " + orgs);
			}	

			if ("retrieve_data_mode".equalsIgnoreCase(attr.getName()) ){
				retrieveDataMode = attr.getValue();
				LOGGER.log(Level.INFO,"retrieve_data_mode " + retrieveDataMode);
			}

			if("streaming_url".equalsIgnoreCase(attr.getName())){
				endpoint = attr.getValue();
			}
			
			if("screenId".equalsIgnoreCase(attr.getName())){
				screenId = attr.getValue();
			}

		}		
		// CHOOSE WHAT GETAPIKEY YOU WANT TO USE:
		// Get apikey for the specific organization
		//String apikey = ServiceConfigManager.getApikey(orgs);
		// otherwise get apikey from cedus configuration
		String apikey = ServiceConfigManager.getServicePathApiKeyTM(fiwareservice, fiwareservicepath) ;
		LOGGER.log(Level.INFO,"Organization APIKEY " + apikey);

		LOGGER.log(Level.INFO,"Preparing the bean to send to the confirm modal.......");
		// TRANSPORT HTTP
		if (transportProtocol.equalsIgnoreCase("http")){
			LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);

			switch (dataformatProtocol){
			case ("UL2_0"): { // UL2_0
				LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);
				
				//http://217.172.12.177:8085/iot/d?i=sSound1&k=123123
				String measuresEndpoint = 	ConfIotaUL20.getString("iota.ul20.infopoint.endpoint")+
											ConfIdas.getString("iota.resource") + 
											"?i=" + deviceId + "&k=" + apikey;
				LOGGER.log(Level.INFO,"IOT Agent Endpoint " + measuresEndpoint);
				

				switch (retrieveDataMode.toLowerCase()){
				case ("push"): { // PUSH
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEHttpPushDevicePublishedConfirmBean devicePublishedConfirm = new FEHttpPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					
					devicePublishedConfirm.setMeasuresEndpoint(measuresEndpoint);
					
					devicePublishedConfirm.setDataformat(DataformatProtocol.UL2_0);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				case ("pull"): 
				default: { // PULL
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEHttpPullDevicePublishedConfirmBean devicePublishedConfirm = new FEHttpPullDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);

					if(device.getEndpoint()!=null)
						endpoint= device.getEndpoint();

					devicePublishedConfirm.setEndpoint(endpoint);
					LOGGER.log(Level.INFO,"Device Endpoint waiting for command: " + endpoint);

					devicePublishedConfirm.setMeasuresEndpoint(measuresEndpoint);
					devicePublishedConfirm.setDataformat(DataformatProtocol.UL2_0);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				}// switch push/pull
				break;
			}// case UL2_0
			case "JSON": {// JSON
				LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);

				//http://217.172.12.177:8085/iot/d?i=sSound1&k=123123
				String measuresEndpoint= 
						ConfIotaJSON.getString("iota.json.infopoint.endpoint") +
						":"	+ ConfIotaJSON.getString("iota.json.southbound.port") +
						ConfIdas.getString("iota.resource") + 
						"?i=" + deviceId + "&k=" + apikey;
				LOGGER.log(Level.INFO,"IOT Agent Endpoint " + measuresEndpoint);

				
				
				
				
				switch (retrieveDataMode.toLowerCase()){
				case ("push"): { // PUSH
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEHttpPushDevicePublishedConfirmBean devicePublishedConfirm = new FEHttpPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					devicePublishedConfirm.setMeasuresEndpoint(measuresEndpoint);
					
					devicePublishedConfirm.setDataformat(DataformatProtocol.JSON);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}case ("pull"): { // PULL
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEHttpPullDevicePublishedConfirmBean devicePublishedConfirm = new FEHttpPullDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					
					devicePublishedConfirm.setMeasuresEndpoint(measuresEndpoint);
					endpoint= device.getEndpoint();
					devicePublishedConfirm.setEndpoint(endpoint);
					LOGGER.log(Level.INFO,"Device Endpoint waiting for command: " + endpoint);

					devicePublishedConfirm.setMeasuresEndpoint(measuresEndpoint);
					devicePublishedConfirm.setDataformat(DataformatProtocol.JSON);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				}// switch push/pull
				break;
			}
			} // END switch dataformat

		}// end HTTP

		// TRANSPORT MQTT
		else if (transportProtocol.equalsIgnoreCase("mqtt")){
			switch (dataformatProtocol){
			case ("UL2_0"): {
				switch (retrieveDataMode.toLowerCase()){
				case ("push"): { 
					// PUSH
					//mosquitto_pub -h 127.0.0.1 -t '/GmJszisVlK8ccGR/colosseo/attrs' -m 'status|occupied'
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEMqttPushDevicePublishedConfirmBean devicePublishedConfirm = new FEMqttPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					
					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));
					
					String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
					if (mqttBrokerSslEnabled) {
						mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						String certDownloadLink = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.cert.download-link");
						devicePublishedConfirm.setSslcertlink(certDownloadLink);
					}

					endpoint= mqttBrokerProtocol+
							ConfMQTTBroker.getString("mqtt.broker.ul20.host") + ":" + 
							ConfMQTTBroker.getString("mqtt.broker.ul20.port");
					LOGGER.log(Level.INFO,"MQTT Broker Endpoint " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setTopic("/"+apikey+"/"+deviceId + ConfIdas.getString("iota.mqtt.topic.attrs"));
					devicePublishedConfirm.setDataformat(DataformatProtocol.UL2_0);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);
					devicePublishedConfirm.setName(deviceId);
					
					boolean mqttAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.enabled"));
					
					if (mqttAuthEnabled) {
						
						String  mqttBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.username");
						String  mqttBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.password");
						
						devicePublishedConfirm.setMqttusername(mqttBrokerJSONAuthUsername);
						devicePublishedConfirm.setMqttpassword(mqttBrokerJSONAuthPassword);
						
					}
					
					
					
					
					
					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}case ("pull"): {
					// PULL
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEMqttPullDevicePublishedConfirmBean devicePublishedConfirm = new FEMqttPullDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					
					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));

					String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
					if (mqttBrokerSslEnabled) {
						mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						String certDownloadLink = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.cert.download-link");
						devicePublishedConfirm.setSslcertlink(certDownloadLink);
					}

					

					endpoint= mqttBrokerProtocol+
							ConfMQTTBroker.getString("mqtt.broker.ul20.host") + ":" + 
							ConfMQTTBroker.getString("mqtt.broker.ul20.port");
					LOGGER.log(Level.INFO,"Platform MQTT Broker Endpoint " + endpoint);
					LOGGER.log(Level.INFO,"Platform MQTT Broker Endpoint waiting for command: " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setTopic("/"+apikey+"/"+ deviceId + ConfIdas.getString("iota.mqtt.topic.cmd"));
					devicePublishedConfirm.setDataformat(DataformatProtocol.UL2_0);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);
					
					boolean mqttAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.enabled"));
					
					if (mqttAuthEnabled) {
						
						String  mqttBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.username");
						String  mqttBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.password");
						
						devicePublishedConfirm.setMqttusername(mqttBrokerJSONAuthUsername);
						devicePublishedConfirm.setMqttpassword(mqttBrokerJSONAuthPassword);
						
					}

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				}
				break;
			}
			case "JSON": {
				switch (retrieveDataMode.toLowerCase()){
				case ("push"): {
					// PUSH
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEMqttPushDevicePublishedConfirmBean devicePublishedConfirm = new FEMqttPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					
					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));
					
					String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
					
					if (mqttBrokerSslEnabled) {
						
						mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						String certDownloadLink = ConfMQTTBroker.getString("mqtt.broker.json.ssl.cert.download-link");
						devicePublishedConfirm.setSslcertlink(certDownloadLink);
					}
					
					
					endpoint=mqttBrokerProtocol +
							ConfMQTTBroker.getString("mqtt.broker.json.host") + ":" + 
							ConfMQTTBroker.getString("mqtt.broker.json.port");
					LOGGER.log(Level.INFO,"MQTT Broker Endpoint " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setTopic("/"+apikey+"/"+deviceId + ConfIdas.getString("iota.mqtt.topic.attrs"));
					devicePublishedConfirm.setDataformat(DataformatProtocol.JSON);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);
					
					boolean mqttAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.authentication.enabled"));
					
					if (mqttAuthEnabled) {
						
						String  mqttBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.json.authentication.username");
						String  mqttBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.json.authentication.password");
						
						devicePublishedConfirm.setMqttusername(mqttBrokerJSONAuthUsername);
						devicePublishedConfirm.setMqttpassword(mqttBrokerJSONAuthPassword);
						
					}
					
					

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}case ("pull"): { 
					// PULL
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FEMqttPullDevicePublishedConfirmBean devicePublishedConfirm = new FEMqttPullDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);

					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));
					
					String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
					if (mqttBrokerSslEnabled) {
						
						mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						String certDownloadLink = ConfMQTTBroker.getString("mqtt.broker.json.ssl.cert.download-link");
						devicePublishedConfirm.setSslcertlink(certDownloadLink);
					}
					
					endpoint=mqttBrokerProtocol +
							ConfMQTTBroker.getString("mqtt.broker.json.host") + ":" + 
							ConfMQTTBroker.getString("mqtt.broker.json.port");
					LOGGER.log(Level.INFO,"Platform MQTT Broker Endpoint " + endpoint);
					LOGGER.log(Level.INFO,"Platform MQTT Broker Endpoint waiting for command: " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setTopic("/"+apikey+"/"+deviceId+ ConfIdas.getString("iota.mqtt.topic.cmd"));
					devicePublishedConfirm.setDataformat(DataformatProtocol.JSON);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);
					
					boolean mqttAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.authentication.enabled"));
					
					if (mqttAuthEnabled) {
						
						String  mqttBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.json.authentication.username");
						String  mqttBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.json.authentication.password");
						
						devicePublishedConfirm.setMqttusername(mqttBrokerJSONAuthUsername);
						devicePublishedConfirm.setMqttpassword(mqttBrokerJSONAuthPassword);
						
					}
					
					
					

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				}
				break;
			}

			}
		}
		// SIGFOX
		else if (transportProtocol.toLowerCase().equalsIgnoreCase("sigfox")){
			LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);
			switch (dataformatProtocol){
			case ("SIGFOX"): { 
				LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);

				switch (retrieveDataMode.toLowerCase()){
				case ("push"): { // PUSH
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FESigfoxPushDevicePublishedConfirmBean devicePublishedConfirm = new FESigfoxPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					//http://217.172.12.177:8085/iot/d?i=sSound1&k=123123
					endpoint = 
							ConfIotaSigfox.getString("iota.sigfox.infopoint.endpoint") +
							ConfIdas.getString("iota.resource") + 
							"?i=" + deviceId + "&k=" + apikey;
					LOGGER.log(Level.INFO,"IOT Agent Endpoint " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setDataformat(DataformatProtocol.SIGFOX);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				
				}
				break;
			}// case SIGFOX
			} // END switch dataformat
		}// END SIGFOX

		// LWM2M over COAP
		else if (transportProtocol.toLowerCase().equalsIgnoreCase("coap")){
			LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);
			switch (dataformatProtocol){
			case ("LWM2M"): { 
				LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);

				switch (retrieveDataMode.toLowerCase()){
				case ("push"): { // PUSH
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					FELwm2mPushDevicePublishedConfirmBean devicePublishedConfirm = new FELwm2mPushDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					endpoint = 
							ConfIotaLWM2M.getString("iota.lwm2m.infopoint.endpoint") +
							ConfIdas.getString("iota.resource") + 
							"?i=" + deviceId + "&k=" + apikey;
					LOGGER.log(Level.INFO,"IOT Agent Endpoint " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					devicePublishedConfirm.setDataformat(DataformatProtocol.LWM2M);
					devicePublishedConfirm.setName(deviceId);
					devicePublishedConfirm.setOrganization(orgs);

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
					break;
				}
				
				}
				break;
			}
			} // END switch dataformat
		}// END LWMTM
		
		
		// OPCUA
		else if (transportProtocol.toLowerCase().equalsIgnoreCase("OPCUA")){
			LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);
			LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);
			LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
			
			FEOpcuaDevicePublishedConfirmBean devicePublishedConfirm = new FEOpcuaDevicePublishedConfirmBean();

			devicePublishedConfirm.setApikey(apikey);
			endpoint = 		ConfIotaOPCUA.getString("iota.opcua.infopoint.endpoint") +
							ConfIdas.getString("iota.resource") + 
							"?i=" + deviceId + "&k=" + apikey;
			LOGGER.log(Level.INFO,"IOT Agent Endpoint " + endpoint);

			devicePublishedConfirm.setEndpoint(endpoint);
			devicePublishedConfirm.setDataformat(DataformatProtocol.OPCUA);
			devicePublishedConfirm.setName(screenId);
			devicePublishedConfirm.setDeviceId(deviceId);
			
			devicePublishedConfirm.setOrganization(orgs);
			devicePublishedConfirm.setServerEndpoint(device.getEndpoint());

			devicePublishedConfirmBean=devicePublishedConfirm;
			LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
				
			}		
		
			// LORA
			else if (transportProtocol.toLowerCase().equalsIgnoreCase("LORA")){
					LOGGER.log(Level.INFO,"Case TRANSPORT PROTOCOL " + transportProtocol);
					LOGGER.log(Level.INFO,"Case DATA FORMAT PROTOCOL " + dataformatProtocol);
					LOGGER.log(Level.INFO,"Case retrieveDataMode " + retrieveDataMode);
					
					FELoraDevicePublishedConfirmBean  devicePublishedConfirm = new FELoraDevicePublishedConfirmBean();

					devicePublishedConfirm.setApikey(apikey);
					endpoint = 		ConfIotaLoRa.getString("iota.lora.infopoint.endpoint") +
							ConfIdas.getString("iota.resource") + 
									"?i=" + deviceId + "&k=" + apikey;
					LOGGER.log(Level.INFO,"IOT Agent Endpoint " + endpoint);

					devicePublishedConfirm.setEndpoint(endpoint);
					
					
					if (dataformatProtocol.equalsIgnoreCase("CAYENNELPP"))
						devicePublishedConfirm.setDataformat(DataformatProtocol.CAYENNELPP);
					else if ((dataformatProtocol.equalsIgnoreCase("CBOR")))
						devicePublishedConfirm.setDataformat(DataformatProtocol.CBOR);
					else if ((dataformatProtocol.equalsIgnoreCase("APPLICATION_SERVER")))
						devicePublishedConfirm.setDataformat(DataformatProtocol.APPLICATION_SERVER);
					
					
					devicePublishedConfirm.setName(screenId);
					devicePublishedConfirm.setDeviceId(deviceId);
					
					devicePublishedConfirm.setOrganization(orgs);
					
					String loraStack = LoraUtils.getLoraStack(device.getStatic_attributes());
					
					devicePublishedConfirm.setLoraStack(loraStack);
					

					devicePublishedConfirmBean=devicePublishedConfirm;
					LOGGER.log(Level.INFO,"Composed bean for transport " + devicePublishedConfirmBean.getTransport() + " | " + " data format " + devicePublishedConfirmBean.getDataformat());
						
			}
			
		return devicePublishedConfirmBean;
	}



	/* Return the FEDevice */
	@SuppressWarnings("null")
	public static FEDevice composeFEDevice( DeviceEntityBean ocbDevice, FEDevice fedevice, String connectedUserId, Set<String> organizationSet, boolean userIsSeller) throws Exception{


		String deviceId = ocbDevice.getId();
		String fiwareservice = fedevice.getCategory().getParentCategory();
		
		fiwareservice = fiwareservice.replace(" ", "_").toLowerCase();//TODO FIl fix 
		
		String fiwareservicepath = fedevice.getCategory().getId();

		fedevice.setProtocol(ocbDevice.getDataformatProtocol().getValue());
		fedevice.setTransport(ocbDevice.getTransportProtocol().getValue());
		
		String streaming_url = ocbDevice.getStreaming_url().getValue();
		
		if(enabledUrlshortener.equals("true") && streaming_url.contains(urlshortenerPattern)) {
			//get stream url decoded value from urlshortener
			try {
					
				streaming_url = UrlshortenerUtils.decodeUrl(streaming_url);
				LOGGER.log(Level.INFO,"Url decoded value: "+ streaming_url);
					
			}catch(Exception e) {
				LOGGER.log(Level.ERROR,e.getMessage());
			}
		}
		fedevice.setStreaming_url(streaming_url);
		
	//	fedevice.setEndpoint(ocbDevice.getEndpoint()); //TODO

		// DEVICE PULL
//		Set<IdasDeviceCommand> commands = idasDevice.getCommands();
//		for(IdasDeviceCommand command : commands){
//			fedevice.addCommand(command);
//		}



		boolean visibleDevice = false;
		//Add static attributes
//		Set<IdasDeviceStaticAttribute> staticattributes = idasDevice.getStatic_attributes();
//		
//		LOGGER.log(Level.INFO,"attributi IDAS size  - " +idasDevice.getStatic_attributes().size());
		
		
		if (!ocbDevice.getLocation().getValue().isEmpty()) {
			String geopoint_ser = ocbDevice.getLocation().getValue();
			String[] coords = geopoint_ser.split(",");
			fedevice.setLatitude(Double.parseDouble(coords[0]));
			fedevice.setLongitude(Double.parseDouble(coords[1]));
		}else {
			fedevice.setLatitude(0.0);
			fedevice.setLongitude(0.0);
		}
		
		
		Set<String> set = new HashSet<>();
		String[] orgs = ocbDevice.getOrganization().getValue().split(",");
		for(String org : orgs){
			set.add(org);
		}
		set.retainAll(organizationSet);
		if (!set.isEmpty()) {
			visibleDevice = true;
			//LOGGER.log(Level.INFO,visibleDevice);
		}
		fedevice.setOrganization(ocbDevice.getOrganization().getValue());
		LOGGER.log(Level.INFO,"DEVICE ORGANIZATION " + ocbDevice.getOrganization().getValue());
		
		String device_owner = ocbDevice.getDeviceOwner().getValue();
		fedevice.setDeviceOwner(device_owner);
		LOGGER.log(Level.INFO,"Device Owner " + device_owner);
		
		if (connectedUserId.equals(device_owner)){
			visibleDevice = true;
		}

		String screenId = ocbDevice.getScreenId().getValue();
		fedevice.setScreenId(screenId);
		LOGGER.log(Level.INFO,"Screen Id " + screenId);

		// push/pull
		String retrieveDataMode =ocbDevice.getRetrieveDataMode().getValue();
		fedevice.setRetrieveDataMode(retrieveDataMode);
		LOGGER.log(Level.INFO,"Retrieve Data Mode " + retrieveDataMode);

		String mobileDevice = ocbDevice.getMobileDevice().getValue();
		fedevice.setMobileDevice(mobileDevice);
		LOGGER.log(Level.INFO,"Mobile device " + mobileDevice);
		/* If the device is mobile, read the location on orion and set it into the fedevice model */
		if (mobileDevice.equalsIgnoreCase("true")){ //TODO Does it work ? The agent accepts only attribute or static attribute, infact it is always 0,0
			//Device Location:11.33017122745514,44.49168482081139
			String geopoint_serM = DeviceConfigManager.getDeviceLocation(fiwareservice, fiwareservicepath, deviceId);
			String[] coordsM = geopoint_serM.split(",");
			fedevice.setLatitude(Double.parseDouble(coordsM[0]));
			fedevice.setLongitude(Double.parseDouble(coordsM[1]));
		}
		
		//Add hubs for DeMa version 2
		if (ocbDevice.getHubName() != null) {
			fedevice.setHubName(ocbDevice.getHubName().getValue());
		}
		if (ocbDevice.getHubType() != null) {
			fedevice.setHubType(ocbDevice.getHubType().getValue());
		}
		if (ocbDevice.getGateway() != null) {
			fedevice.setGateway(ocbDevice.getGateway().getValue());
		}
		if (ocbDevice.getAsset() != null) {
		    fedevice.setAsset(ocbDevice.getAsset().getValue());
		}
		if (ocbDevice.getModbusAddr() != null) {
		    fedevice.setModbusAddr(ocbDevice.getModbusAddr().getValue());
		}
		
		if (userIsSeller){
			visibleDevice = true;
		}

		if (!visibleDevice)	throw new Exception("Device not visible");



		LOGGER.log(Level.INFO, "FEDevice:" + fedevice);
		return fedevice;
	}


	/* Return the FEDevice */
	public static FEDevice composeFEDevice(IdasDevice idasDevice, FEDevice fedevice, String connectedUserId, Set<String> organizationSet, boolean userIsSeller) throws Exception{


		String deviceId = idasDevice.getDevice_id();
		String fiwareservice = fedevice.getCategory().getParentCategory();
		
		fiwareservice = fiwareservice.replace(" ", "_").toLowerCase();//TODO FIl fix 
		
		String fiwareservicepath = fedevice.getCategory().getId();

		fedevice.setProtocol(idasDevice.getProtocol());
		fedevice.setTransport(idasDevice.getTransport());
		String deviceEndpoint = idasDevice.getEndpoint();
		
		if(null!=deviceEndpoint) {
			if(enabledUrlshortener.equals("true") && deviceEndpoint.contains(urlshortenerPattern)) {
				//get stream url decoded value from urlshortener
				try {
							
					deviceEndpoint = UrlshortenerUtils.decodeUrl(deviceEndpoint);
					LOGGER.log(Level.INFO,"Url decoded value: "+ deviceEndpoint);
					fedevice.setEndpoint(deviceEndpoint);		
				}catch(Exception e) {
							LOGGER.log(Level.ERROR,e.getMessage());
				}
			
			}else {
			
				fedevice.setEndpoint(deviceEndpoint);
			
			}
		}	
		// DEVICE PULL
		Set<IdasDeviceCommand> commands = idasDevice.getCommands();
		for(IdasDeviceCommand command : commands){
			fedevice.addCommand(command);
		}



		boolean visibleDevice = false;
		//Add static attributes
		Set<IdasDeviceStaticAttribute> staticattributes = idasDevice.getStatic_attributes();
		
		LOGGER.log(Level.INFO,"attributi IDAS size  - " +idasDevice.getStatic_attributes().size());
		
		for(IdasDeviceStaticAttribute attr : staticattributes){
			LOGGER.log(Level.INFO,"attributi IDAS " + attr.getName() + " - " + attr.getValue());
			//LOGGER.log(Level.INFO,"attributi Connected user " + connectedUserId + " - " + organizationSet);
			if("location".equalsIgnoreCase(attr.getName())){
				String geopoint_ser = attr.getValue();
				String[] coords = geopoint_ser.split(",");
				fedevice.setLatitude(Double.parseDouble(coords[0]));
				fedevice.setLongitude(Double.parseDouble(coords[1]));
			} 
			else if ("organization".equalsIgnoreCase(attr.getName()) ){
				Set<String> set = new HashSet<>();
				String[] orgs = attr.getValue().split(",");
				for(String org : orgs){
					set.add(org);
				}
				set.retainAll(organizationSet);
				if (!set.isEmpty()) {
					visibleDevice = true;
					//LOGGER.log(Level.INFO,visibleDevice);
				}
				fedevice.setOrganization(attr.getValue());
				LOGGER.log(Level.INFO,"DEVICE ORGANIZATION " + attr.getValue());
			}
			else if("device_owner".equalsIgnoreCase(attr.getName())){
				String device_owner = attr.getValue();
				fedevice.setDeviceOwner(attr.getValue());
				LOGGER.log(Level.INFO,"Device Owner " + device_owner);
			} 
			else if ("device_owner".equalsIgnoreCase(attr.getName()) && connectedUserId.equals(attr.getValue())){
				visibleDevice = true;
				//LOGGER.log(Level.INFO,visibleDevice);
			}
			else if("screenId".equalsIgnoreCase(attr.getName())){
				String screenId = attr.getValue();
				fedevice.setScreenId(attr.getValue());
				LOGGER.log(Level.INFO,"Screen Id " + screenId);
			} 
			// push/pull
			else if("retrieve_data_mode".equalsIgnoreCase(attr.getName())){
				String retrieveDataMode = attr.getValue();
				fedevice.setRetrieveDataMode(attr.getValue());
				LOGGER.log(Level.INFO,"Retrieve Data Mode " + retrieveDataMode);
			} 
			else if("auth_method".equalsIgnoreCase(attr.getName())){
				String authMethod = attr.getValue();
				fedevice.setAuthMethod(attr.getValue());
				LOGGER.log(Level.INFO,"Authentication Method " + authMethod);
			}
			else if("auth_user".equalsIgnoreCase(attr.getName())){
				String username = attr.getValue();
				fedevice.setAuthUser(attr.getValue());
				LOGGER.log(Level.INFO,"Authentication Method " + username);
			}
			else if("auth_pwd".equalsIgnoreCase(attr.getName())){
				String password = attr.getValue();
				fedevice.setAuthPwd(attr.getValue());
				LOGGER.log(Level.INFO,"Authentication Method " + password);
			}
			else if("auth_apikey".equalsIgnoreCase(attr.getName())){
				String apikey = attr.getValue();
				fedevice.setAuthApikey(attr.getValue());
				LOGGER.log(Level.INFO,"Authentication Method " + apikey);
			}
			else if("mobile_device".equalsIgnoreCase(attr.getName())){
				String mobileDevice = attr.getValue();
				fedevice.setMobileDevice(attr.getValue());
				LOGGER.log(Level.INFO,"Mobile device " + mobileDevice);
				/* If the device is mobile, read the location on orion and set it into the fedevice model */
				if (mobileDevice.equalsIgnoreCase("true")){
					//Device Location:11.33017122745514,44.49168482081139
					String geopoint_ser = DeviceConfigManager.getDeviceLocation(fiwareservice, fiwareservicepath, deviceId);
					String[] coords = geopoint_ser.split(",");
					fedevice.setLatitude(Double.parseDouble(coords[0]));
					fedevice.setLongitude(Double.parseDouble(coords[1]));
				}
			}else if("dataformat_protocol".equalsIgnoreCase(attr.getName())){
				
				String dataformat_protocol = attr.getValue();
				fedevice.setDataformat_protocol(dataformat_protocol);
				
			}else if("streaming_url".equalsIgnoreCase(attr.getName())){
				
				String streaming_url = attr.getValue();
				
				
					if(enabledUrlshortener.equals("true") && streaming_url.contains(urlshortenerPattern)) {
				
						//get stream url decoded value from urlshortener
						try {
							
							streaming_url = UrlshortenerUtils.decodeUrl(streaming_url);
							LOGGER.log(Level.INFO,"Url decoded value: "+ streaming_url);
							
						}catch(Exception e) {
							LOGGER.log(Level.ERROR,e.getMessage());
						}
					}	
					
					fedevice.setStreaming_url(streaming_url);
					
					
			}else if("historicizing".equalsIgnoreCase(attr.getName())){
				
				String historicizing = attr.getValue();
				fedevice.setHistoricizing(Boolean.valueOf(historicizing)); 
			
			// Add attributes for kapua devices in DeMa version 2
			} else if ("hub_name".equalsIgnoreCase(attr.getName())) {
				fedevice.setHubName(attr.getValue());
			} else if ("hub_type".equalsIgnoreCase(attr.getName())) {
				fedevice.setHubType(attr.getValue());
			} else if ("gateway".equalsIgnoreCase(attr.getName())) {
				fedevice.setGateway(attr.getValue());
			} else if ("asset".equalsIgnoreCase(attr.getName())) {
                fedevice.setAsset(attr.getValue());
			} else if ("modbus_addr".equalsIgnoreCase(attr.getName())) {
                fedevice.setModbusAddr(attr.getValue());
                
			}else if (userIsSeller){
				visibleDevice = true;
			}


		}

		if (!visibleDevice)	throw new Exception("Device not visible");

		//Add attributes
		Set<IdasDeviceAttribute> attrs = idasDevice.getAttributes();
		for(IdasDeviceAttribute attr : attrs){
			fedevice.addAttribute(attr);
		}



		LOGGER.log(Level.INFO, "FEDevice:" + fedevice);
		return fedevice;
	}


	

	/* Get device infopoint */
	public static Set<BasicNameValuePair> getDeviceInfopoint(String headerService, String headerServicePath, String deviceId){

		Set<BasicNameValuePair> deviceInfopoint = new HashSet<BasicNameValuePair>();
		String dataformatProtocol = "";
		String transportProtocol = "";

		try{
			// STEP 1 - Get the entity corresponding to the notificationPayload for listeners
			Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "Device"));
			params.add(new BasicNameValuePair("id", deviceId));

			String resp =  orion.getEntities(headerService, headerServicePath, params);
			LOGGER.log(Level.INFO, "resp: " + resp);

			String payload = resp;
			
			JSONArray entity = new JSONArray(payload);
			JSONObject subscritpionData = new JSONObject();
			subscritpionData.put("data", entity);
			subscritpionData.put("subscriptionId", "5dce824e9ff4cfd7d079dfd1");//fake subscriptionId
			deviceInfopoint.add(new BasicNameValuePair("payload", subscritpionData.toString()));

			// STEP 2 - Get Device Confirmation Data
			Type type = new TypeToken<Set<DeviceEntityBean>>(){}.getType();
			Set<DeviceEntityBean> devs = new Gson().fromJson(resp, type);
			for (DeviceEntityBean dev:devs){
				dataformatProtocol = dev.getDataformatProtocol().getValue();
				transportProtocol = dev.getTransportProtocol().getValue();
			}
			
			
			// SWITCH AGENT // 
			Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
			IdasDevice idasDevice = idas.getDevice(headerService, headerServicePath, deviceId);
			FEDevicePublishedConfirmBean devConfirmData = getDeviceConfirmantionData (transportProtocol, dataformatProtocol, idasDevice, headerService, headerServicePath);

			deviceInfopoint.add(new BasicNameValuePair("deviceId", deviceId));
			deviceInfopoint.add(new BasicNameValuePair("transportProtocol", transportProtocol));
			deviceInfopoint.add(new BasicNameValuePair("dataformatProtocol", dataformatProtocol));
			
			String pushPull = devConfirmData.getRetrieveDataMode().toString();
			deviceInfopoint.add(new BasicNameValuePair("RetrieveDataMode", pushPull));
			
			
			if (transportProtocol.equals (TransportProtocol.HTTP.toString())  ) {
				
				FEHttpDevicePublishedConfirmBean devConfirmDataHttp  = (FEHttpDevicePublishedConfirmBean ) devConfirmData;
				deviceInfopoint.add(new BasicNameValuePair("measuresEndpoint", devConfirmDataHttp.getMeasuresEndpoint()  ));
				
			}
				
			
			
			if (pushPull.equalsIgnoreCase(RetrieveDataMode.PULL.toString())) {
				
				String authMethod = CommonUtils.getValueByJsonArrayNGSI(entity, "auth_method");
				
				if (authMethod.equalsIgnoreCase("basic")) {
					
					String authUsername = CommonUtils.getValueByJsonArrayNGSI(entity, "auth_user");
					String authPassword = CommonUtils.getValueByJsonArrayNGSI(entity, "auth_pwd");
					
					deviceInfopoint.add(new BasicNameValuePair("authUsername", authUsername));
					deviceInfopoint.add(new BasicNameValuePair("authPassword", authPassword));
					
				}else if (authMethod.equalsIgnoreCase("apikey")) {
					
					String authApikey = CommonUtils.getValueByJsonArrayNGSI(entity, "auth_apikey");
					deviceInfopoint.add(new BasicNameValuePair("authApikey", authApikey));
					
				}
				
				
			}
			
			

			
			

			deviceInfopoint.add(new BasicNameValuePair("fiware-service", headerService));
			deviceInfopoint.add(new BasicNameValuePair("fiware-servicepath", headerServicePath));
			
			
			deviceInfopoint.add(new BasicNameValuePair("liveDataEndpoint", orionPublicHost+"/"+deviceId));
			deviceInfopoint.add(new BasicNameValuePair("liveDataEndpointDoc", orionFilteringDocumentation));
			
			
			
			String serviceName = ServiceConfigManager.getServiceNameByServiceTM(headerService); // from Tenant Manager
			String servicePathName = ServiceConfigManager.getServicePathNameTM(headerServicePath, headerService); // from Tenant Manager
			
			
			deviceInfopoint.add(new BasicNameValuePair("context", serviceName ));
			deviceInfopoint.add(new BasicNameValuePair("category", servicePathName ));
			
			String serviceApikey = devConfirmData.getApikey();
			
			 if (! (transportProtocol.equals("LORA") || transportProtocol.equals("OPCUA"))) {
			
				
//				deviceInfopoint.add(new BasicNameValuePair("apikey", serviceApikey));
	
				String endpoint = devConfirmData.getEndpoint();
				deviceInfopoint.add(new BasicNameValuePair("endpoint", endpoint));
			 }
			
			
			if (transportProtocol.equals("OPCUA")) {
				
				String serverEndpoint = idasDevice.getEndpoint();
				deviceInfopoint.add(new BasicNameValuePair("Server Endpoint", serverEndpoint));
				
			}else if (transportProtocol.equals("LORA")) {
				
				String loraStack = LoraUtils.getLoraStack(idasDevice.getStatic_attributes());
				deviceInfopoint.add(new BasicNameValuePair("Lora network",loraStack ));
				
				String devEui = LoraUtils.getDevEui(idasDevice);
						
				deviceInfopoint.add(new BasicNameValuePair("DevEUI",devEui ));
			}else if (transportProtocol.equals("MQTT")) {
				
				
				String topic = getMqttPublishMeasureTopic( serviceApikey,  deviceId );
				
				
				deviceInfopoint.add(new BasicNameValuePair("mqttPublishMeasureTopic", topic));
				
				
				if (pushPull.equalsIgnoreCase(RetrieveDataMode.PULL.toString())) {
					
					String topicSendCommand = getMqttSendCommandTopic(serviceApikey, deviceId, pushPull);
					deviceInfopoint.add(new BasicNameValuePair("mqttSendCommandTopic", topicSendCommand));
					
				}
				
				
				
				
				
				
				
				
				if (dataformatProtocol.equalsIgnoreCase(DataformatProtocol.JSON.toString())) {
					
					//Authentication
					boolean  MQTTBrokerAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.authentication.enabled"));
					if (MQTTBrokerAuthEnabled) {
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerUsername", ConfMQTTBroker.getString("mqtt.broker.json.authentication.username")));
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerPassword", ConfMQTTBroker.getString("mqtt.broker.json.authentication.password") ));
					}
					
					//SSL
					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));
					
					if (mqttBrokerSslEnabled)
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerSSLCertificate",ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.cert.download-link") ));
					
					
				}else if (dataformatProtocol.equalsIgnoreCase(DataformatProtocol.UL2_0.toString())){
					
					//Authentication
					boolean  MQTTBrokerAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.enabled"));
					if (MQTTBrokerAuthEnabled) {
						
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerUsername", ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.username")));
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerPassword", ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.password") ));
					}
					
					//SSL
					boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));
					if (mqttBrokerSslEnabled)
						deviceInfopoint.add(new BasicNameValuePair("MQTTBrokerSSLCertificate",ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.cert.download-link") ));
					
				}//mqtt dataformatProtocol
				
				
			}//transport protocol 
			
	

		}
		catch(Exception ex){
			
			LOGGER.log(Level.ERROR,"Device not present in case of dataformat protocol " + dataformatProtocol);
			LOGGER.log(Level.ERROR,"getDeviceInfopoint  Error " + ex.getMessage());
		}


		return deviceInfopoint;
	}


	/* Get device command infopoint */
	public static Set<BasicNameValuePair> getDeviceCmdInfopoint(String headerService, String headerServicePath, String deviceId, String dataformatProtocol){

		Set<BasicNameValuePair> deviceCmdInfopoint = new HashSet<BasicNameValuePair>();

		try{
			
			deviceCmdInfopoint.add(new BasicNameValuePair("deviceId", deviceId));

			// SWITCH AGENT and get device on IOTAgent
			Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
			IdasDevice idasDevice = idas.getDevice(headerService, headerServicePath, deviceId);
			Set<IdasDeviceCommand> commands = idasDevice.getCommands();
			
			JSONArray cmds = new JSONArray();
			for(IdasDeviceCommand cmd : commands){
				cmds.put(cmd.getName());
			}
			deviceCmdInfopoint.add(new BasicNameValuePair("commands", cmds.toString()));

		}
		catch(Exception ex){
			LOGGER.log(Level.ERROR, ex.getMessage());
			LOGGER.log(Level.ERROR,"Device not present in case of dataformat protocol " + dataformatProtocol);
		}

		return deviceCmdInfopoint;
	}


	/* Platform Sends command to Device */
	public static String sendCommandToDevice(String headerService, String headerServicePath, String deviceId, String commandName, String commandMessage, String dataformatProtocol, String transportProtocol, String retrieveDataMode){

		LOGGER.log(Level.INFO,"Received command message " + commandMessage);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", headerService);
		headers.put("fiware-servicepath", "/" + headerServicePath);

		String result = "200 OK";

		try{
			// STEP 1 - Get the device entity from orion
			Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
			params.add(new BasicNameValuePair("type", "Device"));
			params.add(new BasicNameValuePair("id", deviceId));

			// CHOOSE WHAT GETAPIKEY YOU WANT TO USE:
			// Get apikey for the specific organization
			//String apikey = ServiceConfigManager.getApikey(orgs);
			// otherwise get apikey from cedus configuration
			//String serviceApikey = ServiceConfigManager.getApikey(headerService, headerServicePath);
			String serviceApikey = ServiceConfigManager.getServicePathApiKeyTM(headerService, headerServicePath);
			
			if (retrieveDataMode.equalsIgnoreCase("pull")){
				switch (transportProtocol.toLowerCase()) {
				case "http":{
					try {
						String orionEndpointV1 = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v1.updateContext") ;
						String payload = "{\"contextElements\": [ {\"type\": \"Device\","+
								"\"isPattern\": \"false\", " +
								" \"id\": \"" + deviceId + "\","+
								"\"attributes\": [ {"  +
								"\"name\": \"" + commandName + "\"," +
								"\"type\": \"command\"," + 
								// deviceid:annapull02|apikey:GmJszisVlK83sGR|dataformatprotocol:UL2_0|servicepath:malaga_1_parking|cmdmessage:ciao
								"\"value\": \"";
						if (ConfMQTTSimulator.getString("mqtt.simulator.isRunning").equalsIgnoreCase("true")){
							payload = payload +
									"deviceid:" + deviceId + 
									"|apikey:" + serviceApikey + 
									"|dataformatprotocol:" + dataformatProtocol + 
									"|servicepath:" + headerServicePath + 
									"|cmdmessage:"
									;
						}
						payload = payload + commandMessage +
								"\"" +
								"}]"	+
								"}]," + 
								"\"updateAction\": \"UPDATE\"" +
								"}" ;
						LOGGER.log(Level.INFO,"# Platform is sending command to device...");
						result = RestUtils.consumePost(orionEndpointV1, payload, MediaType.APPLICATION_JSON_TYPE, headers);
						LOGGER.log(Level.INFO, "# Command successully sent to device id [" + deviceId + "]");
					}catch (Exception e) {
						LOGGER.log(Level.ERROR, e.getMessage());
						LOGGER.log(Level.ERROR, "# Device id [" + deviceId + "] not available");
					}
					break;
				}
				case "mqtt":{
					
						
						if (dataformatProtocol.equalsIgnoreCase(DataformatProtocol.JSON.toString())){
							// JSON/MQTT PROTOCOL
							try{
								/*
								 * Publish measure on MQTT Broker
										mosquitto_pub -h 127.0.0.1 -p 8099 -t '/apikey/deviceid/attrs' -m '{"status" : "free"}'
								 */
								
								boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));
								
								String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
								if (mqttBrokerSslEnabled)
									mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
								
								
								String MQTTBrokerHost = ConfMQTTBroker.getString("mqtt.broker.json.private.host");
								String MQTTBrokerJSONPort = ConfMQTTBroker.getString("mqtt.broker.json.private.port=");
								boolean  MQTTBrokerJSONAuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.authentication.enabled"));
								String  MQTTBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.json.authentication.username");
								String  MQTTBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.json.authentication.password");
								
								String mqttBrokerSslCaFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.caFilePath");
								String mqttBrokerSslClientCrtFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.clientCrtFilePath");
								String mqttBrokerSslClientKeyFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.clientKeyFilePath");
								
								
								String MQTTBrokerJSON = mqttBrokerProtocol + MQTTBrokerHost + ":" + MQTTBrokerJSONPort ;
								LOGGER.info("# MQTTBroker configuration: " + MQTTBrokerJSON);

								JSONObject payload = new JSONObject();
								payload.put(commandName, commandMessage);
								
								// String payload = "{\"" + commandName+"\":\""+commandMessage + "\"" + "}";
									
									LOGGER.info("# payload: " + payload);
									try{
										// PUBLISH MEASURE ON MOSQUITTO BROKER
										String broker =  MQTTBrokerJSON;
										String topic = "/" + serviceApikey + "/" + deviceId + ConfIdas.getString("iota.mqtt.topic.cmd");
										//String content = payload; 
										LOGGER.info("# topic: " + topic);
										LOGGER.info("# DEVICE ID [" + deviceId + "] IS Publishing MEASURE...");
										String resultCode = MqttUtils.mqttPublish(broker, payload.toString(), topic, MQTTBrokerJSONAuthEnabled, MQTTBrokerJSONAuthUsername, MQTTBrokerJSONAuthPassword,
												mqttBrokerSslEnabled, mqttBrokerSslCaFilePath, mqttBrokerSslClientCrtFilePath, mqttBrokerSslClientKeyFilePath);
										LOGGER.info("# send command Done!");
										
									}catch (Exception e) {

										LOGGER.log(Level.ERROR, e.getMessage());
										LOGGER.log(Level.INFO, "# Device id [" + deviceId + "] not available");
									}
							
							}catch (Exception e) {
								LOGGER.log(Level.ERROR, e.getMessage());
							}
						
					break;
						
					/*
					
					try {
						String mqttDeviceSimEndpoint = ConfMQTTSimulator.getString("mqtt.simulator.protocol") + 
								ConfMQTTSimulator.getString("mqtt.simulator.host") +
								":" + ConfMQTTSimulator.getString("mqtt.simulator.port") +
								ConfMQTTSimulator.getString("mqtt.simulator.resource");

						String payloadDevice = "";
						if (ConfMQTTSimulator.getString("mqtt.simulator.isRunning").equalsIgnoreCase("true")){
							LOGGER.log(Level.INFO,"### Device identity request for MQTTDevice Simulator");
							LOGGER.log(Level.INFO,"# MQTT device simulator endpoint: " + mqttDeviceSimEndpoint);
							payloadDevice = 
									"deviceId:" + deviceId + 
									"|apikey:" + serviceApikey + 
									"|dataformatprotocol:" + dataformatProtocol + 
									"|cmdmessage:" ; 
						}
						payloadDevice = payloadDevice + commandMessage;
						try{
							RestUtils.consumePost(mqttDeviceSimEndpoint, payloadDevice, MediaType.TEXT_PLAIN_TYPE, headers);
						}catch (Exception e) {
							if (ConfMQTTSimulator.getString("mqtt.simulator.isRunning").equalsIgnoreCase("true")){
								LOGGER.log(Level.INFO,"### Sending identity request to MQTTDevice Simulator...");
								LOGGER.log(Level.SEVERE, e.getMessage());
								LOGGER.log(Level.INFO, "### CAUSE: ");
								LOGGER.log(Level.INFO, "# 1 # MQTTDevice Simulator is NOT up and running");
								LOGGER.log(Level.INFO, "# 2 # Device " + deviceId + " is NOT identified to the MQTTDevice Simulator.");
								result = "500";
								return result;
							}
						}

						String orionEndpointV1 = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v1.updateContext") ;
						LOGGER.log(Level.FINE,"# Context broker endpoint: " + orionEndpointV1);
						String payload = "{\"contextElements\": [ {\"type\": \"Device\","+
								"\"isPattern\": \"false\", " +
								" \"id\": \"" + deviceId + "\","+
								"\"attributes\": [ {"  +
								"\"name\": \"" + commandName + "\"," +
								"\"type\": \"command\"," + 
								// deviceid:mqtt-pulljson|apikey:GmJszisVlK83sGR|dataformatprotocol:JSON|servicepath:malaga_1_parking|cmdmessage:message"
								"\"value\": \"";
						if (ConfMQTTSimulator.getString("mqtt.simulator.isRunning").equalsIgnoreCase("true")){
							payload = payload +
									"deviceid:" + deviceId + 
									"|apikey:" + serviceApikey + 
									"|dataformatprotocol:" + dataformatProtocol + 
									"|servicepath:" + headerServicePath + 
									"|cmdmessage:";	
						}

						payload = payload + commandMessage +
								"\"" +
								"}]"	+
								"}]," + 
								"\"updateAction\": \"UPDATE\"" +
								"}" ;
						RestUtils.consumePost(orionEndpointV1, payload, MediaType.APPLICATION_JSON_TYPE, headers);
						LOGGER.log(Level.INFO,"### Platform has sent command to device " + deviceId);
					}catch (Exception e) {
						LOGGER.log(Level.SEVERE, e.getMessage());
						LOGGER.log(Level.INFO, "# Device id [" + deviceId + "] not available");
					}
					break;
					
					
				*/	
					
					
				}else {//UL20
					
					
					try{
						
						boolean mqttBrokerSslEnabled = Boolean.parseBoolean(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));
						
						String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
						if (mqttBrokerSslEnabled)
							mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						String MQTTBrokerHost = ConfMQTTBroker.getString("mqtt.broker.ul20.private.host");
						String MQTTBrokerUL20Port = ConfMQTTBroker.getString("mqtt.broker.ul20.private.port");
						boolean  MQTTBrokerUL20AuthEnabled = Boolean.parseBoolean(ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.enabled"));
						String  MQTTBrokerUL20AuthUsername = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.username");
						String  MQTTBrokerUL20AuthPassword = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.password");
						
						String mqttBrokerSslCaFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.caFilePath");
						String mqttBrokerSslClientCrtFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.clientCrtFilePath");
						String mqttBrokerSslClientKeyFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.clientKeyFilePath");
						
						String MQTTBrokerUL20 = mqttBrokerProtocol + MQTTBrokerHost + ":" + MQTTBrokerUL20Port ;
						LOGGER.log(Level.INFO, "# MQTTBroker configuration: " + MQTTBrokerUL20);
						
						
						String 	payload =  commandName+"|"+commandMessage;
								
						LOGGER.info("# payload: " + payload);
						try{
									// PUBLISH MEASURE ON MOSQUITTO BROKER
									String broker =  MQTTBrokerUL20;
									String topic = "/" + serviceApikey + "/" + deviceId + "/cmd";
									String content = payload; 
									LOGGER.info("# topic: " + topic);
									LOGGER.info("# DEVICE ID [" + deviceId + "] IS Publishing MEASURE...");
									String resultCode = MqttUtils.mqttPublish(broker, content, topic, MQTTBrokerUL20AuthEnabled, MQTTBrokerUL20AuthUsername, MQTTBrokerUL20AuthPassword ,
											mqttBrokerSslEnabled, mqttBrokerSslCaFilePath, mqttBrokerSslClientCrtFilePath, mqttBrokerSslClientKeyFilePath);
									LOGGER.info("# Done!");
						}catch (Exception e) {
									LOGGER.log(Level.ERROR, e.getMessage());
									LOGGER.log(Level.INFO, "# Device id [" + deviceId + "] not available");
						}
					
					
					
					}catch (Exception e) {
						LOGGER.log(Level.ERROR, e.getMessage());
					}
					
				}
						
				}
				default:
					LOGGER.log(Level.ERROR, "Unsupported Transport Protocol");
					break;
				}
			}

		}
		catch(Exception ex){
			LOGGER.log(Level.ERROR, "Error sending command" + commandName + " to device " + deviceId);
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return result;
	}



	/** 
	 * Register the Input File String as IDAS device 
	 * The input string contains one or more device 
	 */
	public static JSONObject registerInputFileStringAsIdasDevices(String service, String servicepath, String userid, String organizationid, String inputFileString) {

		JSONObject jResp = null;

		JSONArray devicesJArr = new JSONArray(inputFileString);
		LOGGER.log(Level.INFO,"### number of device in input: " + devicesJArr.length());

		for (int i = 0; i < devicesJArr.length(); i++) {
			Set<IdasDevice> devices =  new HashSet<IdasDevice>();
			IdasDeviceList devlist = new IdasDeviceList();

			IdasDevice newdevice = null;
			String inputDevice = devicesJArr.getJSONObject(i).toString();
			newdevice = new Gson().fromJson(inputDevice, IdasDevice.class);
			// set default elements
			newdevice.setEntity_name(newdevice.getDevice_id());
			newdevice.setEntity_type("Device");

			String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
			IdasDeviceStaticAttribute dateModifiedAttr = new IdasDeviceStaticAttribute("dateModified", "DateTime", dateModified);
			newdevice.getStatic_attributes().add(dateModifiedAttr);

			IdasDeviceStaticAttribute screenIdAttr = new IdasDeviceStaticAttribute("screenId", "Text", newdevice.getDevice_id());
			newdevice.getStatic_attributes().add(screenIdAttr);

			IdasDeviceStaticAttribute dataformatProtocolAttr = new IdasDeviceStaticAttribute("dataformat_protocol", "Text", newdevice.getProtocol());
			newdevice.getStatic_attributes().add(dataformatProtocolAttr);

			IdasDeviceStaticAttribute transportProtocolAttr = new IdasDeviceStaticAttribute("transport_protocol", "Text", newdevice.getTransport());
			newdevice.getStatic_attributes().add(transportProtocolAttr);

			IdasDeviceStaticAttribute organizationAttr = new IdasDeviceStaticAttribute("organization", "Text", organizationid);
			newdevice.getStatic_attributes().add(organizationAttr);

			IdasDeviceStaticAttribute deviceownerAttr = new IdasDeviceStaticAttribute("device_owner", "Text", userid);
			newdevice.getStatic_attributes().add(deviceownerAttr);
			
			IdasDeviceStaticAttribute opTypeAttr = new IdasDeviceStaticAttribute("opType", "Text", "ready");
			newdevice.getStatic_attributes().add(opTypeAttr);

			devices.add(newdevice);
			devlist.setDevices(devices);

			// SWITCH AGENT
			String dataformatProtocol = newdevice.getProtocol().trim();
			LOGGER.log(Level.INFO,"### dataformatProtocol: " + dataformatProtocol);

			try {
				Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
				jResp = idas.registerDevice(service, servicepath, devlist);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
			}
		}
		return jResp;
	}

	
	/**
	 * @param protocol
	 * @return
	 */
	public static boolean isOpcua(String protocol) {
		
		try {
			
			if (protocol.equalsIgnoreCase("OPCUA"))
				return true;
				
		}catch(Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return false;
		}
		return false;
	}
	

	/**
	 * @param fedevice
	 * @return
	 */
	public static boolean isOpcua(FEDevice fedevice) {
		
		try {
			String protocol = fedevice.getProtocol();
			
			return isOpcua( protocol);
				
		}catch(Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return false;
		}
	}
	
	
	/**
	 * @param fedevice
	 * @return
	 */
	public static boolean isLora(FEDevice fedevice) {
		
		try {
			String protocol = fedevice.getProtocol();
			return isLora( protocol);
			
		}catch(Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return false;
		}
		
	}	
	
	/**
	 * @param protocol
	 * @return
	 */
	public static boolean isLora(String protocol) {
		
		try {
			
			if (protocol.equalsIgnoreCase("CAYENNELPP") || protocol.equalsIgnoreCase("CBOR") || protocol.equalsIgnoreCase("APPLICATION_SERVER")) 
				return true;
			
		}catch(Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return false;
		}
		
		
		return false;
	}
	
	
	
	

	/**
	 * @param s
	 * @return
	 */
	public static String fixDevicePrefix (String s) {
		
		if (hasDevicePrefix(s))
			return s;
		else
			return (DEVICE_PREFIX+s);
	}
	
	

	/**
	 * @param s
	 * @return
	 */
	public static boolean hasDevicePrefix (String s){
		
		if (s.length()<7)
		return false;
			
		String sub = s.substring(0,7);
		if (sub.equals(DEVICE_PREFIX))
			return true;
		else
			return false;
		
	}
	
	public static IdasDevice setOpcuaDeviceFields (IdasDevice device) {
		
		Set<IdasDeviceAttribute> opAttrs = device.getAttributes();
		for (IdasDeviceAttribute opAttr:opAttrs) {
			opAttr.setObject_id("ns=1;s="+device.getDevice_id()+"_"+opAttr.getName());
		}
		
		String deviceIdFixed = fixDevicePrefix(device.getDevice_id());
		device.setDevice_id(deviceIdFixed);
		
		String entityNameFixed = fixDevicePrefix(device.getEntity_name());
		device.setEntity_name(entityNameFixed);
		
		return device;
		
		
	}
	
	/**
	 * @param device
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @return
	 */
	public static IdasDevice setInternalAttributeLoraAndPublishOnAppServer(IdasDevice device, String fiwareservice, String fiwareservicepath, boolean isUpdate, IdasDevice oldDevice) {
		
		/*
		 "internal_attributes": {
               "lorawan": {
                   "application_server": {
                       "host": "147.27.60.184:8083",
                       "provider": "TTN"
                   },
                   "dev_eui": "e62d032c07d15301",
                   "app_eui": "0000000000000000",
                   "application_id": "6",
                   "application_key": "444B8EF16415B5F6ED777EAFE695C49",
                   "data_model": "cayennelpp"
               }
           }
		*/
		
		
		
		String loraStack = LoraUtils.getLoraStack(device.getStatic_attributes());
		
		
		String appName = "";
		String appId = "";
		String devEUI = "";
		
		if (loraStack.equalsIgnoreCase("LORASERVER")) {
		
		
			String jwt = LoraUtils.getJWTSingleton();
			String deviceName = device.getDevice_id();
			String deviceProfileID = device.getLoraDeviceProfileId();
			devEUI = LoraUtils.getDevEui(device);
			//devEUI = LoraUtils.getRandomDevEui();
			String appKey = LoraUtils.getAppKey(device);
			
			if(!isUpdate) {
				
				appName = ServiceConfigManager.getServicePathApiKeyTM(fiwareservice, fiwareservicepath);
				
				
				appId = LoraUtils.getApplicationId(appName, jwt);
				if (appId.equals("")) {
					LoraUtils.createApplication(appName, fiwareservicepath, jwt);
					appId = LoraUtils.getApplicationId(appName, jwt);
				}
				LoraUtils.createDeviceOnServer( appId,  deviceName,  devEUI,  deviceProfileID,  jwt );
				
				LoraUtils.setApplicationKeyOnServer( appKey, devEUI, jwt, false);
			
			} else {
				
				appId = LoraUtils.getAppId(oldDevice);
				appName = LoraUtils.getAppName(oldDevice);
				LoraUtils.updateDeviceOnServer(appId, deviceName, devEUI, deviceProfileID, jwt);
				
				String appKeyOld = LoraUtils.getAppKey(oldDevice);
				
				if (!appKey.equals(appKeyOld))				
					LoraUtils.setApplicationKeyOnServer( appKey, devEUI, jwt, true);
				
			}
		
			IdasDeviceStaticAttribute loraDeviceProfileId  = new IdasDeviceStaticAttribute("loraDeviceProfileId", "Text", deviceProfileID);
			device.getStatic_attributes().add(loraDeviceProfileId);
			
			
			LorawanAttribute lorawanAttrs = new LorawanAttribute();
				lorawanAttrs.setDev_eui(devEUI);
				lorawanAttrs.setApp_eui(loraAppEuiDefault);
				lorawanAttrs.setApplication_id(appId);
				lorawanAttrs.setApplication_key(appKey); 
		
			String datamodel = LoraUtils.getDatamodelFromProtocol(device.getProtocol());
				lorawanAttrs.setData_model(datamodel);
		
			ApplicationServerAttribute appServerAttrs = new ApplicationServerAttribute();
				appServerAttrs.setHost(loraMqttEndpoint);
				appServerAttrs.setProvider(loraServerProvider);
				lorawanAttrs.setApplication_server(appServerAttrs);
		
			IdasDeviceInternalAttributeLoRa attrLora = new IdasDeviceInternalAttributeLoRa();
				attrLora.setLorawan(lorawanAttrs);
				device.setInternal_attributes(attrLora);
			
			
		
		}else if (loraStack.equalsIgnoreCase("TTN")) {
			
			
			 Object iAttrOb = device.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attrLora = LoraUtils.getLoraInternalAttribute(iAttrOb);
			
			 attrLora.getLorawan().getApplication_server().setHost(loraTtnHost);
			 attrLora.getLorawan().getApplication_server().setProvider(loraTTNProvider);
			 attrLora.getLorawan().getApplication_server().setUsername(attrLora.getLorawan().getApplication_id());//username is equal to appId
			 
			 device.setInternal_attributes(attrLora);
			 
		}
		
		
		return device;
	}
	
	
	
	
	
	
	public static Set<DeviceEntityBean> getOCBDevices (String fiwareservice, String fiwareservicepath){
	
		//http://{{OrionHost}}/v2/entities/?limit=1000&type=Device&attrs=dataformat_protocol,device_owner,location,organization,screenId,streaming_url&q=opType!=deleted
		
		
		Set<DeviceEntityBean> devices = new HashSet<DeviceEntityBean>();
		int offset = 0;
		String orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities") ;
		
		String queryString = "?limit=1000&type=Device&attrs=dataformat_protocol,device_owner,location,organization,screenId,streaming_url,transport_protocol,retrieve_data_mode,hub_name,hub_type,gateway,asset,modbus_addr&q=opType!=deleted&orderBy=id";
		String endpointQuery = orionEndpoint + queryString;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", "/" + fiwareservicepath);
		String serviceResponse="";
		try {
			serviceResponse = RestUtils.consumeGet(endpointQuery,headers);
		
			Type type = new TypeToken<Set<DeviceEntityBean>>(){}.getType();
			devices = new Gson().fromJson(serviceResponse, type);
		
			Set<DeviceEntityBean> devicesOffset = new HashSet<DeviceEntityBean>(devices);	
			
			//Orion API returns max 1000 entities
			while(devicesOffset.size()==1000) {
				offset +=1000;
				 queryString = "?offset="+offset+"&limit=1000&type=Device&attrs=dataformat_protocol,device_owner,location,organization,screenId,streaming_url,transport_protocol,retrieve_data_mode,hub_name,hub_type,gateway,asset,modbus_addr&q=opType!=deleted&orderBy=id";
				 endpointQuery = orionEndpoint + queryString;
				 serviceResponse = RestUtils.consumeGet(endpointQuery,headers);
				 devicesOffset = new Gson().fromJson(serviceResponse, type);
				 devices.addAll(devicesOffset);
			}
		 
		
		
//		Set<DeviceEntityBean> devicesToInclude = new HashSet<>();
//		for(DeviceEntityBean dev:devices){
//		//	if (!dev.getId().contains("Device:") || dev.getDataformatProtocol().equals("OPCUA")){
//				devicesToInclude.add(dev);
//		//	}
//		}
//		devices.retainAll(devicesToInclude);

		} catch (Exception e) {
			e.printStackTrace();
			return devices;
		}
		
		return devices;
		
	}
	
	
//	static private ExecutorService service = Executors.newCachedThreadPool();
//	static public ExecutorService service = Executors.newFixedThreadPool(3);
	
	
	static private ExecutorService service =  Executors.newFixedThreadPool(3,
		    new ThreadFactory() {
		        public Thread newThread(Runnable runnable) {
		            Thread thread = Executors.defaultThreadFactory().newThread(runnable);
		            thread.setName("asyncGetOCBDevices");
		            thread.setDaemon(true);
		            return thread;
		        }
		    });
	
	
	

	static public CompletableFuture<Set<DeviceEntityBean>> asyncGetOCBDevices(String fiwareservice, String fiwareservicepath){
	    
		return CompletableFuture.supplyAsync(() -> getOCBDevices ( fiwareservice,  fiwareservicepath), service);
	    
	   
	    
	}
	
	

	/**
	 * @param dev
	 * @return
	 */
	public static String getFiwareServiceByFEDevice (FEDevice dev) {
		
		String ret ="";
		
		if ( null != dev.getCategory().getParentCategory() )
			ret = dev.getCategory().getParentCategory().toLowerCase().replace(" ", "_");
		
		return ret;
	}
	
	/**
	 * @param dev
	 * @return
	 */
	public static String getFiwareServicepathByFEDevice (FEDevice dev) {
		
		String ret ="";
		
		if ( null != dev.getCategory().getId() )
			ret = dev.getCategory().getId();
		
		return ret;
	}
	

	
	/**
	 * @param fiwareService
	 * @return
	 */
	public static MapCenter getMapCenter (String fiwareService) {
	
		MapCenter mapcenter = new MapCenter();
		
		
		String resp =  TenantManager.getScopeById(fiwareService);
		
		Type type = new TypeToken<ScopeDTO>(){}.getType();
		ScopeDTO scope = new Gson().fromJson(resp, type);
		
		
		mapcenter.setLat(Double.parseDouble(scope.getScope().getLat()));
		mapcenter.setLng(Double.parseDouble(scope.getScope().getLng()));
		mapcenter.setZoom(Integer.parseInt(scope.getScope().getZoom()));
		
		
		return mapcenter;
	}
	
	
	
	
	
	
	/**
	 * @param apiKey
	 * @param deviceId
	 * @param isPush
	 * @return
	 */
	public static String getMqttSendCommandTopic(String apiKey, String deviceId, String pushPull ) {
		
		String topic = "/"+apiKey+"/"+deviceId;
		
		if (pushPull.equalsIgnoreCase(RetrieveDataMode.PULL.toString()))
			topic+=ConfIdas.getString("iota.mqtt.topic.cmd");
		
		return topic;
		
	}
	
	public static String getMqttPublishMeasureTopic(String apiKey, String deviceId ) {
		
		String topic = "/"+apiKey+"/"+deviceId;
		topic+=ConfIdas.getString("iota.mqtt.topic.attrs");
		
		return topic;
		
	}
	
	
	
	
	
}
