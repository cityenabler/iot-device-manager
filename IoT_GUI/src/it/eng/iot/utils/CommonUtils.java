package it.eng.iot.utils;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.eng.iot.configuration.ConfIotaJSON;
import it.eng.iot.configuration.ConfIotaLWM2M;
import it.eng.iot.configuration.ConfIotaLoRa;
import it.eng.iot.configuration.ConfIotaOPCUA;
import it.eng.iot.configuration.ConfIotaSigfox;
import it.eng.iot.configuration.ConfIotaUL20;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.servlet.model.Permission;
import it.eng.tools.keycloak.User;

public abstract class CommonUtils {
	
	private static final Logger LOGGER = LogManager.getLogger(CommonUtils.class);

	public static boolean isValidURL(String urlString){
	    boolean out = false;
		try{
	        URL url = new URL(urlString);
	        url.toURI();
	        out = true;
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on isValidURL method "+ exception.getMessage());
	        out = false;
	    }
		
		return out;
	}
	
	/**
	 * @param fiwareservicepath
	 * @return
	 */
	public static String normalizeServicePath(String fiwareservicepath) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		return servicepath;
		
	}
	
	/**
	 * @param fiwareservicepath
	 * @return
	 */
	public static String cleanServicePath(String fiwareservicepath) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath.substring(1) : fiwareservicepath;
		return servicepath;
		
	}
	
	
	public static boolean isPermittedAction(String actionOnDevice, FEDevice fedevice, User userInfo, Set<Permission> userPerms) throws Exception {
		
		boolean permit = false;
		boolean isDeviceOwner = false;
		boolean isOrgOwner = false;
		boolean isOrgMember = false;
		boolean isCityManager = false;
		boolean isContextManager = false;
		boolean isCitizen = false;
		
		// 1. Verify if user is device owner
		String deviceOwner = fedevice.getDeviceOwner();
		String connectedUserId = userInfo.getId();
		if (deviceOwner.equalsIgnoreCase(connectedUserId)) {
			isDeviceOwner = true;
		}
		
		Set<String> roles = userInfo.getRoles().get();
		//Map<Organization, Map<String, List<Role>>> rolesMap = userInfo.getOrganizationsRoles().get();
		
		// 2. Verify if user is member or owner in the organization of the device
		String devOrg = fedevice.getOrganization(); // Get device organization
		Set<String> orgSet = userInfo.getOrganizations().get();
		for(String org: orgSet) {
			if(devOrg.equalsIgnoreCase(org)) {
				isOrgOwner = true;
			}
		}
		Set<String> groupSet = userInfo.getGroups().get();
		for(String gr: groupSet) {
			if(devOrg.equalsIgnoreCase(gr)) {
				isOrgMember = true;
			}
		}
		//Set<Organization> connectedUserOrgs = userInfo.getOrganizationsRoles().get().keySet();	 // Get connectedUser organization
		//Organization devOrgObj = null;
		
		for(String role: roles) {
			if ("citymanager".equalsIgnoreCase(role)) {
				isCityManager = true;
			}
			if ("citizen".equalsIgnoreCase(role)) {
				isCitizen = true;
			}
		}
		
		if(isCityManager && (isOrgOwner || isOrgMember)) {
			if(actionOnDevice.equalsIgnoreCase("D")) {
				permit = false;
			} else {
				permit = true;
			}		
		} 
		
		if (isDeviceOwner) {
			permit = true;
		}
		
		// Get applicativeRole of the connected user in the device organization
//		String userApplicationRoleOnDevice = "";
//		for (String org : orgSet) {
//			LOGGER.log(Level.INFO, "Organization >>> " + org);
//			//TODO Danilo org.getName()
//			if (org.equalsIgnoreCase(devOrg)){
//				//devOrgObj = org;
//				List<Role> userOrgRoles = rolesMap.get(org).get(UserDTO.ORGANIZATION_ROLE);
//				for (Role applicationUserRole : userOrgRoles) {
//					userApplicationRoleOnDevice = applicationUserRole.getName().trim().replaceAll("\\s","").toLowerCase();
//					if (userApplicationRoleOnDevice.equalsIgnoreCase("citymanager") || userApplicationRoleOnDevice.equalsIgnoreCase("urbanserviceprovider")){
//						break;
//					}
//					LOGGER.log(Level.INFO, "Applicative user role in the device organization role: " + userApplicationRoleOnDevice);
//				}
//			}
//		}
//		LOGGER.log(Level.INFO, "Applicative user role in the device organization role: " + userApplicationRoleOnDevice);


		//String userOrganizationRole = null;
		// get connected user role in the device organization
//		try {
//			
//			
//			userOrganizationRole = rolesMap.get(devOrgObj).get(UserDTO.MEMBERSHIP_TYPE).get(0).getName();
//			LOGGER.log(Level.INFO, "userOrganizationRole " + userOrganizationRole);
//			String crud = "R";
//			
//			
//			for (Permission perm : userPerms) {
//				if (perm.getApplicationRole().equalsIgnoreCase(userApplicationRoleOnDevice) &&  perm.getOrganizationsRole().equalsIgnoreCase(userOrganizationRole) && perm.getAsset().equalsIgnoreCase("device")){
//					if (isDeviceOwner) {
//						if (perm.getAssetRole().equalsIgnoreCase("owner")){	
//							LOGGER.log(Level.INFO, "Application user role: " + perm.getApplicationRole());
//				        	LOGGER.log(Level.INFO, "Organization user role: " + perm.getOrganizationsRole());
//				        	LOGGER.log(Level.INFO, "Organization user role: " + perm.getAsset());
//				        	LOGGER.log(Level.INFO, "Organization user role: " + perm.getAssetRole());
//				        	crud = perm.getPermissionCRUD();
//				        	LOGGER.log(Level.INFO, "CRUD: " + crud);
//				        	break;
//						}
//					} else if (perm.getAssetRole().equalsIgnoreCase("member")){	// not owner of the device
//						LOGGER.log(Level.INFO, "Application user role: " + perm.getApplicationRole());
//				        LOGGER.log(Level.INFO, "Application user role: " + perm.getOrganizationsRole());
//				        LOGGER.log(Level.INFO, "Application user role: " + perm.getAsset());
//				        LOGGER.log(Level.INFO, "Application user role: " + perm.getAssetRole());
//				        crud = perm.getPermissionCRUD();
//				        LOGGER.log(Level.INFO, "CRUD: " + crud);
//					}
//				} 
//		     }
//			
//			LOGGER.log(Level.INFO, "CRUD: " + crud);
//			if (crud.contains(actionOnDevice)){
//				permit = true;
//			}
//		} catch (Exception e){
//			LOGGER.log(Level.ERROR, "Action not permitted: the user has no role in the organization ");
//			permit = false;
//		}
//		
		return permit;
	}
	
	/**
	 * @param url
	 * @return
	 */
	public  static String encode(String url)  {  

    try {  

         String encodeURL=URLEncoder.encode( url, "ISO-8859-1" );  

         return encodeURL;  

    } catch (UnsupportedEncodingException e) {  

  	  	LOGGER.log(Level.ERROR,"Issue while encoding on BigdataNotifier " + e.getMessage());
         return "";
    }  

}
	
	/**
	 * @param entity
	 * @param attr
	 * @return
	 */
	public static String getValueByJsonArrayNGSI (JSONArray entity, String attr) {
		
		String val = "";
		
		try {
			if (entity.length() >0) {
				
				JSONObject jOb =  (JSONObject) entity.get(0);
				
				
				JSONObject obJson = (JSONObject) jOb.get(attr);
				
				val = obJson.get("value").toString();
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return val;
	}

	
	/**
	 * @param dataformatProtocol
	 * @return
	 */
	public static boolean isEnabledAgent (String dataformatProtocol) {
		
		boolean ret = false;
		
		switch (dataformatProtocol) {
		
    		case "JSON":
    			ret =  Boolean.parseBoolean( ConfIotaJSON.getString("iota.json.enabled") ) ;
    			break;
    		
    		case "UL2_0":
    			ret =  Boolean.parseBoolean( ConfIotaUL20.getString("iota.ul20.enabled") ) ;
    			break;
    		
	    	case "SIGFOX":
	    		ret =  Boolean.parseBoolean( ConfIotaSigfox.getString("iota.sigfox.enabled") ) ;
	    		break;
	    		
	    	case "LWM2M":
	    		ret =  Boolean.parseBoolean( ConfIotaLWM2M.getString("iota.lwm2m.enabled") ) ;
	    		break;
	    		
	    	case "OPCUA":
	    		ret =  Boolean.parseBoolean( ConfIotaOPCUA.getString("iota.opcua.enabled") ) ;
	    		break;
	    		
	    	case ("CAYENNELPP"):
	    	case ("CBOR"):
	    	case ("APPLICATION_SERVER"):
	    		ret =  Boolean.parseBoolean( ConfIotaLoRa.getString("iota.lora.enabled") ) ;
	    		break;
	    		
	    	default :
	    		ret = false;
		}	
	    		
		
		return ret;
		
	}

}
