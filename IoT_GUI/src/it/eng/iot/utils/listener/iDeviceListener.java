package it.eng.iot.utils.listener;

import it.eng.tools.model.IdasDevice;

public interface iDeviceListener {
	
	public enum EventOnDevice {
	    DEVICE_CREATION,
	    DEVICE_EDIT,
	    DEVICE_DELETE
	  }
	
	public void onDeviceCreate(IdasDevice device, String fiwareservice, String fiwareservicepath);
	
	public void onDeviceEdit(IdasDevice device, String fiwareservice, String fiwareservicepath);
	
	public void onDeviceDelete(String deviceId, String fiwareservice, String fiwareservicepath);
	


}
