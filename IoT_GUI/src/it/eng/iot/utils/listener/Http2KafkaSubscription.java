package it.eng.iot.utils.listener;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.SubscriptionManager;

public class Http2KafkaSubscription implements IServiceListener {

	private static final Logger LOGGER = LogManager.getLogger(Http2KafkaSubscription.class);

	private static final boolean HTTP2KAFKA_ENABLED = Boolean.parseBoolean(ConfTools.getString("http2kafka.enabled"));

	@Override
	public void onServiceCreate(String fiwareservice, String fiwareservicepath) {

		if (!HTTP2KAFKA_ENABLED)
			return;

		try {
			SubscriptionManager.createServiceHttp2kafkaSubscription(fiwareservice, fiwareservicepath);
		} catch (Exception jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
		}

	}

	@Override
	public void onServiceDelete(String fiwareservice, String fiwareservicepath) {

		if (!HTTP2KAFKA_ENABLED)
			return;

		try {

			SubscriptionManager.deleteServiceHttp2kafkaSubscription(fiwareservice, fiwareservicepath);

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

	}

}
