package it.eng.iot.utils.listener;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.RestUtils;
import it.eng.tools.model.IdasDevice;

public class LoggerNotifier implements iDeviceListener {
	
	private final static String grayLogEndpoint = 	ConfTools.getString("graylogGelfEndpoint");
	private final static boolean  graylogGelfEnabled = 	Boolean.parseBoolean( ConfTools.getString("graylogGelfEnabled"));
	private final static String demaHost = 	ConfIDM.getString("dema.host");
	
	private static final Logger LOGGER = LogManager.getLogger(LoggerNotifier.class);
	
	
	/* (non-Javadoc)
	 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceCreate(it.eng.tools.model.IdasDevice, java.lang.String, java.lang.String)
	 */
	public void onDeviceCreate(IdasDevice device, String fiwareservice, String fiwareservicepath) {
		
		graylogNotify( EventOnDevice.DEVICE_CREATION , device.getDevice_id(), fiwareservice, fiwareservicepath);
		
	}
	
	/* (non-Javadoc)
	 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceEdit(it.eng.tools.model.IdasDevice, java.lang.String, java.lang.String)
	 */
	public void onDeviceEdit(IdasDevice device, String fiwareservice, String fiwareservicepath) {
		
		graylogNotify( EventOnDevice.DEVICE_EDIT , device.getDevice_id(), fiwareservice, fiwareservicepath);
		
	}
	
	/* (non-Javadoc)
	 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceDelete(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void onDeviceDelete(String deviceId, String fiwareservice, String fiwareservicepath) {
		
		graylogNotify( EventOnDevice.DEVICE_DELETE , deviceId, fiwareservice, fiwareservicepath);
		
		
	}
	

	
	/**
	 * @param type
	 * @param deviceId
	 * @param fiwareservice
	 * @param fiwareservicepath
	 */
	private static void graylogNotify(EventOnDevice loggerEvent, String deviceId, String fiwareservice, String fiwareservicepath){
		
		if (!graylogGelfEnabled)
			return;
		
		
         String fiwareservicepathC =  CommonUtils.cleanServicePath(fiwareservicepath);
		    
		 JSONObject jobj = new JSONObject();
		 
		 	 jobj.put("short_message", loggerEvent + " on DeMa");
		 	 jobj.put("host", demaHost);
			 jobj.put("event_type", loggerEvent);
			 jobj.put("resourceid", deviceId);
			 jobj.put("context", fiwareservice);//key for stream
			 jobj.put("category", fiwareservicepathC);
				 
		try {
			 RestUtils.consumePost(grayLogEndpoint, jobj, null);
		} catch (Exception e) {
			
			LOGGER.log(Level.ERROR,"GrayLog notification error " + e.getMessage());
			
		}	
		
		
		
		
	}
	
	
}
