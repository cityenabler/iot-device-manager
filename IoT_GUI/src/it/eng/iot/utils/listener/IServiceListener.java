package it.eng.iot.utils.listener;

public interface IServiceListener {
	
	public enum EventOnService {
	    SERVICE_CREATION,
	    SERVICE_DELETE
	  }
	
	public void onServiceCreate(String fiwareservice, String fiwareservicepath);
	
	public void onServiceDelete( String fiwareservice, String fiwareservicepath);


}
