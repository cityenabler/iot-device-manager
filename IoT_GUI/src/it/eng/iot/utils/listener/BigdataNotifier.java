package it.eng.iot.utils.listener;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.jersey.api.client.ClientResponse;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.DeviceUtils;
import it.eng.iot.utils.RestUtils;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.iot.utils.SubscriptionManager.SubscriptionsTarget;
import it.eng.tools.model.DeviceEndpointSubscriptionEntity;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceAttribute;

public class BigdataNotifier implements iDeviceListener {
	
	
	private static final boolean  bigdataEnabled = 	Boolean.parseBoolean( ConfTools.getString("bigdataEnabled"));
	
	private static final String BIGDATA_ADDRESS = ConfTools.getString("bigdata.api.address");
	private static final String BIGDATA_PATH_CREATE = ConfTools.getString("bigdata.api.endpoint.path.create");
	private static final String BIGDATA_PATH_DELETE = ConfTools.getString("bigdata.api.endpoint.path.delete");
	private static final String BIGDATA_PATH_INFO = ConfTools.getString("bigdata.api.endpoint.path.info");
	private static final String BIGDATA_ADDRESS_ON_ORION = ConfTools.getString("bigdata.api.address.onorion");
	
	private static final Integer throttling = null;
	private static final boolean isInternal = true;
	
	
	private static final Logger LOGGER = LogManager.getLogger(BigdataNotifier.class);
	
	Map<String, String> headers = new HashMap<String, String>();
	
	
/* (non-Javadoc)
 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceCreate(it.eng.tools.model.IdasDevice, java.lang.String, java.lang.String)
 */
public void onDeviceCreate(IdasDevice device, String fiwareservice, String fiwareservicepath) {
		
		if (!bigdataEnabled)
			return;
		
		boolean isHistoryActive = DeviceUtils.isHistoryActive(device);
		
		if (!isHistoryActive)
			return;
		
		
		String endpoint = getEndpoint(EventOnDevice.DEVICE_CREATION,  device.getDevice_id(),   fiwareservice,  fiwareservicepath, "");
		
		JSONObject jobj = prepareObject(device);
		
		ClientResponse response = null;
		try {
			  response = RestUtils.consumePostAnyResponse(endpoint, jobj, null);
		} catch (Exception e) {
			LOGGER.log(Level.ERROR,"Big Data notification error " + e.getMessage());
		}
		
		boolean isResponseOk =  isResponseOk(response);
		
		if  (isResponseOk) {
			
			String subscriptionIdCreated="";
			try {
				subscriptionIdCreated = SubscriptionManager.createdOnUpdateDeviceSubscriptionAndSubEntity(device.getDevice_id(),BIGDATA_ADDRESS_ON_ORION, fiwareservice, fiwareservicepath, throttling, isInternal, SubscriptionsTarget.BIG_DATA.toString() );
				LOGGER.log(Level.INFO,"create subscription"+subscriptionIdCreated);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"ERROR on BigDataNotifier. Subscription not created"+ e.getMessage());
				
				//TODO what to do ? Roolback on BigData ?
			}
			
		}else {
			LOGGER.log(Level.ERROR,"ERROR on big data communication. Do no create subscription on Orion");
		}
		
}

	
/* (non-Javadoc)
 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceEdit(it.eng.tools.model.IdasDevice, java.lang.String, java.lang.String)
 */
public void onDeviceEdit(IdasDevice device, String fiwareservice, String fiwareservicepath){
		
		if (!bigdataEnabled)
			return;
		
		String deviceId = device.getDevice_id();
		
		//retrieve the subscription about the device to BIGDATA 
		DeviceEndpointSubscriptionEntity deviceEndpointSubscriptions = SubscriptionManager.getDeviceEndpointSubscriptionByTarget(fiwareservice, fiwareservicepath, deviceId, false, SubscriptionsTarget.BIG_DATA.toString());
		
		boolean isEmptySubscription = false;
		if ( ("").equals(deviceEndpointSubscriptions.getId()) )
			isEmptySubscription=true;
		
		
		boolean isHistoryActive = DeviceUtils.isHistoryActive(device);
		
		if (!isHistoryActive) {
			
			//removeSubscription
			try {
				if (!isEmptySubscription ) 						
					SubscriptionManager.deleteDeviceSubscription(fiwareservice,fiwareservicepath, deviceId, deviceEndpointSubscriptions.getSubscriptionId().getValue());
			} catch (Exception e) {

				LOGGER.log(Level.ERROR,"Big Data notification error onDeviceEdit deleteDeviceSubscription" + e.getMessage());
			}
			
			
			boolean isRemoveOldHistoricalData = DeviceUtils.isRemoveOldHistoricalData(device);
			
			//Notify the BigData removing the device
			if (isRemoveOldHistoricalData)
				onDeviceDelete(deviceId, fiwareservice,  fiwareservicepath);
			
			return;
		}
		
		
		 
		
		
		//check if the subscription exists and in case it doesn't, then I create it
		if ( isEmptySubscription ) {
			
			String subscriptionIdCreated="";
			try {
				subscriptionIdCreated = SubscriptionManager.createdOnUpdateDeviceSubscriptionAndSubEntity(device.getDevice_id(),BIGDATA_ADDRESS_ON_ORION, fiwareservice, fiwareservicepath, throttling, isInternal, SubscriptionsTarget.BIG_DATA.toString() );
				LOGGER.log(Level.INFO,"create subscription"+subscriptionIdCreated);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"ERROR on BigDataNotifier. Subscription not created"+ e.getMessage());
			}
			
		}
		
		
		//call the BigData system
		String endpoint = getEndpoint(EventOnDevice.DEVICE_EDIT,  device.getDevice_id(),   fiwareservice,  fiwareservicepath, "");
		cleanOldMeasure( device, fiwareservice, fiwareservicepath);
		JSONObject jobj = prepareObject(device);
		
		try {
			  RestUtils.consumePostAnyResponse(endpoint, jobj, null);
		} catch (Exception e) {
			LOGGER.log(Level.ERROR,"Big Data notification error " + e.getMessage());
		}
		
		
}
	
/* (non-Javadoc)
 * @see it.eng.iot.utils.listener.iDeviceListener#onDeviceDelete(java.lang.String, java.lang.String, java.lang.String)
 */
public void onDeviceDelete(String deviceId, String fiwareservice, String fiwareservicepath){
		
		if (!bigdataEnabled)
			return;
		
		String endpoint = getEndpoint(EventOnDevice.DEVICE_DELETE,  deviceId,   fiwareservice,  fiwareservicepath, "");
		
		try {
			RestUtils.consumeDelete(endpoint, headers);
		} catch (Exception e) {
			LOGGER.log(Level.ERROR,"ERROR on BigDataNotifier onDeviceDelete for "+deviceId +" deletion. No response from BigData system "+ e.getMessage());
		}
		
}
	

	
	/**
	 * @param ev
	 * @param deviceID
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @return
	 */
private String getEndpoint (EventOnDevice ev, String deviceID,  String fiwareservice, String fiwareservicepath, String type) {
		
		String encDeviceId = CommonUtils.encode(deviceID);
		String encFiwareservice = CommonUtils.encode(fiwareservice);
		String encFiwareservicepath = CommonUtils.encode(fiwareservicepath);
		
		// /datastore_device_create/{service}/{servicePath}/{resourceId}
		String endpoint = BIGDATA_ADDRESS+BIGDATA_PATH_CREATE+"/"+encFiwareservice+"/"+encFiwareservicepath+"/"+encDeviceId;
		
		// /datastore_device_delete/{service}/{servicePath}/{resourceId}
		if (ev == EventOnDevice.DEVICE_DELETE)
			endpoint = BIGDATA_ADDRESS+BIGDATA_PATH_DELETE+"/"+encFiwareservice+"/"+encFiwareservicepath+"/"+encDeviceId;
		
		
		
		// /datastore_device_info/{service}/{servicePath}/{resourceId}
		if (type.equals("info"))
			endpoint = BIGDATA_ADDRESS+BIGDATA_PATH_INFO+"/"+encFiwareservice+"/"+encFiwareservicepath+"/"+encDeviceId;
		
			
		return endpoint;
}



	
/**
 * 
 * @param device
 * @return
 * 
 * {
	  "listMeasureType": [
	    "temperature","pressure"
	  ]
	}
 * 
 * 
 */
private JSONObject prepareObject(IdasDevice device) {
		
		JSONObject jobj = new JSONObject();
		
		
		JSONArray jArr = new JSONArray();
		
		
		Set<IdasDeviceAttribute> attributes = device.getAttributes();
		
		for(IdasDeviceAttribute attr : attributes){
			jArr.put(attr.getName());
		}
		 
	 	 jobj.put("listMeasureType", jArr);
		
	 	 return jobj;
		
}


/**
 * @param device
 * @param fiwareservice
 * @param fiwareservicepath
 */
private void cleanOldMeasure(IdasDevice device, String fiwareservice, String fiwareservicepath) {
		
	
	//retrieve already existing record
	String endpointInfo = getEndpoint(EventOnDevice.DEVICE_EDIT,  device.getDevice_id(),   fiwareservice,  fiwareservicepath, "info");
	String endpointDelete = getEndpoint (EventOnDevice.DEVICE_DELETE, device.getDevice_id(), fiwareservice, fiwareservicepath, "");		
			
			
	String responseOld="";
			
	try {
				
	/*
				{
					  "measures": [
					    {
					      "firstMeasureDateTime": "2019-10-25T15:31:55.618Z",
					      "measureType": "string",
					      "uuid": "string"
					    }
					  ],
					  "resourceId": "string"
					}
	*/
				
				
			responseOld = RestUtils.consumeGet(endpointInfo, headers);
	} catch (Exception e) {
			LOGGER.log(Level.ERROR,"ERROR on BigDataNotifier onDeviceEdit. No response from BigData system "+ e.getMessage());
		
		//TODO what to do ? 
	}
	
	if ("".equals(responseOld))
		return;
	
	
	
		
	JSONObject jobOld = new JSONObject(responseOld);
	JSONArray jArrOld = jobOld.getJSONArray("measures");
	
	Set<IdasDeviceAttribute> attributes = device.getAttributes();
		
	for (int i = 0 ; i < jArrOld.length(); i++) {
	       JSONObject measure = jArrOld.getJSONObject(i);	
	       String measureType = measure.get("measureType").toString();
	       
	       boolean found = false;
	       
	       for(IdasDeviceAttribute attr : attributes){
	    	   
	    	   if (attr.getName().equals(measureType)) 
	    		   found=true;
	       }   
	    	   
	       if (!found)
	    	   deleteSingleMeasure(endpointDelete,device.getDevice_id(), fiwareservice, fiwareservicepath,  measureType);
	}
		
		
}
	
	
	
/**
 * @param endpointDelete
 * @param deviceID
 * @param fiwareservice
 * @param fiwareservicepath
 * @param measureType
 */
private void deleteSingleMeasure (String endpointDelete, String deviceID, String fiwareservice, String fiwareservicepath, String measureType) {
	
	
	String encMeasureType = CommonUtils.encode(measureType);	
	String endpoint=endpointDelete+"?measureType="+encMeasureType;
	
	try {
		RestUtils.consumeDelete(endpoint, headers);
	} catch (Exception e) {
		LOGGER.log(Level.ERROR,"ERROR on BigDataNotifier onDeviceEdit for "+deviceID+" on "+measureType +" deletion. No response from BigData system "+ e.getMessage());
	}
		
	
}


	
	

/**
 * @param clientResponse
 * @return
 */
private static boolean isResponseOk (ClientResponse clientResponse) {

	boolean isOk = false;
	
	if (null == clientResponse)
		return isOk;
	
	
	int statusId = clientResponse.getStatus();
	
	
	if (statusId == 200 || //200 only device
		statusId == 201 || //201 device with measure s
		statusId == 409 ) //409 in case of update
		isOk = true;
	
	
	return isOk;
}






}
