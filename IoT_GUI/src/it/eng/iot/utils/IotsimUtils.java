package it.eng.iot.utils;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource.Builder;

import it.eng.iot.configuration.ConfTools;



public abstract class IotsimUtils {

	private static final Logger LOGGER = LogManager.getLogger(IotsimUtils.class);
	
	private static final String iotsimStopAction = "/stop";
	private static final String iotsimStatusAction = "/status";
	private static final String iotsimConfigAction = "/config";
	
	private static final String iotsimURL = ConfTools.getString("iotsim.url");
	
	
	/**
	 * @param url
	 * @param headers
	 * @return
	 * @throws Exception
	 */
	public static String consumeGet(String url, Map<String, String> headers) throws Exception{

		Client client = Client.create();

		Builder builder = client.resource(url).getRequestBuilder(); 

		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}

		ClientResponse resp = builder.get(ClientResponse.class);

		if(resp.getStatus()>301){
			throw new Exception("URL "+url +
					" responded with status "+resp.getStatus() +
					" and message: "+resp.getEntity(String.class));
		}

		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		return r;



	}


	
	/**
	 * Stop the simulation for all attributes of this device
	 * @param headerService
	 * @param headerServicePath
	 * @param deviceId
	 * @return
	 */
	public static JSONObject stopDeviceSimulation (String headerService, String headerServicePath, String deviceId) {
		
		
		// 0. STOP DEVICE SIMULATIONS
		String queryParams = "?service="+headerService+"&servicepath="+headerServicePath;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", headerService);
		headers.put("fiware-servicepath", headerServicePath);
		queryParams = queryParams.concat("&deviceid="+deviceId);
		headers.put("deviceid", deviceId);
					
		
		String url = iotsimURL + iotsimStopAction + queryParams;
		LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
		String result = "";
		try {
				result = IotsimUtils.consumeGet(url, headers);
		} catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
		}

		return new JSONObject(result);
					
	}
	/**
	 * Stop the simulation for context
	 * @param headerService
	 * @param headerServicePath
	 * @return
	 */
	public static JSONObject stopSimulation (String headerService, String headerServicePath) {
		
		
		// 0. STOP CONTEXT SIMULATIONS
		String queryParams = "?service="+headerService+"&servicepath="+headerServicePath;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", headerService);
		headers.put("fiware-servicepath", headerServicePath);
		
		String url = iotsimURL + iotsimStopAction + queryParams;
		LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
		String result = "";
		try {
				result = IotsimUtils.consumeGet(url, headers);
		} catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
		}

		return new JSONObject(result);
		
	}
	
	/**
	 * @param deviceid
	 * @return
	 */
	public static boolean hasDeviceId(String deviceid){
		boolean out = true;
		try{
			if (deviceid == null || deviceid.trim().equals("")) {
				out = false;
			}
		} 
		catch (Exception exception){
			LOGGER.log(Level.ERROR, "Error on hasDeviceId method"); 
			out = false;
		}

		return out;
	}
	
	/**
	 * @param measureid
	 * @return
	 */
	public static boolean hasMeasureId(String measureid){
	    boolean out = true;
		try{
			if (measureid == null || measureid.trim().equals("")) {
				out = false;
			}
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}

	/**
	 * @param service
	 * @param servicepath
	 * @param deviceid
	 * @param measure_id
	 * @return
	 * @throws Exception
	 */
	public static JSONObject getSimStatus(String service, String servicepath, String deviceid, String measure_id) throws Exception {
		
		
		String queryParams = "?service="+service+"&servicepath="+servicepath;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", service);
		headers.put("fiware-servicepath", servicepath);
		if (hasDeviceId(deviceid)) {
			queryParams = queryParams.concat("&deviceid="+deviceid);
			headers.put("deviceid", deviceid);
			headers.put("measure_id", measure_id);
		}
		
		String url = iotsimURL + iotsimStatusAction + queryParams;
		LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
		String result = IotsimUtils.consumeGet(url, headers);
		return new JSONObject(result);
	}
	


	/**
	 * @param body
	 * @return
	 * @throws Exception
	 */
	public static JSONArray getSimStatusMultidevices(JSONArray body) throws Exception {
		
		Map<String, String> headers = new HashMap<String, String>();
		
		String url = iotsimURL + iotsimStatusAction ;
		LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
		
		String result = RestUtils.consumePost(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		
		return new JSONArray(result);
		
	
	}
	
	
	/**
	 * @param servicepath
	 * @param measure_id
	 * @param measure_type
	 * @param service
	 * @return
	 * @throws Exception
	 */
	public static String loadBusinessDataModel(String servicepath, String  measure_id, String measure_type, String service, String categoryName) throws Exception {
		
		Map<String, String> headers = new HashMap<String, String>();
		String resp = "";
		
		headers.put("fiware-service", service);
		headers.put("fiware-servicepath", servicepath);
		headers.put("measure_id", measure_id);
		headers.put("measure_type", measure_type);
		
		String url = iotsimURL + iotsimConfigAction;
		
		
		String queryParams = url+"?categoryName="+categoryName;
		
		LOGGER.log(Level.INFO, "SIMULATOR URL {}" , queryParams);
		
		resp = RestUtils.consumeGet(queryParams, headers);
		
		
		return resp;
	
	}
}

