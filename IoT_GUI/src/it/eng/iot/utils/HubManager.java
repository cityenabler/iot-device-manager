package it.eng.iot.utils;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import it.eng.iot.configuration.ConfServiceCrudHub;
import it.eng.iot.hub.HubFactory;
import it.eng.iot.hub.model.HubEntity;

public abstract class HubManager {
	private static final Logger LOGGER = LogManager.getLogger(HubManager.class);
	
	static String hubServiceUrl = ConfServiceCrudHub.getString("service.crud.address");
	static String hubServicePort = ConfServiceCrudHub.getString("service.crud.port");
	static String hubServiceFindAll = ConfServiceCrudHub.getString("service.crud.findAll");
	static String hubServiceFindAllPaginated = ConfServiceCrudHub.getString("service.crud.findAllPaginated");
	static String hubServiceCount = ConfServiceCrudHub.getString("service.crud.count");
	static String hubServiceFindByName = ConfServiceCrudHub.getString("service.crud.findByName");
	static String hubServiceFindByType = ConfServiceCrudHub.getString("service.crud.findByType");
	static String hubServiceSave = ConfServiceCrudHub.getString("service.crud.save");
	static String hubServiceUpdate = ConfServiceCrudHub.getString("service.crud.update");
	
//	public static List<HubEntity> getAllHubs() {
//		LOGGER.log(Level.INFO, "Get all Hubs configured on MONGO DB");
//		
//		try {
//			String endpoint = hubServiceUrl + ":" + hubServicePort + hubServiceFindAll;
//			LOGGER.log(Level.INFO, "Invoking " + endpoint);
//			String serviceResponse = RestUtils.consumeGet(endpoint);
//			return HubFactory.getHubList(serviceResponse);
//		} catch (Exception ex) {
//			LOGGER.log(Level.ERROR, ex.getMessage());
//		}
//		return null;
//	}
	
	public static List<HubEntity> getAllHubsPaginated(String pageNumber, String pageSize) {
		LOGGER.log(Level.INFO, "Get all Hubs configured on MONGO DB paginated");
		
		try {
			String endpoint = hubServiceUrl + ":" + hubServicePort + hubServiceFindAllPaginated;
			endpoint = endpoint.replace("$1", pageNumber).replace("$2", pageSize);
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			return HubFactory.getHubList(serviceResponse);
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static String countHubs() {
		LOGGER.log(Level.INFO, "Count Hubs configured on MONGO DB");
		
		try {
			String endpoint = hubServiceUrl + ":" + hubServicePort + hubServiceCount;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			JsonObject jsonResp = new Gson().fromJson(serviceResponse, JsonObject.class);
			String count = jsonResp.get("count").toString();
			return count;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		
		return null;
	}
	
	public static HubEntity findHubByName(String name) {
		LOGGER.log(Level.INFO, "Get Hub configured on MONGO DB by name");
		
		try {
			String endpoint = hubServiceUrl + ":" + hubServicePort + hubServiceFindByName + name;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			return HubFactory.getHubList(serviceResponse).get(0);	//Assuming there is only one json element with this name thanks to "add hub" procedure
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static HubEntity findHubByType(String hubType) {
		LOGGER.log(Level.INFO, "Get Hub configured on MONGO DB by type");

		try {
			String endpoint = hubServiceUrl + ":" + hubServicePort + hubServiceFindByType + hubType;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			return HubFactory.getHubList(serviceResponse).get(0);	//Assuming there is only one json element with this name thanks to "add hub" procedure
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
}
