package it.eng.iot.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import it.eng.iot.configuration.ConfMQTTBroker;

public class MqttUtils {
	
	private static final Logger LOGGER = LogManager.getLogger(MqttUtils.class);

	public static String mqttPublish(String broker, String content, String topic, 
									boolean MQTTBrokerAuthEnabled, String MQTTBrokerAuthUsername, String MQTTBrokerAuthPassword,
									boolean sslEnabled, String sslCaFilePath, String sslClientCrtFilePath, String sslClientKeyFilePath
									) throws Exception {
		String clientId = ConfMQTTBroker.getString("pusblisherclientid");
		int qos = Integer.parseInt(ConfMQTTBroker.getString("mqtt.quality.service"));
		MemoryPersistence persistence = new MemoryPersistence();

		String r = "";
		try {
			MqttClient client = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			
			connOpts.setCleanSession(true);
			
			
			if (sslEnabled) {
				
				LOGGER.log(Level.INFO,"MQTT SSL Connection ");
				
				SSLSocketFactory socketFactory = getSocketFactory(sslCaFilePath,sslClientCrtFilePath, sslClientKeyFilePath, "");
				connOpts.setSocketFactory(socketFactory);
			}
			
			
			if (MQTTBrokerAuthEnabled) {
				
				LOGGER.log(Level.INFO,"Connecting to MQTT broker in authenticated way ");
				connOpts.setUserName(MQTTBrokerAuthUsername);
				connOpts.setPassword(MQTTBrokerAuthPassword.toCharArray());
			}
			
			
			LOGGER.log(Level.INFO,"Connecting to broker: " + broker);
			client.connect(connOpts);
			LOGGER.log(Level.INFO,"Connected");
			LOGGER.log(Level.INFO,"Publishing message: " + content);
			MqttMessage message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(topic, message);
			LOGGER.log(Level.INFO,"Message published");
			client.disconnect();
			LOGGER.log(Level.INFO,"Disconnected");
			client.close();
			LOGGER.log(Level.INFO,"Client close");
			
			
			r = "200"; // OK
		} catch (MqttException me) {
			
			LOGGER.log(Level.ERROR,"reason " + me.getReasonCode());
			LOGGER.log(Level.ERROR,"msg " + me.getMessage());
			LOGGER.log(Level.ERROR,"loc " + me.getLocalizedMessage());
			LOGGER.log(Level.ERROR,"cause " + me.getCause());
			LOGGER.log(Level.ERROR,"exception: " + me);
			r = me.getReasonCode() + "";
		}
		LOGGER.log(Level.INFO,"Result code: " + r);
		return r;
	}

	
	
	public static SSLSocketFactory getSocketFactory(final String caCrtFile, final String crtFile, final String keyFile, final String password) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        // load CA certificate
        X509Certificate caCert = null;

        FileInputStream fis = new FileInputStream(caCrtFile);
        BufferedInputStream bis = new BufferedInputStream(fis);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        while (bis.available() > 0) {
            caCert = (X509Certificate) cf.generateCertificate(bis);
        }

        // load client certificate
        bis = new BufferedInputStream(new FileInputStream(crtFile));
        X509Certificate cert = null;
        while (bis.available() > 0) {
            cert = (X509Certificate) cf.generateCertificate(bis);
        }

        // load client private key
        PEMParser pemParser = new PEMParser(new FileReader(keyFile));
        Object object = pemParser.readObject();
        PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(password.toCharArray());
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        KeyPair key;
        if (object instanceof PEMEncryptedKeyPair) {
        	LOGGER.log(Level.INFO,"Encrypted key - we will use provided password");
            key = converter.getKeyPair(((PEMEncryptedKeyPair) object).decryptKeyPair(decProv));
        } else {
        	LOGGER.log(Level.INFO,"Unencrypted key - no password needed");
            key = converter.getKeyPair((PEMKeyPair) object);
        }
        pemParser.close();

        // CA certificate is used to authenticate server
        KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
        caKs.load(null, null);
        caKs.setCertificateEntry("ca-certificate", caCert);
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(caKs);

        // client key and certificates are sent to server so it can authenticate
        // us
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.setCertificateEntry("certificate", cert);
        ks.setKeyEntry("private-key", key.getPrivate(), password.toCharArray(), new java.security.cert.Certificate[] { cert });
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, password.toCharArray());

        // finally, create SSL socket factory
        SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        return context.getSocketFactory();
    }

	
	
	
	
//	public static void main(String[] args) {
//		String broker = "tcp://217.172.12.209:8098";
//		String topic = "/GmJszisVlK83sGR/mqttpushul20/attrs";
//		String content = "status|occupied";
//		try {
//			mqttPublish(broker, content, topic);
//
//		} catch (Exception e) {
//			LOGGER.log(Level.ERROR, e.getMessage());
//		}
//	}

}
