package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.sun.jersey.api.client.ClientResponse;

import it.eng.iot.configuration.ConfIotaLoRa;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceInternalAttributeLoRa;
import it.eng.tools.model.IdasDeviceStaticAttribute;

public  class LoraUtils {
	
	static String appServerEndpoint = ConfIotaLoRa.getString("iota.lora.appserver.endpoint");
	static String defaultOrganizationId = ConfIotaLoRa.getString("iota.lora.appserver.defaultOrganizationId");
	static String defaultServiceProfile = ConfIotaLoRa.getString("iota.lora.appserver.defaultServiceProfile");
	static String secretKey = ConfIotaLoRa.getString("iota.lora.appserver.jwt.secret");
	static String audience = ConfIotaLoRa.getString("iota.lora.appserver.jwt.audience");
	static String issuer = ConfIotaLoRa.getString("iota.lora.appserver.jwt.issuer");
	static String subject = ConfIotaLoRa.getString("iota.lora.appserver.jwt.subject");
	
	
	
	/**
	 * @return
	 */
	public static String getJWT () {
		
		String jwt = createJWT( audience,  issuer,  subject, secretKey);
		
		return jwt;
	}
	
	/**
	 * @return
	 */
	public static String getJWTSingleton () {
		
		//it's necessary to use a Singleton to avoid too many validation on server fail
		
		
		CreateJwtSingleton jwtSingleton = CreateJwtSingleton.getInstance();
		String jwt = jwtSingleton.token;
		
		System.out.println(jwt);
		
		
		return jwt;
	}
	
	
	
	
	
	/**
	 * @param audience
	 * @param issuer
	 * @param subject
	 * @param ttlMillis
	 * @return
	 */
	private static String createJWT(String audience, String issuer, String subject, String secretKey) {
		
		// Compose the JWT claims set
		Date now = new Date();
		Date expTime = new Date(now.getTime() + 1000*3600*24*365);//expires in 1 year
		

		JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder()
		    .issuer(issuer)
		    .subject(subject)
		    .audience(audience)
		    .expirationTime(expTime) 
		    .issueTime(now)
		    .claim("username", "admin")
		    .build();

		byte[] secret = secretKey.getBytes();

		// Create HMAC signer
		JWSSigner signer = null;
		try {
			signer = new MACSigner(secret);
		} catch (KeyLengthException e) {
			e.printStackTrace();
		}

		SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), jwtClaims);

		// Apply the HMAC protection
		try {
			signedJWT.sign(signer);
		} catch (JOSEException e) {
			e.printStackTrace();
		}

		// Serialize to compact form, produces something like
		// eyJhbGciOiJIUzI1NiJ9.SGVsbG8sIHdvcmxkIQ.onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA
		String s = signedJWT.serialize();
		
		return s;
		
	}
	
	
	/**
	 * @param jwt
	 * @return
	 */
	public static String  getApplications (String jwt) {
				
		String url = appServerEndpoint+"/api/applications?limit=1000000&organizationID="+defaultOrganizationId;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		
		String jwt_header = "Bearer " + jwt;
		
		System.out.println("jwt_header");
		System.out.println(jwt_header);
		
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		String serviceResponse="500";
		
		try {
			serviceResponse = RestUtils.consumeGet(url, headers);
		} catch (Exception e) {
			
			e.printStackTrace();
			return serviceResponse;
		}
		
		return serviceResponse;
		
	}
	
	
	/**
	 * @param categoryApiKey
	 * @param jwt
	 * @return
	 */
	public static String getApplicationId (String categoryApiKey, String jwt) {
		
		String appId ="";
		
		String serviceResponse = getApplications (jwt);
		
		JSONObject resp = new JSONObject(serviceResponse);
		
		JSONArray results = resp.getJSONArray("result");
		
		for (int i=0; i < results.length(); i++) {
			JSONObject res = results.getJSONObject(i);
			if (res.get("name").equals(categoryApiKey)) {
				appId =  res.getString("id");
				break;
			}
		}
		
		return appId;
	}
	
	
	public static String createApplication (String categoryApiKey, String fiwareservicepath, String jwt) {
		
		
		String result ="500";
		
		String url = appServerEndpoint+"/api/applications";
		
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		/*
		{
			  "device": {
			    "applicationID": "6",
			    "description": "testApiDeviceDescr",
			    "devEUI": "e6f1752d1a71bdd8",
			    "deviceProfileID": "03ec3716-a286-49e5-8342-34e2949b800b",
			    "name": "testApiDevice2"
			  }
			}
		*/
		
		JSONObject body = new JSONObject();
		JSONObject application = new JSONObject();
		
		application.put("description", "Application for "+fiwareservicepath+" created by DeMa");
		application.put("name", categoryApiKey);
		application.put("organizationID", defaultOrganizationId);
		application.put("serviceProfileID", defaultServiceProfile);
		application.put("payloadCodec", "CAYENNE_LPP");
		
		body.put("application", application);
		
		
		try {
			 result = RestUtils.consumePost(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
		return result;
	}
	
	
	public static String deleteApplication  (String appId, String jwt) {
		
		String result ="500";
		
		String url = appServerEndpoint+"/api/applications/"+appId;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		ClientResponse serviceResponse = null;
		try {
			serviceResponse = RestUtils.consumeDelete(url, headers);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
		result = serviceResponse.getEntity(String.class);
		
		
		return result;
		
		
	}
	
	
	
	
	/**
	 * @param applicationID
	 * @param jwt
	 * @return
	 */
	public static JSONArray getDeviceProfiles (String jwt ) {
		
		String url = appServerEndpoint+"/api/device-profiles?limit=1000000&organizationID="+defaultOrganizationId;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		String serviceResponse="";
		
		try {
			serviceResponse = RestUtils.consumeGet(url, headers);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		JSONObject resp = new JSONObject(serviceResponse);
		JSONArray results = resp.getJSONArray("result");
		
		return results;
	}
	
	/**
	 * @return
	 */
	public static String getRandomDevEui () {
		
		int counter = 0;
		Random rand = new Random();
		Random rand2 = new Random();

		if (++counter == 0) rand = new Random(); // reset every 4 billion values.

		long randomLong = rand.nextLong() ^ rand2.nextLong() << 1;
		
		String hexadSt = Long.toHexString(randomLong);
		
		return hexadSt;
	}
	
	
	
	/**
	 * @param applicationKey
	 * @param devEUI
	 * @param jwt
	 * @return
	 */
	public static String setApplicationKeyOnServer(String applicationKey, String devEUI, String jwt, boolean isUpdate) {
		
		String url = appServerEndpoint+"/api/devices/"+devEUI+"/keys";
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		/*
			{
			  "deviceKeys": {	
			    "devEUI": "17446f057c1054a1",
			    "nwkKey": "2a65089d5c205645be0d6f17299dbae6",
			    "appKey": "00000000000000000000000000000000"
			  }
			}
		*/
		
		JSONObject body = new JSONObject();
		JSONObject deviceKeys = new JSONObject();
		
		deviceKeys.put("devEUI",devEUI);
		deviceKeys.put("nwkKey",applicationKey);
		deviceKeys.put("appKey","00000000000000000000000000000000");
		
		body.put("deviceKeys", deviceKeys);
		
		String result ="500";
		
		try {
			
			 if(!isUpdate)			
				 result = RestUtils.consumePost(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
			 else
				 result = RestUtils.consumePut(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
			 
			 
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
		return result;
		
	}
	
	
	public static String createDeviceOnServer(String applicationID, String name, String devEUI, String deviceProfileID, String jwt ) {
		
		String url = appServerEndpoint+"/api/devices";
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		/*
		{
			  "device": {
			    "applicationID": "6",
			    "description": "testApiDeviceDescr",
			    "devEUI": "e6f1752d1a71bdd8",
			    "deviceProfileID": "03ec3716-a286-49e5-8342-34e2949b800b",
			    "name": "testApiDevice2"
			  }
			}
		*/
		
		JSONObject body = new JSONObject();
		JSONObject device = new JSONObject();
		device.put("applicationID", applicationID);
		device.put("description", "Device "+name+"created by DeMa");
		device.put("devEUI", devEUI);
		device.put("deviceProfileID", deviceProfileID);
		device.put("name", name);
		
		body.put("device", device);
		
		String result ="500";
		
		try {
			 result = RestUtils.consumePost(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
		return result;
		
	}
	
	
	public static void updateDeviceOnServer(String applicationID, String name, String devEUI, String deviceProfileID, String jwt ) {
		
		String url = appServerEndpoint+"/api/devices/"+devEUI;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		/*
		{
			  "device": {
			    "applicationID": "6",
			    "description": "testApiDeviceDescr",
			    "devEUI": "e6f1752d1a71bdd8",
			    "deviceProfileID": "03ec3716-a286-49e5-8342-34e2949b800b",
			    "name": "testApiDevice2"
			  }
			}
		*/
		
		JSONObject body = new JSONObject();
		JSONObject device = new JSONObject();
		device.put("applicationID", applicationID);
		device.put("description", "Device "+name+" created and updated by DeMa ");
		device.put("deviceProfileID", deviceProfileID);
		device.put("name", name);
		
		body.put("device", device);
		
		
		try {
			  RestUtils.consumePut(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public static String deleteDeviceOnServer(String devEUI, String jwt) {
		
		String result ="500";
		
		String url = appServerEndpoint+"/api/devices/"+devEUI;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		String jwt_header = "Bearer " + jwt;
		headers.put("Grpc-Metadata-Authorization", jwt_header);
		
		
		ClientResponse serviceResponse = null;
		try {
			serviceResponse = RestUtils.consumeDelete(url, headers);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		
		result = serviceResponse.getEntity(String.class);
		
		
		return result;
		
		
	}
	
	/**
	 * @param protocol
	 * @return
	 */
	public static String getDatamodelFromProtocol (String protocol) {
		
		String datamodel = "";
		switch(protocol) {
		
			case "CAYENNELPP":
				datamodel="cayennelpp";
			break;
			
			case "CBOR":
				datamodel="cbor";
			break;
			
			case "APPLICATION_SERVER":
				datamodel="application_server";

		}
		
		return datamodel;
	}
	
	
	
	/**
	 * @param iAttrOb
	 * @return
	 */
	public static IdasDeviceInternalAttributeLoRa getLoraInternalAttribute (Object iAttrOb) {
		
		 Gson gson = new Gson();
		 String iAttrS = gson.toJson(iAttrOb);

		Type type = new TypeToken<IdasDeviceInternalAttributeLoRa>(){}.getType();
		IdasDeviceInternalAttributeLoRa attr = new Gson().fromJson(iAttrS, type);
		
		return attr;
		
		
	}
	
	
	
	/**
	 * @param idasdev
	 * @return
	 */
	public static String getDevEui(IdasDevice idasdev) {
		
		String devEUI = "";
		
		try {
			 Object iAttrOb = idasdev.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attr = getLoraInternalAttribute(iAttrOb);
			
			 devEUI = attr.getLorawan().getDev_eui();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return devEUI;
	}
	
	/**
	 * @param idasdev
	 * @return
	 */
	public static String getAppEui(IdasDevice idasdev) {
		
		String appEui = "";
		
		try {
			 Object iAttrOb = idasdev.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attr = getLoraInternalAttribute(iAttrOb);
			
			 appEui = attr.getLorawan().getApp_eui();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return appEui;
	}
	
	/**
	 * @param idasdev
	 * @return
	 */
	public static String getAppId(IdasDevice idasdev) {
		
		String appId = "";
		
		try {
			 Object iAttrOb = idasdev.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attr = getLoraInternalAttribute(iAttrOb);
			
			 appId = attr.getLorawan().getApplication_id();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return appId;
	}
	
	/**
	 * @param idasdev
	 * @return
	 */
	public static String getAppKey(IdasDevice idasdev) {
		
		String appKey = "";
		
		try {
			 Object iAttrOb = idasdev.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attr = getLoraInternalAttribute(iAttrOb);
			
			 appKey = attr.getLorawan().getApplication_key();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return appKey;
	}
	
	
	
	/**
	 * @param idasdev
	 * @return
	 */
	public static String getAppName(IdasDevice idasdev) {
		
		String devEUI = "";
		
		try {
			 Object iAttrOb = idasdev.getInternal_attributes();
			 IdasDeviceInternalAttributeLoRa attr = getLoraInternalAttribute(iAttrOb);
			
			devEUI = attr.getLorawan().getApplication_id();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return devEUI;
	}
	
	/**
	 * @param staticAttrs
	 * @return
	 */
	public static String getLoraStack(Set<IdasDeviceStaticAttribute> staticAttrs){
		String loraStack ="TTN";
		try{
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("loraStack".equalsIgnoreCase(attr.getName()) ){
					loraStack =  attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	exception.printStackTrace();
	    }
		
		return loraStack;
	}
	
	
	
	/**
	 * @param staticAttrs
	 * @return
	 */
	public static String getLoraDeviceProfileId(Set<IdasDeviceStaticAttribute> staticAttrs){
		String loraDeviceProfileId ="";
		try{
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("loraDeviceProfileId".equalsIgnoreCase(attr.getName()) ){
					loraDeviceProfileId =  attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	exception.printStackTrace();
	    }
		
		return loraDeviceProfileId;
	}
	
	
	/**
	 * @param idasDevice
	 * @return
	 */
	public static boolean isTTN (IdasDevice idasDevice) {
		
		try {
			String stack = getLoraStack(idasDevice.getStatic_attributes());
			
			if (stack.equalsIgnoreCase("TTN"))
				return true;
			else
				return false;
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	
	public static void main(String[] args) {
	
		/*
		 * String deviceName = "deviceNameTest1";
		
		
		for(int i =0; i<10; i ++) {
			try {
				
				String jwt = getJWTSingleton();
				System.out.println(jwt);
				String appId = getApplicationId ("ciaooo", jwt);
				
			} catch (Exception e) {
				
				e.printStackTrace();
				continue;
			}
			
		}
		
		
		
		String deviceProfileID = "03ec3716-a286-49e5-8342-34e2949b800b";
		
		
		JSONArray deviceProfiles  = getDeviceProfiles(appId, jwt);
		
		
		
		System.out.println("deviceProfiles");
		System.out.println(deviceProfiles);
		
		String devEUI = getRandomDevEui();
		
		
		createDeviceOnServer( appId,  deviceName,  devEUI,  deviceProfileID,  jwt );
		
		String resp = deleteDeviceOnServer("e847995f40c17cc1",jwt );
		
		
		String resp = createApplication("19dsfioj292dk", "test_issues", jwt);
	
		
		String resp = deleteApplication("8", jwt);
		String resp2 = deleteApplication("9", jwt);
		System.out.println(resp);
		
			*/
		
		String jwt = getJWTSingleton();
		
		updateDeviceOnServer( "2",  "Lora1a",  "99b1613025f649bf",  "cbe12593-d29a-4fa2-ba46-4b993b9cad26",  jwt );
		
		
		
		
	}
	
	
	
	
}
