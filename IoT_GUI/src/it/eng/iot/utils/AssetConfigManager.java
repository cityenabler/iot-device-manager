package it.eng.iot.utils;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.eng.iot.configuration.ConfServiceAssetConfig;
import it.eng.iot.hub.model.kapua.Device;

public abstract class AssetConfigManager {
	private static final Logger LOGGER = LogManager.getLogger(AssetConfigManager.class);
	
	static String assetConfigServiceUrl = ConfServiceAssetConfig.getString("service.asset.config.address");
	static String assetConfigServicePort = ConfServiceAssetConfig.getString("service.asset.config.port");
	static String assetConfigServiceFindAllPaginated = ConfServiceAssetConfig.getString("service.asset.config.findAllPaginated");
	static String assetConfigServiceCount = ConfServiceAssetConfig.getString("service.asset.config.count");
	static String assetConfigServiceFindById = ConfServiceAssetConfig.getString("service.asset.config.findById");
	static String assetConfigServiceFindByType = ConfServiceAssetConfig.getString("service.asset.config.findByType");
	static String assetConfigServiceSave = ConfServiceAssetConfig.getString("service.asset.config.save");
	static String assetConfigServiceUpdate = ConfServiceAssetConfig.getString("service.asset.config.update");
	
	public static List<Device> getAllDevicesPaginated(String pageNumber, String pageSize) {
		LOGGER.log(Level.INFO, "Get all Devices configured on MONGO DB paginated");
		
		try {
			List<Device> deviceList = new ArrayList<Device>();
			String endpoint = assetConfigServiceUrl + ":" + assetConfigServicePort + assetConfigServiceFindAllPaginated;
			endpoint = endpoint.replace("$1", pageNumber).replace("$2", pageSize);
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			JsonArray jsonDeviceArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonDevice : jsonDeviceArray) {
				Device device = new Gson().fromJson(jsonDevice, Device.class);
				deviceList.add(device);
			}
			return deviceList;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static String countDevices() {
		LOGGER.log(Level.INFO, "Count Devices configured on MONGO DB");
		
		try {
			String endpoint = assetConfigServiceUrl + ":" + assetConfigServicePort + assetConfigServiceCount;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			JsonObject jsonResp = new Gson().fromJson(serviceResponse, JsonObject.class);
			String count = jsonResp.get("count").toString();
			return count;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static Device findDeviceById(String deviceId) {
		LOGGER.log(Level.INFO, "Get Devices configured on MONGO DB by id");
		
		try {
			List<Device> deviceList = new ArrayList<Device>();
			String endpoint = assetConfigServiceUrl + ":" + assetConfigServicePort + assetConfigServiceFindById + deviceId;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			if (serviceResponse == null || serviceResponse.isEmpty()) {
				LOGGER.log(Level.ERROR, "Device not found in DB");
			}
			JsonArray jsonDeviceArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonDevice : jsonDeviceArray) {
				Device device = new Gson().fromJson(jsonDevice, Device.class);
				deviceList.add(device);
			}
			if (deviceList.isEmpty()) {
				LOGGER.log(Level.ERROR, "Device list from DB is empty");
				return null;
			}
			return deviceList.get(0);	//Assuming there is only one json element with this name
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static List<Device> findDeviceByType(String deviceType) {
		LOGGER.log(Level.INFO, "Get Device configured on MONGO DB by type");

		try {
			List<Device> deviceList = new ArrayList<Device>();
			String endpoint = assetConfigServiceUrl + ":" + assetConfigServicePort + assetConfigServiceFindByType + deviceType;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			JsonArray jsonDeviceArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonDevice : jsonDeviceArray) {
				Device device = new Gson().fromJson(jsonDevice, Device.class);
				deviceList.add(device);
			}
			return deviceList;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static Device updateDevice(Device device) {
		LOGGER.log(Level.INFO, "Update Device configured on MONGO DB");

		try {
			String endpoint = assetConfigServiceUrl + ":" + assetConfigServicePort + assetConfigServiceUpdate;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String body = new Gson().toJson(device);
			String jsonDevice = RestUtils.consumePost(endpoint, body, MediaType.APPLICATION_JSON_TYPE, null);
			return new Gson().fromJson(jsonDevice, Device.class);
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
}
