package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfServiceAssetTemplate;
import it.eng.iot.hub.model.kapua.Asset;

public abstract class AssetTemplateManager {
	private static final Logger LOGGER = LogManager.getLogger(AssetTemplateManager.class);
	
	static String assetTemplateServiceUrl = ConfServiceAssetTemplate.getString("service.asset.template.address");
	static String assetTemplateServicePort = ConfServiceAssetTemplate.getString("service.asset.template.port");
	static String assetTemplateServiceFindAll = ConfServiceAssetTemplate.getString("service.asset.template.findAll");
	static String assetTemplateServiceFindAllPaginated = ConfServiceAssetTemplate.getString("service.asset.template.findAllPaginated");
	static String assetTemplateServiceCount = ConfServiceAssetTemplate.getString("service.asset.template.count");
	static String assetTemplateServiceFindByName = ConfServiceAssetTemplate.getString("service.asset.template.findByName");
	static String assetTemplateServiceFindByType = ConfServiceAssetTemplate.getString("service.asset.template.findByType");
	static String assetTemplateServiceSave = ConfServiceAssetTemplate.getString("service.asset.template.save");
	static String assetTemplateServiceUpdate = ConfServiceAssetTemplate.getString("service.asset.template.update");
	
	public static List<Asset> getAllAssetsPaginated(String pageNumber, String pageSize) {
		LOGGER.log(Level.INFO, "Get all Assets configured on MONGO DB paginated");
		
		try {
			List<Asset> assetList = new ArrayList<Asset>();
			String endpoint = assetTemplateServiceUrl + ":" + assetTemplateServicePort + assetTemplateServiceFindAllPaginated;
			endpoint = endpoint.replace("$1", pageNumber).replace("$2", pageSize);
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			
			//Type type = new TypeToken<Asset>(){}.getType();
			JsonArray jsonAssetArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonAsset : jsonAssetArray) {
				Asset asset = new Gson().fromJson(jsonAsset, Asset.class);
				assetList.add(asset);
			}
			return assetList;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static String countAssets() {
		LOGGER.log(Level.INFO, "Count Assets configured on MONGO DB");
		
		try {
			String endpoint = assetTemplateServiceUrl + ":" + assetTemplateServicePort + assetTemplateServiceCount;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			JsonObject jsonResp = new Gson().fromJson(serviceResponse, JsonObject.class);
			String count = jsonResp.get("count").toString();
			return count;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static Asset findAssetByName(String assetName) {
		LOGGER.log(Level.INFO, "Get Asset configured on MONGO DB by name");
		
		try {
			List<Asset> assetList = new ArrayList<Asset>();
			String endpoint = assetTemplateServiceUrl + ":" + assetTemplateServicePort + assetTemplateServiceFindByName + assetName;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			if (serviceResponse == null || serviceResponse.isEmpty()) {
				LOGGER.log(Level.ERROR, "Asset not found in DB");
			}
			
			JsonArray jsonAssetArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonAsset : jsonAssetArray) {
				Asset asset = new Gson().fromJson(jsonAsset, Asset.class);
				assetList.add(asset);
			}
			if (assetList.isEmpty()) {
				LOGGER.log(Level.ERROR, "Asset list from DB is empty");
				return null;
			}
			return assetList.get(0);	//Assuming there is only one json element with this name
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
	
	public static List<Asset> findAssetByType(String assetType) {
		LOGGER.log(Level.INFO, "Get Asset configured on MONGO DB by type");

		try {
			List<Asset> assetList = new ArrayList<Asset>();
			String endpoint = assetTemplateServiceUrl + ":" + assetTemplateServicePort + assetTemplateServiceFindByType + assetType;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String serviceResponse = RestUtils.consumeGet(endpoint);
			
			JsonArray jsonAssetArray = new JsonParser().parse(serviceResponse).getAsJsonArray();
			for(JsonElement jsonAsset : jsonAssetArray) {
				Asset asset = new Gson().fromJson(jsonAsset, Asset.class);
				assetList.add(asset);
			}
			return assetList;
		} catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
		return null;
	}
}
