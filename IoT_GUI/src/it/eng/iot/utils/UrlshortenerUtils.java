package it.eng.iot.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;


import com.sun.jersey.api.client.ClientResponse;

import it.eng.iot.configuration.ConfTools;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Level;

public class UrlshortenerUtils {
	
private static final Logger LOGGER = LogManager.getLogger(UrlshortenerUtils.class);
	
	private static ClientResponse resp;
	private static final String ENCODE_URL = ConfTools.getString("urlshortener.url.encode");
	private static final String DECODE_URL = ConfTools.getString("urlshortener.url.decode");
	
	public static String encodeUrl(String url) {
		
		JSONObject body = new JSONObject();
		body.put("url",url);
		String requestUrl = ENCODE_URL;
		LOGGER.log(Level.INFO," URL : " + url);
		Map<String, String> configHeaders = new HashMap<String, String>();
		String encodedUrlResponse = "";
	
		try {
			
			resp =  RestUtils.consumePostAnyResponse(requestUrl, body.toString(), configHeaders);
			String response = resp.getEntity(String.class);
			JSONObject jsonObject = new JSONObject(response);
			
			if(null!=jsonObject) {
				
				LOGGER.log(Level.INFO, jsonObject);
				encodedUrlResponse = jsonObject.getString("Encoded");
				
			}
			
		} catch (Exception e) {
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
		
		return encodedUrlResponse;
	}
	
	public static String decodeUrl(String url) {
		
		JSONObject body = new JSONObject();
		body.put("url",url);
		String requestUrl = DECODE_URL;
		Map<String, String> configHeaders = new HashMap<String, String>();
		String decodedUrlResponse = "";
		
		try {
				
			resp = RestUtils.consumePostAnyResponse(requestUrl, body, configHeaders);
			String response = resp.getEntity(String.class);
			JSONObject jsonObject = new JSONObject(response);
				
			if(null!=jsonObject) {
				
				LOGGER.log(Level.INFO, jsonObject);
				decodedUrlResponse = jsonObject.getString("Decoded");
					
			}
				
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e);
		} 
			
		return decodedUrlResponse;
	}
	
	public static String getForbiddenChars(String inputString, String[] items) {
		
		String forbiddenChars = "";
		List<String> forbiddenCharsList= new ArrayList<String>();
		
		for(int i =0; i < items.length; i++){
	        if(inputString.contains(items[i])){
	        	forbiddenCharsList.add(items[i]);
	        }
	    }
	    
		if(forbiddenCharsList.size() > 0) 
			forbiddenChars = String.join(", ",forbiddenCharsList);
		
		
		
		return forbiddenChars;
	}
}