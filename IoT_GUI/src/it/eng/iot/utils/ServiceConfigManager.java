package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfIdas;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.DataformatProtocol;
import it.eng.iot.servlet.model.MapCenter;
import it.eng.tools.Idas;
import it.eng.tools.Orion;
import it.eng.tools.TenantManager;
import it.eng.tools.model.CategoryDTO;
import it.eng.tools.model.ContextCategoryDTO;
import it.eng.tools.model.EntityAttribute;
import it.eng.tools.model.IdasDeviceGetResult;
import it.eng.tools.model.IdasDeviceListGetResult;
import it.eng.tools.model.IdasDeviceStaticAttribute;
import it.eng.tools.model.Scope;
import it.eng.tools.model.ScopeDTO;
import it.eng.tools.model.ServiceEntityBean;

public abstract class ServiceConfigManager {
	
	private static final Logger LOGGER = LogManager.getLogger(ServiceConfigManager.class);
	
	private static Orion orion;
		
	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}
		
	
	
	/*
	 * Delete Service: 
	 *  - Action1 : Delete the service on IDAS 
	 *  - Action2 : Delete the service configuration on ORION 
	 *  - Action3 : Delete all devices linked to the service
	 *  
	 */
	public static boolean deleteServiceProcess(String headerService, String headerServicePath, CategoryDTO serviceConfig)  {
		boolean deletedIotService = true;
		boolean simulatorActive = Boolean.parseBoolean(ConfTools.getString("simulator.active"));
		
			

			String apikey = serviceConfig.getApikey();
			String resource = ConfIdas.getString("iota.resource");

			//If a simulation is running, stop it!
			if (simulatorActive) {
				// 0. STOP CONTEXT SIMULATION
				JSONObject simStopped  = IotsimUtils.stopSimulation(headerService, headerServicePath);
				LOGGER.log(Level.INFO,"IotsimUtils.consumeGet result {}" , simStopped);
			}
			
			
			for (DataformatProtocol dataformat: DataformatProtocol.values()) {
				
				
				String dataformatProtocol = dataformat.toString();
				
				if (!CommonUtils.isEnabledAgent(dataformatProtocol))
					continue;
				
				
				try {
				
					deletedIotService = false;
							
							// SWITCH AGENT // 
							Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
							try {
								// 1. DELETE SERVICE ON IDAS
								deletedIotService = idas.deleteService(headerService, headerServicePath, resource, apikey);
							}catch (Exception ex) {
								LOGGER.log(Level.ERROR, ex.getMessage());
							}
							// 3. GET DEVICES AND DELETE THE RELATED SERVICE DEVICES ON IDAS
							IdasDeviceListGetResult devicelist = idas.getDeviceList(headerService, headerServicePath);
							for(IdasDeviceGetResult dev : devicelist.getDevices()){
								try {
									String deviceId = dev.getDevice_id();
									String dataformat_protocol = dev.getProtocol();
									LOGGER.log(Level.INFO,"dataformat_protocol {}" , dataformat_protocol);
									
									Set<IdasDeviceStaticAttribute> staticAttrs = dev.getStatic_attributes();
									
									boolean isCamera=false; 
									for (IdasDeviceStaticAttribute staticAttr:staticAttrs) {
										
										if (staticAttr.getName().equalsIgnoreCase("streaming_url")) {
											if (!("").equals(staticAttr.getValue())) {
												isCamera = true;
											}
											break;
										}
									}
									
									DeviceConfigManager.deleteDeviceProcess(headerService, headerServicePath, deviceId, dataformat_protocol, isCamera);
								} 
								catch (Exception ex) {
									LOGGER.log(Level.ERROR, ex.getMessage());
								}
							}
			
			
			}catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
				if (deletedIotService) {
					// TODO ROOLBACK ON IDAS
				}
			}


			} 
			

		return deletedIotService ;
	}
	
		
	
	/**
	 * @param headerService
	 * @param headerServicePath
	 * @param service
	 * @return
	 */
	private static String getOCBCategories (String headerService, String headerServicePath, String service ) {
		
		Set<BasicNameValuePair> params = new HashSet<>();
		params.add(new BasicNameValuePair("type", "urbanservice"));
		params.add(new BasicNameValuePair("q", "refScope=='"+service+"'"));

		return  orion.getEntities(headerService, headerServicePath, params);
		
	}
	
	private static ExecutorService serviceTh = Executors.newCachedThreadPool();

	/**
	 * @param headerService
	 * @param headerServicePath
	 * @param service
	 * @return
	 */
	public static CompletableFuture<String> asyncgetOCBCategories(String headerService, String headerServicePath, String service){
	    return CompletableFuture.supplyAsync(() -> getOCBCategories ( headerService,  headerServicePath, service ), serviceTh);
	}
	
	

	
	/** 
	 * Allows to get the scopes permitted 
	 * according to the configured roles internal to the user organization
	 * as defined into IDM and containing the configured string 
	 * filterscope.convention.string set into the IDM configuration properties file 
	 */	
	public static Set<ServiceEntityBean> getPermittedServices(Set<ServiceEntityBean> services, Set<String> orgs) {
	
			Set<ServiceEntityBean> serviceSubset = new HashSet<>();
	//		String scopeConventionString = ConfIDM.getString("filterscope.convention.string").trim().toLowerCase();
			
			for(String org : orgs) {
				for(ServiceEntityBean serv : services) {
					if(serv.getId().equalsIgnoreCase(org)) {
						serviceSubset.add(serv);
						break;
					}
				}
			}
			services.retainAll(serviceSubset);
	
		LOGGER.log(Level.INFO,"==================================");
		LOGGER.log(Level.INFO,"Number of permitted scopes: {}" , services.size());
		return services;
	}
		
	
	/**
	 * @param fiwareservice
	 * @param token
	 * @return
	 */
	public static Set<String> getAllServicePathIdByService (String fiwareservice, String token){
		
		
		Set<String> ids = new HashSet<>();
		Set<ServiceEntityBean> categoriesS = ServiceConfigManager.getCategoriesTM(fiwareservice, false, token);
		
		 
		 for (ServiceEntityBean category:categoriesS) {
			 
			 ids.add(category.getId());
		 }
		 
		 return ids;
		 
	}

	/**
	 * @param scopes
	 * @param service
	 * @return
	 */
	public static String getEnablerByService (Set<ServiceEntityBean> scopes, String service) {
	
	
	
		for (ServiceEntityBean scope:scopes) {
			if (scope.getId().equalsIgnoreCase(service))			
					return scope.getRefScope().getValue();
		}
		
		return "";
	
	}

	
	//*********   TENANT MANAGER ************
	
	/**
	 * get all contexts(services) from Tenant Manager
	 * 
	 * @return
	 */
	public static Set<ServiceEntityBean> getServicesTM(){
		
		String resp =  TenantManager.getAllScopes();
		
		Type typeScope = new TypeToken<Set<ScopeDTO>>(){}.getType();
		Set<ScopeDTO> apiResp = new Gson().fromJson(resp, typeScope);
		
		Set<ServiceEntityBean> setServiceEntity = new HashSet<>();
		EntityAttribute<MapCenter> mapCenterEntity = null;
		Date date = new Date();
		
		for(ScopeDTO scope:apiResp) {
			
			Double latitude = Double.parseDouble(scope.getScope().getLat());
			Double longitude = Double.parseDouble(scope.getScope().getLng());
			Integer zoom = Integer.parseInt(scope.getScope().getZoom());
			
			MapCenter mapCenter = new MapCenter(latitude,longitude, zoom);
			mapCenterEntity = new EntityAttribute<MapCenter>(mapCenter);
			
			String scopeId = scope.getScope().getId();
			String service = scope.getScope().getService();
			String scopeName = scope.getScope().getName();
			String enabler = scope.getEnabler();
			
			ServiceEntityBean newServiceEntity = new ServiceEntityBean(
					scopeId,
					"scope", //scope type name
					new EntityAttribute<String>(service), // service
					null, // subservice
					null, // entity_type
					null, // api_key
					null, // resource
					mapCenterEntity, // map_center
					new EntityAttribute<String>(scopeName), // name
					new EntityAttribute<Date>(date), // dateModified
					null, // opType
					new EntityAttribute<String>(enabler) // refScope
			);
			
			setServiceEntity.add(newServiceEntity);
		}
		
		return setServiceEntity;
	}
	
	/**
	 *  get all categories(service_paths) from Tenant Manager
	 * 
	 * @param currentServiceId
	 * @return
	 */
	public static Set<ServiceEntityBean> getServicePathsTM(String currentServiceId){
		
		String resp =  TenantManager.getAllUrbanscopes(currentServiceId);
		
		Type typeCategory = new TypeToken<Set<CategoryDTO>>(){}.getType();
		Set<CategoryDTO> apiResp = new Gson().fromJson(resp, typeCategory);
		
		Set<ServiceEntityBean> setServiceEntity = new HashSet<>();
		EntityAttribute<MapCenter> mapCenterEntity = null;
		Date date = new Date();
		
		for(CategoryDTO category:apiResp) {
			
			Double latitude = Double.parseDouble(category.getRefEnabler4scope().getRefScope().getLat());
			Double longitude = Double.parseDouble(category.getRefEnabler4scope().getRefScope().getLng());
			Integer zoom = Integer.parseInt(category.getRefEnabler4scope().getRefScope().getZoom());
			
			MapCenter mapCenter = new MapCenter(latitude,longitude, zoom);
			mapCenterEntity = new EntityAttribute<MapCenter>(mapCenter);
			
			String urbanserviceId = category.getId();
			String service = category.getRefEnabler4scope().getRefScope().getService();
			String subservice = category.getId();
			String urbanserviceName = category.getName();
			String context = category.getRefEnabler4scope().getRefEnabler().getName();
			String apiKey = category.getApikey();
			ServiceEntityBean newServiceEntity = new ServiceEntityBean(
					urbanserviceId,
				    "urbanservice", // type
					new EntityAttribute<String>(service), // service
					new EntityAttribute<String>(subservice), // subservice
					null, // entity_type
					new EntityAttribute<String>(apiKey), // api_key
					null, // resource
					mapCenterEntity, // map_center
					new EntityAttribute<String>(urbanserviceName), // name
					new EntityAttribute<Date>(date), // dateModified
					null, // opType
					new EntityAttribute<String>(context) // refScop
			);
			
			setServiceEntity.add(newServiceEntity);
		}
		
		return setServiceEntity;
	}
	
	/**
	 * get context(service) Name from Tenant Manager
	 * 
	 * @param serviceId
	 * @return
	 */
	public static String getServiceNameByServiceTM( String serviceId){
		
		String serviceName = "";
		String resp =  TenantManager.getScopeById(serviceId);
		
		Type type = new TypeToken<ScopeDTO>(){}.getType();
		ScopeDTO apiResp = new Gson().fromJson(resp, type);
		
		serviceName = apiResp.getScope().getName();
		
		LOGGER.log(Level.INFO,"Service: {} Service name: {}" , serviceId , serviceName);
		return serviceName;
	}
	
	/**
	 * get category(service_path) Name from Tenant Manager
	 * 
	 * @param urbanserviceId
	 * @param serviceId
	 * @return
	 */
	public static String getServicePathNameTM(String urbanserviceId, String serviceId){
	 
		String servicePathName = "";
		String resp =  TenantManager.getUrbanserviceById(serviceId, urbanserviceId);
		
		Type type = new TypeToken<CategoryDTO>(){}.getType();
		CategoryDTO apiResp = new Gson().fromJson(resp, type);
		
		servicePathName = apiResp.getName();
						
		LOGGER.log(Level.INFO,"Urbanservice: {}  Urbanservice name: {}" , urbanserviceId ,  servicePathName);
		return servicePathName;
	}
	
	/* get category(service_path) APIkey from Tenant Manager by specific Id of the contexts and category*/
	/**
	 *  get category(service_path) APIkey from Tenant Manager by specific Id of the contexts and category
	 * 
	 * @param fiwareService
	 * @param fiwareServicepath
	 * @return
	 */
	public static String getServicePathApiKeyTM(String fiwareService, String fiwareServicepath){
	 
		String servicePathApiKey = "";
		String resp =  TenantManager.getUrbanserviceById(fiwareService, fiwareServicepath);
		
		Type type = new TypeToken<CategoryDTO>(){}.getType();
		CategoryDTO apiResp = new Gson().fromJson(resp, type);
		
		servicePathApiKey = apiResp.getApikey();
						
		LOGGER.log(Level.INFO,"Urbanservice: {} APIkey: {}" , fiwareServicepath , servicePathApiKey);
		return servicePathApiKey;
	}
	
   /* get Enabler for specific Context by Id from Tenant Manager */
   public static String getEnablerTM(String id) {
	   
	    String enabler = "";
	    String resp =  TenantManager.getScopeById(id);
	    
	    Type type = new TypeToken<ScopeDTO>(){}.getType();
	    ScopeDTO apiResp = new Gson().fromJson(resp, type);
	   
	    enabler = apiResp.getEnabler();
	   
		return enabler;
		
	}
   
	 /**
	 * get all categories(service_paths) IDs for specific context(sevice) by serviceId from Tenant Manager
	 * @param service
	 * @return
	 */
	public static Set<String> getAllServicePathIdByServiceTM(String service){
		
		Set<String> servicePaths = new HashSet<>();
		String resp =  TenantManager.getAllUrbanscopes(service);
		
		Type type = new TypeToken<Set<CategoryDTO>>(){}.getType();
		Set<CategoryDTO> apiResp = new Gson().fromJson(resp, type);
		
		 for (CategoryDTO category:apiResp) {
			 
			servicePaths.add(category.getId());
		 }
	
		return servicePaths;
	}
   
	 /**
	 * get all categories(service_paths) from Tenant Manage
	 * @param currServices
	 * @param async
	 * @param token
	 * @return
	 */
	public static Set<ServiceEntityBean> getCategoriesTM(String currServices, boolean async, String token) {
	  
	   String[] services = currServices.split(",");
		Set<ServiceEntityBean> sResp = new HashSet<>();
		List<CompletableFuture<Void>> generalCFs = new ArrayList<CompletableFuture<Void>>();
		Set<ServiceEntityBean> resp= new HashSet<>();
		
		for (String service:services) {
			
			if (!async) {

				if(service!=null && !service.equals("")) {
					resp = getServicePathsTM(service);
					sResp.addAll(resp);	
				}
				
			}else {

				if(service!=null && !service.equals("")) {
					CompletableFuture<Void> cf = asyncgetOCBCategoriesTM(service).thenAcceptAsync(respCF -> 
							sResp.addAll(respCF)	
						);
					generalCFs.add(cf);
				}
			 }
		}

		if (async) {
		
			CompletableFuture<Void> combinedFuture =CompletableFuture.allOf(generalCFs.toArray(new CompletableFuture[generalCFs.size()]));
			 
			 try {
				combinedFuture.get();//wait for the end of all CompletableFuture
				LOGGER.log(Level.INFO, "getCategories All  CompletableFuture finished.");
			} catch (InterruptedException | ExecutionException e) {
				LOGGER.log(Level.ERROR,"getCategoriesTM error: {} ", e.getMessage());
			}
		 
		}
		
		return sResp;
	}
   
   	/**
   	 * @param service
   	 * @return
   	 */
    public static CompletableFuture<Set<ServiceEntityBean>> asyncgetOCBCategoriesTM(String service){
	    return CompletableFuture.supplyAsync(() -> getServicePathsTM ( service ), serviceTh);
	}
   	
   	/**
   	 * modify map center on Tenant Manager
   	 * 
   	 * @param service
   	 * @param mapcenter
   	 * @return
   	 * @throws Exception
   	 */
   	public static boolean modifyMapCenterTM(String service, MapCenter mapcenter) throws Exception {
		
		boolean out = false;
		
		String resp =  TenantManager.getScopeById(service);
		
		Type type = new TypeToken<ScopeDTO>(){}.getType();
		ScopeDTO scope = new Gson().fromJson(resp, type);
		
			
		Scope scopeBody = new Scope(
				scope.getScope().getId(), 						//id
				String.valueOf(mapcenter.getLat()), 			//latitude
				String.valueOf(mapcenter.getLng()),				//longitude
				scope.getScope().getZoom(), 					//zoom
				scope.getScope().getName(), 					//name
				scope.getScope().getService(),  				// service
				scope.getScope().getImageUrl() 				    //url image
		);
				
		out = TenantManager.updateScopeById( service, scopeBody);
		
		return out;
   }
   
	
 /**
 * get all couples fiware-service fiware-servicepath from TenantManager
 * 
 * @return
 */
public static List<ContextCategoryDTO> getAllCouplesContextCategory(){
	   
	   List<ContextCategoryDTO> ret = new ArrayList<>();
	   Set<ServiceEntityBean> services = ServiceConfigManager.getServicesTM();
		
	   services.
	   stream().
	   parallel().
	   forEach( service -> {
		   
		   					LOGGER.info("service {} ",service.getService().getValue());
		   
	   		   				Set<ServiceEntityBean> paths = ServiceConfigManager.getServicePathsTM (service.getService().getValue());
	   		   				
	   		   				paths.
	   		   				stream().
	   		   				parallel().
	   		   				forEach(path -> {
	   		   					
				   		   				LOGGER.info("service {} servicepath  {}",path.getService().getValue(), path.getSubservice().getValue() );
										
										ContextCategoryDTO current = new ContextCategoryDTO (path.getService().getValue(), path.getSubservice().getValue());
										ret.add(current);
	   		   						}
	   		   				);	
	   					}
			   
			   );//forEach
	   
	   
	   return ret;
	   
   }
   	
   	
	
	
}
