package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;

import it.eng.iot.configuration.ConfOrionCB;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.SubcriptionSubjectEntity;
import it.eng.iot.servlet.model.Subscription;
import it.eng.iot.servlet.model.SubscriptionNotification;
import it.eng.iot.servlet.model.SubscriptionNotificationHttpCustom;
import it.eng.iot.servlet.model.SubscriptionSubject;
import it.eng.iot.servlet.model.SubscriptionSubjectCondition;
import it.eng.iot.servlet.model.SubscriptionSubjectConditionExpression;
import it.eng.tools.Orion;
import it.eng.tools.model.DeviceEndpointSubscriptionEntity;
import it.eng.tools.model.DeviceSubscriptionEntity;
import it.eng.tools.model.EntityAttribute;
import it.eng.tools.model.ServiceSubscriptionEntity;


public abstract class SubscriptionManager {
	
	private static final Logger LOGGER = LogManager.getLogger(SubscriptionManager.class);

	static String orionHostURL = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host");
	static String orionEntity = ConfOrionCB.getString("orion.v2.entities");
	static String orionSubscription = ConfOrionCB.getString("orion.v2.subscriptions");
	static String notificationHttpURL = ConfOrionCB.getString("orion.notification.http.url");
	

	

	// Type of expression to define the conditions in the subscription
	static String expressionTypeQ = "q";
	static String expressionTypeMq = "mq";
	static String expressionTypeGeorel = "georel";
	static String expressionTypeGeometri = "geometri";
	static String expressionTypeCoords = "coords";
	
	public static final String HTTP2_KAFKA_SUB_TYPE = "http2kafka";
	private static final String HTTP2KAFKA_ADDRESS_ON_ORION_PROTOCOL = ConfTools.getString("http2kafka.api.address.onorion.protocol");
	private static final String HTTP2KAFKA_ADDRESS_ON_ORION_HOST = ConfTools.getString("http2kafka.api.address.onorion.host");
	private static final String HTTP2KAFKA_ADDRESS_ON_ORION_PORT = ConfTools.getString("http2kafka.api.address.onorion.port");
	private static final String HTTP2KAFKA_ADDRESS_ON_ORION_PATH = ConfTools.getString("http2kafka.api.address.onorion.path");
	
	
	
	private static final String LOCATION = "Location";
	private static final String FIWARE_SERVICE = "fiware-service";
	private static final String FIWARE_SERVICEPATH = "fiware-servicepath";
	private static final String SUBSCRIPTION_ID = "subscriptionId";
	private static final String MESSAGE = "message";
	
	
	public enum SubscriptionsTarget {
		CUSTOM,
	    MEASURE_NOTIFICATION,
	    BIG_DATA,
	 }
	


	private static Orion orion;

	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}


	
	/**
	 * @param deviceId
	 * @param url
	 * @param notificationAttrs
	 * @param headerService
	 * @param headerServicePath
	 * @return
	 * @throws Exception
	 */
	public static String createDeviceSubscription(String deviceId, String url, HashSet<String> notificationAttrs, String headerService, String headerServicePath, Integer  throttling, boolean isInternal) throws Exception {
		LOGGER.log(Level.INFO, "Create Subscription onDevice on ORION CONTEXT BROKER");
		// Composing the subscriptionBean with the client parameters

		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionSubscription;
			LOGGER.log(Level.INFO, "Invoking {}" , endpoint);

			Subscription subscription = new Subscription();
			// Create the subscription

			// Description
			
			if (isInternal)
				subscription.setDescription("Internal_Subscprition_onDevice " + deviceId);
			else
				subscription.setDescription("Subscprition onDevice " + deviceId);

			// Status
			subscription.setStatus("active");

			//subjectEntity
			SubcriptionSubjectEntity subjectEntity = new SubcriptionSubjectEntity();
			String id = deviceId;
			String type = "Device";
			subjectEntity.setId(id);
			subjectEntity.setType(type);

			Set<SubcriptionSubjectEntity> setSubcriptionSubjectEntity = new HashSet<>();
			setSubcriptionSubjectEntity.add(subjectEntity);

			SubscriptionSubject subject= new SubscriptionSubject();
			subject.setEntities(setSubcriptionSubjectEntity);

			//subjectCondition - conditionAttrs
			SubscriptionSubjectCondition subjectCondition = new SubscriptionSubjectCondition();
			HashSet<String> conditionAttrs = new HashSet<>();
			conditionAttrs.add("dateModified");
			subjectCondition.setAttrs(conditionAttrs);

			//subjectCondition - expression
			SubscriptionSubjectConditionExpression expression = new SubscriptionSubjectConditionExpression();
			String conditionExpression = "opType==ready,deleted"; 
			expression.setQ(conditionExpression);
			subjectCondition.setExpression(expression);
			subject.setCondition(subjectCondition);
			LOGGER.log(Level.INFO, "subjectCondition: {}" , subjectCondition);

			subscription.setSubject(subject);
			
			
			if ( throttling != null &&  throttling > 0)
				subscription.setThrottling(throttling);
			

			// notification
			SubscriptionNotification subscriptionNotification = new SubscriptionNotification();

			subscriptionNotification.setTimesSent(2);
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			subscriptionNotification.setLastNotification(timeStamp);

			// notification - attrs 
			subscriptionNotification.setAttrs(notificationAttrs);

			subscriptionNotification.setAttrsFormat("normalized");
			
			subscriptionNotification.setOnlyChangedAttrs(true);

			SubscriptionNotificationHttpCustom httpCustom = new SubscriptionNotificationHttpCustom();
			httpCustom.setUrl(url);

			httpCustom.putHeader("contextUrl", orion.getBaseUrl()+orion.getPathEntities());
			
			subscriptionNotification.setHttpCustom(httpCustom);
			
			subscription.setNotification(subscriptionNotification);

			// Transform the bean into json
			String subscriptionJson = beanToJson(subscription);
			LOGGER.log(Level.INFO, "SubscriptionJson {}" , subscriptionJson);

			Map<String, String> configHeaders = new HashMap<>();
			configHeaders.put(FIWARE_SERVICE, headerService);
			configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
			LOGGER.log(Level.INFO, "theConfigHaders  {}" , configHeaders);

			LOGGER.log(Level.INFO, "body:  {}" , subscriptionJson);

			ClientResponse clientResponse = RestUtils.consumePostFull(endpoint, subscriptionJson, configHeaders);

			String location = clientResponse.getHeaders().getFirst(LOCATION);
			String subscriptionId = location.substring(location.lastIndexOf("/")+1, location.length());
			LOGGER.log(Level.INFO, "Subscription {} successfully created!", subscriptionId);

			serviceResponse = clientResponse.getEntity(String.class);

			try {
				jResp = new JSONObject(serviceResponse);
				jResp.put(SUBSCRIPTION_ID, subscriptionId);
			} catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				jResp = new JSONObject();
				jResp.put(MESSAGE, serviceResponse);
				jResp.put(SUBSCRIPTION_ID, subscriptionId);
				
			}

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return jResp.toString();
	}

	/*
	 * Sets the status of the subscription.
	 * 
	 * ENDPOINT: http://{{host}}:1026/v2/subscriptions/58a2f9d8701698a211c76f6b
	 * 
	 * METHOD: PATCH
	 * HEADERS:
	 * fiware-service: air
	 * fiware-servicepath: /naples
	 * Content-Type: application/json
	 * 
	 * BODY
	 * {   "status": "active" }
	 * 
	 */

	public static boolean changeSubscriptionStatus(String headerService, String headerServicePath, String subscriptionId, String status) throws Exception {

		headerServicePath=CommonUtils.normalizeServicePath(headerServicePath);
		
		LOGGER.log(Level.INFO, SubscriptionManager.class.getName());
		LOGGER.log(Level.INFO, "Going to change Subscription Status on ORION CONTEXT BROKER");
		LOGGER.log(Level.INFO, "Subscription id {}" , subscriptionId );

		String endpoint = orionHostURL+orionSubscription;

		if (subscriptionId!=null) endpoint = endpoint.concat("/").concat(subscriptionId);
		LOGGER.log(Level.INFO, "Invoking URL {}" , endpoint);

		Map<String, String> configHeaders = new HashMap<>();
		configHeaders.put(FIWARE_SERVICE, headerService);
		configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
		configHeaders.put("Content-Type", MediaType.APPLICATION_JSON);
		LOGGER.log(Level.INFO, "TheConfigHaders {} " , configHeaders);
		JSONObject json = new JSONObject();
		json.put("status", status);
		
		LOGGER.log(Level.INFO, "ThePayload {}" ,json.toString());

		RestUtils.consumePatch(endpoint, json, configHeaders);

		LOGGER.log(Level.INFO, "Invocation completed");
		return true;

	}



	/*
	 * Get Subscription on orion Context Broker 
	 * METHOD: GET
	 * ENDPOINT: {{host}}:1026/v2/subscriptions/<subscriptionId>
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */
	public static Subscription getSubscription(String headerService, String headerServicePath, String subscriptionId) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getSubscription by subscriptionId");

		JSONObject jResp = new JSONObject();
		String endpoint = orionHostURL+orionSubscription;
		Subscription apiResp = new Subscription();

		try {
			Map<String, String> headers = new HashMap<>();
			headers.put(FIWARE_SERVICE, headerService);
			headers.put(FIWARE_SERVICEPATH, "/"+headerServicePath);
			LOGGER.log(Level.INFO, "theHaders {}" , headers);

			endpoint=endpoint+"/"+ subscriptionId;
			LOGGER.log(Level.INFO, "Invoking {} " , endpoint);
			String resp = RestUtils.consumeGet(endpoint, headers);

			Type type = new TypeToken<Subscription>(){}.getType();
			apiResp = new Gson().fromJson(resp, type);

			LOGGER.log(Level.INFO, "Subscription: {}" , apiResp);

		} catch (org.json.JSONException jsone) {
			jResp = new JSONObject();
			jResp.put("message: ", jsone.getMessage());
			LOGGER.log(Level.ERROR, jsone.getMessage());
		}
		return apiResp;
	}


	/**
	 * Get Subscription for the specific deviceId among the entities on Orion Context Broker 
	 * METHOD: GET
	 * ENDPOINT: http://{{host}}:1026/v2/entities/?type=DeviceSubscription&q=deviceId==<deviceId>
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */
	public static Set<DeviceSubscriptionEntity> getDeviceSubscriptions(String headerService, String headerServicePath, String deviceId) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getDeviceSubscriptions");

		Set<BasicNameValuePair> params = new HashSet<>();
		params.add(new BasicNameValuePair("type", "DeviceSubscription"));	
		params.add(new BasicNameValuePair("q", "deviceId=='"+deviceId+"'"));

		String resp =  orion.getEntities(headerService, headerServicePath, params);

		Type type = new TypeToken<Set<DeviceSubscriptionEntity>>(){}.getType();
		Set<DeviceSubscriptionEntity> devSubEntities = new Gson().fromJson(resp, type);
		
		//Retrieve the subscription status
		Set<DeviceSubscriptionEntity> devSubEntitiesRet = new HashSet<>();
		for (DeviceSubscriptionEntity  devSubEntity : devSubEntities) {
			
			 Subscription subscription = getSubscription(headerService, headerServicePath, devSubEntity.getSubscriptionId().getValue());
			 String status = subscription.getStatus();
			 devSubEntity.setStatus(new EntityAttribute<String>(status));
			 devSubEntitiesRet.add(devSubEntity);
		}
		

		LOGGER.log(Level.INFO, "subscription Ids: {}" , devSubEntitiesRet);
		return devSubEntitiesRet;
	}

	
	/**
	 * @param headerService
	 * @param headerServicePath
	 * @param deviceId
	 * @param withoutInternal
	 * @return
	 * @throws Exception
	 */
	public static Set<DeviceEndpointSubscriptionEntity> getDeviceEndpointSubscriptions(String headerService, String headerServicePath, String deviceId, boolean withoutInternal, String target) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getDeviceEndpointSubscriptions");

		Set<BasicNameValuePair> params = new HashSet<>();
		

		
		//It doesn't work , it filtes only on deviceId if I put the target at the end of the URL it works, but I cannot know the order of the parameters
//		if (!target.equals("") )
//			params.add(new BasicNameValuePair("q", "target=='"+target+"'"));
		
		params.add(new BasicNameValuePair("type", "DeviceEndpointSubscription"));	
		params.add(new BasicNameValuePair("q", "deviceId=='"+deviceId+"'"));

		String resp =  orion.getEntities(headerService, headerServicePath, params);

		Type type = new TypeToken<Set<DeviceEndpointSubscriptionEntity>>(){}.getType();
		Set<DeviceEndpointSubscriptionEntity> devSubEntities = new Gson().fromJson(resp, type);
		
		//Retrieve the subscription status
		Set<DeviceEndpointSubscriptionEntity> devSubEntitiesRet = new HashSet<>();
		for (DeviceEndpointSubscriptionEntity  devSubEntity : devSubEntities) {
					
			 Subscription subscription = getSubscription(headerService, headerServicePath, devSubEntity.getSubscriptionId().getValue());
			 
			 if (withoutInternal&& subscription.getDescription().contains("Internal_Subscprition_onDevice")) 
				 	continue;
			 
			 
			 String status = subscription.getStatus();
			 devSubEntity.setStatus(new EntityAttribute<String>(status));
			 devSubEntitiesRet.add(devSubEntity);
		}

		LOGGER.log(Level.INFO, "subscription Ids: {}" , devSubEntitiesRet);
		return devSubEntitiesRet;
	}


	/** 
	 * Delete the subscriptions existing for the specific deviceId
	 * - STEP 1 - Get the deviceSubscriptionEntity for the specific device
	 * - STEP 2 - Delete the subscription on Orion
	 * - STEP 3 - Delete the entity having type=DeviceSubscription and deviceId=<deviceId>
	 * 			  The DeviceSubscription Entity has the relationship among deviceId-subscriptionId-mashupId
	 * 			  The DeviceEndpointSubscriptionEntity Entity has the relationship among deviceId-subscriptionId-endpoint (generic not mashup)
	 * DeviceSubscription
	 */

	public static boolean deleteDeviceSubscriptions(String headerService, String headerServicePath, String deviceId) throws Exception {

		boolean out = true;
		
		boolean withoutInternal = false;
		
		try{
			
			headerServicePath = CommonUtils.normalizeServicePath(headerServicePath);
			
			Set<DeviceSubscriptionEntity> subscriptions = getDeviceSubscriptions(headerService, headerServicePath, deviceId);

			for (DeviceSubscriptionEntity subscription : subscriptions){
				deleteDeviceSubscription(headerService,headerServicePath, deviceId, subscription.getSubscriptionId().getValue());
			}
			
			Set<DeviceEndpointSubscriptionEntity> endpointSubscriptions = getDeviceEndpointSubscriptions(headerService, headerServicePath, deviceId, withoutInternal, "");

			for (DeviceEndpointSubscriptionEntity endpointSubscription : endpointSubscriptions){
				deleteDeviceSubscription(headerService,headerServicePath, deviceId, endpointSubscription.getSubscriptionId().getValue());
			}

		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
			out = false;
		}

		return out;
	}
	
	
	
	
	public static boolean deleteDeviceSubscription(String headerService, String headerServicePath, String deviceId, String subscriptionId) throws Exception {

		boolean out = true;

		try{
			
			String entitiyId = deviceId+"_"+subscriptionId;

			// STEP 2: Delete orion subscription
			LOGGER.log(Level.INFO, "Deleting subscription Id: {}" , subscriptionId);
			deleteEntityOnOrion(orionHostURL+orionSubscription,  subscriptionId,  headerService,  headerServicePath);

			// STEP 3: Delete orion entity DeviceSubscription
			LOGGER.log(Level.INFO, "Deleting entity Id: {}" , entitiyId);
			deleteEntityOnOrion(orionHostURL+orionEntity,  entitiyId,  headerService,  headerServicePath);

		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
			out = false;
		}

		return out;
	}
	
	

	/*
	 * Delete an entity on Orion Context Broker 
	 * METHOD: DELETE
	 * ENDPOINT: {{host}}:1026/v2/subscriptions/
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */

	public static boolean deleteEntityOnOrion(String orionUrlPath, String entityId, String headerService, String headerServicePath) throws Exception {

		LOGGER.log(Level.DEBUG, "Deleting entity {} on Orion...", entityId);
		ClientResponse serviceResponse = null;
		String endpoint = "";

		if (entityId!=null) endpoint = orionUrlPath.concat("/").concat(entityId);

		LOGGER.log(Level.DEBUG, "Invoking URL {}" , endpoint);

		Map<String, String> configHeaders = new HashMap<>();
		configHeaders.put(FIWARE_SERVICE, headerService);
		configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
		LOGGER.log(Level.DEBUG, "TheConfigHaders {}" , configHeaders);

		boolean out = true;
		try{
			serviceResponse = RestUtils.consumeDelete(endpoint, configHeaders);
			if (serviceResponse.getStatus() != 200 && serviceResponse.getStatus() != 201
					&& serviceResponse.getStatus() != 301 && serviceResponse.getStatus() != 204
					&& serviceResponse.getStatus() != 404) {
				throw new Exception("URL " + endpoint + " responded with status " + serviceResponse.getStatus()
				+ " and message: " + serviceResponse.getEntity(String.class));
			}
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
			out = false;
		}
		LOGGER.log(Level.DEBUG, "Invocation completed: {}" , out);
		return out;
	}


	/* Create an entity on orion with the relationship between device and subscription */
	public static String createDeviceSubscriptionEntity(String deviceId, String subscriptionId, String mashupId, String headerService, String headerServicePath) throws Exception {
		
		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionEntity;
			LOGGER.log(Level.INFO, "Invoking  {}" , endpoint);

			DeviceSubscriptionEntity entity = new DeviceSubscriptionEntity();
			// Create the device subscription entity
			entity.setId(deviceId+"_"+subscriptionId); 
			entity.setType("DeviceSubscription"); 

			entity.setDeviceId(new EntityAttribute<String>(deviceId));
			entity.setSubscriptionId(new EntityAttribute<String>(subscriptionId));
			entity.setMashupId(new EntityAttribute<String>(mashupId));

			// Transform the bean into json
			String entityJ = beanToJson(entity);
			LOGGER.log(Level.INFO, "EntityJson  {}" , entityJ);

			Map<String, String> configHeaders = new HashMap<>();
			configHeaders.put(FIWARE_SERVICE, headerService);
			configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
			LOGGER.log(Level.INFO, "theConfigHaders  {}" , configHeaders);
			LOGGER.log(Level.INFO, "body:  {}" , entityJ);

			serviceResponse = RestUtils.consumePost(endpoint, entityJ, configHeaders);
			LOGGER.log(Level.INFO, "Create Device Subscription Entity on Orion:  {}" , serviceResponse);

			try {
				jResp = new JSONObject(serviceResponse);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put(MESSAGE, serviceResponse);
				LOGGER.log(Level.ERROR, jsone.getMessage());
			}

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return serviceResponse;
	}
	
	
	/**
	 * Create an entity on orion with the relationship between device and subscription
	 * 
	 * @param deviceId
	 * @param subscriptionId
	 * @param endpointURL
	 * @param headerService
	 * @param headerServicePath
	 * @return
	 * @throws Exception
	 */
	public static String createDeviceEndpointSubscriptionEntity(String deviceId, String subscriptionId, String endpointURL, String headerService, String headerServicePath, String target) throws Exception {
		
		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionEntity;
			LOGGER.log(Level.INFO, "Invoking {}" , endpoint);

			DeviceEndpointSubscriptionEntity entity = new DeviceEndpointSubscriptionEntity();
			// Create the device subscription entity
			entity.setId(deviceId+"_"+subscriptionId); 
			entity.setType("DeviceEndpointSubscription"); 

			entity.setDeviceId(new EntityAttribute<String>(deviceId));
			entity.setSubscriptionId(new EntityAttribute<String>(subscriptionId));
			entity.setEndpointURL(new EntityAttribute<String>(endpointURL));
			entity.setTarget(new EntityAttribute<String>( target));

			// Transform the bean into json
			String entityJ = beanToJson(entity);
			LOGGER.log(Level.INFO, "EntityJson {}" , entityJ);

			Map<String, String> configHeaders = new HashMap<>();
			configHeaders.put(FIWARE_SERVICE, headerService);
			configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
			LOGGER.log(Level.INFO, "theConfigHaders {}" , configHeaders);
			LOGGER.log(Level.INFO, "body: {}" , entityJ);

			serviceResponse = RestUtils.consumePost(endpoint, entityJ, configHeaders);
			LOGGER.log(Level.INFO, "Create Device Subscription Entity on Orion: {}" , serviceResponse);

			try {
				jResp = new JSONObject(serviceResponse);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put(MESSAGE, serviceResponse);
				LOGGER.log(Level.ERROR, jsone.getMessage());
			}

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return serviceResponse;
	}



	/**
	 * @param beanObject
	 * @return
	 * @throws Exception
	 */
	public static String beanToJson(Object beanObject) throws Exception {

		LOGGER.log(Level.INFO, "Transforming the bean into json specific structure...");

		String obj = new Gson().toJson(beanObject);
		LOGGER.log(Level.DEBUG, "JSON: {}" , obj);
		return obj;

	}

	
	
	/**
	 * @param deviceId
	 * @param endpointUrl
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @param throttling
	 * @param isInternal
	 * @return
	 * @throws Exception
	 */
	public static String createdOnUpdateDeviceSubscriptionAndSubEntity (String deviceId, String endpointUrl,  String fiwareservice, String fiwareservicepath, Integer  throttling, boolean isInternal, String target) throws Exception {
		
		String subscriptionIdCreated = "";
		
		// STEP 1: Create Subscription for Device
		String createdOnUpdateDeviceSubscription = SubscriptionManager.createDeviceSubscription(deviceId, endpointUrl, null, fiwareservice, fiwareservicepath, throttling, isInternal);
		
		
		// STEP 2: Get the subscriptionId
		JSONObject subscrIdj = new JSONObject(createdOnUpdateDeviceSubscription);
		 subscriptionIdCreated = subscrIdj.getString(SUBSCRIPTION_ID);
		LOGGER.log(Level.DEBUG,"Subscription Id Created: {}", subscriptionIdCreated);
		
		// STEP3: CREATE ENTITY WITH subscription_id and device_id
		String createdDeviceSubscriptionEntity = SubscriptionManager.createDeviceEndpointSubscriptionEntity(deviceId, subscriptionIdCreated, endpointUrl, fiwareservice, fiwareservicepath, target);
		LOGGER.log(Level.DEBUG,"Created Device Subscription Entity on Orion: {}", createdDeviceSubscriptionEntity);
		
		return subscriptionIdCreated;
		
	}
	
	
	/**
	 * @param deviceSubscriptions
	 * @param target
	 * @return
	 */
	public static DeviceEndpointSubscriptionEntity getSubScriptionEntityByTarget(Set<DeviceEndpointSubscriptionEntity> deviceSubscriptions, String target) {
		
		
		DeviceEndpointSubscriptionEntity devEndSubEntity = new DeviceEndpointSubscriptionEntity();
		
		for (DeviceEndpointSubscriptionEntity deviceSubscription : deviceSubscriptions){
			
			if (deviceSubscription.getTarget().getValue().equalsIgnoreCase(target))
			 return deviceSubscription;
		}
		
		return devEndSubEntity;
		
	}
	
	
	/**
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @param deviceId
	 * @param withoutInternal
	 * @param target
	 * @return
	 */
	public static DeviceEndpointSubscriptionEntity getDeviceEndpointSubscriptionByTarget (String fiwareservice, String fiwareservicepath, String deviceId, boolean withoutInternal, String target)  {
		
		Set<DeviceEndpointSubscriptionEntity> deviceEndpointSubscriptions = new HashSet<>();
		try {
			deviceEndpointSubscriptions = getDeviceEndpointSubscriptions(fiwareservice, fiwareservicepath, deviceId, withoutInternal, target);
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, "error on getDeviceEndpointSubscriptionByTarget {}", e.getMessage());
		}
		
		return getSubScriptionEntityByTarget(deviceEndpointSubscriptions, target);
		
		
	}
	
	/**
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @param subscriptionJson
	 * @return
	 */
	public static JSONObject createSubscriptionForHttp2kafka (String fiwareservice, String fiwareservicepath, JSONObject subscriptionJson) {
		
		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		
		try {
			
			String endpoint = orionHostURL+ orionSubscription;
			LOGGER.log(Level.DEBUG, "Invoking {}" , endpoint);
			
			
			Map<String, String> configHeaders = new HashMap<>();
			configHeaders.put(FIWARE_SERVICE, fiwareservice);
			configHeaders.put(FIWARE_SERVICEPATH, fiwareservicepath);
			LOGGER.log(Level.DEBUG, "theConfigHaders {}" , configHeaders);
			LOGGER.log(Level.DEBUG, "body: {} " , subscriptionJson);
			
			
			ClientResponse clientResponse = RestUtils.consumePostFull(endpoint, subscriptionJson, configHeaders);

			String location = clientResponse.getHeaders().getFirst(LOCATION);
			String subscriptionId = location.substring(location.lastIndexOf("/")+1, location.length());
			LOGGER.log(Level.INFO, "Subscription {} successfully created!" , subscriptionId );

			serviceResponse = clientResponse.getEntity(String.class);

			try {
				jResp.put(SUBSCRIPTION_ID, subscriptionId);
			} catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				jResp = new JSONObject();
				jResp.put(MESSAGE, serviceResponse);
				jResp.put(SUBSCRIPTION_ID, subscriptionId);
				
			}

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return jResp;
			
	}
	
	
	/* Create an entity on orion with the relationship between service and subscription */
	/**
	 * @param subscriptionId
	 * @param headerService
	 * @param headerServicePath
	 * @return
	 * @throws Exception
	 */
	public static JSONObject createServiceSubscriptionEntity( String subscriptionId, String headerService, String headerServicePath) throws Exception {
		
		JSONObject jResp = new JSONObject();
		
		try {

			String endpoint = orionHostURL+ orionEntity;
			LOGGER.log(Level.INFO, "Invoking {}" , endpoint);

			ServiceSubscriptionEntity entity = new ServiceSubscriptionEntity();
			// Create the service subscription entity
			entity.setId(HTTP2_KAFKA_SUB_TYPE+"_"+subscriptionId); 
			entity.setType("ServiceSubscription"); 

			entity.setSubscriptionId(new EntityAttribute<>(subscriptionId));

			// Transform the bean into json
			String entityJ = beanToJson(entity);
			LOGGER.log(Level.DEBUG, "EntityJson {}" , entityJ);

			Map<String, String> configHeaders = new HashMap<>();
			configHeaders.put(FIWARE_SERVICE, headerService);
			configHeaders.put(FIWARE_SERVICEPATH, headerServicePath);
			LOGGER.log(Level.DEBUG, "theConfigHaders {}" , configHeaders);
			LOGGER.log(Level.DEBUG, "body: {}" , entityJ);

			
			ClientResponse clientResponse = RestUtils.consumePostFull(endpoint, entityJ, configHeaders);
			
			String location = clientResponse.getHeaders().getFirst(LOCATION);
			String entityId = location.substring(location.lastIndexOf("/")+1, location.length());
			
			LOGGER.log(Level.INFO, "Create Device Subscription Entity on Orion {}", entityId );

			try {
				jResp.put("entityId", entityId);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				LOGGER.log(Level.ERROR, jsone.getMessage());
			}

		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return jResp;
	}
	
	/**
	 * @param headerService
	 * @param headerServicePath
	 * @return
	 * @throws Exception
	 */
	public static Set<ServiceSubscriptionEntity> getServiceSubscription(String headerService, String headerServicePath) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getServiceSubscription");

		Set<BasicNameValuePair> params = new HashSet<>();
		
		params.add(new BasicNameValuePair("type", "ServiceSubscription"));	

		String resp =  orion.getEntities(headerService, headerServicePath, params);

		Type type = new TypeToken<Set<ServiceSubscriptionEntity>>(){}.getType();
		Set<ServiceSubscriptionEntity> servSubEntities = new Gson().fromJson(resp, type);
		
		//Retrieve the subscription status
		Set<ServiceSubscriptionEntity> devSubEntitiesRet = new HashSet<>();
		for (ServiceSubscriptionEntity  servSubEntity : servSubEntities) {
					
			 Subscription subscription = getSubscription(headerService, headerServicePath, servSubEntity.getSubscriptionId().getValue());
			 
			 String status = subscription.getStatus();
			 servSubEntity.setStatus(new EntityAttribute<String>(status));
			 devSubEntitiesRet.add(servSubEntity);
		}

		LOGGER.log(Level.INFO, "subscription Ids: {}" , devSubEntitiesRet);
		return devSubEntitiesRet;
	}
	
	
	/**
	 * @param headerService
	 * @param headerServicePath
	 * @param type
	 * @param subscriptionId
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteServiceSubscription(String headerService, String headerServicePath, String type, String subscriptionId) throws Exception {

		boolean out = true;

		try{
			
			String entitiyId = type+"_"+subscriptionId;

			// STEP 2: Delete orion subscription
			LOGGER.log(Level.INFO, "Deleting subscription Id: {}" , subscriptionId);
			deleteEntityOnOrion(orionHostURL+orionSubscription,  subscriptionId,  headerService,  headerServicePath);

			// STEP 3: Delete orion entity DeviceSubscription
			LOGGER.log(Level.INFO, "Deleting entity Id: {}" , entitiyId);
			deleteEntityOnOrion(orionHostURL+orionEntity,  entitiyId,  headerService,  headerServicePath);

		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
			out = false;
		}

		return out;
	}
	
	
	/**
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @throws Exception
	 */
	public static void createServiceHttp2kafkaSubscription (String fiwareservice, String fiwareservicepath) throws Exception {
		
		
		fiwareservicepath=CommonUtils.normalizeServicePath(fiwareservicepath);
		
		// STEP 1: Create Subscription for Service
		JSONObject subscription = buildSubscritionJsonCreate();
		JSONObject createdSubscription = SubscriptionManager.createSubscriptionForHttp2kafka (fiwareservice, fiwareservicepath, subscription );
		LOGGER.info("createSubscriptionForHttp2kafka {}", createdSubscription);
		
		
		// STEP 2: Get the subscriptionId
		String subscriptionIdCreated = createdSubscription.getString(SUBSCRIPTION_ID);
		LOGGER.info("Subscription Id Created: {}", subscriptionIdCreated);
		
		// STEP3: CREATE ENTITY WITH subscription_id
		JSONObject createdServiceSubscriptionEntity = SubscriptionManager.createServiceSubscriptionEntity(subscriptionIdCreated, fiwareservice, fiwareservicepath);
		LOGGER.info("Created Device Subscription Entity on Orion: {}" , createdServiceSubscriptionEntity);
		
	}
	
	
	/**
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @throws Exception
	 */
	public static void deleteServiceHttp2kafkaSubscription (String fiwareservice, String fiwareservicepath) throws Exception {
		
		
		fiwareservicepath=CommonUtils.normalizeServicePath(fiwareservicepath);
		 
		Set<ServiceSubscriptionEntity> subs = SubscriptionManager.getServiceSubscription(fiwareservice, fiwareservicepath);
		
		for (ServiceSubscriptionEntity sub:subs) {
			
			try {
				SubscriptionManager.deleteServiceSubscription(fiwareservice, fiwareservicepath,	SubscriptionManager.HTTP2_KAFKA_SUB_TYPE, sub.getSubscriptionId().getValue());
			} catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
			}
		}
		
	}
	
	
	
	/**
	 * @return
	 */
	private static JSONObject buildSubscritionJsonCreate () {
		
		JSONObject subscription = new JSONObject();
		
		subscription.put("description", "[HTTP2KAFKA] A subscription to forward data to http2kafka-bridge who is in charge of forward them to Kafka  ");
		
		JSONObject subject = new JSONObject();
			JSONArray entities = new JSONArray();
				JSONObject entity = new JSONObject();
				entity.put("idPattern", ".*");
				entity.put("type", "Device");
				entities.put(entity);
				
			subject.put("entities", entities);
		subscription.put("subject", subject);
		
		JSONObject notification = new JSONObject();
			JSONObject http = new JSONObject();
				http.put("url", getHttp2kafkaFullAddress());
			notification.put("http", http);
			JSONArray attrsN = new JSONArray();
			notification.put("attrs", attrsN);
		subscription.put("notification", notification);
		
		return subscription;
		
	}
	
	/**
	 * @return
	 */
	private static String getHttp2kafkaFullAddress () {
		
		return HTTP2KAFKA_ADDRESS_ON_ORION_PROTOCOL+HTTP2KAFKA_ADDRESS_ON_ORION_HOST+":"+HTTP2KAFKA_ADDRESS_ON_ORION_PORT+HTTP2KAFKA_ADDRESS_ON_ORION_PATH;
		
	}

}
