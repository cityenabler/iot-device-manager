package it.eng.iot.utils;

import java.util.concurrent.TimeUnit;

/*
 * it's necessary to use a Singleton to avoid the validation on server fails
 * */

public class CreateJwtSingleton {
	
	
	
	// static variable single_instance of type Singleton 
    private static CreateJwtSingleton createJwt = null; 
  
    // variable of type String 
    public String token; 
  
    // private constructor restricted to this class itself 
    private CreateJwtSingleton(){ 
    	token = LoraUtils.getJWT();
    } 
  
    // static method to create instance of Singleton class 
    public static CreateJwtSingleton getInstance(){ 
        if (createJwt == null) { 
        	createJwt = new CreateJwtSingleton();
        	
        	try {
				TimeUnit.SECONDS.sleep(1);//in order to avoid "Token used before issued" see https://github.com/dgrijalva/jwt-go/issues/314
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }	
        return createJwt; 
    } 
	

}
