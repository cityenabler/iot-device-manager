package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfOrionCB;
import it.eng.tools.model.DeviceEntityBean;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceStaticAttribute;



public abstract class DeviceUtils {
	
	private static final Logger LOGGER = LogManager.getLogger(DeviceUtils.class);
	
	private static final String  orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities");
	private static final String kapuaHubType = "kapua";

	/**
	 * @param staticAttrs
	 * @return
	 */
	public static boolean isMobileDevice(Set<IdasDeviceStaticAttribute> staticAttrs){
		boolean isMobile =false; 
		try{
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("mobile_device".equalsIgnoreCase(attr.getName()) ){
					 isMobile = Boolean.valueOf( attr.getValue());
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on isMobileDevice method "+ exception.getMessage());
	    }
		
		return isMobile;
	}
	
	/**
	 * @param staticAttrs
	 * @return
	 */
	public static boolean isHistoryActive(Set<IdasDeviceStaticAttribute> staticAttrs){
		boolean historicizing =false; 
		try{
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("historicizing".equalsIgnoreCase(attr.getName()) ){
					historicizing = Boolean.valueOf( attr.getValue());
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on isHistoryActive method "+ exception.getMessage());
	    }
		
		return historicizing;
	}
	
	/**
	 * @param device
	 * @return
	 */
	public static boolean isRemoveOldHistoricalData(IdasDevice device){
		
		Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();
		
		return isRemoveOldHistoricalData(staticAttrs);
	}
	
	/**
	 * @param staticAttrs
	 * @return
	 */
	public static boolean isRemoveOldHistoricalData(Set<IdasDeviceStaticAttribute> staticAttrs){
		boolean historicizing =false; 
		try{
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("removeOldHistoricalData".equalsIgnoreCase(attr.getName()) ){
					historicizing = Boolean.valueOf( attr.getValue());
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on isRemoveOldHistoricalData method "+ exception.getMessage());
	    }
		
		return historicizing;
	}
	
	/**
	 * @param device
	 * @return
	 */
	public static boolean isHistoryActive(IdasDevice device){
		
		Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();
		
		return isHistoryActive(staticAttrs);
	}
	
	
	
	
	
	/**
	 * @param fiwareservice
	 * @param fiwareservicepath
	 * @param deviceId
	 * @return
	 */
	public static List<String> getDeviceLocationFromOCB(String fiwareservice, String fiwareservicepath, String deviceId) {
		
		List<String> location = new ArrayList<>();
		
		String queryString = "/"+deviceId+"?attrs=location";
		String endpointQuery = orionEndpoint + queryString;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", fiwareservicepath);
		String serviceResponse="";
		try {
			serviceResponse = RestUtils.consumeGet(endpointQuery,headers);
		
			Type type = new TypeToken<DeviceEntityBean>(){}.getType();
			DeviceEntityBean device = new Gson().fromJson(serviceResponse, type);
			
			String locs = device.getLocation().getValue();
			
			LOGGER.log(Level.ERROR, "location "+ locs);
			
			
			String[] coords = locs.split(",");
			
			location.add(coords[0]);
			location.add(coords[1]);
			 
		 } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getDeviceLocationFromOCB method "+ exception.getMessage());
	    	return location;
	    }
			
		return location;
	}
	
	public static String getHubName(Set<IdasDeviceStaticAttribute> staticAttrs) {
		String result = null;
		
		try {
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("hub_name".equalsIgnoreCase(attr.getName()) ){
					 result = attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getHubName method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static String getHubType(Set<IdasDeviceStaticAttribute> staticAttrs) {
		String result = null;
		
		try {
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("hub_type".equalsIgnoreCase(attr.getName()) ){
					 result = attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getHubType method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static boolean isKapuaHub(IdasDevice device) {
		boolean result = false;
		Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();
		
		try {
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("hub_type".equalsIgnoreCase(attr.getName()) ){
					if (kapuaHubType.compareToIgnoreCase(attr.getValue()) == 0) {
						result = true;
					}
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on isKapuaHub method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static String getGateway(Set<IdasDeviceStaticAttribute> staticAttrs) {
		String result = null;
		
		try {
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("gateway".equalsIgnoreCase(attr.getName()) ){
					 result = attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getGateway method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static String getAsset(Set<IdasDeviceStaticAttribute> staticAttrs) {
		String result = null;
		
		try {
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("asset".equalsIgnoreCase(attr.getName()) ){
					 result = attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getAsset method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static String getModbusAddress(Set<IdasDeviceStaticAttribute> staticAttrs) {
		String result = null;
		
		try {
	        
			for(IdasDeviceStaticAttribute attr : staticAttrs){
				if ("modbus_addr".equalsIgnoreCase(attr.getName()) ){
					 result = attr.getValue();
				}
			}
			
	    } 
	    catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on getModbusAddress method "+ exception.getMessage());
	    }
		
		return result;
	}
	
	public static String parseEndpoint(String deviceEndpoint) {
		String result = null;
		String pattern1 = "[a-zA-Z]+://\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+";
		String pattern2 = "\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+";
		String pattern3 = "\\d+\\.\\d+\\.\\d+\\.\\d+";
		
		if (deviceEndpoint == null || deviceEndpoint.isEmpty()) {
			LOGGER.log(Level.ERROR, "Null or empty endpoint " + deviceEndpoint);
			return null;
		}
		if (!deviceEndpoint.matches(pattern1) && !deviceEndpoint.matches(pattern2) && !deviceEndpoint.matches(pattern3)) {
			LOGGER.log(Level.ERROR, "Invalid endpoint " + deviceEndpoint);
			return null;
		}
		
		try {
			String[] split = deviceEndpoint.split(":");
			
			switch (split.length) {
			case 1:	//only IP address
				result = "tcp://".concat(split[0]).concat(":502");
				break;
			case 2:	//only IP address and port
				result = "tcp://".concat(deviceEndpoint);
				break;
			case 3:	//protocol, IP address, port
			    result = deviceEndpoint;
				break;
			default:
				LOGGER.log(Level.ERROR, "Invalid endpoint " + deviceEndpoint);
				result = null;
			}
		}
		catch (Exception exception){
	    	LOGGER.log(Level.ERROR, "Error on parseEndpoint method: " + exception.getMessage());
	    	result = null;
	    }
		
		return result;
	}
}
