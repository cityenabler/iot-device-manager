package it.eng.iot.utils;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfOrionCB;
import it.eng.tools.Orion;
import it.eng.tools.model.DeviceEntityBean;
import it.eng.tools.model.FilterEntity;
import it.eng.tools.model.ServiceEntityBean;

public abstract class FilteringManager {

	private static String LIMIT = "&limit=" +  ConfOrionCB.getString("orion.limit.search");
	private static String excludeOpTypeDeleted = "&q=opType!='deleted'"   ;
	
	private static final Logger LOGGER = LogManager.getLogger(FilteringManager.class);

	private static Orion orion;

	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}

	/* 
	 * searchDeviceById
	 * http://217.172.12.177:1026/v2/entities?idPattern=.*-20*&type=Device
	 */
	static String DEFAULT_ENTITY_TYPE = "Device";
	public static Set<DeviceEntityBean> searchDeviceById(String headerService, String headerServicePath, String type, String id) throws Exception {
		LOGGER.log(Level.INFO, "Search Keyword id " + id);
		
		
		Set<DeviceEntityBean> devices = new HashSet<DeviceEntityBean>();

		String _type = DEFAULT_ENTITY_TYPE;
		if (type != null && !type.trim().equalsIgnoreCase("undefined") && !type.trim().equalsIgnoreCase("")) {
			_type = type.trim();
		}
		String filterForType = "&type=" + _type;


		String _idPattern = "idPattern=(?i).*";
		if (id!=null && !id.trim().equalsIgnoreCase("undefined") && !id.trim().equalsIgnoreCase("")) {
			_idPattern = _idPattern + id.trim() + "(?-i).*";
		}
		String _screenIdPattern = ".*";
		if (id!=null && !id.trim().equalsIgnoreCase("undefined") && !id.trim().equalsIgnoreCase("")) {
			_screenIdPattern = _screenIdPattern + id.trim() + ".*";
		}
		
		//(?-i) case insensitive regular expression https://stackoverflow.com/q/9655164/4173912
		
		String queryParams = "?" + _idPattern + filterForType + excludeOpTypeDeleted + LIMIT;
		LOGGER.log(Level.INFO, "Search Device Query Params " + queryParams);
		try {
			LOGGER.log(Level.INFO, headerService + " /" + headerServicePath + " " + queryParams);
			
					String resp =  orion.getEntity(headerService, headerServicePath, queryParams);
					LOGGER.log(Level.INFO, "Output: " + resp);
					Type types = new TypeToken<Set<DeviceEntityBean>>(){}.getType();
					//Type types = new TypeToken<Set<CBEntity>>(){}.getType();
					devices = new Gson().fromJson(resp, types);
				
					try {
						//search Device by screenId
						Set<DeviceEntityBean> allDevices = DeviceConfigManager.getOCBDevices(headerService, headerServicePath);
						LOGGER.log(Level.INFO, headerService + " /" + headerServicePath + " " + _screenIdPattern);
						Pattern pattern = Pattern.compile(_screenIdPattern, Pattern.CASE_INSENSITIVE);
						LOGGER.log(Level.INFO, "Search device by screenId by pattern: " + pattern);
						for(DeviceEntityBean device : allDevices) {
							Matcher matcher = pattern.matcher(device.getScreenId().getValue());
								
							if(matcher.matches()) {
								devices.add(device);
							}
						}
					}
					catch (Exception e) {
						LOGGER.log(Level.ERROR, e.getMessage());
						throw new Exception("Search failed");
					}
				
		} 
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			throw new Exception("Search failed");
		}

		return devices;
	}


		//http://217.172.12.177:1026/v2/entities?idPattern=.*parking*&type=service
		public static Set<ServiceEntityBean> searchServiceById(String id) throws Exception {
			
			id = id.toLowerCase();
			LOGGER.log(Level.INFO, "Search Keyword id " + id);
					
			Set<ServiceEntityBean> filteredServices = new HashSet<ServiceEntityBean>();
			
			try {
				
				
				// when we use entities from Tenant Manager 
				Set<ServiceEntityBean> scopes = ServiceConfigManager.getServicesTM(); // all scopes from TenantManager
				
				for (ServiceEntityBean scope:scopes) {
					Set<ServiceEntityBean> urbanservices =  ServiceConfigManager.getServicePathsTM(scope.getId());
						for(ServiceEntityBean urbanservice:urbanservices) {
							filteredServices.add(urbanservice);
						}
				}
			} 
			catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
				throw new Exception("Search failed");
			}
			LOGGER.log(Level.INFO, "Number of entities found: " + filteredServices.size());
			return filteredServices;
		}

	
	public static Set<FilterEntity> searchFullTextById(String searchKeyword, Set<ServiceEntityBean> permittedScope) throws Exception {

		Set<FilterEntity> filteredEntities = new HashSet<FilterEntity>();

		Set<ServiceEntityBean> services = new HashSet<ServiceEntityBean>();
		
		if (searchKeyword != null && !searchKeyword.trim().equalsIgnoreCase("undefined") && !searchKeyword.trim().equalsIgnoreCase("")) {
			searchKeyword = searchKeyword.trim();
			//Replace the internal space with _
			searchKeyword = searchKeyword.replaceAll("\\s","_"); 
		}

		LOGGER.log(Level.INFO, "=== searchKeyword === " + searchKeyword );
		try {

			services.clear();
			services = searchServiceById("");
			
			// SEARCH DEVICES
			for(ServiceEntityBean service : services){
				
				try {
						//when we use entities from Tenant Manager
						if (service.getType().equalsIgnoreCase("urbanservice")){
							String fiwareservice = service.getService().getValue();
							String fiwareservicepath = service.getSubservice().getValue();
							LOGGER.log(Level.INFO, "fiwareservice " + fiwareservice + " - " + fiwareservicepath);
							if (permittedScope!=null){
							for (ServiceEntityBean scope:permittedScope){
								if (scope.getService().getValue().equalsIgnoreCase(fiwareservice)){
									LOGGER.log(Level.INFO, "Starting device search for fiwareservice " + fiwareservice + " - " + fiwareservicepath);
									
									Set<DeviceEntityBean> devices = searchDeviceById(fiwareservice, fiwareservicepath, "Device", searchKeyword);
									Set<FilterEntity> filtered = composeDeviceEntity(searchKeyword, fiwareservice, fiwareservicepath, devices);
									filteredEntities.addAll(filtered);
								}
							}
							}else {
								Set<DeviceEntityBean> devices = searchDeviceById(fiwareservice, fiwareservicepath, "Device", searchKeyword);
								Set<FilterEntity> filtered = composeDeviceEntity(searchKeyword, fiwareservice, fiwareservicepath, devices);
								filteredEntities.addAll(filtered);
							}
						}
				} 
				catch (Exception e) {
					LOGGER.log(Level.ERROR, e.getMessage());
					continue;
				}
			}
			 


		} 
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			throw new Exception("Search failed");
		}

		LOGGER.log(Level.INFO, "Num devices " + filteredEntities.size());
		return filteredEntities;
	}


	


	public static FilterEntity composeScopeEntity(String searchKeyword, String fiwareservice, String serviceName) throws Exception {
		FilterEntity filteredEntity = new FilterEntity();
		if (fiwareservice.toLowerCase().indexOf(searchKeyword.toLowerCase())>=0) {
			filteredEntity.setId(fiwareservice);
			filteredEntity.setType("scope");
			filteredEntity.setName(serviceName);
		} 
		return filteredEntity;
	}

	public static FilterEntity composeUrbanserviceEntity(String searchKeyword, String fiwareservice, String fiwareservicepath, String serviceName) throws Exception {
		FilterEntity filteredEntity = new FilterEntity();
		
		if (fiwareservicepath.toLowerCase().indexOf(searchKeyword.toLowerCase())>=0){
			filteredEntity.setType("urbanservice");
			filteredEntity.setScopeId(fiwareservice);
			String scopeName = ServiceConfigManager.getServiceNameByServiceTM(fiwareservice);
			filteredEntity.setScopeName(scopeName);
			filteredEntity.setId(fiwareservicepath);
			filteredEntity.setName(serviceName);
			filteredEntity.setUrbanserviceId(fiwareservicepath);
			filteredEntity.setUrbanserviceName(serviceName);
			
		}
		return filteredEntity;
	}
	

	public static Set<FilterEntity> composeDeviceEntity(String searchKeyword, String fiwareservice, String fiwareservicepath, Set<DeviceEntityBean> devices) throws Exception {
		FilterEntity filteredEntity = new FilterEntity();
		Set<FilterEntity> filteredEntities = new HashSet<FilterEntity>();
		
		for(DeviceEntityBean device : devices){
			filteredEntity = new FilterEntity();
			String deviceId  = device.getId();
			String deviceType = device.getType();
			String deviceName = device.getScreenId().getValue();
			
			filteredEntity.setId(deviceId);
			filteredEntity.setType(deviceType);
			filteredEntity.setScopeId(fiwareservice);
			String scopeName = ServiceConfigManager.getServiceNameByServiceTM(fiwareservice);
			filteredEntity.setScopeName(scopeName); 
			filteredEntity.setUrbanserviceId(fiwareservicepath);
			String urbanserviceName = ServiceConfigManager.getServicePathNameTM(fiwareservicepath, fiwareservice);
			filteredEntity.setUrbanserviceName(urbanserviceName); 
			filteredEntity.setName(deviceName);
			filteredEntity.setDataformat(device.getDataformatProtocol().getValue());
			filteredEntity.setTransportProtocol(device.getTransportProtocol().getValue());
			filteredEntity.setRetrieveDataMode(device.getRetrieveDataMode().getValue());
			
			filteredEntities.add(filteredEntity);
		}
		return filteredEntities;
	}

	//Unused
//	public static void main(String[] args, String token) throws Exception {
//
//
//		String searchKeyword = " trent ";
//		Set<ServiceEntityBean> permittedScope = new HashSet<ServiceEntityBean>();
//		ServiceEntityBean elem = new ServiceEntityBean();
//		elem.setId("malaga_1");
//		elem.setType("scope");
//		permittedScope.add(elem);
//		Set<FilterEntity> filterEntities = searchFullTextById(searchKeyword, null, token);
//		LOGGER.log(Level.INFO, "================ Num filterEntity ===============>" + filterEntities.size());
//		for(FilterEntity filterEntiy : filterEntities){
//			String deviceId  = filterEntiy.getId();
//			String deviceType = filterEntiy.getType();
//			
//			String scopeId = filterEntiy.getScopeId();
//			String scopeName = filterEntiy.getScopeName();
//			String urbanserviceId = filterEntiy.getUrbanserviceId();
//			String urbanserviceName = filterEntiy.getUrbanserviceName();
//			
//			LOGGER.log(Level.INFO, "filterEntiy " + deviceType + " - " + deviceId);
//			LOGGER.log(Level.INFO, "\t " + scopeId + " - " + scopeName);
//			LOGGER.log(Level.INFO, "\t " + urbanserviceId + " - " + urbanserviceName);
//		}
//
//	}
	
}
