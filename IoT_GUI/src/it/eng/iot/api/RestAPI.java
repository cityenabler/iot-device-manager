package it.eng.iot.api;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfIdas;
import it.eng.iot.configuration.ConfOrionCB;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.DataformatProtocol;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IOTAgentManager;
import it.eng.iot.utils.IdentityManagerUtility;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.iot.utils.listener.Http2KafkaSubscription;
import it.eng.iot.utils.listener.IServiceListener;
import it.eng.tools.Idas;
import it.eng.tools.keycloak.KeycloakContext;
import it.eng.tools.keycloak.TokenUserInfo;
import it.eng.tools.model.CategoryDTO;
import it.eng.tools.model.ContextCategoryDTO;
import it.eng.tools.model.IdasServiceList;
import it.eng.tools.model.IdasServiceList.IdasService;
import it.eng.tools.model.ServiceEntityBean;
import it.eng.tools.model.ServiceSubscriptionEntity;


/* 
 * Catches the call to
 * ENDPOINT: http://host:port/contextRootApp/services
 * METHOD: POST
 * PAYLOAD: See Orion notification....
 */
@Path("/")
public class RestAPI {

	private static final Logger LOGGER = LogManager.getLogger(RestAPI.class);
	private static final boolean  HTTP2KAFKA_ENABLED = Boolean.parseBoolean( ConfTools.getString("http2kafka.enabled"));
	private static final String ERROR_MSG_ARG = "Error message: {}";
	private static final String ERROR_S = "error";
	private static final String MESSAGE = "message";
	
	
	
	public static Set<IServiceListener> serviceListeners = new HashSet<>();
	
	static{
		serviceListeners.add(new Http2KafkaSubscription());
	}
	

	public enum RestAction {
		created("created"),
		deleted("deleted");


		private final String text;

		private RestAction(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return this.text;
		}

	}


	@POST
	@Path("/services")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response servicesActionHandler(String fullPayload) {

		Response.ResponseBuilder rb = Response.status(300);
		try{

			LOGGER.log(Level.INFO,"Payload received: {}" , fullPayload);
			Gson gson = new Gson();

			JSONObject job = new JSONObject(fullPayload);
			JSONObject payloadj = job.getJSONObject("payload");

			CategoryDTO thisService = gson.fromJson(payloadj.toString(), CategoryDTO.class);
			
			String fiwareService = thisService.getRefEnabler4scope().getRefScope().getId();
			String fiwareServicepath = thisService.getId();
			

			String action = job.get("action").toString();
			RestAction restAction = RestAction.valueOf(action);

			switch (restAction) {

			case created: {
				LOGGER.log(Level.INFO,"CREATE Category");

						
						String trust = ConfOrionCB.getString("orion.iot.trust");
						String cbHost = ConfOrionCB.getString("orion.host");
						String entityType = "Device";
						String apikey = thisService.getApikey();
						String resource = ConfIdas.getString("iota.resource");
						

						IdasServiceList serviceList = new IdasServiceList();

						IdasService service = serviceList.new IdasService(trust, cbHost, entityType, apikey, resource);
						Set<IdasService> services = new HashSet<>();
						services.add(service);
						serviceList.setServices(services);
						
						boolean allOk = true;
						JSONObject error = new JSONObject();
						String errorMsg = "";
						/* 
						 * PAY ATTENTION to the multiprotocol data format: 
						 * THE SERVICE WILL BE CREATED ON EACH IOTAgent 
						 */
						/* Create service on IDAS */
						for (DataformatProtocol dataformat: DataformatProtocol.values()) {
							
							String dataformatProtocol = dataformat.toString();
							
							
							if (!CommonUtils.isEnabledAgent(dataformatProtocol))
								continue;
							
						   try{
								
								// SWITCH AGENT  
								Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
	
								JSONObject idasJresp = idas.registerService(fiwareService, fiwareServicepath, serviceList);
	
								JSONObject jResp = new JSONObject();
								jResp.put("idas_registration", idasJresp);
								LOGGER.log(Level.INFO,"Service successfully created on IDAS IoTAgent {}" , dataformatProtocol);
							
							
							}catch (Exception e) {
									LOGGER.log(Level.ERROR, e.getMessage()); 
									LOGGER.log(Level.ERROR,"It is not possible propagate a Category on  {}", dataformat);
									allOk = false;
									error.put(ERROR_S, true);
									
									errorMsg+= "It is not possible propagate a Category on  "+ dataformat ;
									
									error.put(MESSAGE, errorMsg);
							}

						}
						
						if (allOk) {
							
							for(IServiceListener listener : serviceListeners)
								listener.onServiceCreate(fiwareService, fiwareServicepath);
							
							
							rb = Response.status(200);
							
						}
						else
							rb = Response.status(500).entity(error.toString());
					
					break;
				}


				
			case deleted: {
			
					LOGGER.log(Level.INFO,"START DELETE Category");

					//Delete category on IDAS and the related devices
					boolean completed = ServiceConfigManager.deleteServiceProcess(fiwareService, fiwareServicepath, thisService);
					LOGGER.log(Level.INFO,"Service successfully deleted on IDAS: {}" , completed);
					
					if (completed) {
						
						for(IServiceListener listener : serviceListeners)
							listener.onServiceDelete(fiwareService, fiwareServicepath);
						
						rb = Response.status(200);
					}
						
					else
						rb = Response.status(500);
				
				
				break;
			}
			default:
				throw new Exception("Unsupported action " + action); 
			}//switch
			
		}catch(Exception e){
			
			JSONObject error = new JSONObject();
			error.put(ERROR_S, true);
			error.put(MESSAGE, e.getMessage());
			rb = Response.status(500).entity(error.toString());
			LOGGER.log(Level.ERROR, ERROR_MSG_ARG , e.getMessage());
			return rb.build();
		}

		return rb.build();

	}


	@PUT
	@Path("/http2kafka-subscriptions")
	public Response updateHttp2kafkaSubscriptions() {
		
		Response.ResponseBuilder rb = Response.status(300);
		
		LOGGER.log(Level.INFO, "env HTTP2KAFKA_ENABLED is {}", HTTP2KAFKA_ENABLED);
		
		
		List<ContextCategoryDTO> contextCategories = ServiceConfigManager.getAllCouplesContextCategory();
		
		contextCategories.
		   stream().
		   parallel().
		   forEach( c -> {
			   
							String fiwareservice = c.getService();
							String  fiwareservicepath = c.getServicepath();
							
							try {
								Set<ServiceSubscriptionEntity> subs = SubscriptionManager.getServiceSubscription(fiwareservice, fiwareservicepath);
								
								if (HTTP2KAFKA_ENABLED  && subs.isEmpty()) {
									
									try {
										SubscriptionManager.createServiceHttp2kafkaSubscription(fiwareservice,fiwareservicepath );
									}
									catch (Exception jsone) {
										LOGGER.log(Level.ERROR, jsone.getMessage());
									}
								}
									
								if (!HTTP2KAFKA_ENABLED  && !subs.isEmpty()) {
									
									try {
										SubscriptionManager.deleteServiceHttp2kafkaSubscription(fiwareservice,fiwareservicepath );
									}
									catch (Exception jsone) {
										LOGGER.log(Level.ERROR, jsone.getMessage());
									}
								}
								
							} catch (Exception e) {
								LOGGER.log(Level.ERROR, ERROR_MSG_ARG , e.getMessage());
							}
						   
					   	}
		
				   );//forEach
		
		
		rb = Response.status(200);
		
		return  rb.build();
		
	}
	
	
	

	/* 
	 * Catches the call to create new device on IDAS
	 * ENDPOINT: http://host:port/contextRootApp/devices
	 * METHOD: POST
	 * PAYLOAD: device
	 */
	@POST
	@Path("/devices")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response devicesActionHandler(String payload, 
			@HeaderParam("service") String service, 
			@HeaderParam("servicepath") String servicepath,
			@HeaderParam("access-token") String userAccessToken,
			@HeaderParam("organizationid") String organizationid) {

		Response.ResponseBuilder rb = Response.status(300);

		try{
			LOGGER.log(Level.INFO,"Called API for device creation");

			LOGGER.log(Level.INFO,"service: {}" , service);
			LOGGER.log(Level.INFO,"servicepath: {}" , servicepath);
			LOGGER.log(Level.INFO,"token: {}" , userAccessToken);
			LOGGER.log(Level.INFO,"organizationid: {}" , organizationid);
			LOGGER.log(Level.INFO,"payload: {}" , payload);


			// Check token
			boolean isValidToken = false;
			try {
				isValidToken = isValidUserAccessToken(userAccessToken);
				if (!isValidToken) {
					throw new Exception();
				}
			}catch(Exception e){
				JSONObject error = new JSONObject();
				error.put(ERROR_S, 401);
				error.put(MESSAGE, "Token Unauthorized");
				rb = Response.status(401).entity(error.toString());
				LOGGER.log(Level.ERROR, ERROR_MSG_ARG , "Token Unauthorized");
				return rb.build();
			}


			// Check if the scope is permitted to user
			boolean isPermittedScope = false;
			try {
				isPermittedScope = isUserPermittedScope(userAccessToken, service);
				if (!isPermittedScope) {
					throw new Exception();
				}
			}catch(Exception e){
				JSONObject error = new JSONObject();
				error.put(ERROR_S, 403);
				error.put(MESSAGE, "Forbidden Scope");
				rb = Response.status(403).entity(error.toString());
				LOGGER.log(Level.ERROR, ERROR_MSG_ARG ,  "Forbidden Scope");
				return rb.build();
			}

			// Check if the scope is permitted to user
			boolean isPermittedOrg = false;
			try {
				isPermittedOrg = isPermittedOrg(userAccessToken, organizationid);
				if (!isPermittedOrg) {
					throw new Exception();
				}
			}catch(Exception e){
				JSONObject error = new JSONObject();
				error.put(ERROR_S, 403);
				error.put(MESSAGE, "Forbidden Organization");
				rb = Response.status(403).entity(error.toString());
				LOGGER.log(Level.ERROR, "Error message: {}. Forbidden Organization", e.getMessage()  );
				return rb.build();
			}

			if (isValidToken && isPermittedScope && isPermittedOrg){
				// Create device
				try{
					boolean isCreatedDevice = createDevice(service, servicepath, userAccessToken, organizationid, payload);
					if (!isCreatedDevice) {
						throw new Exception();
					} 
					JSONObject message = new JSONObject();
					message.put("result", 200);
					message.put(MESSAGE, "Device successully created");
					rb = Response.status(200).entity(message.toString());
					LOGGER.info("Success message: {}" , message.getString(MESSAGE));
					return rb.build();
				} catch(Exception e){
					JSONObject error = new JSONObject();
					error.put(ERROR_S, true);
					error.put(MESSAGE, "ERROR CREATING DEVICE");
					rb = Response.status(403).entity(error.toString());
					LOGGER.log(Level.ERROR, ERROR_MSG_ARG , e.getMessage());
					return rb.build();
				}

			}


		}
		catch(Exception e){
			JSONObject error = new JSONObject();
			error.put(ERROR_S, true);
			error.put(MESSAGE, e.getMessage());
			rb = Response.status(500).entity(error.toString());
			LOGGER.log(Level.ERROR, ERROR_MSG_ARG , e.getMessage());
			return rb.build();
		}

		return rb.build();
	}



	/** Checks if the organization id is contained in the list of the user organization */
	private boolean isPermittedOrg(String userAccessToken, String organizationid) {
		boolean isValid = false;
		try {
			KeycloakContext kc = new KeycloakContext();
			TokenUserInfo userInfo = kc.getTokenUserInfo(userAccessToken);
			isValid = kc.isUserInGroup(organizationid, userInfo.getSub());			
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			isValid = false;
		}
		return isValid;
	}



	/** Checks if the user access token is valid */
	private boolean isValidUserAccessToken(String userAccessToken) {
		boolean isValid = false;
		try {
			isValid = IdentityManagerUtility.checkToken(userAccessToken);
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			isValid = false;
		}
		return isValid;
	}

	/** Checks if the scope is permitted for the userid */
	private boolean isUserPermittedScope(String userAccessToken, String service) {
		boolean isPermittedScope = false;
		try {
			KeycloakContext kc = new KeycloakContext();
			TokenUserInfo userInfo = kc.getTokenUserInfo(userAccessToken);
			Set<ServiceEntityBean> services = new HashSet<>();
			ServiceEntityBean scope = new ServiceEntityBean();
			scope.setId(service);
			services.add(scope);

			// Get the user orgs
			Set<String> orgs = userInfo.getGroups();
			Set<ServiceEntityBean> allPermittedServices = ServiceConfigManager.getPermittedServices(services, orgs);
			for (ServiceEntityBean serv: allPermittedServices){
				LOGGER.log(Level.INFO, "Scope {}" , serv.getId());
				if (serv.getId().contains(service.toLowerCase())){
					isPermittedScope = true;
					break;
				}
			}
		} catch (Exception e) {
			isPermittedScope = false;
			LOGGER.log(Level.ERROR, e.getMessage());
		}

		return isPermittedScope;
	}

	/** 
	 * Create device:
	 * loads the headers and the payload coming from the rest request 
	 */
	private boolean createDevice(String service, String servicepath, String userAccessToken, String organizationid, String payload) {

		boolean isCreatedDevice = false;
		
		try {
			KeycloakContext kc = new KeycloakContext();
			TokenUserInfo userInfo = kc.getTokenUserInfo(userAccessToken);
			String userid = userInfo.getSub();

			// Register the bodycontent/inputFileString as Device on IDAS
			// The payload contains one or more device [{device1}, {device2}]
			DeviceConfigManager.registerInputFileStringAsIdasDevices(service, servicepath, userid, organizationid, payload);
			isCreatedDevice = true;
		} catch (Exception e){
			isCreatedDevice = false;
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}

		return isCreatedDevice;
	}

}
