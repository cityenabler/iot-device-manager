package it.eng.iot.exceptions;

public class DeleteAssetException extends Exception {
	
	public DeleteAssetException(String message) {
		super(message);
	}
}
