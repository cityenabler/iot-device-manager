package it.eng.iot.exceptions;

public class AssetConfigurationException extends Exception {
	
	public AssetConfigurationException(String message) {
		super(message);
	}
}
