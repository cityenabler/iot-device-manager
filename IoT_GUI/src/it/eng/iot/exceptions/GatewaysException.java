package it.eng.iot.exceptions;

public class GatewaysException extends Exception {

	public GatewaysException(String message) {
		super(message);
	}
}
