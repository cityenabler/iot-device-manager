package it.eng.iot.exceptions;

public class GetAssetException extends Exception {
	
	public GetAssetException(String message) {
		super(message);
	}
}
