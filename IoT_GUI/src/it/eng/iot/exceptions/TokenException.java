package it.eng.iot.exceptions;

public class TokenException extends Exception {
	
	public TokenException(String message) {
		super(message);
	}
}
