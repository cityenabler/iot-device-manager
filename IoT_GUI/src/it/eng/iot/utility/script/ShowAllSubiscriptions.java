package it.eng.iot.utility.script;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import it.eng.iot.utils.RestUtils;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.model.ServiceEntityBean;



public abstract class ShowAllSubiscriptions{
	
//	 private static String OCB_URL = "https://cb.digitalenabler.eng.it"; 
	 private static final String OCB_URL = "http://217.172.12.185:1026";
	 private static final String subsURL = OCB_URL+"/v2/subscriptions?limit=1000";
//	 private static	final String urlServices = OCB_URL+"/v2/entities?type=scope&limit=1000";
//	 private static final String urlServicePaths = OCB_URL+"/v2/entities?type=urbanservice&limit=1000";

	
	public static void main(String[] args) throws Exception {
		
			try {
				
				String scriptExecution = startScript();
				System.out.println(scriptExecution);
			
			}catch(NullPointerException e) {
				
			}catch (final IOException e) {
				
				throw new RuntimeException(e);
			}
	}
	
	
	
	public static String startScript() throws Exception {
		
		String scriptExecution = "";

		
		try {
				
			//GET ALL CONTEXTS
			
			int i=0;
			Set<ServiceEntityBean> fiwareServices = ServiceConfigManager.getServicesTM();
			
			for(ServiceEntityBean service : fiwareServices) {	
				
				String headerservice = service.getId();
				System.out.println(headerservice);
				
				//GET ALL CATEGORIES
				Set<ServiceEntityBean> fiwareServicesPaths = ServiceConfigManager.getServicePathsTM(headerservice);

				for(ServiceEntityBean fiwareServicesPath: fiwareServicesPaths) {
					
					i++;
					
					String headerservicepath = fiwareServicesPath.getId();
					
					String resp =  getEntities(headerservice, headerservicepath, subsURL);
					
					//SHOW all subscriptions, manually search the IP and update it
					if (!resp.equals("[]")) {
						System.out.println("index "+i);
						System.out.println(headerservicepath);
						System.out.println(resp);
					}
				}
			}	
			
			System.out.println("final index "+i);
						
		}catch(org.json.JSONException jsone) {
			scriptExecution =  "SANITIZE DATABASE FAILED.";
			
		}
		
		return scriptExecution;
		
	}
	
	
public static String getEntities(String fiwareservice, String fiwareservicepath, String url) {
		
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String resp = "";
		Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", servicepath);
		
		try{ resp = RestUtils.consumeGet(url, headers); }
		catch(Exception e){
		}
						
		return resp;
	}

	
	
	
}

