package it.eng.iot.utility.script;


import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IOTAgentManager;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.Idas;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceAttribute;
import it.eng.tools.model.IdasDeviceCommand;
import it.eng.tools.model.IdasDeviceGetResult;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceListGetResult;
import it.eng.tools.model.IdasDeviceStaticAttribute;
import it.eng.tools.model.ServiceEntityBean;



public abstract class SanitizeDBForInvertedCoordinates {
	
	private static final Logger LOGGER = LogManager.getLogger(SanitizeDBForInvertedCoordinates.class);
	private static Gson gson = new Gson();
	
//	 private static String OCB_URL = "https://cb.digitalenabler.eng.it"; 
	 private static final String OCB_URL = "http://217.172.12.185:1026";
	
	
	public static String startScript() throws Exception {
		
		String scriptExecution = "";
		String newLocation = "";
		String oldLocation = "";
		boolean deviceIsCreated = false;
		boolean deviceIsDeleted = false;
		

		
		String[] protocols = {"JSON", "UL2_0"};
		//String[] protocols = {"JSON", "UL2_0", "SIGFOX", "LWM2M", "OPCUA"};
		Map<String, String> protocolsMap = new HashMap<String, String>();
		
		protocolsMap.put("JSON", "http://json.devices.s4c.eng.it:8094/iot/devices");
		protocolsMap.put("UL2_0", "http://ul20.devices.s4c.eng.it:8084/iot/devices");
		
//		protocolsMap.put("JSON", "http://37.114.8.49:8094/iot/devices");	
//		protocolsMap.put("UL2_0", "http://37.114.8.49:8084/iot/devices");
		
		//protocolsMap.put("SIGFOX", "http://sigfox.devices.s4c.eng.it:8083/iot/devices");
		//protocolsMap.put("LWM2M", "http://lwm2m.devices.s4c.eng.it:8092/iot/devices");
		//protocolsMap.put("OPCUA", "http://devices.s4c.eng.it:8001/iot/devices");
		//protocolsMap.put("CAYENNELPP", "http://lora.devices.s4c.eng.it:8097/iot/devices");
		//protocolsMap.put("CBOR", "http://lora.devices.s4c.eng.it:8097/iot/device");
		//protocolsMap.put("LORAJSON", "http://lora.devices.s4c.eng.it:8097/iot/device");
		
		try {
				
			//GET ALL CONTEXTS
//			String urlServices = OCB_URL+"/v2/entities?type=scope&limit=1000";
			
			Set<ServiceEntityBean> fiwareServices = ServiceConfigManager.getServicesTM();
			
			for(ServiceEntityBean service : fiwareServices) {
				
				String headerservice = service.getId();
				System.out.println(headerservice);
				
				String urlServicePaths = OCB_URL+"/v2/entities?type=urbanservice&limit=1000";
				
				//GET ALL CATEGORIES
				Set<ServiceEntityBean> fiwareServicesPaths = ServiceConfigManager.getServicePathsTM(headerservice);

				for(ServiceEntityBean fiwareServicesPath: fiwareServicesPaths) {
					
					String headerservicepath = fiwareServicesPath.getId();
					System.out.println(headerservicepath);
					
					for(String protocol: protocols) {
						
						Idas idas = IOTAgentManager.switchIOTA(protocol);
						String urlDevices = protocolsMap.get(protocol);
						
						//GET ALL DEVICES FOR THIS COMBINATION OF SERVICE - SERVICE PATH AT SPECIFIC URL FROM HASHMAP PROTOCOLSMAP
						IdasDeviceListGetResult devlist = idas.getDeviceList(headerservice,headerservicepath, urlDevices);
						Set<IdasDeviceGetResult> devicesList = devlist.getDevices();
						
						if(devlist.getCount() != 0 && devlist != null && devicesList!=null && !devicesList.isEmpty()) {
							
						//FOR EVERY DEVICE AT DEVICESLIST GET VALUES FOR ALL FIELDS
						for (Iterator<IdasDeviceGetResult> it1 = devicesList.iterator(); it1.hasNext(); ) {
							IdasDeviceGetResult device = it1.next();
					       
					        String deviceId = device.getDevice_id();
							String entityName = device.getEntity_name();
							String entityType = device.getEntity_type();
							String transport = device.getTransport();
							String dataformatProtocol =  device.getProtocol().trim();
							String endpoint = device.getEndpoint();
							Set<IdasDeviceAttribute> attributes = device.getAttributes();
							Set<IdasDeviceStaticAttribute> staticAttributes = device.getStatic_attributes();
							Set<IdasDeviceCommand> commands = device.getCommands();
							Object internatAttributes = device.getInternal_attributes();
							String loraDeviceProfileId = device.getLoraDeviceProfileId();
							String organization = device.getOrganization();
							String device_owner = device.getDevice_owner();
							String screenId = device.getScreenId();
							String mobile_device = device.getMobileDevice();
							Set<String> lazy = device.getLazy();  
							
							boolean mobileDevice = false;
							
							//CHECK IF IT IS A MOBILE DEVICE
							for (Iterator<IdasDeviceStaticAttribute> it2 = staticAttributes.iterator(); it2.hasNext(); ) {
								IdasDeviceStaticAttribute attr = it2.next();
						        if (attr.getName().equals("mobile_device")) {
						        	mobileDevice = Boolean.valueOf(attr.getValue());
						        }
						    }
							
							if (mobileDevice)
				        		continue;
							
							//GET OLD LOCATION VALUE, INVERT COORDINATES AND SET NEW VALUE
							for (Iterator<IdasDeviceStaticAttribute> it = staticAttributes.iterator(); it.hasNext(); ) {
								IdasDeviceStaticAttribute attr = it.next();
						        if (attr.getName().equals("location")) {
						        	oldLocation = attr.getValue();
						        	
						        	String[] coords = oldLocation.split(",");
									String lng = coords[0];
									String lat = coords[1];
									newLocation = lat + "," + lng;
									
									attr.setValue(newLocation);
									
						        }
						    }
							
							if( newLocation != null  && newLocation != "") {
								
								IdasDevice oldDevice = idas.getDevice(headerservice, headerservicepath, device.getDevice_id(), urlDevices);
								
								IdasDeviceGetResult newDeviceIdas = new IdasDeviceGetResult( deviceId, entityName, entityType,
																		  transport, dataformatProtocol, 
																		  endpoint, loraDeviceProfileId , 
																		  headerservice, headerservicepath, lazy,
																		  attributes, staticAttributes, 
																		  commands, internatAttributes,
																		  organization,  device_owner,
																		  screenId, mobile_device);
								
								
								IdasDevice newIdas = DeviceConfigManager.setInternalAttribute(dataformatProtocol, 
												newDeviceIdas, headerservice, headerservicepath, true, oldDevice);
								
								Set<IdasDevice> setNewIdasDeviceList =  new HashSet<IdasDevice>();
								setNewIdasDeviceList.add(newIdas);
								IdasDeviceList newDevice = new IdasDeviceList();
								newDevice.setDevices(setNewIdasDeviceList);
								
								//Print OLD device
								Type typeOldDevice = new TypeToken<IdasDevice>(){}.getType();
								String strOldDevice = gson.toJson(oldDevice, typeOldDevice);
								System.out.println(strOldDevice);
								
								//Print NEW device
								Type typeNewDeviceIdas = new TypeToken<IdasDeviceGetResult>(){}.getType();
								String strNewDeviceIdas =  gson.toJson(newDeviceIdas, typeNewDeviceIdas);
								System.out.println(strNewDeviceIdas);
								
							
								try {
									
									deviceIsDeleted = idas.deleteDevice(headerservice, headerservicepath, device.getDevice_id(), urlDevices);
									
									if(deviceIsDeleted) {
										try {
											String registeredDevices = idas.registerDevice(headerservice, headerservicepath, newDevice, urlDevices);
											
										if(registeredDevices != null) {
											LOGGER.log(Level.INFO, "Created successffully!");
											deviceIsCreated = true; 
										}
										
										}catch(org.json.JSONException jsone){
											LOGGER.log(Level.ERROR, jsone.getMessage());
											deviceIsCreated = false;
										}
								}
									
								}catch(Exception e){
									LOGGER.log(Level.ERROR, e.getMessage());
									deviceIsDeleted = false;
								}
							
						
							}else {
								System.out.println("Location is not defined for device " + device.getDevice_id());
								
								//throw new Exception("Location is not defined for device " + device.getDevice_id());
								
							}
							
						}
					    
						if(deviceIsCreated && deviceIsDeleted) {
							scriptExecution = "SANITIZE DATABASE SUCCESSFULLY DONE.";
					    }else {
							scriptExecution  = "SANITIZE DATABASE FAILED.";
						}    
					        
				}else{
					
					LOGGER.log(Level.INFO,"Service - " + headerservice + ", servicePath - " + headerservicepath + ", protocol - " + protocol +" : Devices not found!.");
					
				}
						
			}
		 }
					
		}
						
		}catch(org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			scriptExecution =  "SANITIZE DATABASA FAILED.";
			
		}
		
		return scriptExecution;
		
	}
	
	
	
	//MAIN to execute the script
	public static void main(String[] args) throws Exception {
		
		
			try {
				
				String scriptExecution = startScript();
				System.out.println(scriptExecution);
			
			}catch(NullPointerException e) {
				
				LOGGER.log(Level.ERROR, e.getMessage());
				
			}catch (final IOException e) {
				
				throw new RuntimeException(e);
				
			}
	
	}
}

