package it.eng.iot.configuration;

import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;

public class ConfIotaOPCUA {
	private static final String BUNDLE_NAME = "it.eng.iot.configuration.configuration_iotaOPCUA"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private ConfIotaOPCUA() {
	}

	public static String getString(String key) {
		try {
			Optional<String> env = Optional.ofNullable(System.getenv(key));
			return env.orElse(RESOURCE_BUNDLE.getString(key));
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
