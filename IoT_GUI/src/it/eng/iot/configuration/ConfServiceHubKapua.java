package it.eng.iot.configuration;

import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;

public class ConfServiceHubKapua {
	private static final String BUNDLE_NAME = "it.eng.iot.configuration.configuration_kapua_service"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private ConfServiceHubKapua() {
	}

	public static String getString(String key) {
		try {
			Optional<String> env = Optional.ofNullable(System.getenv(key));
			return env.orElse(RESOURCE_BUNDLE.getString(key));
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
