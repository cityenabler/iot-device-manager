package it.eng.iot.servlet;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.iot.configuration.ConfIDM;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/logout")
public class Logout extends HttpServlet {
	
	private static final long serialVersionUID = 693724303976303888L;
	private static final Logger LOGGER = LogManager.getLogger(HttpServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String nofollow = request.getParameter("nofollow");

		HttpSession session = request.getSession();
		session.removeAttribute("token");
		session.removeAttribute("refresh_token");			
		session.removeAttribute("userPerms");
		session.removeAttribute("userIsSeller");
		session.removeAttribute("isSeller");
		session.removeAttribute("userInfo");
			
		session.invalidate();
		
		if (!("true").equalsIgnoreCase(nofollow)) {
			String host = ConfIDM.getString("keycloak.host");
			String realm = ConfIDM.getString("KEYCLOAK_REALM");
			String logoutUrl = ConfIDM.getString("keycloak.logout");
			String redirect = ConfIDM.getString("keycloak.redirect");
			response.sendRedirect(host + "/realms/" + realm + logoutUrl + redirect);
		}	
	}

}
