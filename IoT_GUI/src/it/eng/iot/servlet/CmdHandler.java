package it.eng.iot.servlet;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.utils.DeviceConfigManager;


@WebServlet(description = "cmdhandler", urlPatterns = { "/cmdhandler" })
public class CmdHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = LogManager.getLogger(CmdHandler.class);

	private Gson gson = new Gson();


	public enum Ajaxaction {

		getDeviceCmdInfopoint("getDeviceCmdInfopoint"),
		sendCommandToDevice("sendCommandToDevice");

		private final String text;

		private Ajaxaction(final String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}

	};

	public CmdHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);

		try {
			String action = request.getParameter("action");
			Ajaxaction eAction = Ajaxaction.valueOf(action);

			switch (eAction) {
			// Get Device Infopoint
			case getDeviceCmdInfopoint:{
				try {
					String serviceId = request.getHeader("fiware-service"); 
					String servicePathId = request.getHeader("fiware-servicepath"); 
					String deviceId = request.getHeader("device_id"); 
					String dataformat = request.getHeader("dataformat"); 
					LOGGER.log(Level.INFO,"Received params: " + serviceId + servicePathId + deviceId);

					// Get device Infopoint
					Set<BasicNameValuePair> deviceCommandInfopoint = DeviceConfigManager.getDeviceCmdInfopoint(serviceId, servicePathId, deviceId, dataformat);				

					// send response
					response.getWriter().write(gson.toJson(deviceCommandInfopoint));
					//LOGGER.log(Level.INFO,"deviceCommandInfopoint found:" + deviceCommandInfopoint.size() + " - " + response.toString());

				}
				catch (org.json.JSONException jsone) {
					LOGGER.log(Level.ERROR, jsone.getMessage());
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(500);
				}
				break;
			}
			case sendCommandToDevice:{
				try {
					String serviceId = request.getHeader("fiware-service"); 
					String servicePathId = request.getHeader("fiware-servicepath"); 
					String deviceId = request.getHeader("device_id"); 
					String cmdName = request.getHeader("cmd_name"); 
					String cmdMessage = request.getHeader("cmd_message"); 
					String dataformat = request.getHeader("dataformat"); 
					String transportprotocol = request.getHeader("transportprotocol"); 
					String retrievedatamode = request.getHeader("retrievedatamode"); 

					LOGGER.log(Level.INFO,"### Platform (OrionCB) sends command to device " + deviceId + " ###");
					LOGGER.log(Level.INFO,"Received params: " + serviceId + "|"+ servicePathId + "|"+ deviceId + "|"+ cmdName + "|"+ cmdMessage);

					// Platform Sends Command to Device
					String result = DeviceConfigManager.sendCommandToDevice(serviceId, servicePathId, deviceId, cmdName, cmdMessage, dataformat, transportprotocol, retrievedatamode);				
					// send response
					response.getWriter().write(gson.toJson(result));

					if (result.contains("500")){
						throw new RuntimeException();				
					}

				}
				catch (org.json.JSONException jsone) {
					LOGGER.log(Level.ERROR, jsone.getMessage());
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(500);
				}catch (RuntimeException re) {
					LOGGER.log(Level.ERROR, re.getMessage());
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(501);
				}
				break;
			}

			default:
				throw new Exception("Unsupported action " + action); 
			}
		}
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString()); 

			response.getWriter().write(jResp.toString());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			doGet(request, response);
		} catch (ServletException | IOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}





}
