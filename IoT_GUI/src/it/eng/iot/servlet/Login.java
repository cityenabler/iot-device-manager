package it.eng.iot.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.digitalenabler.identity.manager.model.Token;
import it.eng.digitalenabler.identity.manager.model.User;
import it.eng.digitalenabler.idm.fiware.IdentityManager;
import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.utils.IdentityManagerUtility;

@WebServlet(description = "login", urlPatterns = { "/login" })
public class Login extends HttpServlet {
	
	private static final long serialVersionUID = -4288687805581054812L;
	
	private static final Logger LOGGER = LogManager.getLogger(Login.class);
		
	public Login() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String code = request.getParameter("code");
		
			try {
				
				if("true".equalsIgnoreCase(ConfIDM.getString("idm.fiware.enabled"))) {
					
					IdentityManager idm = IdentityManagerUtility.getIdentityManager();
					Token token = idm.getAccessToken(code, ConfIDM.getString("idm.client.id"), ConfIDM.getString("idm.client.secret"), ConfIDM.getString("idm.landing.page"));
					
					IdentityManagerUtility.checkToken(token.getToken());
	
					User userInfo = IdentityManagerUtility.getIdentityManager().getUserInfo(token.getToken());
					
					HttpSession session = request.getSession(false);
								session.setAttribute("token", token.getToken());
								session.setAttribute("refresh_token", token.getRefreshToken().orElse(null));
								session.setAttribute("userInfo", userInfo);
					
								
					// Load if URBO is integrated with DeMa
					String urboIntegration = Conf.getString("urbo.integration");
					session.setAttribute("urboIntegration", urboIntegration );
					
					
								
					//response.sendRedirect(request.getContextPath() + Conf.getString("login.redirect.page"));
				}

			} 
			catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
			//	response.sendRedirect(request.getContextPath() + Conf.getString("error.page")); //TODO
			}
			
			
			
			
			try {
				response.sendRedirect(request.getContextPath() + Conf.getString("login.redirect.page"));
			}catch (Exception e) {
				LOGGER.log(Level.ERROR, e.getMessage());
				try {
					response.sendRedirect(request.getContextPath() + Conf.getString("error.page"));//TODO
				} catch (IOException e1) {
					LOGGER.log(Level.ERROR, e.getMessage());
				} 
			}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setStatus(405);

	}

	
	public static void setUserInfo(String token, HttpServletRequest request) {
		
		User userInfo = IdentityManagerUtility.getIdentityManager().getUserInfo(token);
		
		HttpSession session = request.getSession(false);
		session.setAttribute("token", token);
		session.setAttribute("userInfo", userInfo);
		
		
	}
	

}
