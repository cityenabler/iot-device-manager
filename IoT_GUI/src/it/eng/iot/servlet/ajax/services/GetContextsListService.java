package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.model.ServiceEntityBean;

public class GetContextsListService implements AjaxHandlerService{
	
	private static final Logger LOGGER = LogManager.getLogger(GetContextsListService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.log(Level.INFO,"called getContextsList " );
		
		try {
			//Set<ServiceEntityBean> entities = ServiceConfigManager.getServiceConfigList(); // from Orion
		      Set<ServiceEntityBean> entities = ServiceConfigManager.getServicesTM(); // from TenantManager
			response.getWriter().write(gson.toJson(entities));
		} 
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}
}
