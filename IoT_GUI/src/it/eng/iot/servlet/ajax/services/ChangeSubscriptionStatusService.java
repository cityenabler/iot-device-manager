package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.SubscriptionManager;

public class ChangeSubscriptionStatusService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(ChangeSubscriptionStatusService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		LOGGER.log(Level.INFO,"Changing Subscription Status..." );
		
		String headerService = request.getParameter("service");
		String headerServicepath = request.getParameter("servicepath");
		String subscriptionIds = request.getParameter("subscriptionId");
		String subscriptionStatus =  request.getParameter("subscriptionStatus");
		
		LOGGER.log(Level.INFO,"Received params: " + headerService + headerServicepath + subscriptionIds + subscriptionStatus);
		
		String[] subscriptionIdsArr = subscriptionIds.split(",");
	
		try {
			for (String subscriptionId: subscriptionIdsArr){
				if (subscriptionId != null && !subscriptionId.trim().equalsIgnoreCase("undefined") && !subscriptionId.trim().equalsIgnoreCase("")) {
					boolean subscriptionStatusChanged = SubscriptionManager.changeSubscriptionStatus(headerService, headerServicepath, subscriptionId, subscriptionStatus);
					response.getWriter().write(gson.toJson(subscriptionStatusChanged));
				}
			}
		} 
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}
}
