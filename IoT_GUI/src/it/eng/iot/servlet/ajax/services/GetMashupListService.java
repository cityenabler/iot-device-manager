package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.tools.DME;
import it.eng.tools.model.MashupEntity;

public class GetMashupListService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetMashupListService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		try {
			//String serviceId = request.getParameter("service"); 
			//String servicePathId = request.getParameter("servicepath"); 
			//String mashupkeyword = request.getParameter("mashupkeyword"); 
			//mashupkeyword = URLEncoder.encode(mashupkeyword, "UTF-8");
			
			//String servicePathName = ServiceConfigManager.getServicePathName(servicePathId, serviceId);
			
			DME dme = new DME();
			String resp = dme.getMashups();
			Type type = new TypeToken<Set<MashupEntity>>(){}.getType();
			Set<MashupEntity> apiResp = new Gson().fromJson(resp, type);
			
			response.getWriter().write(gson.toJson(apiResp));
			LOGGER.log(Level.INFO,"elem:" + response.toString());
			
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
