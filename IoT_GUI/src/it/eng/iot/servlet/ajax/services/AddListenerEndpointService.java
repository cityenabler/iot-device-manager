package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.CommonUtils;

import it.eng.iot.utils.SubscriptionManager;
import it.eng.iot.utils.SubscriptionManager.SubscriptionsTarget;

import it.eng.iot.utils.UrlshortenerUtils;

public class AddListenerEndpointService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(AddListenerEndpointService.class);
	String[] forbiddenEndpointCharacters = new String[]{ "<", ">", "\"", "\'", "=", ";" ,"(", ")"};
	private static String enabledUrlshortener = ConfTools.getString("urlshortener.active");
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
		
		try {
			
			String fiwareservice = request.getHeader("fiware-service"); 
			String fiwareservicepath = request.getHeader("fiware-servicepath");
			fiwareservicepath=CommonUtils.normalizeServicePath(fiwareservicepath);
			String deviceId = request.getHeader("deviceid"); 
			String endpointUrl = request.getHeader("endpointUrl");
			LOGGER.log(Level.INFO,"Received endpointUrl: "+ endpointUrl);

			final Integer throttling = null;
			final boolean isInternal= false;
		
			String subscriptionIdCreated="";
			String endpointUrlEncodedValue = "";
			
			
			String forbiddenChars = UrlshortenerUtils.getForbiddenChars(endpointUrl, forbiddenEndpointCharacters);	

			if(forbiddenChars!="" && enabledUrlshortener.equals("true")){
					
				endpointUrlEncodedValue = UrlshortenerUtils.encodeUrl(endpointUrl);
					
				if(endpointUrlEncodedValue!=null && !endpointUrlEncodedValue.trim().equalsIgnoreCase("undefined") && !endpointUrlEncodedValue.trim().equalsIgnoreCase("")) {
						
					LOGGER.log(Level.INFO,"Encoded endpoint url: "+ endpointUrlEncodedValue);
					endpointUrl = endpointUrlEncodedValue;
					subscriptionIdCreated = SubscriptionManager.createdOnUpdateDeviceSubscriptionAndSubEntity(deviceId,endpointUrl, fiwareservice, fiwareservicepath, throttling, isInternal, SubscriptionsTarget.CUSTOM.toString() );
					
				
				}else {
					LOGGER.log(Level.ERROR, "Url could not be added!");
					jResp = new JSONObject();
					jResp.put("error", true);
					jResp.put("message", "Url could not be added!");
					response.setStatus(400);
				}
					
			}else if(forbiddenChars=="") {
			    
				subscriptionIdCreated = SubscriptionManager.createdOnUpdateDeviceSubscriptionAndSubEntity(deviceId,endpointUrl, fiwareservice, fiwareservicepath, throttling, isInternal, SubscriptionsTarget.CUSTOM.toString() );
		
			}
			
			if(forbiddenChars!="" && !enabledUrlshortener.equals("true")) {
				//urlshortener is disabled and endpoint url contains forbidden chars
				jResp = new JSONObject();
				jResp.put("error", true);
				jResp.put("forbidden", true);
				jResp.put("forbiddenChars", forbiddenChars );
				response.setStatus(400);
				
			}else if(null == subscriptionIdCreated || subscriptionIdCreated.equals("")) {
					
				jResp = new JSONObject();
				jResp.put("error", true);
				jResp.put("message", "Subscription is not created.");
				response.setStatus(400);
					
			}else {
					
				jResp = new JSONObject();
				jResp.put("subscriptionId", subscriptionIdCreated);
				jResp.put("endpointUrl", endpointUrl);
					
			}
				
			response.getWriter().write(jResp.toString());
			
		}catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.ERROR,e.getMessage());
	
			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString());
			response.setStatus(500);
			response.getWriter().write(jResp.toString());
		}	
	}

}
