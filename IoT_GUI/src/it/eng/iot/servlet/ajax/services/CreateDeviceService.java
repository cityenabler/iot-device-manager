package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.configuration.ConfMQTTBroker;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.hub.controller.KapuaController;
import it.eng.iot.servlet.model.FEDevicePublishedConfirmBean;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.DeviceUtils;
import it.eng.iot.utils.IOTAgentManager;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.iot.utils.SubscriptionManager.SubscriptionsTarget;
import it.eng.iot.utils.listener.BigdataNotifier;
import it.eng.iot.utils.listener.LoggerNotifier;
import it.eng.iot.utils.listener.iDeviceListener;
import it.eng.tools.Idas;
import it.eng.tools.Orion;
import it.eng.tools.model.EntityAttributeMetadata;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceStaticAttribute;

public class CreateDeviceService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(CreateDeviceService.class);
	static String measureNotificationApiEndpoint = 	ConfTools.getString("measure-notification.api.endpoint");
	private Gson gson = new Gson();
	
	public static Set<iDeviceListener> deviceListeners = new HashSet<iDeviceListener>();
	
	static{
		deviceListeners.add(new BigdataNotifier());
		deviceListeners.add(new LoggerNotifier());
	}
	
	
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
			jResp.put("error", false);
		
		try {String payload = request.getParameter("payload");
		LOGGER.log(Level.INFO,"Device payload: "+payload);
		String fiwareservice = request.getHeader("fiware-service");
		String fiwareservicepath = request.getHeader("fiware-servicepath");
		
		IdasDeviceList devlist = gson.fromJson(payload, IdasDeviceList.class);
		LOGGER.log(Level.INFO,"Device: "+devlist);
		
		FEDevicePublishedConfirmBean devicePublishedConfirmData = null;
	
		String dataformatProtocol ="UL2_0"; // Default
		
		String deviceId = "";
		Set<IdasDevice> devices = devlist.getDevices();
		
		Integer kapuaProvisioningErrorCode = 0;
        String kapuaProvisioningErrorMsg = "default";
		
		for (IdasDevice device : devices) {
			
			//it is OPCUA set the ObjectId following a convention
			if (device.getProtocol().equalsIgnoreCase("OPCUA")) {
				device = DeviceConfigManager.setOpcuaDeviceFields(device);
			}
									
			
			
			String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
			IdasDeviceStaticAttribute dateModifiedAttr = new IdasDeviceStaticAttribute("dateModified", "DateTime", dateModified);
			device.getStatic_attributes().add(dateModifiedAttr);
			
			deviceId = device.getDevice_id();
			LOGGER.log(Level.INFO," create_device deviceId: "+ deviceId);
			
			LOGGER.log(Level.INFO,"dataformat protocol: "+ device.getProtocol());
			String transportProtocol = device.getTransport(); // HTTP/MQTT
			device.setTransport(transportProtocol.toUpperCase()); // IT MUST BE UPPERCASE HTTP for IDAS
			LOGGER.log(Level.INFO,"transportProtocol: "+ transportProtocol);
			
			dataformatProtocol = device.getProtocol(); // UL2_0/JSON
			LOGGER.log(Level.INFO,"dataformatProtocol: "+ dataformatProtocol);
			
			// FORCE endpoint in case of MQTT Protocol
			// IF RETRIEVE DATA MODE = PULL then the endpoint 
			if (transportProtocol.equalsIgnoreCase("MQTT")){
				// getRetrieveDataMode
				String retrieveDataMode = "";
				Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();
				for(IdasDeviceStaticAttribute attr : staticAttrs){
					if ("retrieve_data_mode".equalsIgnoreCase(attr.getName()) ){
						retrieveDataMode = attr.getValue();
					}
				}	
				LOGGER.log(Level.INFO,"retrieve_data_mode " + retrieveDataMode);
				if (retrieveDataMode.equalsIgnoreCase("pull")) {
					if (dataformatProtocol.equalsIgnoreCase("UL2_0")){
						
						boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));
						
						String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
						if (mqttBrokerSslEnabled)
							mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						
						String mqttBrokerEndpoint = mqttBrokerProtocol +
								ConfMQTTBroker.getString("mqtt.broker.ul20.host") + ":" + 
								ConfMQTTBroker.getString("mqtt.broker.ul20.port");
								LOGGER.log(Level.INFO,"MQTT Broker Endpoint using ul20 " + mqttBrokerEndpoint);
						device.setEndpoint(mqttBrokerEndpoint);	
					} else if (dataformatProtocol.equalsIgnoreCase("JSON")){
						
						boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));
						
						String mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
						if (mqttBrokerSslEnabled)
							mqttBrokerProtocol = ConfMQTTBroker.getString("mqtt.broker.protocol.ssl");
						
						
						String mqttBrokerEndpoint = mqttBrokerProtocol +
								ConfMQTTBroker.getString("mqtt.broker.json.host") + ":" + 
								ConfMQTTBroker.getString("mqtt.broker.json.port");
								LOGGER.log(Level.INFO,"MQTT Broker Endpoint using json " + mqttBrokerEndpoint);
						device.setEndpoint(mqttBrokerEndpoint);	
					}
				}
			}
			
				final Integer throttling = 1 ;
				final boolean isInternal = true;
			
				//create notification to measureNotification
				// STEP 1: create Subscription
				String createdDeviceSubscription = SubscriptionManager.createDeviceSubscription(deviceId, measureNotificationApiEndpoint, null, fiwareservice, fiwareservicepath, throttling, isInternal);
				
				
				
				// STEP 2: Get the subscriptionId
				JSONObject subscrIdj = new JSONObject(createdDeviceSubscription);
				String subscriptionIdCreated = subscrIdj.getString("subscriptionId");
				LOGGER.log(Level.INFO,"Subscription Id Created: "+ subscriptionIdCreated);
				
				// STEP3: CREATE ENTITY WITH subscription_id and device_id
				String createdDeviceSubscriptionEntity = SubscriptionManager.createDeviceEndpointSubscriptionEntity(deviceId, subscriptionIdCreated, measureNotificationApiEndpoint, fiwareservice, fiwareservicepath, SubscriptionsTarget.MEASURE_NOTIFICATION.toString());
			
			LOGGER.log(Level.INFO,"Device Subscription Id Created: "+ createdDeviceSubscriptionEntity);
			
			devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
			
			device = DeviceConfigManager.setInternalAttribute(dataformatProtocol, device, fiwareservice, fiwareservicepath, false, null);
			
			
			for(iDeviceListener listener : deviceListeners){
				listener.onDeviceCreate(device, fiwareservice, fiwareservicepath);
			}
			
			//STEP 4 - Create Kapua device on Kapua-Kura
			if (DeviceUtils.isKapuaHub(device)) {
				String topic = DeviceConfigManager.getMqttPublishMeasureTopic(devicePublishedConfirmData.getApikey(), deviceId);
				
				Triplet<String, Integer, String> kapuaProvisioningResult = KapuaController.provisioningDeviceProcedure(device, fiwareservice, topic);
				kapuaProvisioningErrorCode = kapuaProvisioningResult.getValue1();
				kapuaProvisioningErrorMsg = kapuaProvisioningResult.getValue2();
				if (kapuaProvisioningResult.getValue0().equalsIgnoreCase("error")) {
					LOGGER.log(Level.ERROR,"Error registering the device \"" + device.getDevice_id() + "\" on Kapua hub");
					
					//Delete subscription previously created in Orion
					LOGGER.log(Level.INFO,"Executing rollback for Orion subscription and subscription entity of the device \"" + device.getEntity_name() + "\"");
					SubscriptionManager.deleteDeviceSubscription(fiwareservice, fiwareservicepath, deviceId, subscriptionIdCreated);
					
//					Skip registration to Orion for those devices that haven't been registered in Kapua for error in STEP 4
					devlist.getDevices().remove(device);
				}
			}
		}
		
		//List is empty in the case there are only Kapua devices to be created and something went wrong on STEP 4 
		if (!devlist.getDevices().isEmpty()) {
		
			// STEP 5: Create Device on IDAS
			// SWITCH AGENT
			LOGGER.log(Level.INFO,"dataformatProtocol: "+ dataformatProtocol);
			Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
			
			//Idas idas = new Idas();
			jResp = idas.registerDevice(fiwareservice, fiwareservicepath, devlist);
			
			// UPDATE METADATA ON ORION IN case of JSON dataformat ***********************************************
			if (dataformatProtocol.equalsIgnoreCase("json")){
				
					String attrjsonschema = request.getParameter("attrjsonschema");
					LOGGER.log(Level.INFO,"Device attribute json schema: "+attrjsonschema);
					
					JSONObject jobj = new JSONObject(attrjsonschema);
					JSONArray jarr = jobj.getJSONArray("attrjsonschema");
			  try {
					Type type = new TypeToken<Set<EntityAttributeMetadata>>(){}.getType();
					Set<EntityAttributeMetadata> schemas = gson.fromJson(jarr.toString(), type);
					LOGGER.log(Level.INFO,"Device: "+schemas);
					for (EntityAttributeMetadata schema:schemas){
						String attrName = schema.getName();
						String jsonSchema = schema.getJsonschema();					
						Orion orion = new Orion();
						orion.updateEntityAttributeMetadata(fiwareservice, fiwareservicepath, deviceId, attrName, jsonSchema);
						LOGGER.log(Level.INFO,"Attribute " + attrName + " successfully updated");
					}
				} catch (Exception e){
					LOGGER.log(Level.ERROR,"Wrong jsonschema " + attrjsonschema);
				}
			}
			
			response.getWriter().write(gson.toJson(devicePublishedConfirmData));
		} else {
		    jResp = new JSONObject();
		    jResp.put("error", true); //$NON-NLS-1$
		    jResp.put("message", kapuaProvisioningErrorMsg);
		    response.setStatus(kapuaProvisioningErrorCode);
		    response.getWriter().write(jResp.toString());
		}
		}catch (Exception e) {			
		
			e.printStackTrace();
			LOGGER.log(Level.ERROR,e.getMessage());
		
			jResp = new JSONObject();
			jResp.put("error", true); //$NON-NLS-1$
			jResp.put("message", e.toString()); //$NON-NLS-1$
			response.setStatus(500);
		
			response.getWriter().write(jResp.toString());
			
		}
	
	}
	
}
		

