package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.servlet.model.Subscription;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.tools.model.DeviceSubscriptionEntity;
import it.eng.tools.model.EntityAttribute;

public class GetDeviceSubscriptionsService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceSubscriptionsService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			
			String serviceId = request.getHeader("fiware-service"); 
			String servicePathId = request.getHeader("fiware-servicepath"); 
			String deviceId = request.getParameter("device_id"); 
			LOGGER.log(Level.INFO,"Received params: " + serviceId + serviceId + deviceId);
			
			String subscriptionId = "";
			String subscriptionStatus = "";
			String mashupId = "";
			
			Set<DeviceSubscriptionEntity> deviceSubscriptions = SubscriptionManager.getDeviceSubscriptions(serviceId, servicePathId, deviceId);
			Set<DeviceSubscriptionEntity> resultEntities = new HashSet<DeviceSubscriptionEntity>();
			
			for (DeviceSubscriptionEntity entity : deviceSubscriptions){
				subscriptionId = entity.getSubscriptionId().getValue();
				LOGGER.log(Level.INFO,"Retrieved SubscriptionId " + subscriptionId);
				
				mashupId = entity.getMashupId().getValue();
				LOGGER.log(Level.INFO,"Retrieved MashupId " + mashupId);
				// getSubscritionStatus
				Subscription subscriptionEntity = SubscriptionManager.getSubscription(serviceId, servicePathId, subscriptionId);
				
				subscriptionStatus = subscriptionEntity.getStatus();	
				// setStatus
				entity.setStatus(new EntityAttribute<String>(subscriptionStatus));
				// add
				resultEntities.add(entity);
				LOGGER.log(Level.INFO,"entity loaded:" + resultEntities.size());
				 
			}
			response.getWriter().write(gson.toJson(resultEntities));
			LOGGER.log(Level.INFO,"elem:" + resultEntities.size());
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
