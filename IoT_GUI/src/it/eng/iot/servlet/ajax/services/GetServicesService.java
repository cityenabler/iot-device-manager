package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.configuration.Conf;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.keycloak.User;
import it.eng.tools.model.ServiceEntityBean;

public class GetServicesService implements AjaxHandlerService{
	
	private static final Logger LOGGER = LogManager.getLogger(GetServicesService.class);
	
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
			//Set<ServiceEntityBean> services = ServiceConfigManager.getServices(); // from Orion
			Set<ServiceEntityBean> services = ServiceConfigManager.getServicesTM(); // from TenantManager
			
			String enabler = request.getParameter("enabler");//$NON-NLS-1$
			
			Set<String> allEnablers = new HashSet<String>();
			
			if (enabler == null ) {
				
				allEnablers.add(Conf.getString("MultiEnabler.default"));
			}else {
				
				String[] enablersA = enabler.split(",");
				for (String enab:enablersA) {
					allEnablers.add(enab);
				}
			}
			// Get user info 
			//UserInfoBean userInfo = (UserInfoBean) session.getAttribute("userInfo");
			User userInfo = (User) session.getAttribute("userInfo");
			// Verify userIsSeller 
			boolean userIsSeller= false;
			if(session.getAttribute("userIsSeller") != null){
			    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
			}
			
			LOGGER.log(Level.INFO,"getServices: userIsSeller " + userIsSeller );
			
			if (!userIsSeller){		
				Set<String> orgs = userInfo.getGroups().get();	
				Set<ServiceEntityBean> permittedServices = ServiceConfigManager.getPermittedServices(services, orgs);
				services.retainAll(permittedServices);
			}
			Set<ServiceEntityBean> servicesResp =  new HashSet<ServiceEntityBean>();
			
			for (ServiceEntityBean serv:services) {
				
				if (allEnablers.contains(serv.getRefScope().getValue()))
					servicesResp.add(serv);
			}
			
			List<ServiceEntityBean> sortedListResp = new ArrayList<ServiceEntityBean>(servicesResp);
			
			Collections.<ServiceEntityBean>sort(sortedListResp);
			
			response.getWriter().write(gson.toJson(sortedListResp));
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
		
	}

}
