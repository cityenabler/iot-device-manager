package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import it.eng.iot.exceptions.DeleteAssetException;
import it.eng.iot.hub.controller.KapuaController;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.servlet.model.Permission;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.listener.BigdataNotifier;
import it.eng.iot.utils.listener.LoggerNotifier;
import it.eng.iot.utils.listener.iDeviceListener;
import it.eng.tools.keycloak.User;

public class DeleteDevicesService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(DeleteDevicesService.class);
	
	public static Set<iDeviceListener> deviceListeners = new HashSet<iDeviceListener>();
	
	static{
		deviceListeners.add(new BigdataNotifier());
		deviceListeners.add(new LoggerNotifier());
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		boolean permit = false;
		String actionOnDevice = "D";
		JSONObject jResp = new JSONObject();
		jResp.put("error", false);
		
		try {
			String body = request.getParameter("payload");
			String headerService = request.getHeader("fiware-service");
			String headerServicePath = request.getHeader("fiware-servicepath");
			
			// fix permission
			HttpSession session = request.getSession();
//			String userAccessToken = (String)session.getAttribute("token");
			User userInfo = (User) session.getAttribute("userInfo");
			Set<Permission> userPerms = (Set<Permission>) session.getAttribute("userPerms");
			boolean userIsSeller = (boolean) session.getAttribute("userIsSeller");
			
			JSONObject jbody = new JSONObject(body);
			JSONArray jsArray = (JSONArray) jbody.get("devices");
			
			for (int i = 0; i < jsArray.length(); i++) {
				try {
					JSONObject currDevice = jsArray.getJSONObject(i);
					String device_id = currDevice.getString("device_id");
					// check if the action is permitted
					JSONObject device = currDevice.getJSONObject("device");
					
					String streaming_url="";
					if (device.has("streaming_url"))
							 streaming_url = device.getString("streaming_url");
					
					boolean isCamera = false;
					if (!("").equals(streaming_url))
						 isCamera = true;
					
					if (userIsSeller){
						permit = true;
					} else {
						FEDevice fedevice = new FEDevice(device_id);
						fedevice.setDeviceOwner(device.getString("device_owner"));
						fedevice.setOrganization(device.getString("organization"));
						permit = CommonUtils.isPermittedAction(actionOnDevice, fedevice, userInfo, userPerms);
					}
					if (permit){
						//String dataformat_protocol = currDevice.has("dataformat_protocol") ? currDevice.getString("dataformat_protocol") : "UL2_0";
						
						String dataformat_protocol = "UL2_0";
						if (currDevice.has("dataformat_protocol")) {
							String dfp = currDevice.getString("dataformat_protocol");
							if (!dfp.equals("")) {
								dataformat_protocol = dfp;
							}
						}
						
						//Add Hubs for DeMa version 2
						//For Kapua device call delete and only if success proceed with delete process in IDAS and ORION
						String hubName = "";
						String hubType = "";
						String gateway = "";
//						String asset = "";
						if(device.has("hubName")) hubName = device.getString("hubName");
						if(device.has("hubType")) hubType = device.getString("hubType");
						if(device.has("gateway")) gateway = device.getString("gateway");
//						if(device.has("asset")) asset = device.getString("asset");
						
						if (hubType.equalsIgnoreCase("kapua")) {
    						String ret = KapuaController.deleteDeviceProcedure(device_id, hubName, headerService, gateway);
    						if (ret == null) {
    						    throw new DeleteAssetException("Error trying to delete device on Kapua");
    						} else {
    						    LOGGER.log(Level.INFO,"Device \"" + device_id + "\" correctly deleted on hub \"" + hubName + "\" for gateway \"" + gateway + "\"");
    						}
						}
						//End Add Hubs for DeMa version 2
						
						LOGGER.log(Level.INFO,"dataformat_protocol *** : " + dataformat_protocol);
						boolean completed = DeviceConfigManager.deleteDeviceProcess(headerService, headerServicePath, device_id,dataformat_protocol, isCamera);
						
						String normalizedServicepath = CommonUtils.normalizeServicePath(headerServicePath);
						
						for(iDeviceListener listener : deviceListeners){
							
							listener.onDeviceDelete(device_id, headerService, normalizedServicepath);
						}
						
						jResp.put("error", !completed);
					} else {
						jResp.put("error", true); 
						jResp.put("message", "Permission Denied");
					}
				}
				catch (DeleteAssetException ex) {
                    LOGGER.log(Level.ERROR, ex.getMessage());
                    jResp.put("error", true); 
                    jResp.put("message", "Delete error");
                }
				catch (Exception ex) {
					LOGGER.log(Level.ERROR, ex.getMessage());
					jResp.put("error", true); 
					jResp.put("message", "Permission Denied");
				}
			}

		} 
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			jResp = new JSONObject();
			response.setStatus(500);
		}
		response.getWriter().write(jResp.toString());
		
	}
	
}
