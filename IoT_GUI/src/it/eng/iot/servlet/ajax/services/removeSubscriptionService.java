package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfDme;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.SubscriptionManager;

public class removeSubscriptionService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(removeSubscriptionService.class);
	static String notificationOrionDmeOnDeviceUpdate = 	ConfDme.getString("dme.address") + ConfDme.getString("orion.dme.notification.update");
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
		
		try {
			
			String fiwareservice = request.getHeader("fiware-service"); 
			String fiwareservicepath = request.getHeader("fiware-servicepath"); 
			fiwareservicepath=CommonUtils.normalizeServicePath(fiwareservicepath);
			String deviceId = request.getHeader("deviceid"); 
			String subscriptionId = request.getHeader("subscriptionId");
			
			SubscriptionManager.deleteDeviceSubscription(fiwareservice, fiwareservicepath, deviceId, subscriptionId);

			jResp.put("Dev subscription deleted ", subscriptionId);
			response.getWriter().write(jResp.toString());
			
			
			
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
