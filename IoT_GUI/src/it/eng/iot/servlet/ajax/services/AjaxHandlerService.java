package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AjaxHandlerService {
	
	public void handleAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception;
	
}
