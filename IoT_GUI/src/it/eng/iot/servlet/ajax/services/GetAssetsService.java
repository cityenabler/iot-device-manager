package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import it.eng.iot.hub.model.kapua.Asset;
import it.eng.iot.hub.model.kapua.AttributesDema;
import it.eng.iot.hub.model.kapua.KuraAssetConfiguration;
import it.eng.iot.hub.model.kapua.KuraAssetProperty;
import it.eng.iot.hub.utils.kapua.KapuaAssetUtils;
import it.eng.iot.utils.AssetTemplateManager;

public class GetAssetsService implements AjaxHandlerService {
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			String pageSize = request.getParameter("pagesize");//$NON-NLS-1$
			List<Asset> assets = AssetTemplateManager.getAllAssetsPaginated("0", pageSize);
			Set<JsonObject> jsonAssets = new HashSet<JsonObject>();
			
			for (Asset asset : assets) {
				JsonObject assetObj = new JsonObject();
				assetObj.addProperty("name", asset.getName());
				assetObj.addProperty("type", asset.getType());
				
				List<AttributesDema> attributesDemaList = asset.getAttributesDemaList();
				JsonArray jsonAttrArray = new JsonArray();
				for (AttributesDema attr : attributesDemaList) {
					JsonObject jsonAttr = new JsonObject();
					jsonAttr.addProperty("name", attr.getName());
					jsonAttr.addProperty("type", attr.getType());
					jsonAttrArray.add(jsonAttr);
				}
				
				assetObj.addProperty("attributes", jsonAttrArray.toString());
				KuraAssetConfiguration conf = asset.getTemplateAsset().getConfiguration().get(0);
				
				//Check driver to diversify input parameters in new device page of frontend based on sensor type
				String driverPid = KapuaAssetUtils.getPropertyByName(conf, "driver.pid").getValue().get(0);
				assetObj.addProperty("driverPid", driverPid);
				jsonAssets.add(assetObj);
			}
			
			response.getWriter().write(gson.toJson(jsonAssets));
		}
		catch (org.json.JSONException jsone) {
			jsone.printStackTrace();
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}
}
