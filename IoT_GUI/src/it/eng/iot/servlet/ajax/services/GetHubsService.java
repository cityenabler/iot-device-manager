package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import it.eng.iot.hub.model.HubEntity;
import it.eng.iot.utils.HubManager;

public class GetHubsService implements AjaxHandlerService {
	
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			String countHubs = HubManager.countHubs();
			List<HubEntity> allHubs = HubManager.getAllHubsPaginated("0", countHubs);
			
			//Order the list by ascending name
			allHubs.sort(Comparator.comparing(HubEntity::getName));
			
			Set<JsonObject> jsonHubs = new HashSet<JsonObject>();
			
			for (HubEntity hub:allHubs) {
				JsonObject hubObj = new JsonObject();
				hubObj.addProperty("name", hub.getName());
				hubObj.addProperty("type", hub.getType());
				hubObj.addProperty("endpoint", hub.getUrl() + ":" + hub.getPort());
				jsonHubs.add(hubObj);
			}
			
			//Static add of Idas hub entry that is the one in the existing in Fiware stack
			JsonObject idasHubObj = new JsonObject();
			idasHubObj.addProperty("name", "Idas");
			idasHubObj.addProperty("type", "idas");
			idasHubObj.addProperty("endpoint", "");
			jsonHubs.add(idasHubObj);
			
			response.getWriter().write(gson.toJson(jsonHubs));
			
			}
			catch (org.json.JSONException jsone) {
				jsone.printStackTrace();
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
