package it.eng.iot.servlet.ajax.services;

public enum AjaxHandlerServiceType {
	
		getContextsList("getContextsList"), 
		subscribe("subscribe"),
		getServices("getServices"), 
		getServicePaths("getServicePaths"),
		getServiceName("getServiceName"), 
		getServicePathName("getServicePathName"),
		create_device("create_device"), 
		getDevice("getDevice"),
		deleteDevices("deleteDevices"), 
		modifyDevice("modifyDevice"), 
		getMapCenter("getMapCenter"), 
		modifyMapCenter("modifyMapCenter"),
		changeSubscriptionStatus("changeSubscriptionStatus"),
		getDeviceSubscriptions("getDeviceSubscriptions"),
		getMashupList("getMashupList"),
		getDeviceMashup("getDeviceMashup"),
		getDeviceInfopoint("getDeviceInfopoint"),
		getCategories("getCategories"),
		getGatewaysKapua("getGatewaysKapua"),
		getHubs("getHubs"),
		getDeviceListMulticontext("getDeviceListMulticontext"),
		getLoraDeviceProfiles("getDeviceProfiles"),
		check_name("check_name"),
		addMashup("addMashup"),
		addListenerEndpoint("addListenerEndpoint"),
		removeSubscription("removeSubscription"),
		getDeviceListenerEndpoints("getDeviceListenerEndpoints"),	
		checkDeviceId("checkDeviceId"),
		getAssets("getAssets");
	

		private final String text;

		private AjaxHandlerServiceType(final String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}

}
