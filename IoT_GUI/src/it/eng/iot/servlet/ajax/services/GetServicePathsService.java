package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.model.ServiceEntityBean;

public class GetServicePathsService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetServicePathsService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		try {
			String currService = request.getParameter("service"); //$NON-NLS-1$
			Set<ServiceEntityBean> servicepaths = new HashSet<ServiceEntityBean>();
			
			// from Orion
			//servicepaths = ServiceConfigManager.getServicePaths(currService);
			
			// from Tenant Manager
			servicepaths = ServiceConfigManager.getServicePathsTM(currService);
				
			response.getWriter().write(gson.toJson(servicepaths));
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
			
	}
}
