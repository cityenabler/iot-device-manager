package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfDme;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.SubscriptionManager;

public class AddMashupService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(AddMashupService.class);
	static final String notificationOrionDmeOnDeviceUpdate = 	ConfDme.getString("dme.address.onorion") + ConfDme.getString("orion.dme.notification.update");
	static final String notificationOrionDmeOnDeviceUpdateFinal = 	 ConfDme.getString("orion.dme.notification.update.final");
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
		
		try {
			String fiwareservice = request.getHeader("fiware-service"); 
			String fiwareservicepath = request.getHeader("fiware-servicepath"); 
			fiwareservicepath=CommonUtils.normalizeServicePath(fiwareservicepath);
			String deviceId = request.getHeader("deviceid"); 
		

			String mashupId = request.getHeader("mashupid");
			LOGGER.log(Level.INFO,"Received MashupIds: "+ mashupId);
			
		
			String subscriptionIdCreated="";
			
			// IF the mashup has been configured on the new device form, the subscription will be created
			if (mashupId != null && !mashupId.trim().equalsIgnoreCase("undefined") && !mashupId.trim().equalsIgnoreCase("")) {
					
					final Integer throttling = null;
					final boolean isInternal= false;
					final HashSet<String> notificationAttrs = null;
				
					// STEP 1: Create Subscription for Device
					String mashupUrl = notificationOrionDmeOnDeviceUpdate+mashupId+notificationOrionDmeOnDeviceUpdateFinal;
					String createdOnUpdateDeviceSubscription = SubscriptionManager.createDeviceSubscription(deviceId, mashupUrl, notificationAttrs, fiwareservice, fiwareservicepath, throttling, isInternal);
					
					// STEP 2: Get the subscriptionId
					JSONObject subscrIdj = new JSONObject(createdOnUpdateDeviceSubscription);
					 subscriptionIdCreated = subscrIdj.getString("subscriptionId");
					LOGGER.log(Level.INFO,"Subscription Id Created: "+ subscriptionIdCreated);
					
					// STEP3: CREATE ENTITY WITH subscription_id and device_id
					String createdDeviceSubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(deviceId, subscriptionIdCreated, mashupId, fiwareservice, fiwareservicepath);
					LOGGER.log(Level.INFO,"Created Device Subscription Entity on Orion: " + createdDeviceSubscriptionEntity);
			}
			
			
			jResp.put("subscriptionId", subscriptionIdCreated);
			
			response.getWriter().write(jResp.toString());
			
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
