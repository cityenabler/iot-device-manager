package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.eng.iot.servlet.model.MapCenter;
import it.eng.iot.utils.DeviceConfigManager;

public class GetMapCenterService implements AjaxHandlerService {

	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		String fiwareService = request.getHeader("fiware-service");
		
		MapCenter mapcenter = new MapCenter();
		
		try {
			
			mapcenter = DeviceConfigManager.getMapCenter(fiwareService);
			response.getWriter().write(gson.toJson(mapcenter));
			
		}catch(Exception e) {

			response.getWriter().write(gson.toJson(mapcenter));//it returns default values 0,0
			
		}
	}
}
