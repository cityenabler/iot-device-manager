package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import it.eng.tools.Orion;

public class SubscribeService implements AjaxHandlerService{
	
	private static final Logger LOGGER = LogManager.getLogger(SubscribeService.class);
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);
		try {
			String body = request.getParameter("payload");
			String fiwareservice = request.getHeader("fiware-service");
			String fiwareservicepath = request.getHeader("fiware-servicepath");
			
			Orion orion = new Orion();
			boolean result = orion.subscribe(fiwareservice, fiwareservicepath, body);
			jResp.put("error", !result);
			
			response.getWriter().write(jResp.toString());
		}
		catch (Exception e) {			
			
			e.printStackTrace();
			LOGGER.log(Level.ERROR,e.getMessage());
		
			jResp = new JSONObject();
			jResp.put("error", true); //$NON-NLS-1$
			jResp.put("message", e.toString()); //$NON-NLS-1$
			response.setStatus(500);
		
			response.getWriter().write(jResp.toString());
			
		}
		
	}
	
}
