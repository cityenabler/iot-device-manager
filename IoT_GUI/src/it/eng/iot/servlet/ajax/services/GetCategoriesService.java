package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.keycloak.KeycloakContext;
import it.eng.tools.model.ServiceEntityBean;

public class GetCategoriesService implements AjaxHandlerService {
	
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		KeycloakContext kc = new KeycloakContext(request);
		String service_token = kc.getServiceAccessToken();
		
		try {
			
			String currServices = request.getParameter("services");//$NON-NLS-1$ 					
			
			
			
			//Set<ServiceEntityBean> categories = ServiceConfigManager.getCategories(currServices, true); //  FROM ORION
			
			/*
			 
			  FROM TENANT MANAGER
			 
			 */
			Set<ServiceEntityBean> categories = ServiceConfigManager.getCategoriesTM(currServices, true, service_token); //TODO FIL check it was false in Tamara's code
			Set<String> categoriesS = new HashSet<String>();
			
			for (ServiceEntityBean cat:categories) {
				categoriesS.add(cat.getName().getValue());
			}

			List<String> categoriesSsorted = new ArrayList<String>(categoriesS);
			Collections.<String>sort(categoriesSsorted);
			
			response.getWriter().write(gson.toJson(categoriesSsorted));
			
			}
			catch (org.json.JSONException jsone) {
				jsone.printStackTrace();
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
