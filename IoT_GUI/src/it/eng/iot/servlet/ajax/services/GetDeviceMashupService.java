package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.tools.DME;
import it.eng.tools.model.DeviceSubscriptionEntity;
import it.eng.tools.model.MashupEntity;

public class GetDeviceMashupService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceMashupService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			String serviceId = request.getHeader("fiware-service"); 
			String servicePathId = request.getHeader("fiware-servicepath"); 
			servicePathId=CommonUtils.normalizeServicePath(servicePathId);
			String deviceId = request.getParameter("device_id"); 
			LOGGER.log(Level.INFO,"Received params: " + serviceId + serviceId + deviceId);
			
			
			// Get mashupId for the deviceId
			Set<DeviceSubscriptionEntity> deviceSubscriptions = SubscriptionManager.getDeviceSubscriptions(serviceId, servicePathId, deviceId);
			

			// Get all mashups
			DME dme = new DME();
			String resp = dme.getMashups();
			LOGGER.log(Level.INFO,"DME response: " + resp);
			Type type = new TypeToken<Set<MashupEntity>>(){}.getType();
			Set<MashupEntity> apiResp = new Gson().fromJson(resp, type);
			
			Set<MashupEntity> selectedMashup = new HashSet<MashupEntity>();
			
				for ( DeviceSubscriptionEntity deviceSubscription: deviceSubscriptions) {
					for (MashupEntity mashup : apiResp){
						LOGGER.log(Level.INFO,"mashup: " + mashup.getName());
						if (mashup.getMashupId().equalsIgnoreCase(deviceSubscription.getMashupId().getValue())) {
							LOGGER.log(Level.INFO,"=== " + deviceSubscription.getMashupId().getValue());
							mashup.setStatus(deviceSubscription.getStatus().getValue());
							mashup.setSubscriptionId(deviceSubscription.getSubscriptionId().getValue());
							selectedMashup.add(mashup);
						}
					}
			}
			
			response.getWriter().write(gson.toJson(selectedMashup));
			LOGGER.log(Level.INFO,"Mashup found:" + selectedMashup.size() + " - " + response.toString());
			
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
