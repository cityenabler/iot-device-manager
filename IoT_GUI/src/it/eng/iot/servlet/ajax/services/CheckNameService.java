package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import it.eng.iot.utils.DeviceConfigManager;
import it.eng.tools.model.DeviceEntityBean;

public class CheckNameService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(CheckNameService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		LOGGER.log(Level.INFO,"called check_name " );
		
		boolean nameAvailability = true;
		
		String fiwareservice = request.getHeader("fiware-service");
		String fiwareservicepath = request.getHeader("fiware-servicepath").replace("/", "");
		String devicename = request.getParameter("devicename");
		
		Set<DeviceEntityBean> devicesResp = DeviceConfigManager.getOCBDevices(fiwareservice,fiwareservicepath);
		
		for(DeviceEntityBean device : devicesResp) {
			if(device.getScreenId().getValue().equals(devicename) == true) {
				nameAvailability = false;
				break;
			}
		}
		response.getWriter().write(gson.toJson(nameAvailability));
	}
}
