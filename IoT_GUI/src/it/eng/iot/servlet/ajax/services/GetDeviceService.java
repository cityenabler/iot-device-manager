package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import it.eng.iot.servlet.model.DataformatProtocol;
import it.eng.iot.servlet.model.FECategory;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.servlet.model.Permission;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IOTAgentManager;
import it.eng.iot.utils.LoraUtils;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.Idas;
import it.eng.tools.keycloak.User;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceInternalAttributeLoRa;

public class GetDeviceService implements AjaxHandlerService{
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceService.class);
	private Gson gson = new Gson();

	String editActionOnDevice = "U";
	String deleteActionOnDevice = "D";
	
	@Override
	@SuppressWarnings("unchecked")
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
				
		String fiwareservice = request.getHeader("fiware-service");
		String fiwareservicepath = request.getHeader("fiware-servicepath");
		String deviceid = request.getParameter("deviceid");
		String dataformatProtocolParam = request.getParameter("dataformat");
		
		LOGGER.log(Level.INFO,"deviceid " + deviceid);
		FEDevice fedevice = new FEDevice(deviceid);
		
		// Get user info 
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		String connectedUserId = userInfo.getId();
		
		// Verify userIsSeller 
		boolean userIsSeller= false;
		if(session.getAttribute("userIsSeller") != null){
		    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
		}
		LOGGER.log(Level.INFO,"Connected user" + connectedUserId  + " and is seller " + userIsSeller);
		Set<String> organizationSet = userInfo.getGroups().get();
//	     for (Organization org : connectedUserOrgs) {
//	    	 String orgId = org.getId();
//			 organizationSet.add(orgId);
//	         LOGGER.log(Level.INFO,orgId);
//	      }
		LOGGER.log(Level.INFO,"Connected user organizationSet " + organizationSet);

		for (DataformatProtocol dataformat: DataformatProtocol.values()) {
			
			String dataformatProtocol = dataformat.toString();
			
			//the loop is necessary in order to cover the case of the Camera and the details starting from the map, when dataformatProtocolParam is empty/null
			if (null != dataformatProtocolParam  && !dataformatProtocolParam.isEmpty())
				dataformatProtocol = dataformatProtocolParam;
			
			
			if (!CommonUtils.isEnabledAgent(dataformatProtocol))
				continue;
			
			
			// SWITCH AGENT // 
			Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
			
			try{
				IdasDevice idasDevice = idas.getDevice(fiwareservice, fiwareservicepath, deviceid);
				
				/*
				FECategory fecategory = new FECategory(fiwareservicepath, fiwareservice);
				fedevice.setCategory(fecategory);
				*/
				String serviceId = fiwareservice;
				
				
//				String serviceName = ServiceConfigManager.getServiceName(serviceId);
//			//	String servicePathId = fiwareservicepath;
//				String servicePathName = ServiceConfigManager.getServicePathName(servicePathId, serviceId);
				
				String serviceName = ServiceConfigManager.getServiceNameByServiceTM(fiwareservice); // from Tenant Manager
				String servicePathName = ServiceConfigManager.getServicePathNameTM(fiwareservicepath, fiwareservice);
				
				
				
				LOGGER.log(Level.INFO,"Category " + fiwareservicepath + " " + servicePathName + " " + serviceName);
				FECategory fecategory = new FECategory(fiwareservicepath, servicePathName, serviceName, serviceId);//serviceName
				fedevice.setCategory(fecategory);
				
				Set<Permission> userPerms = (Set<Permission>) session.getAttribute("userPerms");
				
				fedevice = DeviceConfigManager.composeFEDevice(idasDevice, fedevice, connectedUserId, organizationSet, userIsSeller);
				
				// Check permission Edit Delete
				boolean permitEdit = false;
				boolean permitDelete = false;
				if (userIsSeller){
					permitEdit = true ;
					permitDelete = true;
				} else {
					permitEdit = CommonUtils.isPermittedAction(editActionOnDevice, fedevice, userInfo, userPerms);
					permitDelete = CommonUtils.isPermittedAction(deleteActionOnDevice, fedevice, userInfo, userPerms);
				}
				
				
				boolean isLora = DeviceConfigManager.isLora(fedevice);
				
				if (isLora) {
					
					String devEui = LoraUtils.getDevEui(idasDevice);
					fedevice.setDeveui(devEui);
					String loraDeviceProfileId = LoraUtils.getLoraDeviceProfileId(idasDevice.getStatic_attributes());
					fedevice.setLoraDeviceProfileId(loraDeviceProfileId);
					
					IdasDeviceInternalAttributeLoRa intAttrs = LoraUtils.getLoraInternalAttribute(idasDevice.getInternal_attributes());
					fedevice.setInternal_attributes(intAttrs);
				}
				
				fedevice.setPermitEdit(permitEdit);
				fedevice.setPermitDuplicate(true);
				fedevice.setPermitDelete(permitDelete);

				break;
			}
			catch(Exception ex){
				LOGGER.log(Level.ERROR,"Device not present in case of dataformat protocol " + dataformatProtocol);
			}
		
		}
		LOGGER.log(Level.INFO,gson.toJson(fedevice));
		response.getWriter().write(gson.toJson(fedevice));
		

	}
	
}
