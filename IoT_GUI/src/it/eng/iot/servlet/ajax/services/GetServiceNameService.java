package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.ServiceConfigManager;

public class GetServiceNameService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetServiceNameService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			String fiwareservice = request.getParameter("id");
			//String serviceName = ServiceConfigManager.getServiceName(fiwareservice); // from Orion
			String serviceName = ServiceConfigManager.getServiceNameByServiceTM(fiwareservice); // from Tenant Manager
			
			response.getWriter().write(gson.toJson(serviceName));
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}

}
