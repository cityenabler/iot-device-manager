package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import it.eng.iot.utils.LoraUtils;

//Get Mashup list
public class GetLoraDeviceProfilesService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetLoraDeviceProfilesService.class);

	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			
			//String serviceId = request.getParameter("service"); 
			//String servicePathId = request.getParameter("servicepath"); 
				
			String jwt = LoraUtils.getJWTSingleton();
			//String categoryApiKey = ServiceConfigManager.getApikey(serviceId, servicePathId);
			//String applicationID = LoraUtils.getApplicationId(categoryApiKey, jwt);
			JSONArray deviceProfiles = LoraUtils.getDeviceProfiles( jwt);
				
			response.getWriter().write(deviceProfiles.toString());
			LOGGER.log(Level.INFO,"getLoraDeviceProfiles found:" + deviceProfiles.length() + " - " + deviceProfiles.toString());
				
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}
}
