package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.servlet.model.MapCenter;
import it.eng.iot.utils.ServiceConfigManager;

public class ModifyMapCenterService implements AjaxHandlerService {
	
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
		jResp.put("error", false);
		String payload = request.getParameter("payload");
		String headerService = request.getHeader("fiware-service");
		
		MapCenter mapcenter = gson.fromJson(payload, MapCenter.class);
		
		Double lat = mapcenter.getLat();
		if (lat<-90 || lat>90) { 
			throw new Exception("invalidLat");
		}
		
		Double lng = mapcenter.getLng();
		if (lng<-180 || lng>180) { 
			throw new Exception("invalidLng");
		}
		
		Integer zoom = mapcenter.getZoom();
		if (zoom<0 || zoom > 21) { 
			throw new Exception("invalidZoom");
		} 
		
		else {
			//boolean completed = ServiceConfigManager.modifyMapCenter(headerService, mapcenter); // Orion
			boolean completed = ServiceConfigManager.modifyMapCenterTM(headerService, mapcenter); // Tenant Manager
			if(!completed)
				throw new Exception("somethingWrong");
		}
		
		response.getWriter().write(jResp.toString());
	}
}
