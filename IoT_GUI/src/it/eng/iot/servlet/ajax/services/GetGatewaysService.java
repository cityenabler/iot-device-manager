package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import it.eng.iot.hub.controller.KapuaController;
import it.eng.iot.hub.model.kapua.KuraGatewayMap;

public class GetGatewaysService implements AjaxHandlerService {
	
	private static final String deviceStatus = "connected";
	
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		try {
			String hubName = request.getParameter("hubName");
			List<JsonObject> gatewayDeviceList = KapuaController.getGatewaysProcedure(hubName, deviceStatus);
			
			//Build instance map so it'll be possible to get the specific Kura id by key = clientId
			KuraGatewayMap.getInstance().populate(gatewayDeviceList);
			
			response.getWriter().write(gson.toJson(gatewayDeviceList));
			}
			catch (org.json.JSONException jsone) {
				jsone.printStackTrace();
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
