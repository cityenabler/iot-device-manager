package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.FECategory;
import it.eng.iot.servlet.model.FEDevice;
import it.eng.iot.servlet.model.Permission;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IotsimUtils;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.keycloak.User;
import it.eng.tools.model.DeviceEntityBean;
import it.eng.tools.model.ServiceEntityBean;

//Gets list of all devices from all the agents
public class GetDeviceListMulticontextService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceListMulticontextService.class);
	private Gson gson = new Gson();
	
	String editActionOnDevice = "U";
	String deleteActionOnDevice = "D";
	
	@Override
	@SuppressWarnings("unchecked")
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
				
		// forcingUpdate if filters change
		Optional<String> optReloadDevice = Optional.ofNullable(request.getParameter("reloadDevices"));
		
		Boolean forceReload = false;
		try{ forceReload = optReloadDevice.isPresent() && Boolean.parseBoolean(optReloadDevice.get()); }
		catch(Exception e){ /* DO Nothing */ }
		
		List<FEDevice> sortedDeviceList = (List<FEDevice>) Optional.ofNullable(request.getSession(true).getAttribute("lastSearchDeviceList")).orElse(new LinkedList<FEDevice>());
		
		if(forceReload || sortedDeviceList.isEmpty()) {//if it is already on session
			
			sortedDeviceList = new LinkedList<FEDevice>(); //if forceReload, then clean the list
			
			Set<FEDevice> out = new HashSet<FEDevice>();
			List<FEDevice> simDeviceList =  new LinkedList<FEDevice>();
			
			String selContexts = request.getParameter("selContexts");//$NON-NLS-1$ 
			String selCategories = request.getParameter("selCategories");//$NON-NLS-1$ 
			String selHubs = request.getParameter("selHubs");//$NON-NLS-1$
			
			
			String[] contexts = selContexts.split(",");
			final List<String> contextsL = new ArrayList<String>(Arrays.asList(contexts));
			contextsL.removeAll(Arrays.asList("", null));//to remove empty string
			
			String[] categories = selCategories.split(",");
			final List<String> categoriesL = new ArrayList<String>(Arrays.asList(categories));
			categoriesL.removeAll(Arrays.asList("", null));//to remove empty string
			
			String[] hubs = selHubs.split(",");
			final List<String> hubsL = new ArrayList<String>(Arrays.asList(hubs));
			hubsL.removeAll(Arrays.asList("", null));//to remove empty string
			
			HttpSession session = request.getSession();
			User userInfo = (User) session.getAttribute("userInfo");
			String connectedUserId = userInfo.getId();
			
			Set<Permission> userPerms = (Set<Permission>) session.getAttribute("userPerms");
			
			
			boolean userIsSeller= false;
			if(session.getAttribute("userIsSeller") != null){
			    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
			}
			
			final boolean userIsSellerF = userIsSeller;
			
			LOGGER.log(Level.DEBUG,"Connected user" + connectedUserId + " and is seller " + userIsSeller);
			
			//CompletableFuture<Set<ServiceEntityBean>> futurescopes = CompletableFuture.supplyAsync(() -> { return ServiceConfigManager.getServices(); });
			
			//Map<Organization, Map<String, List<Role>>> orgs = userInfo.getOrganizationsRoles().get();
			Set<String> organizationSet = userInfo.getGroups().get();
			
//			Set<Organization> connectedUserOrgs = orgs.keySet();
//			
//			 for (Organization org : connectedUserOrgs) {
//		    	 String orgId = org.getId();
//				 organizationSet.add(orgId);
//		         LOGGER.log(Level.INFO,orgId);
//		      }
		    LOGGER.log(Level.INFO,"Connected User organizationSet " + organizationSet);

		    JSONArray checkSimDevs = new JSONArray();
		    List<CompletableFuture<Void>> generalCFs = new ArrayList<CompletableFuture<Void>>();
		    
		    //Set<ServiceEntityBean> scopes = ServiceConfigManager.getServices();
		    Set<ServiceEntityBean> scopes = ServiceConfigManager.getServicesTM(); // all scopes from TenantManager
		    for(String context:contextsL) {
		    	
		    	String  fiwareservice = context.toLowerCase();
		    	
//		    	System.out.println("fiwareservice "+fiwareservice);
		    	
				//String enabler = ServiceConfigManager.getEnablerByService ( scopes,  fiwareservice);
				String enabler= ServiceConfigManager.getEnablerTM(fiwareservice); //enabler from TenantManager by Service
				
				//Set<String> allServicePath= ServiceConfigManager.getAllServicePathIdByService(fiwareservice);
				Set<String> allServicePath = ServiceConfigManager.getAllServicePathIdByServiceTM(fiwareservice); // From Tenant Manager
				
				
				for(String category:categoriesL) {
//				categoriesL.parallelStream().forEach(category -> {
					
						String fiwareservicepath= fiwareservice+"_"+category.toLowerCase();
						if (allServicePath.contains(fiwareservicepath)) {
							
//							String serviceName = ServiceConfigManager.getServiceName(fiwareservice);
//							String servicePathName = ServiceConfigManager.getServicePathName(fiwareservicepath, fiwareservice);
							
							String serviceName = ServiceConfigManager.getServiceNameByServiceTM(fiwareservice); // from Tenant Manager
							String servicePathName = ServiceConfigManager.getServicePathNameTM(fiwareservicepath, fiwareservice); // from Tenant Manager
							
							
							LOGGER.log(Level.INFO,"Category " + fiwareservicepath + " " + servicePathName + " " + fiwareservice);
							FECategory fecategory = new FECategory(fiwareservicepath, servicePathName, serviceName, fiwareservice);
							
						
							final String fiwareservicepathF = fiwareservicepath;
							CompletableFuture<Void> cf = DeviceConfigManager.asyncGetOCBDevices(fiwareservice, fiwareservicepathF).thenAcceptAsync(ocbDevices -> {
				                try {
				                	for (DeviceEntityBean ocbDevice:ocbDevices) {
				                	
				                	//ocbDevices.stream().forEach(ocbDevice->{
				                		
											FEDevice fedevice = new FEDevice(ocbDevice.getId()); 
											boolean isCamera =false;
											
											if (null != ocbDevice.getStreaming_url() && (!("").equals(ocbDevice.getStreaming_url().getValue().trim())))
												isCamera=true;
											
											fedevice.setCategory(fecategory);
											
											try{
												fedevice = DeviceConfigManager.composeFEDevice(ocbDevice, fedevice, connectedUserId, organizationSet, userIsSellerF);
												
												// Check permission Edit Delete
												boolean permitEdit = false;
												boolean permitDelete = false;
												if (userIsSellerF){
													permitEdit = true ;
													permitDelete = true;
												} else {
													permitEdit = CommonUtils.isPermittedAction(editActionOnDevice, fedevice, userInfo, userPerms);
													permitDelete = CommonUtils.isPermittedAction(deleteActionOnDevice, fedevice, userInfo, userPerms);
												}
												
												
												//boolean isOpcua = DeviceConfigManager.isOpcua(fedevice);
												//boolean isLora = DeviceConfigManager.isLora(fedevice);
												
												if (!isCamera) {
												
													JSONObject checkSimDev = new JSONObject();	
													checkSimDev.put("fiwareservice", fiwareservice);
													checkSimDev.put("fiwareservicepath", fiwareservicepathF);
													checkSimDev.put("deviceId", ocbDevice.getId());
													checkSimDev.put("dataformatProtocol", ocbDevice.getDataformatProtocol().getValue());
													
													checkSimDevs.put(checkSimDev);
												}
												
												
												fedevice.setEnabler(enabler);
												fedevice.setPermitEdit(permitEdit);
												fedevice.setPermitDuplicate(true);
												fedevice.setPermitDelete(permitDelete);
												fedevice.setRunningSimulation(false);
				
												LOGGER.log(Level.INFO,"Device Hub name: \"" + fedevice.getHubName());
												//Check if the current device has to be added to the response
												//If dropdown selector on GUI is populated with hubs list, filter devices to show by hubName
												if ((hubsL != null && !hubsL.isEmpty()) && (hubsL.contains(fedevice.getHubName()) || (hubsL.contains("Idas") && fedevice.getHubName().isEmpty()))) {	//For legacy devices, hubName is not specified
													out.add(fedevice);
												}
											} catch (Exception ex){
												LOGGER.log(Level.ERROR,"Error in ComposeFEDevice: " + ex.getMessage());
											}
				                	};
							} catch (Exception ex){
								LOGGER.log(Level.INFO,"=================================================");
								LOGGER.log(Level.ERROR,"Error on getDeviceListMulticontext: Orion CB  NOT available");
								LOGGER.log(Level.INFO,"=================================================");
							//	continue;
							}
			               
			                LOGGER.log(Level.INFO,"CompletableFuture returned devices  " + ocbDevices.size());
			            });
						generalCFs.add(cf);
					  	}//if fiwareservicepath contains
					 
				};
		    };
		    
		 CompletableFuture<Void> combinedFuture =CompletableFuture.allOf(generalCFs.toArray(new CompletableFuture[generalCFs.size()]));					 
		 combinedFuture.get();// at the end of CompletableFuture.allOf above 					 					 
		 LOGGER.log(Level.INFO,"ocbDevice All  CompletableFuture finished." );
		 
		 if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
			 try{
				//check if simulation is active
				JSONArray devSim = IotsimUtils.getSimStatusMultidevices(checkSimDevs);
				
				for (FEDevice dev: out) {
					try{
						String fiwareServiceD = DeviceConfigManager.getFiwareServiceByFEDevice(dev);
						String fiwareServicepathD = DeviceConfigManager.getFiwareServicepathByFEDevice(dev);
						
						boolean simFound=false;
						for (int z = 0; z < devSim.length(); z++) {
							
							JSONObject device = devSim.getJSONObject(z);
							
							if (!device.getString("deviceId").equalsIgnoreCase(dev.getId()))
								continue;
							
							if (!device.getString("fiwareservice").equalsIgnoreCase(fiwareServiceD))
								continue;
							
							if (!device.getString("fiwareservicepath").equalsIgnoreCase(fiwareServicepathD))
								continue;
							
							if (device.has("isRunningSimulation")) {
								dev.setRunningSimulation(device.getBoolean("isRunningSimulation"));
								simFound=true;
							}
							break;
						}
						
						if (!simFound)
							dev.setRunningSimulation(false);
						
						
						simDeviceList.add(dev);
						
					}
					catch(Exception e){
						LOGGER.log(Level.ERROR, e.getMessage());
						
					}
				}//for (FEDevice
			 }
			 catch(Exception e){
				 //DO Nothing
			 }
		 
			sortedDeviceList.addAll(simDeviceList);
		}
		else{
			 sortedDeviceList.addAll(out);
		}
		Collections.<FEDevice>sort(sortedDeviceList);

		request.getSession(true).setAttribute("lastSearchDeviceList", sortedDeviceList);
		
		LOGGER.info("Total number of devices " + sortedDeviceList.size());
		}
		
		int start = Math.max(0, Integer.parseInt(Optional.ofNullable(request.getParameter("start")).orElse("0")));//$NON-NLS-1$ 
		int size = Integer.parseInt(Optional.ofNullable(request.getParameter("size")).orElse("6"));//$NON-NLS-1$ 
		
		//Get the interesting page
		response.getWriter().write(gson.toJson(sortedDeviceList.subList(start, Math.min(sortedDeviceList.size(), start+size))));
		response.setHeader("totalCount", String.valueOf(sortedDeviceList.size()));
	}

}
