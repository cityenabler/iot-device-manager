package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import it.eng.iot.servlet.model.FEDevicePublishedConfirmBean;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.DeviceUtils;
import it.eng.iot.utils.listener.BigdataNotifier;
import it.eng.iot.utils.listener.LoggerNotifier;
import it.eng.iot.utils.listener.iDeviceListener;
import it.eng.tools.Orion;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceAttribute;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceStaticAttribute;

public class ModifyDeviceService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(ModifyDeviceService.class);
	private Gson gson = new Gson();
	
	public static Set<iDeviceListener> deviceListeners = new HashSet<iDeviceListener>();
	
	static{
		deviceListeners.add(new BigdataNotifier());
		deviceListeners.add(new LoggerNotifier());
	}
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		String payload = request.getParameter("payload");
		String fiwareservice = request.getHeader("fiware-service");
		String fiwareservicepath = request.getHeader("fiware-servicepath");
		
		IdasDeviceList devlist = gson.fromJson(payload, IdasDeviceList.class);
		FEDevicePublishedConfirmBean devicePublishedConfirmData = null;
		
		String deviceId = "";
		Set<IdasDevice> devices = devlist.getDevices();
		
		for (IdasDevice device : devices) {
			String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
			IdasDeviceStaticAttribute dateModifiedAttr = new IdasDeviceStaticAttribute("dateModified", "DateTime", dateModified);
			device.getStatic_attributes().add(dateModifiedAttr);
			
			deviceId = device.getDevice_id();
			LOGGER.log(Level.INFO,"deviceId: "+ deviceId);
			
			String transportProtocol = device.getTransport(); // HTTP/MQTT
			device.setTransport(transportProtocol.toUpperCase()); // IT MUST BE UPPERCASE HTTP for IDAS
			LOGGER.log(Level.INFO,"transportProtocol: "+ transportProtocol);
			
			String dataformatProtocol = device.getProtocol(); // UL2_0/JSON
			LOGGER.log(Level.INFO,"dataformatProtocol: "+ dataformatProtocol);
			
			Set<IdasDeviceStaticAttribute> staticAttrs = device.getStatic_attributes();
			boolean isMobile =DeviceUtils.isMobileDevice(staticAttrs);
			
			Set<IdasDeviceAttribute> attributes = device.getAttributes();
			
			//if it isn't mobile, 
			if (!isMobile) {
				//if I switched from mobile to fixed, I have to delete the attribute location
				IdasDeviceAttribute toRemove = null ;
				for(IdasDeviceAttribute attr : attributes){
					if ("location".equalsIgnoreCase(attr.getName()) ){
						toRemove = attr;
						attributes.remove(toRemove);
						device.setAttributes(attributes);
						break;
					}
				}
				
			}else {
				
				//if you are editing a mobile device, a second location is attached as attributed. Delete the first one
				int locFound=0;
				for(IdasDeviceAttribute attr : attributes){
					if ("location".equalsIgnoreCase(attr.getName()) ){
						locFound += 1;
					}
				}
				IdasDeviceAttribute toAttrsRemove = null ;
				if (locFound ==2) {
					for(IdasDeviceAttribute attr : attributes){
						if ("location".equalsIgnoreCase(attr.getName()) ){
							toAttrsRemove = attr;
							attributes.remove(toAttrsRemove);
							device.setAttributes(attributes);
							break;
						}
					}
				}
				//if I switched from fixed to mobilr, then remove the static attribute location
				IdasDeviceStaticAttribute toRemove = null ;
				for(IdasDeviceStaticAttribute attr : staticAttrs){
					if ("location".equalsIgnoreCase(attr.getName()) ){
						toRemove = attr;
						staticAttrs.remove(toRemove);
						device.setStatic_attributes(staticAttrs);
						break;
					}
				}
				
			}
			
			devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
			
			for(iDeviceListener listener : deviceListeners){
				listener.onDeviceEdit(device, fiwareservice, fiwareservicepath);
			}
		}
		
		DeviceConfigManager.modifyDeviceProcess(fiwareservice, fiwareservicepath, devlist);
		
		

		response.getWriter().write(gson.toJson(devicePublishedConfirmData));}
}
