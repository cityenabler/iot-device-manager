package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.DeviceConfigManager;
import it.eng.tools.keycloak.User;

public class GetDeviceInfopointService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceInfopointService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
				
		try {
			String serviceId = request.getHeader("fiware-service"); 
			String servicePathId = request.getHeader("fiware-servicepath"); 
			String deviceId = request.getHeader("device_id"); 
			LOGGER.log(Level.INFO,"Received params: " + serviceId + servicePathId + deviceId);
			
			
			LOGGER.log(Level.INFO,"deviceid " + deviceId);
			//FEDevice fedevice = new FEDevice(deviceId);
			
			// Get user info 
			HttpSession session = request.getSession();
			User userInfo = (User) session.getAttribute("userInfo");
			String connectedUserId = userInfo.getId();
			
			// Verify userIsSeller 
			boolean userIsSeller= false;
			if(session.getAttribute("userIsSeller") != null){
			    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
			}
			LOGGER.log(Level.INFO,"Connected user" + connectedUserId + " and is seller " + userIsSeller);
			Set<String> organizationSet = userInfo.getGroups().get();
			LOGGER.log(Level.INFO,"Connected user organizationSet " + organizationSet);
			
			// Get device Infopoint
			Set<BasicNameValuePair> deviceInfopoint = DeviceConfigManager.getDeviceInfopoint(serviceId, servicePathId, deviceId);				
			
			// send response
			response.getWriter().write(gson.toJson(deviceInfopoint));
			LOGGER.log(Level.INFO,"deviceInfopoint found:" + deviceInfopoint.size() + " - " + response.toString());
			
			}
			catch (org.json.JSONException jsone) {
				LOGGER.log(Level.ERROR, jsone.getMessage());
				JSONArray emptyarr = new JSONArray();
				response.getWriter().write(emptyarr.toString());
				response.setStatus(500);
			}
	}
}
