package it.eng.iot.servlet.ajax.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfDme;
import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.SubscriptionManager;
import it.eng.iot.utils.UrlshortenerUtils;
import it.eng.tools.model.DeviceEndpointSubscriptionEntity;
import it.eng.tools.model.EntityAttribute;

public class GetDeviceListenerEndpointsService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetDeviceListenerEndpointsService.class);
	static String notificationOrionDmeOnDeviceUpdate = 	ConfDme.getString("dme.address") + ConfDme.getString("orion.dme.notification.update");
	static String decodePattern = ConfTools.getString("urlshortener.url.pattern");
	static String enabledUrlshortener = ConfTools.getString("urlshortener.active");
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, Exception {
		
		JSONObject jResp = new JSONObject();
		 final boolean withoutInternal = true;
		
		try {
			

			String serviceId = request.getHeader("fiware-service"); 
			String servicePathId = request.getHeader("fiware-servicepath"); 
			servicePathId=CommonUtils.normalizeServicePath(servicePathId);
			String deviceId = request.getParameter("deviceid"); 
			LOGGER.log(Level.INFO,"Received params: " + serviceId + serviceId + deviceId);
			
			String decodedEndpointUrl = "";
			Set<DeviceEndpointSubscriptionEntity> deviceSubscriptionsWithCheckedEndpoints = new HashSet<DeviceEndpointSubscriptionEntity>();
			DeviceEndpointSubscriptionEntity deviceSubscriptionDecodedEndpointValue = new DeviceEndpointSubscriptionEntity();
			// Get endpointUrl for the deviceId
			Set<DeviceEndpointSubscriptionEntity> deviceSubscriptions = SubscriptionManager.getDeviceEndpointSubscriptions(serviceId, servicePathId, deviceId, withoutInternal, "");
			
			for(DeviceEndpointSubscriptionEntity deviceSubscription:deviceSubscriptions) {
			
				String url = deviceSubscription.getEndpointURL().getValue();
				
				if( enabledUrlshortener.equals("true") &&  url.contains(decodePattern)) {
					
					decodedEndpointUrl = UrlshortenerUtils.decodeUrl(url);
						
					if(decodedEndpointUrl!=null && !decodedEndpointUrl.trim().equalsIgnoreCase("undefined") && !decodedEndpointUrl.trim().equalsIgnoreCase("")) {
						
						EntityAttribute<String> newAttributeValue = new EntityAttribute<String>("");
						
						newAttributeValue.setType(deviceSubscription.getEndpointURL().getType());
						newAttributeValue.setValue(decodedEndpointUrl);
						
						deviceSubscriptionDecodedEndpointValue = deviceSubscription;
						deviceSubscriptionDecodedEndpointValue.setEndpointURL(newAttributeValue);
						
						deviceSubscriptionsWithCheckedEndpoints.add(deviceSubscriptionDecodedEndpointValue);

					
					}else {
						LOGGER.log(Level.ERROR, "Url could not be added!");
						jResp = new JSONObject();
						jResp.put("error", true);
						jResp.put("message", "Url could not be added!");
						response.setStatus(400);
					}
						
				}else {
						deviceSubscriptionsWithCheckedEndpoints.add(deviceSubscription);
				}
				
		}
			
		LOGGER.log(Level.INFO,"Number of retrieved endpoint Url " + deviceSubscriptions.size());
		LOGGER.log(Level.INFO,"Number of retrieved endpoint Url checked " + deviceSubscriptionsWithCheckedEndpoints.size());
		response.getWriter().write(gson.toJson(deviceSubscriptionsWithCheckedEndpoints));
			
		}
		catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.ERROR,e.getMessage());
	
			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString());
			response.setStatus(500);
			response.getWriter().write(jResp.toString());
		}
	}
}
