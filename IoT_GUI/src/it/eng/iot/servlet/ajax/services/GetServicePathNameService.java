package it.eng.iot.servlet.ajax.services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import com.google.gson.Gson;

import it.eng.iot.utils.ServiceConfigManager;

public class GetServicePathNameService implements AjaxHandlerService {
	
	private static final Logger LOGGER = LogManager.getLogger(GetServicePathNameService.class);
	private Gson gson = new Gson();
	
	@Override
	public void handleAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			String refScope = request.getParameter("service");
			String urbanserviceId = request.getParameter("servicepath");
			//String servicePathName = ServiceConfigManager.getServicePathName(urbanserviceId, refScope); // from Orion
			String servicePathName = ServiceConfigManager.getServicePathNameTM(urbanserviceId, refScope); // from Tenant Manager
			
			response.getWriter().write(gson.toJson(servicePathName));
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
	}
}
