package it.eng.iot.servlet.ajax.service.discovery;


import java.util.HashMap;
import java.util.Map;

import it.eng.iot.servlet.ajax.services.AddListenerEndpointService;
import it.eng.iot.servlet.ajax.services.AddMashupService;
import it.eng.iot.servlet.ajax.services.AjaxHandlerService;
import it.eng.iot.servlet.ajax.services.AjaxHandlerServiceType;
import it.eng.iot.servlet.ajax.services.ChangeSubscriptionStatusService;
import it.eng.iot.servlet.ajax.services.CheckDeviceIdService;
import it.eng.iot.servlet.ajax.services.CheckNameService;
import it.eng.iot.servlet.ajax.services.CreateDeviceService;
import it.eng.iot.servlet.ajax.services.DeleteDevicesService;
import it.eng.iot.servlet.ajax.services.GetAssetsService;
import it.eng.iot.servlet.ajax.services.GetCategoriesService;
import it.eng.iot.servlet.ajax.services.GetContextsListService;
import it.eng.iot.servlet.ajax.services.GetDeviceInfopointService;
import it.eng.iot.servlet.ajax.services.GetDeviceListMulticontextService;
import it.eng.iot.servlet.ajax.services.GetDeviceListenerEndpointsService;
import it.eng.iot.servlet.ajax.services.GetDeviceMashupService;
import it.eng.iot.servlet.ajax.services.GetDeviceService;
import it.eng.iot.servlet.ajax.services.GetDeviceSubscriptionsService;
import it.eng.iot.servlet.ajax.services.GetGatewaysService;
import it.eng.iot.servlet.ajax.services.GetHubsService;
import it.eng.iot.servlet.ajax.services.GetLoraDeviceProfilesService;
import it.eng.iot.servlet.ajax.services.GetMapCenterService;
import it.eng.iot.servlet.ajax.services.GetMashupListService;
import it.eng.iot.servlet.ajax.services.GetServiceNameService;
import it.eng.iot.servlet.ajax.services.GetServicePathNameService;
import it.eng.iot.servlet.ajax.services.GetServicePathsService;
import it.eng.iot.servlet.ajax.services.GetServicesService;
import it.eng.iot.servlet.ajax.services.ModifyDeviceService;
import it.eng.iot.servlet.ajax.services.ModifyMapCenterService;
import it.eng.iot.servlet.ajax.services.SubscribeService;
import it.eng.iot.servlet.ajax.services.removeSubscriptionService;

public class AjaxHandlerLookup {
	
	private Map<AjaxHandlerServiceType, Class<? extends AjaxHandlerService>> setAjaxHandlerMap() {
		 
		 Map<AjaxHandlerServiceType, Class<? extends AjaxHandlerService>> ajaxHandlerMap = new HashMap<AjaxHandlerServiceType, Class<? extends AjaxHandlerService>>();
		 
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getContextsList ,  GetContextsListService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.create_device ,  CreateDeviceService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.subscribe ,  SubscribeService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getServices ,  GetServicesService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getServicePaths ,  GetServicePathsService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getServiceName ,  GetServiceNameService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getServicePathName ,  GetServicePathNameService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDevice ,  GetDeviceService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.deleteDevices ,  DeleteDevicesService.class);
//		 ajaxHandlerMap.put( AjaxHandlerServiceType.fireSimulation ,  FireSimulationService.class);
//		 ajaxHandlerMap.put( AjaxHandlerServiceType.stopSimulation ,  StopSimulationService.class);
//		 ajaxHandlerMap.put( AjaxHandlerServiceType.checkSimulationStatus ,  CheckSimulationStatusService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.modifyDevice ,  ModifyDeviceService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getMapCenter ,  GetMapCenterService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.modifyMapCenter ,  ModifyMapCenterService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.changeSubscriptionStatus ,  ChangeSubscriptionStatusService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDeviceSubscriptions ,  GetDeviceSubscriptionsService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getMashupList ,  GetMashupListService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDeviceMashup ,  GetDeviceMashupService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDeviceInfopoint ,  GetDeviceInfopointService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getCategories ,  GetCategoriesService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getGatewaysKapua , GetGatewaysService.class);	//MB: add hubs
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getHubs , GetHubsService.class);	//MB: add hubs
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDeviceListMulticontext ,  GetDeviceListMulticontextService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getLoraDeviceProfiles ,  GetLoraDeviceProfilesService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.check_name,  CheckNameService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.addMashup,  AddMashupService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.addListenerEndpoint,  AddListenerEndpointService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.removeSubscription,  removeSubscriptionService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getDeviceListenerEndpoints,  GetDeviceListenerEndpointsService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.checkDeviceId,  CheckDeviceIdService.class);
		 ajaxHandlerMap.put( AjaxHandlerServiceType.getAssets, GetAssetsService.class);	//MB: add hubs
	 		
	 	return ajaxHandlerMap;
	 }
	 	
	public AjaxHandlerService getAjaxHandlerService(AjaxHandlerServiceType action) throws Exception {
		 		
		Map<AjaxHandlerServiceType,Class<? extends AjaxHandlerService>> ajaxMap = setAjaxHandlerMap();
		AjaxHandlerService ajaxHandlerService;
		 		
		 if(ajaxMap.containsKey(action)) {
		 			
		 	ajaxHandlerService = ajaxMap.get(action).newInstance();
		 			
		 }else {
		 	throw new Exception("Unsupported action " + action); //$NON-NLS-1$
		 }
		 		
		 return ajaxHandlerService;
	}
}
