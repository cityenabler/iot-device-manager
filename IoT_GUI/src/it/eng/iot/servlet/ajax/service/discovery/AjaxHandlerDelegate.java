package it.eng.iot.servlet.ajax.service.discovery;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.eng.iot.servlet.ajax.services.AjaxHandlerService;
import it.eng.iot.servlet.ajax.services.AjaxHandlerServiceType;

public class AjaxHandlerDelegate {
	
	private AjaxHandlerLookup ajaxHandlerLookupService;
	private AjaxHandlerService ajaxHandlerService;
	private AjaxHandlerServiceType ajaxHandlerServiceType;
	
	
	public void setAjaxHandlerLookup(AjaxHandlerLookup ajaxHandlerLookup) {
		this.ajaxHandlerLookupService = ajaxHandlerLookup;
	}
	
	public void setAjaxHandlerServiceType(AjaxHandlerServiceType ajaxHandlerServiceType) {
		this.ajaxHandlerServiceType = ajaxHandlerServiceType;
	}
	
	
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ajaxHandlerService = ajaxHandlerLookupService.getAjaxHandlerService(ajaxHandlerServiceType);
		ajaxHandlerService.handleAction(request,response);
		
	}
	
}
