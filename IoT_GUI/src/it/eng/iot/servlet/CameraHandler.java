package it.eng.iot.servlet;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONObject;
import com.google.gson.Gson;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.FEDevicePublishedConfirmBean;

import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IOTAgentManager;
import it.eng.iot.utils.UrlshortenerUtils;
import it.eng.tools.Idas;
import it.eng.tools.Orion;
import it.eng.tools.model.DeviceEntityBean;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceStaticAttribute;



@WebServlet("/camerahandler")
public class CameraHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629223L;
	
	private static final Logger LOGGER = LogManager.getLogger(CameraHandler.class);
	private static String[] forbiddenEndpointCharacters = new String[]{ "<", ">", "\"", "\'", "=", ";" ,"(", ")"};
	private Gson gson = new Gson();
	
		
	public enum Ajaxaction {
		
		create_camera("create_camera"),
		modifyDevice("modifyDevice"),
		check_name("check_name");

		private final String text;
		
		private Ajaxaction(final String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}
		
	};

	public CameraHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		JSONObject jResp = new JSONObject();
				jResp.put("error", false);
		
		try {
			String action = request.getParameter("action"); //$NON-NLS-1$
			Ajaxaction eAction = Ajaxaction.valueOf(action);
			
			switch (eAction) {
			
				case create_camera: {
					String payload = request.getParameter("payload");
					LOGGER.log(Level.INFO,"Device payload: "+payload);
					String fiwareservice = request.getHeader("fiware-service");
					LOGGER.log(Level.INFO,"fiware-service: "+fiwareservice);
					String fiwareservicepath = request.getHeader("fiware-servicepath");
					LOGGER.log(Level.INFO,"fiware-servicepath: "+fiwareservicepath);
					
					IdasDeviceList cameraslist = gson.fromJson(payload, IdasDeviceList.class);					
					
					
					FEDevicePublishedConfirmBean devicePublishedConfirmData = null;
					
					String transportProtocol = "HTTP";
					String dataformatProtocol ="UL2_0"; // Default
					
					Set<IdasDevice> cameras = cameraslist.getDevices();
					
					boolean enabledUrlshortener = ConfTools.getString("urlshortener.active").equals("true");
					String forbiddenChars = "";
					
					for (IdasDevice device : cameras) {
						String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
						IdasDeviceStaticAttribute dateModifiedAttr = new IdasDeviceStaticAttribute("dateModified", "DateTime", dateModified);
						device.getStatic_attributes().add(dateModifiedAttr);
						device.setProtocol(dataformatProtocol);
						device.setTransport(transportProtocol);
						
						Set<IdasDeviceStaticAttribute> attributes = device.getStatic_attributes();
						for(IdasDeviceStaticAttribute attribute:attributes) {
							
							if(attribute.getName().equals("streaming_url")){
								
								String streamUrl = attribute.getValue();
								forbiddenChars = UrlshortenerUtils.getForbiddenChars(streamUrl, forbiddenEndpointCharacters);
							    
								if(enabledUrlshortener && forbiddenChars!="") {
									//get encoded link value from urlshortener
									String validUrl = UrlshortenerUtils.encodeUrl(streamUrl);
									
									if(validUrl != null && !validUrl.trim().equalsIgnoreCase("undefined") && !validUrl.trim().equalsIgnoreCase("")) {
										//set camera endpoint and attribute stream_url to encoded value
										attribute.setValue(validUrl);
										device.setEndpoint(validUrl);
										
										//publish camera with encoded stream url value
										devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
										
									}
									
								}else if(forbiddenChars==""){
									//stream url is valid, publish it 
									devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
									
								}
								break;
							}
						}
					}
					
					if(!enabledUrlshortener && forbiddenChars!="") {
						//urlshortener is disabled and stream url contains forbidden chars
						jResp = new JSONObject();
						jResp.put("error", true);
						jResp.put("forbidden", true);
						jResp.put("forbiddenChars", forbiddenChars );
						response.setStatus(400);
						
					}else if(null == devicePublishedConfirmData) {
						//camera failed to be published
						jResp = new JSONObject();
						jResp.put("error", true);
						jResp.put("message", "unpublished");
						response.setStatus(400);
						
					}else {
						//camera has been published on Orion
						jResp.put("response", gson.toJson(devicePublishedConfirmData)); 
						
						// STEP 4: Create Device on IDAS
						// SWITCH AGENT
						LOGGER.log(Level.INFO,"dataformatProtocol: "+ dataformatProtocol);
						Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
						
						idas.registerDevice(fiwareservice, fiwareservicepath, cameraslist);
					}
					
					response.getWriter().write(jResp.toString());
					
					break;
					
				}
				
				/* 
				 * ModifyDevice 
				 * STEP1: Remove from IDAS	
				 * STEP2: Create on IDAS
				 * STEP3: Remove the delta attribute on Orion	
				 * STEP4: Set opType=updated on orion
				 */
				case modifyDevice: {
					
						String payload = request.getParameter("payload");
						String fiwareservice = request.getHeader("fiware-service");
						String fiwareservicepath = request.getHeader("fiware-servicepath");
						
						IdasDeviceList devlist = gson.fromJson(payload, IdasDeviceList.class);
						FEDevicePublishedConfirmBean devicePublishedConfirmData = null;
						
						boolean enabledUrlshortener = ConfTools.getString("urlshortener.active").equals("true");
						String forbiddenChars = "";
						
						String deviceId = "";
						Set<IdasDevice> devices = devlist.getDevices();
						
						for (IdasDevice device : devices) {
							String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
							IdasDeviceStaticAttribute dateModifiedAttr = new IdasDeviceStaticAttribute("dateModified", "DateTime", dateModified);
							device.getStatic_attributes().add(dateModifiedAttr);
							
							deviceId = device.getDevice_id();
							LOGGER.log(Level.INFO,"deviceId: "+ deviceId);
							
							String transportProtocol = "HTTP";
							
							device.setTransport(transportProtocol); // IT MUST BE UPPERCASE HTTP for IDAS
							LOGGER.log(Level.INFO,"transportProtocol: "+ transportProtocol);
							
							String dataformatProtocol ="UL2_0"; // Default
							LOGGER.log(Level.INFO,"dataformatProtocol: "+ dataformatProtocol);
							device.setProtocol(dataformatProtocol);
							
							Set<IdasDeviceStaticAttribute> attributes = device.getStatic_attributes();
							for(IdasDeviceStaticAttribute attribute:attributes) {
								
								if(attribute.getName().equals("streaming_url")){
									
									String streamUrl = attribute.getValue();
									forbiddenChars = UrlshortenerUtils.getForbiddenChars(streamUrl, forbiddenEndpointCharacters);
									
									if(enabledUrlshortener && forbiddenChars!="") {
										//get encoded link value from urlshortener
										String validUrl = UrlshortenerUtils.encodeUrl(streamUrl);
										
										if(validUrl != null && !validUrl.trim().equalsIgnoreCase("undefined") && !validUrl.trim().equalsIgnoreCase("")) {
											//set camera endpoint and attribute stream_url to encoded value
											attribute.setValue(validUrl);
											device.setEndpoint(validUrl);
											
											//publish camera with encoded stream url value
											devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
											
										}
										
									}else if(forbiddenChars==""){
										//stream url is valid, publish it 
										devicePublishedConfirmData = DeviceConfigManager.getDeviceConfirmantionData(transportProtocol, dataformatProtocol, device, fiwareservice, fiwareservicepath);
										
									}
									break;
								}
							}
						}
						
						if(!enabledUrlshortener && forbiddenChars!="") {
							//urlshortener is disabled and stream url contains forbidden chars
							jResp = new JSONObject();
							jResp.put("error", true);
							jResp.put("forbidden", true);
							jResp.put("forbiddenChars", forbiddenChars );
							response.setStatus(400);
							
						}else if(null == devicePublishedConfirmData) {
							//camera failed to be published
							jResp = new JSONObject();
							jResp.put("error", true);
							jResp.put("message", "unpublished");
							response.setStatus(400);
							
						}else {
							//camera has been published on Orion
							jResp.put("response", gson.toJson(devicePublishedConfirmData)); 
							boolean completed = DeviceConfigManager.modifyDeviceProcess(fiwareservice, fiwareservicepath, devlist);
							
						}
						
						response.getWriter().write(jResp.toString());
						
					break;
				}
				
				
			
			case check_name: {
				LOGGER.log(Level.INFO,"called check_name " );
					
					boolean nameAvailability = true;
					
					String fiwareservice = request.getHeader("fiware-service");
					String fiwareservicepath = request.getHeader("fiware-servicepath").replace("/", "");
					String devicename = request.getParameter("devicename");
					
					Set<DeviceEntityBean> devicesResp = DeviceConfigManager.getOCBDevices(fiwareservice,fiwareservicepath);
					
					for(DeviceEntityBean device : devicesResp) {
						if(device.getScreenId().getValue().equals(devicename) == true) {
							nameAvailability = false;
							break;
						}
					}
					    
					response.getWriter().write(gson.toJson(nameAvailability));
					break;
			}
				
				
			default:
					throw new Exception("Unsupported action " + action); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); //$NON-NLS-1$
			jResp.put("message", e.toString()); //$NON-NLS-1$
			
			response.getWriter().write(jResp.toString());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			doGet(request, response);
		} catch (ServletException | IOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}
	
	

	
	
}
