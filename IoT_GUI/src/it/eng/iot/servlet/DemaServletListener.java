package it.eng.iot.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import it.eng.digitalenabler.fiware.keyrock7.Keyrock7;
import it.eng.digitalenabler.idm.fiware.IdentityManager;
import it.eng.digitalenabler.restclient.DefaultRestClient;
import it.eng.digitalenabler.sdk.databinder.DefaultDataBinder;
import it.eng.digitalenabler.sdk.databinder.model.DataBinder;
import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.utils.IdentityManagerUtility;




@WebListener
public class DemaServletListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		if("true".equalsIgnoreCase(ConfIDM.getString("idm.fiware.enabled"))) {
		
			IdentityManager.setBaseUrl(ConfIDM.getString("idm.be.host"));
			IdentityManager.setAdminCredentials(ConfIDM.getString("idm.admin.email"), 
												ConfIDM.getString("idm.admin.password"));
			IdentityManager.setRestClient(new DefaultRestClient());
			DataBinder dataBinder = new DefaultDataBinder();
							  dataBinder.init();
							  
			IdentityManager.setJsonSerde(dataBinder);
			IdentityManagerUtility.setIdentityManager(new Keyrock7());
		
		}
	}

}
