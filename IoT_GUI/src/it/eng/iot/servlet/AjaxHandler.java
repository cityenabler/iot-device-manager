package it.eng.iot.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.iot.servlet.ajax.service.discovery.AjaxHandlerDelegate;
import it.eng.iot.servlet.ajax.service.discovery.AjaxHandlerLookup;
import it.eng.iot.servlet.ajax.services.AjaxHandlerServiceType;

@Path("/")
@WebServlet("/ajaxhandler")
public class AjaxHandler extends HttpServlet {
	
	private static final long serialVersionUID = -8956961196989629222L;
	private static final Logger LOGGER = LogManager.getLogger(AjaxHandler.class);
	
	
	public AjaxHandler() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String action = request.getParameter("action");
		AjaxHandlerDelegate ajaxHandlerDelegate = new AjaxHandlerDelegate();
		
		ajaxHandlerDelegate.setAjaxHandlerLookup(new AjaxHandlerLookup());
		ajaxHandlerDelegate.setAjaxHandlerServiceType(AjaxHandlerServiceType.valueOf(action));
		
		try {
			
			ajaxHandlerDelegate.doAction(request,response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			doGet(request, response);
		} catch (ServletException | IOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}
}


