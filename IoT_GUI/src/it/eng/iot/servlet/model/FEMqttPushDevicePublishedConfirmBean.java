package it.eng.iot.servlet.model;


public class FEMqttPushDevicePublishedConfirmBean extends FEMqttDevicePublishedConfirmBean {

	private String topic;
	
	public FEMqttPushDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
	}

	
	public String getTopic() {
		return topic;
	}


	public void setTopic(String topic) {
		this.topic = topic;
	}

	
}
