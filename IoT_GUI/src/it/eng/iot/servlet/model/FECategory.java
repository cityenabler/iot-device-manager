package it.eng.iot.servlet.model;

public class FECategory {

	private String id;
	private String name;
	private String parentCategory;
	private String parentCategoryId;
	
	public FECategory(String id, String name, String parentCategory,  String parentCategoryId) {
		this.id = id;
		this.name = name;
		this.parentCategory = parentCategory;
		this.parentCategoryId = parentCategoryId;
	}
	
	public FECategory(String id, String parentCategory) {
		this.id = id;
		this.name = id;
		this.parentCategory = parentCategory;
		this.parentCategoryId = parentCategory;
	}
	
	public FECategory(String id) {
		this.id = id;
		this.name = id;
		this.parentCategory = null;
		this.parentCategoryId = null;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(String parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}


	@Override
	public String toString() {
		return " [id=" + id + ", name=" + name + ", parentCategory=" + parentCategory + ", parentCategoryId=" + parentCategoryId + "] ";
	}
	
	
	
}
