package it.eng.iot.servlet.model;

public enum TransportProtocol {
HTTP, MQTT,SIGFOX,COAP,OPCUA, LORA
}
