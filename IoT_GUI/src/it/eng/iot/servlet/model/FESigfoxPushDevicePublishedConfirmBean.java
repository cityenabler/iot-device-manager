package it.eng.iot.servlet.model;

public class FESigfoxPushDevicePublishedConfirmBean extends FESigfoxDevicePublishedConfirmBean {
	
	public FESigfoxPushDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
	}
	
}
