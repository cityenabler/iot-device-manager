package it.eng.iot.servlet.model;

public abstract class FEHttpDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	public FEHttpDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.HTTP);
	}

	private String measuresEndpoint;

	public String getMeasuresEndpoint() {
		return measuresEndpoint;
	}

	public void setMeasuresEndpoint(String measuresEndpoint) {
		this.measuresEndpoint = measuresEndpoint;
	}
	
	
	
	
	
}
