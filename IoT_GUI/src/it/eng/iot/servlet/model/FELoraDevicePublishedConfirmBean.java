package it.eng.iot.servlet.model;

public class FELoraDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	private String loraStack;
	private String deviceId;
	
	public FELoraDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
		super.setTransport(TransportProtocol.LORA);
		
	}


	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getLoraStack() {
		return loraStack;
	}


	public void setLoraStack(String loraStack) {
		this.loraStack = loraStack;
	}



	
}
