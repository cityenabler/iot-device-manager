package it.eng.iot.servlet.model;

public abstract class FEMqttDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	public FEMqttDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.MQTT);
	}
	
}
