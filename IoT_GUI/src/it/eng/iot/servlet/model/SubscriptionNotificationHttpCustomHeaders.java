//package it.eng.iot.servlet.model;
//
//import java.util.HashSet;
//
///*
// * "httpCustom": {
//			"url": "http://217.172.12.224/dme/orion/device/delete",
//			"headers": {
//				"contextService": "cedus",
//				"contextServicePath": "/trento/parking",
//				"typeOut": "ParkingSpot"
//			}
//		}
// */
//public class SubscriptionNotificationHttpCustomHeaders {
//
//	String contextService;
//	String contextServicePath;
//	String typeOut;
//	String contextUrl;
//	
//	public String getContextService() {
//		return contextService;
//	}
//	public void setContextService(String contextService) {
//		this.contextService = contextService;
//	}
//	public String getContextServicePath() {
//		return contextServicePath;
//	}
//	public void setContextServicePath(String contextServicePath) {
//		this.contextServicePath = contextServicePath;
//	}
//	public String getTypeOut() {
//		return typeOut;
//	}
//	public void setTypeOut(String typeOut) {
//		this.typeOut = typeOut;
//	}
//	public String getContextUrl() {
//		return contextUrl;
//	}
//	public void setContextUrl(String contextUrl) {
//		this.contextUrl = contextUrl;
//	}
//	
//	public SubscriptionNotificationHttpCustomHeaders(String contextService, String contextServicePath, String typeOut, String contextUrl) {
//		this.contextService = contextService;
//		this.contextServicePath = contextServicePath;
//		this.typeOut =  typeOut;
//		this.contextUrl =  contextUrl;
//		LOGGER.log(Level.INFO, getClass().getName() + " - Running custom constructor");
//	}
//	
////	public SubscriptionNotificationHttpCustomHeaders() {
////		contextService = "";
////		contextServicePath = "";
////		typeOut = "";
////		contextUrl = "";
////		LOGGER.log(Level.INFO, getClass().getName() + " - Running default constructor");
////	}
//	
//}
