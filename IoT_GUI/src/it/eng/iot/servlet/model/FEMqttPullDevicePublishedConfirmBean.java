package it.eng.iot.servlet.model;

public class FEMqttPullDevicePublishedConfirmBean extends FEMqttDevicePublishedConfirmBean {

	private String topic;
	
	public FEMqttPullDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PULL);
	}
	
	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
