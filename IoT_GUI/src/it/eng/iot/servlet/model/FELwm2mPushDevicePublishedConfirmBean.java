package it.eng.iot.servlet.model;


public class FELwm2mPushDevicePublishedConfirmBean extends FELwm2mDevicePublishedConfirmBean {
	
	public FELwm2mPushDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
	}
	
}
