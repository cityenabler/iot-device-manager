package it.eng.iot.servlet.model;

public class FEHttpPushDevicePublishedConfirmBean extends FEHttpDevicePublishedConfirmBean {
	
	public FEHttpPushDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
	}
	
}
