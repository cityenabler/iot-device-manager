package it.eng.iot.servlet.model;

public class FEOpcuaDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	private String serverEndpoint;
	private String deviceId;
	
	public FEOpcuaDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.OPCUA);
		super.setRetrieveDataMode(RetrieveDataMode.PUSH);
		
		
		
		
	}

	public String getServerEndpoint() {
		return serverEndpoint;
	}

	public void setServerEndpoint(String serverEndpoint) {
		this.serverEndpoint = serverEndpoint;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}



	
}
