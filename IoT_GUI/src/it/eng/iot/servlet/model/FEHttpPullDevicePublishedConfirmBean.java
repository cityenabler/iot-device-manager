package it.eng.iot.servlet.model;

public class FEHttpPullDevicePublishedConfirmBean extends FEHttpDevicePublishedConfirmBean {

	public FEHttpPullDevicePublishedConfirmBean(){
		super();
		super.setRetrieveDataMode(RetrieveDataMode.PULL);
	}
	
}
