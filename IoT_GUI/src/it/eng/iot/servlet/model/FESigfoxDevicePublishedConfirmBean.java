package it.eng.iot.servlet.model;

public abstract class FESigfoxDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	public FESigfoxDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.SIGFOX);
	}
	
}
