package it.eng.iot.servlet.model;

public enum DataformatProtocol {
JSON,UL2_0,SIGFOX,LWM2M, OPCUA, CAYENNELPP, CBOR, APPLICATION_SERVER
}
