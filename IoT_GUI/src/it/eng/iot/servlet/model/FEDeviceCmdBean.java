package it.eng.iot.servlet.model;

import java.util.HashSet;
import java.util.Set;

import it.eng.tools.model.IdasDeviceCommand;

public abstract class FEDeviceCmdBean {
	
	private String name;
	private String organization;
	private TransportProtocol transport; //protocol HTTP/MQTT
	private DataformatProtocol dataformat; // UL2_0/JSON
	private RetrieveDataMode retrieveDataMode; // push/pull
	private String apikey;
	private String endpoint;
	private Set<IdasDeviceCommand> commands = new HashSet<IdasDeviceCommand>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public TransportProtocol getTransport() {
		return transport;
	}
	protected void setTransport(TransportProtocol transport){
		this.transport = transport;
	}
	public DataformatProtocol getDataformat() {
		return dataformat;
	}
	public void setDataformat(DataformatProtocol dataformat) {
		this.dataformat = dataformat;
	}
	public RetrieveDataMode getRetrieveDataMode() {
		return retrieveDataMode;
	}
	protected void setRetrieveDataMode(RetrieveDataMode retrieveDataMode) {
		this.retrieveDataMode = retrieveDataMode;
	}
	
	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public Set<IdasDeviceCommand> getCommands() {
		return commands;
	}
	public void setCommands(Set<IdasDeviceCommand> commands) {
		this.commands = commands;
	}
	
}
