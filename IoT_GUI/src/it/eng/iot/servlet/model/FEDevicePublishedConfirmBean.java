package it.eng.iot.servlet.model;

public abstract class FEDevicePublishedConfirmBean {
	
	private String name;
	private String organization;
	private TransportProtocol transport; //protocol HTTP/MQTT
	private DataformatProtocol dataformat; // UL2_0/JSON
	private RetrieveDataMode retrieveDataMode; // push/pull
	private String apikey;
	private String endpoint;
	
	private String mqttusername;
	private String mqttpassword;
	private String sslcertlink;

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public TransportProtocol getTransport() {
		return transport;
	}
	protected void setTransport(TransportProtocol transport){
		this.transport = transport;
	}
	public DataformatProtocol getDataformat() {
		return dataformat;
	}
	public void setDataformat(DataformatProtocol dataformat) {
		this.dataformat = dataformat;
	}
	public RetrieveDataMode getRetrieveDataMode() {
		return retrieveDataMode;
	}
	protected void setRetrieveDataMode(RetrieveDataMode retrieveDataMode) {
		this.retrieveDataMode = retrieveDataMode;
	}
	
	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getMqttusername() {
		return mqttusername;
	}
	public void setMqttusername(String mqttusername) {
		this.mqttusername = mqttusername;
	}
	public String getMqttpassword() {
		return mqttpassword;
	}
	public void setMqttpassword(String mqttpassword) {
		this.mqttpassword = mqttpassword;
	}
	public String getSslcertlink() {
		return sslcertlink;
	}
	public void setSslcertlink(String sslcertlink) {
		this.sslcertlink = sslcertlink;
	}
	
	
}
