package it.eng.iot.servlet.model;

public class FEDeviceCredentials {
	
	private String username;
	private String password;
	private String apikey;
	

	public FEDeviceCredentials(String username, String password) {
		this.username = username;
		this.password = password;
		this.apikey = null;
	}
	
	public FEDeviceCredentials(String apikey) {
		this.username = null;
		this.password = null;
		this.apikey = apikey;
	}
	
	
	
	public FEDeviceCredentials() {
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	
		
}
