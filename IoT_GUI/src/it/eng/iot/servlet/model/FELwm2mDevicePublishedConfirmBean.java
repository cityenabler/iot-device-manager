package it.eng.iot.servlet.model;

public abstract class FELwm2mDevicePublishedConfirmBean extends FEDevicePublishedConfirmBean {

	public FELwm2mDevicePublishedConfirmBean(){
		super();
		super.setTransport(TransportProtocol.COAP);
	}
	
}
