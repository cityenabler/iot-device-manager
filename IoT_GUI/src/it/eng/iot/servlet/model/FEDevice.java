package it.eng.iot.servlet.model;

import java.util.HashMap;
import java.util.Map;

import it.eng.tools.model.IdasDeviceAttribute;
import it.eng.tools.model.IdasDeviceCommand;
import it.eng.tools.model.IdasDeviceInternalAttributeLoRa;

public class FEDevice implements Comparable<FEDevice>{

	private String id;
	private String name;
	
	private String protocol;
	private String transport;
	private String endpoint;
	
	private FECategory category;
	private String enabler;
	
	private Double latitude;
	private Double longitude;
	
	private Map<String, IdasDeviceAttribute> attributes;
	private Map<String, IdasDeviceCommand> commands;
	
	private String organization;
	private String device_owner;
	private String screenId;
	private String mobile_device;
	private String retrieveDataMode;
	private String auth_method;
	private String auth_user;
	private String auth_pwd;
	private String auth_apikey;
	private String streaming_url;
	
	private boolean permitEdit;
	private boolean permitDuplicate;
	private boolean permitDelete;
	
	private String dataformat_protocol;
	private boolean runningSimulation = false;
	private String deveui;
	private String loraDeviceProfileId;
	private boolean historicizing = false;
	private String hubName;
	private String hubType;
	private String gateway;
	private String asset;
	private String modbusAddr;

	private IdasDeviceInternalAttributeLoRa internal_attributes;
	
	//private FEDeviceCredentials credentials;
	
	

	public FEDevice(String id, String name, String protocol, String transport, String endpoint, FECategory category,
			Double latitude, Double longitude,
			Map<String, IdasDeviceAttribute> attributes, 
			Map<String, IdasDeviceCommand> commands,
			String organization, String device_owner, String screenId, String mobile_device, String retrieveDataMode,
			String auth_method, String auth_user, String auth_pwd, String auth_apikey, String streaming_url, boolean permitEdit, 
			boolean permitDelete, boolean permitDuplicate,String dataformat_protocol, boolean runningSimulation,
			String deveui, String loraDeviceProfileId, boolean historicizing, String hubName, String hubType, String gateway, String asset, String modbusAddr) {
		this.id = id;
		this.name = name;
		this.protocol = protocol;
		this.transport = transport;
		this.endpoint = endpoint;
		this.category = category;
		this.latitude = latitude;
		this.longitude = longitude;
		this.attributes = attributes;
		this.commands = commands;
		this.organization = organization;
		this.device_owner = device_owner;
		this.screenId = screenId;
		this.mobile_device = mobile_device;
		this.retrieveDataMode = retrieveDataMode;
		this.auth_method = auth_method;
		this.auth_user = auth_user;
		this.auth_pwd = auth_pwd;
		this.auth_apikey = auth_apikey;
		this.streaming_url = streaming_url;
		this.permitEdit = permitEdit;
		this.permitDuplicate = permitDuplicate;
		this.permitDelete = permitDelete;
		this.dataformat_protocol = dataformat_protocol;
		this.runningSimulation = runningSimulation;
		this.deveui = deveui;
		this.loraDeviceProfileId = loraDeviceProfileId;
		this.historicizing = historicizing;
		this.hubName = hubName;
		this.hubType = hubType;
		this.gateway = gateway;
		this.asset = asset;
		this.modbusAddr = modbusAddr;
	}
	
	public FEDevice(String id) {
		this.id = id;
		this.name = id;
		this.protocol = null;
		this.transport = null;
		this.endpoint = null;
		this.category = null;
		this.latitude = null;
		this.longitude = null;
		this.attributes = new HashMap<String, IdasDeviceAttribute>();
		this.commands = new HashMap<String, IdasDeviceCommand>();
		this.organization = null;
		this.device_owner = null;
		this.screenId = null;
		this.mobile_device = null;
		this.retrieveDataMode = null;
		this.auth_method = null;
		this.auth_user = null;
		this.auth_pwd = null;
		this.auth_apikey = null;
		this.streaming_url = null;
		this.permitEdit = false;
		this.permitDuplicate = false;
		this.permitDelete = false;
		this.dataformat_protocol = null;
		this.enabler = null;
		this.runningSimulation = false;
		this.deveui = null;
		this.loraDeviceProfileId = null;
		this.historicizing = false;
		this.hubName = null;
		this.hubType = null;
		this.gateway = null;
		this.asset = null;
		this.modbusAddr = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}
	
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public FECategory getCategory() {
		return category;
	}

	public void setCategory(FECategory category) {
		this.category = category;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Map<String, IdasDeviceAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, IdasDeviceAttribute> attributes) {
		this.attributes = attributes;
	}
	public void addAttribute(IdasDeviceAttribute ida){
		this.getAttributes().put(ida.getName(), ida);
	}
	
	
	public Map<String, IdasDeviceCommand> getCommands() {
		return commands;
	}

	public void setCommands(Map<String, IdasDeviceCommand> commands) {
		this.commands = commands;
	}
	public void addCommand(IdasDeviceCommand command){
		this.getCommands().put(command.getName(), command);
	}
	
	
	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getDeviceOwner() {
		return device_owner;
	}

	public void setDeviceOwner(String device_owner) {
		this.device_owner = device_owner;
	}
	
	public String getScreenId() {
		return screenId;
	}

	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}
	
	public String getMobileDevice() {
		return mobile_device;
	}

	public void setMobileDevice(String mobile_device) {
		this.mobile_device = mobile_device;
	}
	
	public String getRetrieveDataMode() {
		return retrieveDataMode;
	}

	public void setRetrieveDataMode(String retrieveDataMode) {
		this.retrieveDataMode = retrieveDataMode;
	}
	
	public String getAuthMethod() {
		return auth_method;
	}

	public void setAuthMethod(String auth_method) {
		this.auth_method = auth_method;
	}

	public String getAuthUser() {
		return auth_user;
	}

	public void setAuthUser(String auth_user) {
		this.auth_user = auth_user;
	}

	public String getAuthPwd() {
		return auth_pwd;
	}

	public void setAuthPwd(String auth_pwd) {
		this.auth_pwd = auth_pwd;
	}

	public String getAuthApikey() {
		return auth_apikey;
	}

	public void setAuthApikey(String auth_apikey) {
		this.auth_apikey = auth_apikey;
	}
	
	public boolean isPermitEdit() {
		return permitEdit;
	}

	public void setPermitEdit(boolean permitEdit) {
		this.permitEdit = permitEdit;
	}

	
	
	
	public String getEnabler() {
		return enabler;
	}

	public void setEnabler(String enabler) {
		this.enabler = enabler;
	}

	public boolean isPermitDuplicate() {
		return permitDuplicate;
	}

	public void setPermitDuplicate(boolean permitDuplicate) {
		this.permitDuplicate = permitDuplicate;
	}

	public boolean isPermitDelete() {
		return permitDelete;
	}

	public void setPermitDelete(boolean permitDelete) {
		this.permitDelete = permitDelete;
	}

	
	public String getDataformat_protocol() {
		return dataformat_protocol;
	}

	public void setDataformat_protocol(String dataformat_protocol) {
		this.dataformat_protocol = dataformat_protocol;
	}

	public boolean isRunningSimulation() {
		return runningSimulation;
	}

	public void setRunningSimulation(boolean runningSimulation) {
		this.runningSimulation = runningSimulation;
	}
	
	
	public String getStreaming_url() {
		return streaming_url;
	}

	public void setStreaming_url(String streaming_url) {
		this.streaming_url = streaming_url;
	}


	public String getDeveui() {
		return deveui;
	}

	public void setDeveui(String deveui) {
		this.deveui = deveui;
	}

	public String getLoraDeviceProfileId() {
		return loraDeviceProfileId;
	}

	public void setLoraDeviceProfileId(String loraDeviceProfileId) {
		this.loraDeviceProfileId = loraDeviceProfileId;
	}

	public String getHubName() {
		return hubName;
	}

	public void setHubName(String hubName) {
		this.hubName = hubName;
	}

	public String getHubType() {
		return hubType;
	}

	public void setHubType(String hubType) {
		this.hubType = hubType;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getModbusAddr() {
		return modbusAddr;
	}

	public void setModbusAddr(String modbusAddr) {
		this.modbusAddr = modbusAddr;
	}

	public boolean isHistoricizing() {
		return historicizing;
	}

	public void setHistoricizing(boolean historicizing) {
		this.historicizing = historicizing;
	}

	public IdasDeviceInternalAttributeLoRa getInternal_attributes() {
		return internal_attributes;
	}

	public void setInternal_attributes(IdasDeviceInternalAttributeLoRa internal_attributes) {
		this.internal_attributes = internal_attributes;
	}

	@Override
	public int compareTo(FEDevice o) {
		return screenId.compareToIgnoreCase(o.screenId);
	}

	@Override
	public String toString() {
		return "FEDevice [id=" + id + ", name=" + name + ", protocol=" + protocol + ", transport=" + transport
				+ ", endpoint=" + endpoint + ", category=" + category + ", latitude=" + latitude + ", longitude="
				+ longitude + ", attributes=" + attributes + ", commands=" + commands + ", organization=" + organization
				+ ", device_owner=" + device_owner + ", screenId=" + screenId + ", mobile_device=" + mobile_device
				+ ", retrieveDataMode=" + retrieveDataMode + ", auth_method=" + auth_method + ", auth_user=" + auth_user
				+ ", auth_pwd=" + auth_pwd + ", auth_apikey=" + auth_apikey + ", permitEdit=" + permitEdit
				+ ", permitDuplicate=" + permitDuplicate + ", permitDelete=" + permitDelete + ", dataformat_protocol="
				+ dataformat_protocol + ", hubName=" + hubName +  ", hubType=" + hubType + ", gateway=" + gateway + ", asset=" + asset + ", modbusAddr=" + modbusAddr + "]";
	}

	
	
	
	
}
