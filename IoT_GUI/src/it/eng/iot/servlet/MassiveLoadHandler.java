package it.eng.iot.servlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import it.eng.iot.utils.DeviceConfigManager;


@WebServlet("/massiveloadhandler")
@MultipartConfig
public class MassiveLoadHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = LogManager.getLogger(MassiveLoadHandler.class);

	private Gson gson = new Gson();


	public enum Ajaxaction {

		fileuploadprocess("fileuploadprocess");


		private final String text;

		private Ajaxaction(final String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}

	};

	public MassiveLoadHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		String service = request.getParameter("service"); 
		String servicepath = request.getParameter("servicepath"); 
		
		try {
			processInputFile(request,  response);

			response.getWriter().write(gson.toJson("200"));
		}
		catch (org.json.JSONException jsone) {
			LOGGER.log(Level.ERROR, jsone.getMessage());
			JSONArray emptyarr = new JSONArray();
			response.getWriter().write(emptyarr.toString());
			response.setStatus(500);
		}
		response.sendRedirect("devices?success=true&msg=Devices%20published%0A&scope="+service+"&urbanservice="+servicepath); 
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			doGet(request, response);
		} catch (IOException | ServletException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}

	/**
	 *  Process the input file containing devices 
	 */
	private void processInputFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String service = request.getParameter("service"); 
		String servicepath = request.getParameter("servicepath"); 
		String organizationid = request.getParameter("organizationid"); 
		String userid = request.getParameter("userid");
		
		LOGGER.log(Level.INFO,"Received params: " + service + "|" + servicepath + "|" + organizationid);
		
		try {
			JSONObject jResp = new JSONObject();
			jResp.put("error", false);

			LOGGER.log(Level.INFO," Get File ");

			Part filePart = request.getPart("file");
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			LOGGER.log(Level.INFO," Processing file " + fileName);

			InputStream fileContent = filePart.getInputStream();

			// Convert the inputstream into String
			String inputFileString = convertStreamToString(fileContent);
						
			// Register the inputFileString as Device on IDAS
			jResp = DeviceConfigManager.registerInputFileStringAsIdasDevices(service, servicepath, userid, organizationid, inputFileString);
			
		} catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());

		} 
	}



	

	public String convertStreamToString(InputStream inputStream) {
		String inputFileString = null;

		BufferedInputStream bis = new BufferedInputStream(inputStream);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result;
		try {
			result = bis.read();
			while(result != -1) {
				buf.write((byte) result);
				result = bis.read();
			}
			// StandardCharsets.UTF_8.name() > JDK 7
			inputFileString = buf.toString("UTF-8");

		} catch (IOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		return inputFileString; 

	}
	

}
