package it.eng.iot.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;


import it.eng.iot.configuration.ConfTools;
import it.eng.iot.servlet.model.MapCenter;
import it.eng.iot.utils.DeviceConfigManager;
import it.eng.iot.utils.IotsimUtils;
import it.eng.iot.utils.ServiceConfigManager;



@WebServlet(description = "iotsimhandler", urlPatterns = { "/iotsimhandler" })
public class IotsimHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = LogManager.getLogger(IotsimHandler.class);

	private static final String iotsimStopAction = "/stop";
	private static final String iotsimStartAction = "/start";
	private static final String iotsimURL = ConfTools.getString("iotsim.url");
	
	public enum Ajaxaction {

		start("start"),
		stop("stop"),
		status("status"),
		getconfig("getconfig"),
		checkattr("checkattr");

		private final String text;

		private Ajaxaction(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return this.text;
		}

	}

	public IotsimHandler() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);

		try {
			String action = request.getParameter("action");
			Ajaxaction eAction = Ajaxaction.valueOf(action);
			
			String service = ""; 
			String servicepath = ""; 
			String categoryName = "";
			String categoryApikey = "";
			if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
				
				service = request.getHeader("fiware-service"); 
				servicepath = request.getHeader("fiware-servicepath"); 
				
				categoryName = ServiceConfigManager.getServicePathNameTM(servicepath, service);
				categoryApikey = ServiceConfigManager.getServicePathApiKeyTM(service, servicepath);
			}
			

			switch (eAction) {
			case start:{
				if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
					try {
						String deviceid = request.getHeader("deviceid"); 
						
						if (servicepath.substring(0,1).equalsIgnoreCase("/"))
							servicepath = servicepath.substring(1, servicepath.length());//fix if it has /
						
	
						String  measure_id = request.getHeader("measure_id"); 
						String  measure_type = request.getHeader("measure_type"); 
						
						String  measure_range = request.getHeader("measure_range");
						if (request.getHeader("measure_range")!= null) {
							measure_range = request.getHeader("measure_range").replace(",", "-");
						}
						
						//only geopoint
						String  unit_Of_Measure = request.getHeader("unitOfMeasure"); 
						String  radius_distance = request.getHeader("radius_distance"); 
						
						MapCenter mapCenter = DeviceConfigManager.getMapCenter(service);
						double latDefault = mapCenter.getLat();
						double lngDefault = mapCenter.getLng();
						String  mapcenterLat = String.valueOf(latDefault);
						String  mapcenterLng = String.valueOf(lngDefault);
						
						String  measure_delay_seconds = request.getHeader("measure_delay_seconds"); 
						String queryParams = "?service="+service+"&servicepath="+servicepath+"&categoryName="+categoryName+"&categoryApikey="+categoryApikey;
	
						Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", service);
						headers.put("fiware-servicepath", "/"+servicepath);
						
						headers.put("mapcenterLat", mapcenterLat);
						headers.put("mapcenterLng", mapcenterLng);
	
						if (IotsimUtils.hasDeviceId(deviceid)) {
							queryParams = queryParams.concat("&deviceid="+deviceid);
							headers.put("deviceid", deviceid);
							headers.put("measure_id", measure_id);		// attribute: status
							headers.put("measure_type", measure_type);	// attribute type: string
							headers.put("measure_range", measure_range);	// range of value
							headers.put("measure_delay_seconds", measure_delay_seconds); // measure_delay_seconds number
							headers.put("unitOfMeasure", unit_Of_Measure);
							headers.put("radius_distance", radius_distance);
							
						}
						
						String url = iotsimURL + iotsimStartAction + queryParams;
						LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
						String result = IotsimUtils.consumeGet(url, headers);
	
						JSONObject jsonRes = new JSONObject(result);
						response.getWriter().write(jsonRes.toString());
	
					}
					catch (org.json.JSONException jsone) {
						LOGGER.log(Level.ERROR, jsone.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
				}
				else{
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(200);
				}
				break;
			}

			case stop:{
				if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
					try {
						String deviceid = request.getHeader("deviceid"); 
						String measure_id = request.getHeader("measure_id"); 
	
						if (servicepath.substring(0,1).equalsIgnoreCase("/"))
							servicepath = servicepath.substring(1, servicepath.length());//fix if it has /
						
						
						String queryParams = "?service="+service+"&servicepath="+servicepath;
	
						Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", service);
						headers.put("fiware-servicepath", servicepath);
						if (IotsimUtils.hasDeviceId(deviceid)) {
							queryParams = queryParams.concat("&deviceid="+deviceid);
							headers.put("deviceid", deviceid);
							headers.put("measure_id", measure_id);
						}
						
						String url = iotsimURL + iotsimStopAction + queryParams;
						LOGGER.log(Level.INFO, "SIMULATOR URL {}" , url);
						String result = IotsimUtils.consumeGet(url, headers);
	
						JSONObject jsonRes = new JSONObject(result);
	
						response.getWriter().write(jsonRes.toString());
					}
					catch (org.json.JSONException jsone) {
						LOGGER.log(Level.ERROR, jsone.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}catch (RuntimeException re) {
						LOGGER.log(Level.ERROR, re.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(501);
					}
				}
				else{
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(200);
				}
				break;
			}

			case status:{
				if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
					try {
						String deviceid = request.getHeader("deviceid"); 
						String measure_id = request.getHeader("measure_id"); 
						
						if (servicepath.substring(0,1).equalsIgnoreCase("/"))
							servicepath = servicepath.substring(1, servicepath.length());//fix if it has /
						
	
						
						JSONObject jsonRes = IotsimUtils.getSimStatus( service,  servicepath,  deviceid,  measure_id);
						
	
						response.getWriter().write(jsonRes.toString());
					}
					catch (Exception e) {
						LOGGER.log(Level.ERROR, e.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
				}
				else{
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(200);
				}
				break;
			}

			case getconfig:{
				if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
					try {
						String measure_id = request.getHeader("measure_id"); 
						String measure_type = request.getHeader("measure_type"); 
						
						
						if (servicepath.substring(0,1).equalsIgnoreCase("/"))
							servicepath = servicepath.substring(1, servicepath.length());//fix if it has /
						
						
						String measures = IotsimUtils.loadBusinessDataModel(servicepath, measure_id, measure_type, service, categoryName);
						
						response.getWriter().write(measures);
						LOGGER.log(Level.INFO,"config: " + " - " + measures);
						
					}
					catch (org.json.JSONException jsone) {
						LOGGER.log(Level.ERROR, jsone.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
				}
				else{
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(200);
				}
				break;
			}

			case checkattr:{
				boolean res = false;
				if(Boolean.parseBoolean(ConfTools.getString("simulator.active"))){
					try {
						String deviceid = request.getHeader("deviceid"); 
						
						
	
						if (IotsimUtils.hasDeviceId(deviceid)) {

	
							Map<String, String> headers = new HashMap<>();
							headers.put("fiware-service", service);
							headers.put("fiware-servicepath",  servicepath);
	
							try {
								res = true;
							} catch (org.json.JSONException jsone){
								LOGGER.log(Level.ERROR, jsone.getMessage());
								res = false;
								JSONArray emptyarr = new JSONArray();
								response.getWriter().write(emptyarr.toString());
								response.setStatus(500);
							}
						}
						JSONObject jsonRes = new JSONObject(res);
						response.getWriter().write(jsonRes.toString());
					}
					catch (org.json.JSONException jsone) {
						LOGGER.log(Level.ERROR, jsone.getMessage());
						JSONArray emptyarr = new JSONArray();
						response.getWriter().write(emptyarr.toString());
						response.setStatus(500);
					}
				}
				else{
					JSONArray emptyarr = new JSONArray();
					response.getWriter().write(emptyarr.toString());
					response.setStatus(200);
				}
				break;
			}

			default:
				throw new Exception("Unsupported action " + action); 
			}
		}
		catch (Exception e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString()); 

			try {
				response.getWriter().write(jResp.toString());
			} catch (IOException e1) {
				LOGGER.log(Level.ERROR, e1.getMessage());
			}
		}
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			doGet(request, response);
		} 
		catch (Exception ex) {
			LOGGER.log(Level.ERROR, ex.getMessage());
		}
	}
	
	
}
