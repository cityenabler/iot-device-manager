package it.eng.tools;

public class IOTA {
	
	/**
	 * 
	 */
	public IOTA() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	/**
	 * @param path_services
	 * @param path_devices
	 * @param iot_resource
	 * @param host_protocol
	 * @param dataProtocol
	 * @param transportProtocol
	 * @param host
	 * @param northboundPort
	 * @param southboundPort
	 */
	public IOTA(String path_services, String path_devices, String iot_resource, String host_protocol, String dataProtocol,
			String transportProtocol, String host, String northboundPort, String southboundPort) {
		this.path_services = path_services;
		this.path_devices = path_devices;
		this.iot_resource = iot_resource;
		this.host_protocol = host_protocol;
		this.dataProtocol = dataProtocol;
		this.transportProtocol = transportProtocol;
		this.host = host;
		this.northboundPort = northboundPort;
		this.southboundPort = southboundPort;
	}
	
	
	
	
	private String path_services; 
	private String path_devices; 
	private String iot_resource;
	private String host_protocol; 
	
	private String dataProtocol;
	private String transportProtocol;
	private String host;
	private String northboundPort;
	private String southboundPort;
	
	
	public String getPath_services() {
		return path_services;
	}

	public void setPath_services(String path_services) {
		this.path_services = path_services;
	}

	public String getPath_devices() {
		return path_devices;
	}

	public void setPath_devices(String path_devices) {
		this.path_devices = path_devices;
	}
	
	public String getIot_resource() {
		return iot_resource;
	}

	public void setIot_resource(String iot_resource) {
		this.iot_resource = iot_resource;
	}
	

	public String getDataProtocol() {
		return dataProtocol;
	}

	public void setDataProtocol(String dataProtocol) {
		this.dataProtocol = dataProtocol;
	}

	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getNorthboundPort() {
		return northboundPort;
	}

	public void setNorthboundPort(String northboundPort) {
		this.northboundPort = northboundPort;
	}

	public String getSouthboundPort() {
		return southboundPort;
	}

	public void setSouthboundPort(String southboundPort) {
		this.southboundPort = southboundPort;
	}

	public String getHost_protocol() {
		return host_protocol;
	}
	public void setHost_protocol(String host_protocol) {
		this.host_protocol = host_protocol;
	}
	
	

	
}
