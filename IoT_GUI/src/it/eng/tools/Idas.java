package it.eng.tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;

import it.eng.iot.configuration.ConfIdas;
import it.eng.iot.utils.RestUtils;
import it.eng.tools.base.DeviceManager;
import it.eng.tools.model.IdasDevice;
import it.eng.tools.model.IdasDeviceGetResult;
import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasDeviceListGetResult;
import it.eng.tools.model.IdasServiceList;


public class Idas extends DeviceManager { 
	private static final Logger LOGGER = LogManager.getLogger(Idas.class);
	
	private String path_services = ConfIdas.getString("iota.services"); 
	private String path_devices = ConfIdas.getString("iota.devices"); 
	
	/* Creates an instance of the IoT Agent */
	public Idas(IOTA iota) throws Exception{
		super(iota.getHost_protocol()+iota.getHost()+ ":"+iota.getNorthboundPort());
		LOGGER.log(Level.INFO, "Instance of IOTAgent: " + iota.getHost_protocol()+iota.getHost()+ ":"+iota.getNorthboundPort() );
	}
	
		
	
	
	@Override
	public JSONObject registerService(String fiwareservice, String fiwareservicepath, IdasServiceList services) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		JSONObject jResp;
		String url = this.getBaseUrl() + path_services;
		LOGGER.log(Level.INFO, "IDAS ENDPOINT: " + url);
		
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		LOGGER.log(Level.INFO, "IDAS HEADERS: " + headers);
		LOGGER.log(Level.INFO, "IDAS PAYLOAD: " + new Gson().toJson(services));
		String resp = RestUtils.consumePost(url, new Gson().toJson(services), headers);
		jResp = new JSONObject(resp);
		
		return jResp;
	}

	@Override
	public JSONObject registerDevice(String fiwareservice, String fiwareservicepath, IdasDeviceList devices) 
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		//clean the devices with empty attribute (they are created if you publish on a mqtt topic before the registration on DeMa
		deleteDirtyDevices ( fiwareservice, fiwareservicepath);
		
		
		JSONObject jResp;
		String url = this.getBaseUrl() + path_devices;
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		
		String body = new Gson().toJson(devices);
		
		LOGGER.log(Level.INFO, "IDAS HEADERS: " + headers);
		LOGGER.log(Level.INFO, "IDAS PAYLOAD: " + body);
		
		String resp = RestUtils.consumePost(url, body, headers);
		jResp = new JSONObject(resp);
		
		return jResp;
	}

	@Override
	public boolean deleteService(String fiwareservice, String fiwareservicepath, String resource, String apikey) 
			throws Exception {
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		LOGGER.log(Level.INFO, fiwareservice + " "+servicepath);
		
		String url = getBaseUrl() + path_services;
		
		Map<String, String> configHeaders = new HashMap<String, String>();
							configHeaders.put("fiware-service", fiwareservice);
							configHeaders.put("fiware-servicepath", servicepath);
		
		url += "?resource=" + resource
			+"&apikey=" + apikey;
		
		RestUtils.consumeDelete(url, configHeaders);
		
		return true;
	}

	@Override
	public boolean deleteDevice(String fiwareservice, String fiwareservicepath, String device_id) 
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		LOGGER.log(Level.INFO, fiwareservice + " " + servicepath + " " + device_id);
		
		String url = getBaseUrl() + path_devices;
		url += "/" + device_id;
		
		LOGGER.log(Level.INFO, "Invoking URL: " + url);
		
		Map<String, String> configHeaders = new HashMap<String, String>();
						configHeaders.put("fiware-service", fiwareservice);
						configHeaders.put("fiware-servicepath", servicepath);

		ClientResponse cr = RestUtils.consumeDelete(url, configHeaders);
		LOGGER.log(Level.INFO, "\t" + cr.getStatus());
		LOGGER.log(Level.INFO, "===================\n");
		
		return true;
	}
	
	
	
	//clean the devices with empty attribute (they are created if you publish on a mqtt topic before the registration on DeMa
	public void deleteDirtyDevices (String fiwareservice, String fiwareservicepath) throws Exception{
		
		IdasDeviceListGetResult allDevices =  getDeviceList(fiwareservice, fiwareservicepath);
		Set<IdasDeviceGetResult> devices = allDevices.getDevices();
		
		for (IdasDeviceGetResult device: devices) {
			
			if (device.getStatic_attributes().size() == 0 ) {
				
				LOGGER.log(Level.INFO, "deleteDirtyDevices: " +device.getDevice_id());
				deleteDevice(fiwareservice,  fiwareservicepath,  device.getDevice_id());
			}
		}
	}
	
	
	
	public IdasDeviceListGetResult getDeviceList(String fiwareservice, String fiwareservicepath) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		String url = getBaseUrl() + path_devices;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasDeviceListGetResult result = new Gson().fromJson(serviceResponse, IdasDeviceListGetResult.class);
		
		return result;
	}
	
	public IdasDevice getDevice(String fiwareservice, String fiwareservicepath, String deviceid) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;

		String url = getBaseUrl() + path_devices + "/" + deviceid;
		LOGGER.log(Level.INFO, "Invoking url: " + url);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasDevice result = new Gson().fromJson(serviceResponse, IdasDevice.class);
		
		return result;
	}
	
	@Override
	public boolean updateDevice (String fiwareservice, String fiwareservicepath, IdasDeviceList devices) throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
	
		String url = this.getBaseUrl() + path_devices;
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		
		String body = new Gson().toJson(devices);
		boolean out = true;
		try {
		RestUtils.consumePut(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		}
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
			out = false;
		}
		return out;
	}

	
	
	
	public IdasServiceList getServices(String fiwareservice, String fiwareservicepath) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;

		String url = getBaseUrl() + path_services;
		LOGGER.log(Level.INFO, url);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasServiceList result = new Gson().fromJson(serviceResponse, IdasServiceList.class);
				
		return result;
	}
	
	public  IdasDeviceListGetResult getDeviceList(String fiwareservice, String fiwareservicepath, String url) 
	throws Exception{

		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		IdasDeviceListGetResult result;
		String serviceResponse = RestUtils.consumeGet(url, headers);
		
		result = new Gson().fromJson(serviceResponse, IdasDeviceListGetResult.class);

		return result;
	}

	public IdasDevice getDevice(String fiwareservice, String fiwareservicepath, String deviceid, String url) 
		throws Exception{
	
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		String fullUrl = url + "/" + deviceid;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(fullUrl, headers);
		IdasDevice result = new Gson().fromJson(serviceResponse, IdasDevice.class);
		
		return result;
	}

	public String registerDevice(String fiwareservice, String fiwareservicepath, IdasDeviceList devices, String url) 
	throws Exception {

		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		//clean the devices with empty attribute (they are created if you publish on a mqtt topic before the registration on DeMa
		
		deleteDirtyDevices ( fiwareservice, fiwareservicepath, url);
		
		
		//JSONObject jResp;
		
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		
		String body = new Gson().toJson(devices);
		
		LOGGER.log(Level.INFO, "IDAS HEADERS: " + headers);
		LOGGER.log(Level.INFO, "IDAS PAYLOAD: " + body);
		
		String resp = RestUtils.consumePost(url, body, headers);
		
		
		return resp;
	}
	
	public boolean deleteDevice(String fiwareservice, String fiwareservicepath, String device_id, String url) 
	throws Exception {

		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		LOGGER.log(Level.INFO, fiwareservice + " " + servicepath + " " + device_id);
		
		
		String FullUrl = url;
		FullUrl += "/" + device_id;
		
		LOGGER.log(Level.INFO, "Invoking URL: " + url);
		
		Map<String, String> configHeaders = new HashMap<String, String>();
						configHeaders.put("fiware-service", fiwareservice);
						configHeaders.put("fiware-servicepath", servicepath);
		
		ClientResponse cr = RestUtils.consumeDelete(FullUrl, configHeaders);
		LOGGER.log(Level.INFO, "\t" + cr.getStatus());
		LOGGER.log(Level.INFO, "===================\n");
		
		return true;
	}

	public void deleteDirtyDevices (String fiwareservice, String fiwareservicepath, String  url) throws Exception{
	
		IdasDeviceListGetResult allDevices =  getDeviceList(fiwareservice, fiwareservicepath, url);
		Set<IdasDeviceGetResult> devices = allDevices.getDevices();
		
		for (IdasDeviceGetResult device: devices) {
			
			if (device.getStatic_attributes().size() == 0 ) {
				
				LOGGER.log(Level.INFO, "deleteDirtyDevices: " +device.getDevice_id());
				deleteDevice(fiwareservice,  fiwareservicepath,  device.getDevice_id(), url);
			}
		}
	}
	
	
}
