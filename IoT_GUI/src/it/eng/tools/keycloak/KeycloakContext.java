package it.eng.tools.keycloak;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.representations.AccessToken;

import com.google.gson.Gson;

import it.eng.iot.configuration.ConfIDM;
import it.eng.iot.utils.RestUtils;

public class KeycloakContext {
	
	private KeycloakPrincipal<RefreshableKeycloakSecurityContext> principal;
	
	public KeycloakPrincipal<RefreshableKeycloakSecurityContext> getPrincipal() {
		return principal;
	}

	public KeycloakContext() {
		this.principal = this.getPrincipal();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public KeycloakContext(HttpServletRequest req) {
		this.principal = (KeycloakPrincipal)req.getUserPrincipal();
	}

	public AccessToken getToken() {
		return this.principal.getKeycloakSecurityContext().getToken();
	}
	
	public String getTokenString() {
		return this.principal.getKeycloakSecurityContext().getTokenString();
	}
	
	public Set<String> getClientRoles() {
		return this.getToken().getResourceAccess(ConfIDM.getString("keycloak.clientId")).getRoles();
	}
	
	public String getRefreshToken() {
		return this.principal.getKeycloakSecurityContext().getRefreshToken();
	}
	
	public boolean isValidSession() {
		String endpoint = ConfIDM.getString("keycloak.host") + "/realms/" + ConfIDM.getString("KEYCLOAK_REALM") + "/protocol/openid-connect/userinfo";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.getTokenString());
		try {
			RestUtils.consumeGet(endpoint, headers);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public User getUserInfo(AccessToken token) {
		Set<String> userRoles = this.getClientRoles();
		ArrayList<String> orgs = (ArrayList<String>) token.getOtherClaims().get("organization");
		ArrayList<String> group = (ArrayList<String>) token.getOtherClaims().get("groups");
		
		Set<String> orgSet = new HashSet<String>();
		Set<String> groupSet = new HashSet<String>();
		if(orgs != null) {
			for (int i = 0; i < orgs.size(); i++) {
				orgSet.add(orgs.get(i));
			}
		}
		if(group != null) {
			for (int i = 0; i < group.size(); i++) {
				groupSet.add(group.get(i));
			}
		}
		
		User user = new User();
		user.setEmail(token.getEmail());
		user.setId(token.getSubject());
		user.setUsername(token.getPreferredUsername());
		user.setOrganizations(Optional.of(orgSet));
		user.setGroups(Optional.of(groupSet));
		user.setRoles(Optional.of(userRoles));
		
		return user;
	}
	
	public TokenUserInfo getTokenUserInfo(String token) {
		String endpoint = ConfIDM.getString("keycloak.host") + "/realms/" + ConfIDM.getString("KEYCLOAK_REALM") + "/protocol/openid-connect/userinfo";
		String response = "";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.getTokenString());
		try {
			response = RestUtils.consumeGet(endpoint, headers);
		} catch (Exception e) {
			
		}
		TokenUserInfo tui = new Gson().fromJson(response, TokenUserInfo.class);
		return tui;
	}
	
	public boolean isUserInGroup(String groupId, String userId) {
		String endpoint = ConfIDM.getString("keycloak.host") + "/realms/" + ConfIDM.getString("KEYCLOAK_REALM") + "/groups/" + groupId + "";
		String response = "";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.getTokenString());
		try {
			response = RestUtils.consumeGet(endpoint, headers);
		} catch (Exception e) {
			
		}
		JSONArray members = new JSONArray(response);
		for(Object member: members) {
			JSONObject jsonMember = (JSONObject) member;
			String memberId = jsonMember.optString("id");
			if(memberId.equalsIgnoreCase(userId)) {
				return true;
			}
		}
		return false;
		
	}
	
	public String getServiceAccessToken() {
		
		String endpoint = ConfIDM.getString("keycloak.host") + "/realms/" + ConfIDM.getString("KEYCLOAK_REALM") + "/protocol/openid-connect/token";
		
		HttpClient client = new HttpClient();
		PostMethod httppost = new PostMethod(endpoint);
		httppost.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		
		httppost.addParameter(new NameValuePair("client_id", ConfIDM.getString("keycloak.tm.clientId")));
		httppost.addParameter(new NameValuePair("client_secret", ConfIDM.getString("keycloak.tm.secret")));
		httppost.addParameter(new NameValuePair("grant_type", "client_credentials"));
		
		JSONObject json = new JSONObject();
		try {
			int statusCode = client.executeMethod(httppost);
			if (statusCode == HttpStatus.SC_OK) {
				String response = httppost.getResponseBodyAsString();
				json = new JSONObject(response);
			}		
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return json.getString("access_token");
		
	}
	
	
	


}
