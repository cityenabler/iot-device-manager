package it.eng.tools.model;

import java.util.Objects;

public  class FilterEntity extends CBEntity{
	
	private String scopeId;
	private String scopeName;
	private String urbanserviceId;
	private String urbanserviceName;
	private String device;
	private String attribute;
	private String dataformat;
	private String transportProtocol;
	private String retrieveDataMode;
	private String name;
	
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getUrbanserviceId() {
		return urbanserviceId;
	}

	public void setUrbanserviceId(String urbanserviceId) {
		this.urbanserviceId = urbanserviceId;
	}
	public String getUrbanserviceName() {
		return urbanserviceName;
	}

	public void setUrbanserviceName(String urbanserviceName) {
		this.urbanserviceName = urbanserviceName;
	}
	

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getDataformat() {
		return dataformat;
	}

	public void setDataformat(String dataformat) {
		this.dataformat = dataformat;
	}

	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	public String getRetrieveDataMode() {
		return retrieveDataMode;
	}

	public void setRetrieveDataMode(String retrieveDataMode) {
		this.retrieveDataMode = retrieveDataMode;
	}

	public FilterEntity() {
		super("","");
		this.scopeId = "";
		this.scopeName = "";
		this.urbanserviceId = "";
		this.urbanserviceName = "";
		this.device = "";
		this.attribute = "";
		this.dataformat = "";
		this.transportProtocol = "";
		this.retrieveDataMode = "";
		this.name = "";
	}

	
	
	/**
	 * @param id
	 * @param type
	 * @param scope
	 * @param urbanservice
	 * @param device
	 * @param attribute
	 * @param name
	 */ 
	public FilterEntity(String id, String type, String scopeId, String scopeName, String urbanserviceId, String urbanserviceName,String device, String attribute, String dataformat,  String transportProtocol,  String retrieveDataMode,  String name) {
		super(id, type);
		this.scopeId = scopeId;
		this.scopeName = scopeName;
		this.urbanserviceId = urbanserviceId;
		this.urbanserviceName = urbanserviceName;
		this.device = device;
		this.attribute = attribute;
		this.dataformat = dataformat;
		this.transportProtocol = transportProtocol;
		this.retrieveDataMode = retrieveDataMode;
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ Objects.hash(attribute, device, name, scopeId, scopeName, urbanserviceId, urbanserviceName);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilterEntity other = (FilterEntity) obj;
		return Objects.equals(attribute, other.attribute) && Objects.equals(device, other.device)
				&& Objects.equals(name, other.name) && Objects.equals(scopeId, other.scopeId)
				&& Objects.equals(scopeName, other.scopeName) && Objects.equals(urbanserviceId, other.urbanserviceId)
				&& Objects.equals(urbanserviceName, other.urbanserviceName);
	}
	
	
	
	
	
	
}
