package it.eng.tools.model;

import java.util.HashSet;
import java.util.Set;


public class IdasDevice{
	
	private String device_id;
	private String entity_name;
	private String entity_type;
	private String transport;
	private String protocol;
	private String endpoint;
	private String loraDeviceProfileId;
	
	private Set<IdasDeviceAttribute> attributes;
	private Set<IdasDeviceStaticAttribute> static_attributes;
	private Set<IdasDeviceCommand> commands;
	private Object internal_attributes;
	
	public IdasDevice(String device_id, String entity_name, String entity_type, String transport, String protocol, String endpoint, String loraDeviceProfileId) {
		this(device_id, entity_name, entity_type, transport, protocol, endpoint, loraDeviceProfileId, new HashSet<IdasDeviceAttribute>(), new HashSet<IdasDeviceStaticAttribute>(), new HashSet<IdasDeviceCommand>(), new Object() );
	}
	
	public IdasDevice(String device_id, String entity_name, String entity_type, String transport, String protocol, String endpoint, String loraDeviceProfileId, Set<IdasDeviceAttribute> attributes, Set<IdasDeviceStaticAttribute> static_attributes, Set<IdasDeviceCommand> commands, Object internal_attributes) {
		this.device_id = device_id;
		this.entity_name = entity_name;
		this.entity_type = entity_type;
		this.transport = transport;
		this.protocol = protocol;
		this.endpoint = endpoint;
		this.loraDeviceProfileId = loraDeviceProfileId;
		this.attributes = attributes;
		this.static_attributes = static_attributes;
		this.commands = commands;
		this.internal_attributes = internal_attributes;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getEntity_name() {
		return entity_name;
	}
	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}
	public String getEntity_type() {
		return entity_type;
	}
	public void setEntity_type(String entity_type) {
		this.entity_type = entity_type;
	}
	public String getTransport() {
		return transport;
	}
	public void setTransport(String transport) {
		this.transport = transport;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public Set<IdasDeviceAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(Set<IdasDeviceAttribute> attributes) {
		this.attributes = attributes;
	}
	public Set<IdasDeviceStaticAttribute> getStatic_attributes() {
		return static_attributes;
	}
	public void setStatic_attributes(
			Set<IdasDeviceStaticAttribute> static_attributes) {
		this.static_attributes = static_attributes;
	}
	public Set<IdasDeviceCommand> getCommands() {
		return commands;
	}
	public void setCommands(Set<IdasDeviceCommand> commands) {
		this.commands = commands;
	}

	
	public Object getInternal_attributes() {
		return internal_attributes;
	}
	
	
	public void setInternal_attributes(
			Object internal_attributes) {
		this.internal_attributes = internal_attributes;
	}

	public String getLoraDeviceProfileId() {
		return loraDeviceProfileId;
	}

	public void setLoraDeviceProfileId(String loraDeviceProfileId) {
		this.loraDeviceProfileId = loraDeviceProfileId;
	}
	
}
