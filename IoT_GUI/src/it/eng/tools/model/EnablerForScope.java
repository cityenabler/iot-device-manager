package it.eng.tools.model;

public class EnablerForScope {
	
	
	private String id;
	
	private Scope refScope;
	
	private Enabler refEnabler;
	
	public EnablerForScope() {
		super();
	}
	
	public EnablerForScope(String id, Scope refScope, Enabler refEnabler) {
		super();
		this.id = id;
		this.refScope = refScope;
		this.refEnabler = refEnabler;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Scope getRefScope() {
		return refScope;
	}
	public void setRefScope(Scope refScope) {
		this.refScope = refScope;
	}
	public Enabler getRefEnabler() {
		return refEnabler;
	}
	public void setRefEnabler(Enabler refEnabler) {
		this.refEnabler = refEnabler;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", refScope=" + refScope + ", refEnabler=" + refEnabler + "]";
	}
	
}

