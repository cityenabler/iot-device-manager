package it.eng.tools.model;

import java.util.Objects;

public class EntityAttribute<T> {

	private T value;
	private String type;
	
	public EntityAttribute(T value) {
		setValue(value);
		setType(this.value.getClass().getSimpleName());
	}
	
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityAttribute other = (EntityAttribute) obj;
		return Objects.equals(type, other.type) && Objects.equals(value, other.value);
	}
	
	
}
