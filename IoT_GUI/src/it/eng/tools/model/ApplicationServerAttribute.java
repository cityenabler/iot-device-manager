package it.eng.tools.model;


public class ApplicationServerAttribute {

	/*
	 "application_server": {
                        "host": "147.27.60.184:8083",
                        "provider": "TTN"
                    }	
	*/
	
	private String host;
	private String provider;
	private String username;
	private String password;

	
	public ApplicationServerAttribute() {
		
	}


	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}


	public String getProvider() {
		return provider;
	}


	public void setProvider(String provider) {
		this.provider = provider;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
