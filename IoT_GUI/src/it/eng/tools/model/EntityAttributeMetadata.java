package it.eng.tools.model;

public class EntityAttributeMetadata {
	private String name;
	private String type;
	private String jsonschema; 
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getJsonschema() {
		return jsonschema;
	}

	public void setJsonschema(String jsonschema) {
		this.jsonschema = jsonschema;
	}

	
	/**
	 * 
	 */
	public EntityAttributeMetadata() {
		super();
		// TODO Auto-generated constructor stub
	}



}
