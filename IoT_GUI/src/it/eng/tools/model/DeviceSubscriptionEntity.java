package it.eng.tools.model;

public  class DeviceSubscriptionEntity extends CBEntity{
	
	
	private EntityAttribute<String> deviceId; 
	private EntityAttribute<String> subscriptionId; 
	private EntityAttribute<String> mashupId; 
	private EntityAttribute<String> status; 
	
	
	public EntityAttribute<String> getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(EntityAttribute<String> deviceId) {
		this.deviceId = deviceId;
	}


	public EntityAttribute<String> getSubscriptionId() {
		return subscriptionId;
	}


	public void setSubscriptionId(EntityAttribute<String> subscriptionId) {
		this.subscriptionId = subscriptionId;
	}


	public EntityAttribute<String> getStatus() {
		return status;
	}


	public void setStatus(EntityAttribute<String> status) {
		this.status = status;
	}
	

	public EntityAttribute<String> getMashupId() {
		return mashupId;
	}


	public void setMashupId(EntityAttribute<String> mashupId) {
		this.mashupId = mashupId;
	}
	
	public DeviceSubscriptionEntity() {
		super("","");
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
	}


	public DeviceSubscriptionEntity(String id, String type, EntityAttribute<String> deviceId,  EntityAttribute<String> subscriptionId, EntityAttribute<String> mashupId) {
		super(id, type);
		this.deviceId = deviceId;
		this.subscriptionId = subscriptionId;
		this.mashupId = mashupId;
	}
	
	
	
	
	
	
}
