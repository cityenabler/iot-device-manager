package it.eng.tools.model;

import java.util.Objects;

public  class CBEntity {
	
	private String id;
	private String type;
	
	public CBEntity(String id, String type) {
		this.id = id;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CBEntity other = (CBEntity) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}
	
	
	
}
