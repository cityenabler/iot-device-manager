package it.eng.tools.model;

public class IdasDeviceCommandsGetResult {
	
	private String object_id;
	private String name;
	private String type;
	
	public IdasDeviceCommandsGetResult(String object_id, String name, String type) {
		this.object_id = object_id;
		this.name = name;
		this.type = type;
	}
	
	public String getObject_id() {
		return object_id;
	}
	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
