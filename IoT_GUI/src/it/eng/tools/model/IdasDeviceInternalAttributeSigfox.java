package it.eng.tools.model;

public class IdasDeviceInternalAttributeSigfox {

	/*
	"internal_attributes": [
        {
          "mapping": "theCounter::uint:32  theParam1::uint:32 param2::uint:8 tempDegreesCelsius::uint:8  voltage::uint:16"
        }
      ]
	*/
	

	
private String mapping;
	
	public IdasDeviceInternalAttributeSigfox(String mapping) {
		this.mapping = mapping;
	}
	
	
	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}
	
	
	
	
}
