package it.eng.tools.model;

public  class DeviceEndpointSubscriptionEntity extends CBEntity{
	
	
	private EntityAttribute<String> deviceId; 
	private EntityAttribute<String> subscriptionId; 
	private EntityAttribute<String> endpointURL; 
	private EntityAttribute<String> status; 
	private EntityAttribute<String> target;
	
	
	public EntityAttribute<String> getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(EntityAttribute<String> deviceId) {
		this.deviceId = deviceId;
	}


	public EntityAttribute<String> getSubscriptionId() {
		return subscriptionId;
	}


	public void setSubscriptionId(EntityAttribute<String> subscriptionId) {
		this.subscriptionId = subscriptionId;
	}


	public EntityAttribute<String> getStatus() {
		return status;
	}


	public void setStatus(EntityAttribute<String> status) {
		this.status = status;
	}
	
	
	public EntityAttribute<String> getEndpointURL() {
		return endpointURL;
	}

	public void setEndpointURL(EntityAttribute<String> endpointURL) {
		this.endpointURL = endpointURL;
	}

	
	

	public EntityAttribute<String> getTarget() {
		return target;
	}


	public void setTarget(EntityAttribute<String> target) {
		this.target = target;
	}


	public DeviceEndpointSubscriptionEntity() {
		super("","");
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
	}


	public DeviceEndpointSubscriptionEntity(String id, String type, EntityAttribute<String> deviceId,  EntityAttribute<String> subscriptionId, EntityAttribute<String> endpointURL, EntityAttribute<String> target) {
		super(id, type);
		this.deviceId = deviceId;
		this.subscriptionId = subscriptionId;
		this.endpointURL = endpointURL;
		this.target = target;
	}
	
	
}
