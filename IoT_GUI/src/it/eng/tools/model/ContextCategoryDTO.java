package it.eng.tools.model;

public class ContextCategoryDTO {
	
	private String service;
	private String servicepath;
	
	public ContextCategoryDTO() {
		super();
	}

	public ContextCategoryDTO(String service, String servicepath) {
		super();
		this.service = service;
		this.servicepath = servicepath;
	}
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getServicepath() {
		return servicepath;
	}
	public void setServicepath(String servicepath) {
		this.servicepath = servicepath;
	}

}
