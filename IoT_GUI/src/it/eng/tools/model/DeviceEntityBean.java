/**
  {
"id": "anna1",
"type": "Device",
"TimeInstant": {
"type": "ISO8601",
"value": " ",
"metadata": {}
},
"device_owner": {
"type": "Text",
"value": "cedus-admin",
"metadata": {}
},
"location": {
"type": "geo:point",
"value": "-77.40245752036572,42.615192004180585",
"metadata": {}
},
"opType": {
"type": "Text",
"value": "created",
"metadata": {}
},
"organization": {
"type": "Text",
"value": "0888009313824014b81d94f9b6c1c4d3",
"metadata": {}
},
"screeanId": {
"type": "Text",
"value": "ANNA1",
"metadata": {}
},
"status": {
"type": "Text",
"value": " ",
"metadata": {}
}
}
*/

package it.eng.tools.model;

import java.util.Date;
import java.util.Objects;

public class DeviceEntityBean extends CBEntity{
	
	private EntityAttribute<Date> dateModified;
	private EntityAttribute<String> device_owner; 
	private EntityAttribute<String> opType;
	private EntityAttribute<String> organization;
	private EntityAttribute<String> screenId; 
	private EntityAttribute<String> mobile_device;
	private EntityAttribute<String> location;
	
	private EntityAttribute<String> dataformat_protocol; 
	private EntityAttribute<String> transport_protocol;
	private EntityAttribute<String> retrieve_data_mode;
	private EntityAttribute<String> streaming_url;
	
	//Add hubs for DeMa version 2
	private EntityAttribute<String> hub_name;
	private EntityAttribute<String> hub_type;
	private EntityAttribute<String> gateway;
	private EntityAttribute<String> asset;
	private EntityAttribute<String> modbus_addr;
		
		
	public DeviceEntityBean() {
		this(new String("Device"));
	}
	
	public DeviceEntityBean(String type) {
		this(type, 
				new EntityAttribute<Date>(new Date()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()), 
				new EntityAttribute<String>(new String()), 
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String())
			);
	}
	
	public DeviceEntityBean(String type,
			
			EntityAttribute<Date> dateModified,
			EntityAttribute<String> device_owner,
			EntityAttribute<String> opType,
			EntityAttribute<String> organization,
			EntityAttribute<String> screenId,
			EntityAttribute<String> mobile_device,
			EntityAttribute<String> location,
			EntityAttribute<String> dataformat_protocol,
			EntityAttribute<String> transport_protocol,
			EntityAttribute<String> retrieve_data_mode,
			EntityAttribute<String> streaming_url,
			EntityAttribute<String> hub_name,
			EntityAttribute<String> hub_type,
			EntityAttribute<String> gateway,
			EntityAttribute<String> asset,
			EntityAttribute<String> modbus_addr) {
		
		super(new String(), type);
		this.dateModified = dateModified;
		this.device_owner = device_owner;
		this.opType = opType;
		this.organization = organization;
		this.screenId = screenId;
		this.mobile_device = mobile_device;
		this.location = location;
		this.dataformat_protocol = dataformat_protocol;
		this.transport_protocol = transport_protocol;
		this.retrieve_data_mode = retrieve_data_mode;
		this.streaming_url = streaming_url;
		this.hub_name = hub_name;
		this.hub_type = hub_type;
		this.gateway = gateway;
		this.asset = asset;
		this.modbus_addr = modbus_addr;

		String _id = screenId.getValue();
		this.setId(_id);
		
	}

	
	public EntityAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(EntityAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	
	public EntityAttribute<String> getDeviceOwner() {
		return device_owner;
	}
	public void setResource(EntityAttribute<String> device_owner) {
		this.device_owner = device_owner;
	}
	
	public EntityAttribute<String> getOpType() {
		return opType;
	}
	public void setOpType(EntityAttribute<String> opType) {
		this.opType = opType;
	}
	
	public EntityAttribute<String> getOrganization() {
		return organization;
	}
	public void setOrganization(EntityAttribute<String> organization) {
		this.organization = organization;
	}
	
	public EntityAttribute<String> getScreenId() {
		return screenId;
	}
	public void setScreenId(EntityAttribute<String> screenId) {
		this.screenId = screenId;
	}
	
	public EntityAttribute<String> getMobileDevice() {
		return mobile_device;
	}
	public void setMobileDevice(EntityAttribute<String> mobile_device) {
		this.mobile_device = mobile_device;
	}
	
	public EntityAttribute<String> getLocation() {
		return location;
	}

	public void setLocation(EntityAttribute<String> location) {
		this.location = location;
	}

	public EntityAttribute<String> getDataformatProtocol() {
		return dataformat_protocol;
	}

	public void setDataformatProtocol(EntityAttribute<String> dataformat_protocol) {
		this.dataformat_protocol = dataformat_protocol;
	}
	
	public EntityAttribute<String> getTransportProtocol() {
		return transport_protocol;
	}

	public void setTransportProtocol(EntityAttribute<String> transport_protocol) {
		this.transport_protocol = transport_protocol;
	}
	
	public EntityAttribute<String> getRetrieveDataMode() {
		return retrieve_data_mode;
	}

	public void setRetrieveDataMode(EntityAttribute<String> retrieve_data_mode) {
		this.retrieve_data_mode = retrieve_data_mode;
	}

	public EntityAttribute<String> getStreaming_url() {
		return streaming_url;
	}

	public void setStreaming_url(EntityAttribute<String> streaming_url) {
		this.streaming_url = streaming_url;
	}

	public EntityAttribute<String> getHubName() {
		return hub_name;
	}

	public void setHubName(EntityAttribute<String> hub_name) {
		this.hub_name = hub_name;
	}

	public EntityAttribute<String> getHubType() {
		return hub_type;
	}

	public void setHubType(EntityAttribute<String> hub_type) {
		this.hub_type = hub_type;
	}

	public EntityAttribute<String> getGateway() {
		return gateway;
	}

	public void setGateway(EntityAttribute<String> gateway) {
		this.gateway = gateway;
	}

	public EntityAttribute<String> getAsset() {
        return asset;
    }

    public void setAsset(EntityAttribute<String> asset) {
        this.asset = asset;
    }

    public EntityAttribute<String> getModbusAddr() {
		return modbus_addr;
	}

	public void setModbusAddr(EntityAttribute<String> modbus_addr) {
		this.modbus_addr = modbus_addr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(dataformat_protocol, dateModified, device_owner, location, mobile_device,
				opType, organization, retrieve_data_mode, screenId, streaming_url, transport_protocol, hub_name, hub_type, gateway, asset, modbus_addr);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceEntityBean other = (DeviceEntityBean) obj;
		return Objects.equals(dataformat_protocol, other.dataformat_protocol)
				&& Objects.equals(dateModified, other.dateModified) && Objects.equals(device_owner, other.device_owner)
				&& Objects.equals(location, other.location) && Objects.equals(mobile_device, other.mobile_device)
				&& Objects.equals(opType, other.opType) && Objects.equals(organization, other.organization)
				&& Objects.equals(retrieve_data_mode, other.retrieve_data_mode)
				&& Objects.equals(screenId, other.screenId) && Objects.equals(streaming_url, other.streaming_url)
				&& Objects.equals(transport_protocol, other.transport_protocol)
				&& Objects.equals(hub_name, other.hub_name)
				&& Objects.equals(hub_type, other.hub_type)
				&& Objects.equals(gateway, other.gateway)
				&& Objects.equals(asset, other.asset)
				&& Objects.equals(modbus_addr, other.modbus_addr);
	}
	
	
	
	
		
}
