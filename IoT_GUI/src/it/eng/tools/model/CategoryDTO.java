package it.eng.tools.model;

public class CategoryDTO {

	private String id;
	private String apikey; 
	private String name; 
	private EnablerForScope refEnabler4scope;
	private String servicePath;
	
	public CategoryDTO(
			String id,
			String apikey, 
			String name,
			EnablerForScope refEnabler4scope, 
			String servicePath) {
		this.id = id;
		this.apikey = apikey;
		this.name = name;
		this.refEnabler4scope = refEnabler4scope;
		this.servicePath = servicePath;
		
	}

	@Override
	public String toString() {
		return "[id=" + id + ", apikey=" + apikey + ", name=" + name + ", ref_enablerForScope=" + refEnabler4scope + ", servicepath=" + servicePath + "]";
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EnablerForScope getRefEnabler4scope() {
		return refEnabler4scope;
	}

	public void setRefEnabler4scope(EnablerForScope refEnabler4scope) {
		this.refEnabler4scope = refEnabler4scope;
	}

	public String getServicePath() {
		return servicePath;
	}

	public void setServicepath(String servicePath) {
		this.servicePath = servicePath;
	}


}
