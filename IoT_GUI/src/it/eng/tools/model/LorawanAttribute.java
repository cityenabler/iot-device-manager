package it.eng.tools.model;


public class LorawanAttribute {

	/*
			
	 "lorawan": {
                    "application_server": {
                        "host": "147.27.60.184:8083",
                        "provider": "TTN"
                    },
                    "dev_eui": "e62d032c07d15301",
                    "app_eui": "6",
                    "application_id": "ciaooo",
                    "application_key": "444B8EF16415B5F6ED777EAFE695C49",
                    "data_model": "application_server"
                }		
	*/
	
	private ApplicationServerAttribute application_server;
	private String app_eui;
	private String application_id;
	private String application_key;
	private String data_model;
	private String dev_eui;
	

	public LorawanAttribute() {
	}


	public ApplicationServerAttribute getApplication_server() {
		return application_server;
	}


	public void setApplication_server(ApplicationServerAttribute application_server) {
		this.application_server = application_server;
	}


	public String getApp_eui() {
		return app_eui;
	}


	public void setApp_eui(String app_eui) {
		this.app_eui = app_eui;
	}


	public String getApplication_id() {
		return application_id;
	}


	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}


	public String getApplication_key() {
		return application_key;
	}


	public void setApplication_key(String application_key) {
		this.application_key = application_key;
	}


	public String getData_model() {
		return data_model;
	}


	public void setData_model(String data_model) {
		this.data_model = data_model;
	}


	public String getDev_eui() {
		return dev_eui;
	}


	public void setDev_eui(String dev_eui) {
		this.dev_eui = dev_eui;
	}
	
	
	
	
}
