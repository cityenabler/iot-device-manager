package it.eng.tools.model;

public  class ServiceSubscriptionEntity extends CBEntity{
	
	
	private EntityAttribute<String> subscriptionId;  
	private EntityAttribute<String> status; 
	
	


	public EntityAttribute<String> getSubscriptionId() {
		return subscriptionId;
	}


	public void setSubscriptionId(EntityAttribute<String> subscriptionId) {
		this.subscriptionId = subscriptionId;
	}


	public EntityAttribute<String> getStatus() {
		return status;
	}


	public void setStatus(EntityAttribute<String> status) {
		this.status = status;
	}
	

	
	public ServiceSubscriptionEntity() {
		super("","");
		new EntityAttribute<String>(new String());
		new EntityAttribute<String>(new String());
	}


	public ServiceSubscriptionEntity(String id, String type,  EntityAttribute<String> subscriptionId) {
		super(id, type);
		this.subscriptionId = subscriptionId;
	}
	
	
	
	
	
	
}
