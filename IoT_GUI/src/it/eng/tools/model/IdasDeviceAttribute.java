package it.eng.tools.model;

public class IdasDeviceAttribute {
	
	private String object_id;
	private String type;
	private String name;
	
	public IdasDeviceAttribute(String name, String type) {
		this.name = name;
		this.type = type;
		this.object_id = null;
	}
	
	public IdasDeviceAttribute(String name, String type, String object_id) {
		this.name = name;
		this.type = type;
		this.object_id = object_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObject_id() {
		return object_id;
	}

	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
