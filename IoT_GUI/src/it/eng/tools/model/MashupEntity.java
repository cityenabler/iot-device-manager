package it.eng.tools.model;

public  class MashupEntity {
	
	
	private String mashupId; 
	private String name; 
	private String description; 
	private String type;
	private String status;
	private String subscriptionId;
	
	
	public String getMashupId() {
		return mashupId;
	}
	public void setMashupId(String mashupId) {
		this.mashupId = mashupId;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}


	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getSubscriptionId() {
		return subscriptionId;
	}
	
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	public MashupEntity() {
		this.mashupId = "";
		this.name = "";
		this.description = "";
		this.type = "";
		this.status = "";
		this.subscriptionId = "";
	}

	public MashupEntity(String mashupId, String name, String description, String type, String status, String subscriptionId) {
		
		this.mashupId = mashupId;
		this.name = name;
		this.description = description;
		this.type = type;
		this.status = status;
		this.subscriptionId = subscriptionId;
		
	}
	
	
	
	
	
	
}
