package it.eng.tools.model;

public class IdasDeviceLoRaLWM2M extends IdasDevice {
	
	private Object internal_attributes;

	public IdasDeviceLoRaLWM2M(String device_id, String entity_name, String entity_type, String transport,
			String protocol, String endpoint, String loraDeviceProfileId) {
		super(device_id, entity_name, entity_type, transport, protocol, endpoint, loraDeviceProfileId);
	}

	public Object getInternal_attributes() {
		return internal_attributes;
	}

	public void setInternal_attributes(Object internal_attributes) {
		this.internal_attributes = internal_attributes;
	}
	
	

}
