package it.eng.tools.model;

public class IdasDeviceInternalAttributeLoRa {

	/*
		 "internal_attributes": {
                "lorawan": {
                    "application_server": {
                        "host": "147.27.60.184:8083",
                        "provider": "TTN"
                    },
                    "dev_eui": "e62d032c07d15301",
                    "app_eui": "6",
                    "application_id": "ciaooo",
                    "application_key": "444B8EF16415B5F6ED777EAFE695C49",
                    "data_model": "application_server"
                }
            }
	*/
	
	private LorawanAttribute lorawan;

	public LorawanAttribute getLorawan() {
		return lorawan;
	}

	public void setLorawan(LorawanAttribute lorawan) {
		this.lorawan = lorawan;
	}

	public IdasDeviceInternalAttributeLoRa() {
	}
	
	


	
}
