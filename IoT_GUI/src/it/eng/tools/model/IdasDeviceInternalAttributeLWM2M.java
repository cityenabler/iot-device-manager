package it.eng.tools.model;

import java.util.Map;

public class IdasDeviceInternalAttributeLWM2M {

	/*
	 	"internal_attributes": {
			"lwm2mResourceMapping": {
				"Battery": {
					"objectType": 1111,
					"objectInstance": 0,
					"objectResource": 1
				},
              "status": {
					"objectType": 1111,
					"objectInstance": 0,
					"objectResource": 2
				}
			}
		},
	*/
	
	private Map<String, ObjectAttribute> lwm2mResourceMapping;
	
	public IdasDeviceInternalAttributeLWM2M(Map<String, ObjectAttribute> lwm2mResourceMapping) {
		this.lwm2mResourceMapping = lwm2mResourceMapping;
	}
	
	public IdasDeviceInternalAttributeLWM2M() {
	}

	public Map<String, ObjectAttribute> getLwm2mResourceMapping() {
		return lwm2mResourceMapping;
	}
	public void setLwm2mResourceMapping(Map<String, ObjectAttribute> lwm2mResourceMapping) {
		this.lwm2mResourceMapping = lwm2mResourceMapping;
	}
	
}
