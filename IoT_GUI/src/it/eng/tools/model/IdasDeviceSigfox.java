package it.eng.tools.model;

import java.util.ArrayList;
import java.util.Set;

public class IdasDeviceSigfox extends IdasDevice {
	
	public IdasDeviceSigfox(String device_id, String entity_name, String entity_type, String transport, String protocol,
			String endpoint, String loraDeviceProfileId) {
		super(device_id, entity_name, entity_type, transport, protocol, endpoint, loraDeviceProfileId);
	}

	private Set<IdasDeviceInternalAttributeSigfox> internal_attributes;

	public Set<IdasDeviceInternalAttributeSigfox> getInternal_attributes() {
		return internal_attributes;
	}

	public void setInternal_attributes(Set<IdasDeviceInternalAttributeSigfox> internal_attributes) {
		this.internal_attributes = internal_attributes;
	}

	

	
	

}
