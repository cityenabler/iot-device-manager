package it.eng.tools.model;

public class ScopeDTO {
	
	private String enabler;
	private String organizationId;
	private String roleId;
	private Scope scope;
	
	public String getEnabler() {
		return enabler;
	}

	public void setEnabler(String enabler) {
		this.enabler = enabler;
	}
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "[ enabler=" + enabler + ", roleId=" + roleId + ", organizationId=" + organizationId + ", scope=" + scope + "]";
	}

	
}

