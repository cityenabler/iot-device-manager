package it.eng.tools;

import java.util.HashMap;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.iot.configuration.ConfDme;
import it.eng.iot.utils.RestUtils;
import it.eng.tools.base.MashupEditor;

public class DME extends MashupEditor {
	
	private static final Logger LOGGER = LogManager.getLogger(DME.class);

	public DME() throws Exception {
		super(ConfDme.getString("dme.address"));
	}

	/** Get All Mashups */
	@Override
	public String getMashups() throws Exception {

		String endpoint = this.getBaseUrl() + ConfDme.getString("dme.api.mashups.get") + "?type=all";
		LOGGER.log(Level.INFO, "Invoking " + endpoint);
		
		String responseData = "";
		try{ responseData = RestUtils.consumeGet(endpoint, new HashMap<String, String>()); }
		catch(Exception e){
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		LOGGER.log(Level.INFO, "Mashups list: " + responseData); 
		
		return responseData;
	}

}
