package it.eng.tools.base;

import org.json.JSONObject;

import it.eng.tools.model.IdasDeviceList;
import it.eng.tools.model.IdasServiceList;

public abstract class DeviceManager extends Tool {
	
	protected DeviceManager(String baseurl) throws Exception{
		super(baseurl);
	}
	
	protected abstract JSONObject registerService(String fiwareservice, String fiwareservicepath, IdasServiceList services)
								throws Exception;
	
	protected abstract JSONObject registerDevice(String fiwareservice, String fiwareservicepath, IdasDeviceList services)
								throws Exception;
	
	protected abstract boolean deleteService(String fiwareservice, String fiwareservicepath, String resource, String apikey)
								throws Exception;
	
	protected abstract boolean deleteDevice(String fiwareservice, String fiwareservicepath, String device_id)
								throws Exception;
	
	protected abstract boolean updateDevice(String fiwareservice, String fiwareservicepath, IdasDeviceList services)
								throws Exception;
	
}
