package it.eng.tools.base;

public abstract class MashupEditor extends Tool {
	
	protected MashupEditor(String baseurl) throws Exception{
		super(baseurl);
	}
	
	protected abstract String getMashups() throws Exception;
	
}
