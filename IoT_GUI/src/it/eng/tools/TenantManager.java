package it.eng.tools;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.iot.configuration.ConfTools;
import it.eng.iot.utils.CommonUtils;
import it.eng.iot.utils.RestUtils;
import it.eng.tools.keycloak.KeycloakContext;
import it.eng.tools.model.Scope;


public class TenantManager {
	
	private static final Logger LOGGER = LogManager.getLogger(TenantManager.class);
	
	private static String baseUrl = ConfTools.getString("tenant.manager.protocol") + ConfTools.getString("tenant.manager.host");
	
	public static String getAllScopes() {
		
		String resp = "";
		String url = baseUrl + "/contexts";
		KeycloakContext kc = new KeycloakContext();
				
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + kc.getServiceAccessToken());
						
		try{ 
			
			resp = RestUtils.consumeGet(url, headers); 
			
			LOGGER.log(Level.INFO, "Response Tenant Scopes: " + resp);
		
		}
		catch(Exception e){
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
						
		return resp;
					
	}
	
	
	public static String getAllUrbanscopes(String scopeId) {
		
		String resp = "";
		String url = baseUrl + "/contexts/" + scopeId + "/verticals";
		
		KeycloakContext kc = new KeycloakContext();
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + kc.getServiceAccessToken());
						
		
		try{ resp = RestUtils.consumeGet(url, headers); 
			
			LOGGER.log(Level.INFO, "Response Tenant Urbanscopes: " + RestUtils.consumeGet(url, headers));
		
		}
		catch(Exception e){
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
						
		return resp;
					
	}
	
	public static String getScopeById(String scopeId) {
		
		String resp = "";
		String url = baseUrl + "/contexts/" + scopeId;
		
		KeycloakContext kc = new KeycloakContext();
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + kc.getServiceAccessToken());			
		
		try{ resp = RestUtils.consumeGet(url, headers); 
			
			LOGGER.log(Level.INFO, "Response Tenant Scope by scopeId: " + RestUtils.consumeGet(url, headers));
		
		}
		catch(Exception e){
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
						
		return resp;
					
	}
	
	public static String getUrbanserviceById(String scopeId, String urbanserviceId) {
		
		urbanserviceId = CommonUtils.cleanServicePath(urbanserviceId);
		
		String resp = "";
		String url = baseUrl + "/contexts/" + scopeId + "/verticals/" + urbanserviceId;
		
		KeycloakContext kc = new KeycloakContext();
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + kc.getServiceAccessToken());				
		
		try{ resp = RestUtils.consumeGet(url, headers); 
			
			LOGGER.log(Level.INFO, "Response Tenant Urbanscope by urbanscopeId: " + RestUtils.consumeGet(url, headers));
		
		}
		catch(Exception e){
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
						
		return resp;
					
	}
	
	public static boolean updateScopeById(String scopeId, Scope body) {
		
		String resp = "";
		String url = baseUrl + "/contexts/" + scopeId;
		
		KeycloakContext kc = new KeycloakContext();
		
		boolean response = false;
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + kc.getServiceAccessToken());						
						
		try{ 
			resp = RestUtils.consumePost(url, body, headers); 
			
			LOGGER.log(Level.INFO, "Response update scope by scopeId: " + resp);
			if(!resp.isEmpty() && resp != "No Content" && resp != "Unauthorized" && resp != "Forbidden") {
				response = true;
			}
		}
		catch(Exception e){
			
			LOGGER.log(Level.ERROR, e.getMessage());
			
		}
						
		return response;
					
	}
	
}
