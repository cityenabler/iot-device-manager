package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.digitalenabler.exception.InvalidTokenException;


/**
 * Servlet implementation class IndexController
 */
@WebServlet("/devices")
public class DevicesController extends HttpServlet {
       
	private static final long serialVersionUID = 7591147451345558646L;
	private static final Logger LOGGER = LogManager.getLogger(DevicesController.class);

	public DevicesController() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{ CommonController.doGet(request, response); }
		catch(InvalidTokenException e){
			
			String scheme = request.getScheme() + "://";
		    String serverName = request.getServerName();
		    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		    String contextPath = request.getContextPath();
						
			String redirectTo = scheme + serverName + serverPort + contextPath;
			
			LOGGER.log(Level.ERROR, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
			
		} 		
		
		String selectedScope = request.getParameter("scope");
		String selectedUrbanservice = request.getParameter("urbanservice");
		
		if( selectedScope == null || selectedScope.trim().isEmpty()){
			response.sendRedirect("index");
		}
		else if(selectedUrbanservice == null || selectedUrbanservice.trim().isEmpty()){
			response.sendRedirect("urbanservices");
		}
		else{
			String nextJSP = "/WEB-INF/view/devices.jsp";
			nextJSP = nextJSP + "?scope="+selectedScope+ "&urbanservice="+selectedUrbanservice;
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
			dispatcher.forward(request,response);
		}
	}
	

}
