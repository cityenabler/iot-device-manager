package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.digitalenabler.exception.InvalidTokenException;
import it.eng.digitalenabler.exception.NewUserException;
import it.eng.iot.configuration.Conf;
import it.eng.iot.configuration.ConfIDM;

/**
 * Servlet implementation class NewDeviceController
 */
@WebServlet("/newcamera")
public class NewCameraController extends HttpServlet {
	private static final long serialVersionUID = 2536975717238521423L;
	private static final Logger LOGGER = LogManager.getLogger(NewCameraController.class);

	public NewCameraController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{ CommonController.doGet(request, response); }
		catch(InvalidTokenException e){
			
			String scheme = request.getScheme() + "://";
		    String serverName = request.getServerName();
		    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		    String contextPath = request.getContextPath();
						
			String redirectTo = scheme + serverName + serverPort + contextPath;
			
			LOGGER.log(Level.ERROR, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
			
		} 		
		
		String selectedScope = request.getParameter("scope");
		String selectedUrbanservice = request.getParameter("urbanservice");
		
		String nextJSP = "/WEB-INF/view/newcamera.jsp";
		if (selectedScope!=null && selectedUrbanservice != null){
			nextJSP = nextJSP + "?scope="+selectedScope+ "&urbanservice="+selectedUrbanservice;
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		
		dispatcher.forward(request,response);
		
				
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("currentDevice", request.getParameter("currentDevice"));
		request.setAttribute("actionmenu", request.getParameter("actionmenu"));
		doGet(request,response);
	}

}
