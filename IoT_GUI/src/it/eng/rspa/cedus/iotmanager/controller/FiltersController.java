package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import it.eng.digitalenabler.exception.InvalidTokenException;
import it.eng.iot.utils.FilteringManager;
import it.eng.iot.utils.ServiceConfigManager;
import it.eng.tools.keycloak.User;
import it.eng.tools.model.FilterEntity;
import it.eng.tools.model.ServiceEntityBean;

/**
 * Servlet implementation class FiltersController
 */
@WebServlet("/filters")
public class FiltersController extends HttpServlet {

	private static final long serialVersionUID = 7591147451345558646L;
	private static final Logger LOGGER = LogManager.getLogger(FiltersController.class);

	public FiltersController() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.log(Level.INFO, "filteredEntities >>> " + request.getAttribute("filteredEntities"));
		try{ CommonController.doGet(request, response); }
		catch(InvalidTokenException e){
			
			String scheme = request.getScheme() + "://";
		    String serverName = request.getServerName();
		    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		    String contextPath = request.getContextPath();
						
			String redirectTo = scheme + serverName + serverPort + contextPath;
			
			LOGGER.log(Level.ERROR, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
			
		} 
		
		LOGGER.log(Level.INFO, "filteredEntities >>> " + request.getAttribute("filteredEntities"));
		
		String nextJSP = "/WEB-INF/view/filterResult.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request,response);
		
				
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
						
		String searchKeyword = request.getParameter("searchKeyword");
		Set<FilterEntity> filteredEntities = new HashSet<FilterEntity>();

		if( searchKeyword == null || searchKeyword.trim().isEmpty()){
			response.sendRedirect("index");
		}
		else{
			
			try { 
				
				// Get user info 
				HttpSession session = request.getSession();
				User userInfo = (User) session.getAttribute("userInfo");
				// Verify userIsSeller 
				boolean userIsSeller= false;
				if(session.getAttribute("userIsSeller") != null){
				    userIsSeller = (Boolean) request.getSession().getAttribute("userIsSeller");
				}
//				Set<ServiceEntityBean> services = ServiceConfigManager.getServices(); // from Orion
				Set<ServiceEntityBean> services = ServiceConfigManager.getServicesTM(); // from Tenant Manager
				
				if (userIsSeller){	
					filteredEntities = FilteringManager.searchFullTextById(searchKeyword, services); 
				} else if (!userIsSeller){		
					Set<String> orgs = userInfo.getGroups().get();
					Set<ServiceEntityBean> permittedServices = ServiceConfigManager.getPermittedServices(services, orgs);
					
					if (!permittedServices.isEmpty() || permittedServices != null){
						services.retainAll(permittedServices);
					} 		
					filteredEntities = FilteringManager.searchFullTextById(searchKeyword, services);
				}
			} 
			catch (Exception e) {
				LOGGER.log(Level.ERROR, "Search Failed");
			}
			
			// Transform into json
			String filteredEntitiesJson = new Gson().toJson(filteredEntities);
			request.setAttribute("filteredEntities", filteredEntitiesJson);
			LOGGER.log(Level.INFO, "filteredEntitiesJson >>> " + filteredEntitiesJson);
			doGet(request,response);
		}
		
		
	}



}
