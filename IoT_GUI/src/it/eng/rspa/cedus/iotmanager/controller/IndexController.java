package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.digitalenabler.exception.InvalidTokenException;
import it.eng.iot.servlet.AjaxHandler;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/index")
public class IndexController extends HttpServlet {
       
	private static final long serialVersionUID = 7591147451345558646L;
	private static final Logger LOGGER = LogManager.getLogger(AjaxHandler.class);
	
	public IndexController() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{ CommonController.doGet(request, response); }
		catch(InvalidTokenException e){
			
			String scheme = request.getScheme() + "://";
		    String serverName = request.getServerName();
		    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		    String contextPath = request.getContextPath();
						
			String redirectTo = scheme + serverName + serverPort + contextPath;
			
			LOGGER.log(Level.ERROR, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
			
		} 
	
		String enabler = request.getParameter("enabler");
        if( enabler == null || enabler.trim().isEmpty()){
            String nextJSP = "home";    
            response.sendRedirect(nextJSP);
        }
        else{
        	
        	boolean isCitizen = (Boolean)(request.getAttribute("isCitizen"));
    		LOGGER.log(Level.INFO, "isCitizen: " + isCitizen);
        	
    		if (isCitizen) {
    			LOGGER.log(Level.INFO, "USER NOT AUTORIZED ");
    			String nextJSP = "/WEB-INF/view/citizenMessage.jsp";
    			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
    			dispatcher.forward(request,response);
    		} else {
    			String nextJSP = "/WEB-INF/view/devices.jsp";
                nextJSP = nextJSP + "?enabler="+enabler;
                
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
                dispatcher.forward(request,response);
    			
    		}
        }
	}
}
