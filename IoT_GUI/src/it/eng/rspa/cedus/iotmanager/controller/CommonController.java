package it.eng.rspa.cedus.iotmanager.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import it.eng.digitalenabler.exception.InvalidTokenException;
import it.eng.iot.configuration.Conf;
import it.eng.iot.servlet.AjaxHandler;
import it.eng.iot.servlet.model.Permission;
import it.eng.tools.keycloak.KeycloakContext;
import it.eng.tools.keycloak.User;

public abstract class CommonController {
	
	private static final Logger LOGGER = LogManager.getLogger(AjaxHandler.class);

	public static void doGet(HttpServletRequest request, HttpServletResponse response) throws InvalidTokenException{
		
		HttpSession session = request.getSession();

		if (request.getParameter("lang") != null) {
			session.setAttribute("lang", request.getParameter("lang"));
		} else if (request.getParameter("lang") == null && session.getAttribute("lang") == null) {
			session.setAttribute("lang", Conf.getString("default.lang"));
		}
		
		KeycloakContext kc = new KeycloakContext(request);

		// Temporary workaround for SSO logout. Should be fixed in Keycloak 12.0.0
		if(!kc.isValidSession()) {
			session.removeAttribute("token");
			session.removeAttribute("refresh_token");				
			session.removeAttribute("userPerms");
			session.removeAttribute("userIsSeller");
			session.removeAttribute("isSeller");
			session.removeAttribute("userInfo");
			session.removeAttribute("username");
			session.invalidate();
			throw new InvalidTokenException();
		}
		
		User userInfo = kc.getUserInfo(kc.getToken());		
		Set<String> userRoles = userInfo.getRoles().get();
		String username = userInfo.getUsername();
		request.setAttribute("username", username);
		session.setAttribute("userInfo", userInfo);

		String token = (String) session.getAttribute("token");
		String refresh_token = (String) session.getAttribute("refresh_token");
		
		if(token == null) {
			session.setAttribute("token", kc.getTokenString());
		}
		if(refresh_token == null) {
			session.setAttribute("refresh_token", kc.getRefreshToken());
		}	
			
		boolean isCitizen = false;
		boolean userIsSeller = false;
		
		if(session.getAttribute("userPerms") == null) {
			// VERIFY THE USER PERMISSIONS
			Permission userPerm;
			Set<Permission> userPerms = new HashSet<Permission>();
			Set<Permission> configPermissions = loadConfigPermissions();
			try {								
				// User is citizen without permission
				
				Set<String> groupList = userInfo.getGroups().get();
				Set<String> organizationsList = userInfo.getOrganizations().get();
				String userOrganizationRole = "";
				
				for(String g: groupList) {
					if(organizationsList.contains(g)) {
						userOrganizationRole = "owner";
					} else {
						userOrganizationRole = "member";
					}
					
					for (String r : userRoles) {
						
						for(Permission perm: configPermissions) {
							if (perm.getApplicationRole().equalsIgnoreCase(r)
									&& perm.getOrganizationsRole().equalsIgnoreCase(userOrganizationRole)) {
								userPerm = new Permission();
								userPerm.setPermissionCRUD(perm.getPermissionCRUD());
								userPerm.setApplicationRole(perm.getApplicationRole());
								userPerm.setOrganizationsRole(perm.getOrganizationsRole());
								userPerm.setAsset(perm.getAsset());
								userPerm.setAssetRole(perm.getAssetRole());
								userPerms.add(userPerm);
							}

						}
						
						if ("citizen".equalsIgnoreCase(r)) {
							isCitizen = true;
						}
						if ("seller".equalsIgnoreCase(r)) {
							userIsSeller = true;
						}
						
					}
					
				}
				
				session.setAttribute("userPerms", userPerms);
				request.setAttribute("userPerms", userPerms);
				
				request.setAttribute("isCitizen", isCitizen);
				session.setAttribute("userIsSeller", userIsSeller);
			}
			catch (Exception ex){
				LOGGER.log(Level.ERROR, "Invalid permission");
				response.setStatus(500);
			}
		}
		
		request.setAttribute("userId", userInfo.getId());
		session.setAttribute("userId", userInfo.getId());
		JSONArray allowedOrgs;
		if(isCitizen) {
			allowedOrgs = new JSONArray(userInfo.getOrganizations().get());
		} else {
			allowedOrgs = new JSONArray(userInfo.getGroups().get());
		}
		request.setAttribute("organization", allowedOrgs);
		session.setAttribute("organization", allowedOrgs);
	}
	
	public static Set<Permission> loadConfigPermissions() {

		Set<Permission> configPermissions = new HashSet<Permission>();

		// Seller
		// CASE: seller.owner.scope.owner=CRUD
		Permission permission = new Permission();
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("owner");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.owner.scope.owner"));
		configPermissions.add(permission);

		// CASE: seller.owner.urbanservice.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("owner");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.owner.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: seller.owner.device.owner=CRUD
		permission = new Permission(); // svuoto
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("owner");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.owner.device.owner"));
		configPermissions.add(permission);

		// FIX
		// Seller - member
		// CASE: seller.member.scope.owner=CRUD
		permission = new Permission(); // svuoto
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("member");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.member.scope.owner"));
		configPermissions.add(permission);

		// CASE: seller.member.urbanservice.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("member");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.member.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: seller.member.device.owner=CRUD
		permission = new Permission(); // svuoto
		permission.setApplicationRole("seller");
		permission.setOrganizationsRole("member");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("seller.member.device.owner"));
		configPermissions.add(permission);

		// ********************************
		// #cityManager OWNER
		// CASE: citymanager.owner.scope.owner=CRUD
		permission = new Permission(); // svuoto
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("owner");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.owner.scope.owner"));
		configPermissions.add(permission);

		// CASE: citymanager.owner.urbanservice.owner=CRUD
		permission = new Permission(); // svuoto
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("owner");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.owner.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: citymanager.owner.device.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("owner");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.owner.device.owner"));
		configPermissions.add(permission);

		// #cityManager MEMBER and Asset OWNER
		// CASE: citymanager.member.scope.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.scope.owner"));
		configPermissions.add(permission);

		// CASE: citymanager.member.urbanservice.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: citymanager.member.device.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.device.owner"));
		configPermissions.add(permission);

		// #cityManager MEMBER and Asset NOT OWNER
		// CASE: citymanager.member.scope.member=R
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("scope");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.scope.member"));
		configPermissions.add(permission);

		// CASE: citymanager.member.urbanservice.member=R
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("urbanservice");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.urbanservice.member"));
		configPermissions.add(permission);

		// CASE: citymanager.member.device.member=R
		permission = new Permission();
		permission.setApplicationRole("citymanager");
		permission.setOrganizationsRole("member");
		permission.setAsset("device");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("citymanager.member.device.member"));
		configPermissions.add(permission);

		// ********************************
		// #urbanserviceProvider OWNER
		// CASE: urbanserviceprovider.owner.scope.owner=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("owner");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.scope.owner"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.owner.urbanservice.owner=R
		permission = new Permission(); // svuoto
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("owner");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.owner.device.owner=CRUD
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("owner");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.owner.device.owner"));
		configPermissions.add(permission);

		// #cityManager MEMBER and Asset OWNER
		// CASE: urbanserviceprovider.member.scope.owner=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("scope");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.scope.owner"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.member.urbanservice.owner=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("urbanservice");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.urbanservice.owner"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.member.device.owner=CRU
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("device");
		permission.setAssetRole("owner");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.device.owner"));
		configPermissions.add(permission);

		// #cityManager MEMBER and Asset NOT OWNER
		// CASE: urbanserviceprovider.member.scope.member=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("scope");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.scope.member"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.member.urbanservice.member=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("urbanservice");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.urbanservice.member"));
		configPermissions.add(permission);

		// CASE: urbanserviceprovider.member.device.member=R
		permission = new Permission();
		permission.setApplicationRole("urbanserviceprovider");
		permission.setOrganizationsRole("member");
		permission.setAsset("device");
		permission.setAssetRole("member");
		permission.setPermissionCRUD(Conf.getString("urbanserviceprovider.member.device.member"));
		configPermissions.add(permission);

		LOGGER.log(Level.INFO, "Loaded configured permissions " + configPermissions.size());
		return configPermissions;

	};


	
}
