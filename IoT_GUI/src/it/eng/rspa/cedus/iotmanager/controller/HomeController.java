package it.eng.rspa.cedus.iotmanager.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.eng.digitalenabler.exception.InvalidTokenException;
import it.eng.iot.servlet.AjaxHandler;

/**
 * Servlet implementation class IndexController
 */
@WebServlet("/home")
public class HomeController extends HttpServlet {
       
	private static final long serialVersionUID = 7591147451345558646L;
	private static final Logger LOGGER = LogManager.getLogger(AjaxHandler.class);
	
	public HomeController() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{ CommonController.doGet(request, response); }
		catch(InvalidTokenException e){
			
			String scheme = request.getScheme() + "://";
		    String serverName = request.getServerName();
		    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
		    String contextPath = request.getContextPath();
						
			String redirectTo = scheme + serverName + serverPort + contextPath;
			
			LOGGER.log(Level.ERROR, "Redirect to "+redirectTo);
			
			response.sendRedirect(redirectTo);
			return;
		} 
		
//		boolean isMultienabler = Boolean.parseBoolean(Conf.getString("MultiEnabler.enabled"));
//		
//		String nextJSP = "/WEB-INF/view/home.jsp";
//		if (!isMultienabler) 
		String	nextJSP="/WEB-INF/view/devices.jsp";//with default enabler
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request,response);
		
	}

}
