/*Manage the loader*/
function toggleLoader(load, caller){
	if(load){
		$("#noresults").fadeOut();
		$("#contentloader").fadeIn();
//		console.log("loader on "+caller)
		$('body').css("overflow", 'hidden');
	}else{
		$("#contentloader").fadeOut();
		$("#noresults").fadeIn();
//		console.log("loader off "+caller)
		$('body').css("overflow", 'auto');
	}
	
}


/**
 * Get all the parameters from the url
 * 
 * @return [string] The vars
 */
var openSearch = false

$(document).ready(function(){
	$('.searchBar #searchform .openSearch').on('click', function(e){
		
		console.log("open search")
		var input = $(this).parent().parent().find("input#search");
		if(!openSearch){
			$('ul.searchBar').css("width", "calc(50% - 450px)");
			input.css("width", "calc( 100% - 54px").focus();
			input.parent().css("width", "auto");
			
			$(".closeSearch").css('display','block');
			openSearch = true;
		}
			
	})
	$('.searchBar #searchform .closeSearch').on('click', function(e){
		
		console.log("close search")
		var input = $(this).parent().find("input#search");
		if(openSearch){
			$(".closeSearch").css('display','none');
			input.css("width", "0%");
			input.parent().css("width", "54px");
			$('ul.searchBar').css("width", "auto");
			
			openSearch = false;
		}

	})
	
});

function get(name) {
	 if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	      return decodeURIComponent(name[1]);
}


function getServiceList() {

	var input = new Object();
	input['action'] = 'getContextsList';

	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			console.log(data);
			appendCategories(data);
		},
		error : function(xhr, status, error) {
			console.error(error);
		}
	});
}

function addMarker(location, map, deviceName) {

	var marker = L.marker(location).bindPopup(deviceName).addTo(map);

	return marker;
}

function addCameraMarker(location, map) {
	
	var marker = L.marker(location).addTo(map);

	return marker;
}

function initMap(domElement, centerPar, zoomPar, deviceName) {
	
	if (typeof zoomPar === 'undefined')
		zoomPar = 2;
	
	if(centerPar.lat < 1 && centerPar.lng < 1)//near 0,0
		zoom = 2;
	
	domElement.innerHTML = "<div id='mapDeviceDetails' style='width: 100%; height: 100%;'></div>";
	var mbAttr = '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
	mbUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	
	var streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr, minZoom: 2}); //base layer
	 map = new L.Map('mapDeviceDetails', {
		zoom: zoomPar, 
		center: centerPar,
		layers: streets
	});
	map.setView([centerPar.lat, centerPar.lng], zoomPar);
	
	return map;
}

function initNewDeviceMap(center){
	var zoom = mapcenter.zoom;
	if(mapcenter.lat < 1 && mapcenter.lng < 1)//near 0,0
		zoom = 2;
	
	if (typeof center !== 'undefined' && center != null ){
		mapcenter=center;
	}
	newdevice.setLatitude(mapcenter.lat);
	newdevice.setLongitude(mapcenter.lng);
	$('#pac-input-lat').val(newdevice.getLatitude());
	$('#pac-input-lng').val(newdevice.getLongitude());
	
	map = initMap(document.getElementById('map'), mapcenter, zoom); 
	if (typeof center !== 'undefined' && center != null ){
		marker = L.marker([newdevice.getLatitude(), newdevice.getLongitude()]).addTo(map);
	}

}

function initCenterMap(center){
	var zoom = mapcenter.zoom;
	if(mapcenter.lat < 1 && mapcenter.lng < 1)//near 0,0
		zoom = 2;
	
	if (typeof center !== 'undefined' && center != null ){
		mapcenter=center;
	}
	
	map = initMap(document.getElementById('map'), mapcenter, zoom); 
	if (typeof center !== 'undefined' && center != null ){
		marker = L.marker([newdevice.getLatitude(), newdevice.getLongitude()]).addTo(map);
	}
	
	map.on('click', function (e) {
		
		if (typeof marker !== 'undefined' && marker != null ){
		    map.removeLayer(marker); //remove previous marker
		}
		marker = L.marker(e.latlng).addTo(map);	  
	  
		$('#pac-input-lat').val(marker._latlng.lat);
		$('#pac-input-lng').val(marker._latlng.lng);
		newdevice.setLatitude(marker._latlng.lat);
		newdevice.setLongitude(marker._latlng.lng);

	});
	
	map.addControl( new L.Control.Search({
		url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
		jsonpParam: 'json_callback',
		propertyName: 'display_name',
		propertyLoc: ['lat','lon'],
		marker: L.circle([51.508, -0.11], {radius:0, opacity: 0}),
		autoCollapse: true,
		autoType: false,
		minLength: 2,
		zoom: 10
	}) );

}

var inputLatitude, inputLongitude;

function initNewDeviceMapFromInputFields(){
	
	var zoomFromFields = mapcenter.zoom;
	
	if(mapcenter.lat < 1 && mapcenter.lng < 1)
		zoomFromFields = 2;
	
	inputLatitude = $("#pac-input-lat").val();
	inputLongitude = $("#pac-input-lng").val();
	
	var center = {
			lat: inputLatitude,
			lng: inputLongitude
	};
	
	if (typeof center !== 'undefined' && center != null ){
		mapcenter=center;
		$('#pac-input-lat').val(mapcenter.lat);
		$('#pac-input-lng').val(mapcenter.lng);
	}
	
	if (inputLatitude != null && inputLongitude != null
		&& inputLatitude != 'undefined'  &&	inputLongitude != 'undefined'
		&& inputLatitude != '' && inputLongitude != ''){
		
		mapcenter=center;
		
		newdevice.setLatitude(mapcenter.lat);
		newdevice.setLongitude(mapcenter.lng);
		
		
		if (typeof center !== 'undefined' && center != null ){
			
			var map = initMap(document.getElementById('map'), mapcenter, 14); 
			marker = L.marker([mapcenter.lat, mapcenter.lng]).addTo(map);
			
		}else{
			
			var map = initMap(document.getElementById('map'), mapcenter, zoomFromFields); 
			
		}
	}
	
	map.on('click', function (e) {
		
		if (typeof marker !== 'undefined' && marker != null ){
		    map.removeLayer(marker); //remove previous marker
		}
		marker = L.marker(e.latlng).addTo(map);	  
	
		$('#pac-input-lat').val(marker._latlng.lat);
		$('#pac-input-lng').val(marker._latlng.lng);
		newdevice.setLatitude(marker._latlng.lat);
		newdevice.setLongitude(marker._latlng.lng);

	});
	
	map.addControl( new L.Control.Search({
		url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
		jsonpParam: 'json_callback',
		propertyName: 'display_name',
		propertyLoc: ['lat','lon'],
		marker: L.circle([51.508, -0.11], {radius:0, opacity: 0}),
		autoCollapse: true,
		autoType: false,
		minLength: 2,
		zoom: 10
	}) );
}

//manage the Toast for user notification
function manageSuccessMessage (afterGetDevices){
	
	var msg = get("msg");
	
	if (msg == undefined)
		return;
	
	
	var devreg_success = get("success");
	var service = get('scope');
	var servicepathName = get('urbanserviceName');
	var categories = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
	
	
	//main, message different than Device published
	if(!afterGetDevices && devreg_success  && !msg.includes("Device published") ){
		
		setTimeout(function(){
			M.toast({html:msg, classes:'green'}); 
		}, 1000);
		removeURLParameters(['msg', 'success']);
		
		return;
	}
	
	
	var contexts = JSON.parse(sessionStorage.getItem("myDropdownSelContext"));
	var categories = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
	
	var msgUpdate = $('#msg_device_published').text()+ '.<br>' +
	  '<p id="updateDeviceFilters" style="text-decoration:underline;">' + 
	  $('#msg_update_filters_to_see_it').text()+ '</p>'; 
	
	
	
	//main, message Device published and empty filters
	if(!afterGetDevices && devreg_success  && msg.includes("Device published") && (contexts  ==null ||  categories==null ) ){
		
		setTimeout(function(){
			M.toast({html:msgUpdate, classes:'green'}); 
		}, 1000);
		removeURLParameters(['msg', 'success']);
		
		return;
	}
	
	//getDevicesMulticontext, message Device published and 
	if(afterGetDevices && devreg_success  && msg.includes("Device published")){
		
		
		if(	 service!==null && service!==undefined	&& servicepathName!=null && servicepathName!==undefined
				
			&&	(!contexts.includes(service) || !categories.includes(servicepathName)) //in case of different filters
				
		){
			setTimeout(function(){
				M.toast({html:msgUpdate, classes:'green'}); 
			}, 1000);
			removeURLParameters(['msg', 'success']);
			
			return;
			
		}else{//in case of equal filters
			setTimeout(function(){
				M.toast({html:msg, classes:'green'}); 
			}, 1000);
			removeURLParameters(['msg', 'success']);
			
			return;
		}	
		
		
	}	
}


var currentLanguage;

function mainOnDocReady(){
	
	$(".activeLang").text(currentLanguage);
	
	$('.languagemenu').each(function(){
		var self = $(this);
		self.find('a').each(function(){
			//$(this).attr('href', $(this).attr('href')+"&scope="+get('scope')+"&urbanservice="+get('urbanservice'));
			$(this).attr('href', $(this).attr('href'));
		});
	});
	
	$("#"+currentLanguage).closest('a').attr('href', '#!');
	$("#"+currentLanguage).removeClass('hide');
	
	var devreg_success = get("success");
	var msg = get("msg");
	
	//var categories = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
	
	
	manageSuccessMessage(false);
	
	
	
	

	$('select.materialselect').formSelect();
	$(".sidenav").sidenav();
	$(".dropdown-trigger").each(function() {
//				var constrainWidth_p = true;
//				constrainWidth_p = constrainWidth_p && $(this).attr('constrainWidth');
				$(this).dropdown({
					hover : false,
					constrainWidth : false,
					alignment : 'right'
				});
			});
};









$(document).on('click', '#mobilesearch_trigger', function() {
	var searchbar = $('#mobilesearch');
	searchbar.is(":visible") ? searchbar.slideUp() : searchbar.slideDown();
	return false;
});

//SSH 
var session_token = "token";
var session_refreshToken = "refresh_token";
function SetLocalStorage (evt) {
   if (evt.data=="RemoveSesionCookie"){
      localStorage.removeItem(session_token);
      localStorage.removeItem(session_refreshToken);
   }
}

if (window.addEventListener) {
// For standards-compliant web browsers
  window.addEventListener("message", SetLocalStorage, false);
}
else {
  window.attachEvent("onmessage", SetLocalStorage);
}

$(document).ready(function() {
    $('select').formSelect();    
});

$(document).on('input change', '#search', function(){
	var input = $(this).val().trim();
	var close_icon = $(this).closest('form').find('.closeicon');
	
	if(input.length>0){ close_icon.removeClass('hide'); }
	else{ close_icon.addClass('hide'); }
});

$(document).on('change', '#search', function(){
	var trimmed = $(this).val().trim();
	$(this).val(trimmed);
});


$(document).on('click', '.closeicon', function(){
	$(this).closest('form').find('#search').val("").change();
});

