/**
 * 
 */
/*

			"lorawan": {
				"application_server": {
					"host": "eu.thethings.network",
					"username": "j1obdxm9gxwlc0sgg",
            		"password": "ttn-account-v2.pwd",
					"provider": "TTN"
				},
				"app_eui": "70B3D57ED001B837",
				"application_id": "j1obdxm9gxwlc0sgg",
				"application_key": "1A33DA63FB6394816CF56AD619133AB3",
				"data_model": "cayennelpp",
				"dev_eui": "17446f057c1054a1"
			}
		}
*/

function Lorawan(app_eui, application_id, application_key, data_model, dev_eui, application_server ){
	
	this.setApp_eui(app_eui);
	this.setApplication_id(application_id);
	this.setApplication_key(application_key);
	this.setData_model(data_model);
	this.setDev_eui(dev_eui);
	this.setApplication_server(application_server);
	
	
	
}

Lorawan.create = function(e){
	var lorawanAttr = new Lorawan();
	lorawanAttr.setApp_eui(e.app_eui);
	lorawanAttr.setApplication_id(e.application_id);
	lorawanAttr.setApplication_key(e.application_key);
	lorawanAttr.setData_model(e.data_model);
	lorawanAttr.setDev_eui(e.dev_eui);
	lorawanAttr.setApplication_server(e.application_server);
	
	return lorawanAttr;
}



Lorawan.prototype.getApp_eui = function(){
	return this.app_eui;
}

Lorawan.prototype.setApp_eui = function(app_eui){
	if(typeof app_eui === 'undefined')
		app_eui = "";
	
	this.app_eui = app_eui;
}


Lorawan.prototype.setApplication_id = function(application_id){
	if(typeof application_id === 'undefined')
		application_id = "";
	
	this.application_id = application_id;
}
Lorawan.prototype.getApplication_id = function(){
	return this.application_id;
}

Lorawan.prototype.setApplication_key = function(application_key){
	if(typeof application_key === 'undefined')
		application_key = "";
	
	this.application_key = application_key;
}

Lorawan.prototype.getApplication_key = function(){
	return this.application_key;
}

Lorawan.prototype.setData_model = function(data_model){
	if(typeof data_model === 'undefined')
		data_model = "";
	
	this.data_model = data_model;
}

Lorawan.prototype.getData_model = function(){
	return this.data_model;
}

Lorawan.prototype.setDev_eui = function(dev_eui){
	if(typeof dev_eui === 'undefined')
		dev_eui = "";
	
	this.dev_eui = dev_eui;
}

Lorawan.prototype.getDev_eui = function(){
	return this.dev_eui;
}


Lorawan.prototype.setApplication_server = function(application_server){
	if(typeof application_server === 'undefined')
		application_server = {};
	
	this.application_server = application_server;
}

Lorawan.prototype.getApplication_server = function(){
	return this.application_server;
}




