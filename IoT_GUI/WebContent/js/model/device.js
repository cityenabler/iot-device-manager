function Device (id, category, lat, lng, organization, device_owner, screenId, mobile_device, transport, protocol, endpoint, auth_method, credential, enabler, runningSimulation, streamingURL, loraDeviceProfileId, historicizing ) {
    this.setId(id);
    this.setName(screenId);
    this.setCategory(category);
	this.setLatitude(lat);
    this.setLongitude(lng);
    this.setOrganization(organization);
    this.setDeviceOwner(device_owner);
    this.setScreenId(screenId);
    this.setMobileDevice(mobile_device);
    
    this.attributes = new Object();
    this.mashups = new Object();
    
	this.setTransport(transport);
	this.setDataformatProtocol(protocol);
	
	this.setEndpoint(endpoint);
	
	this.setAuthMethod(auth_method);
	this.setCredential(credential);

	this.setEnabler(enabler);
	this.setRunningSimulation(runningSimulation);
	this.setStreamingURL(streamingURL);
	this.setLoraDeviceProfileId(loraDeviceProfileId);
	this.setHistoricizing(historicizing);
	
	this.commands = new Object();
}

Device.create = function(object){
	var retDev = new Device();
	$.each(object, function(i, e){
		if('attributes' == i){
			$.each(e, function(n, x){
				var curr = DeviceAttribute.create(x);
				retDev.addAttribute(curr);
			});
		}else if('mashups' == i){
			$.each(e, function(n, x){
				var curr = DeviceMashup.create(x);
				retDev.addMashup(curr);
			});
		}else if('category' == i){ 
			retDev.setCategory(Category.create(e)); 
		}else if('commands' == i){
			$.each(e, function(n, x){
				var curr = DeviceCommand.create(x);
				retDev.addCommand(curr);
			});
		}else if('credentials' == i){
			$.each(e, function(n, x){
				var curr = DeviceCredential.create(x);
				retDev.addCredential(curr);
			});
		}
		else{ retDev[i] = e; }
	});
	
	return retDev;
}

Device.prototype.setId = function(id) {
	if(typeof id === 'undefined')
		id = "";
	
	 this.id = id;
};

Device.prototype.getId = function() {
	 return this.id;
};

Device.prototype.setName = function(name) {
	if(typeof name === 'undefined')
		name = "";
	
	 this.name = name;
};

Device.prototype.getName = function() {
	 return this.name;
};

Device.prototype.setCategory = function(category) {
	if(typeof category === 'undefined')
		category = "";
	
	 this.category = category;
};

Device.prototype.getCategory = function() {
	 return this.category;
};

Device.prototype.setLatitude = function(lat) {
	if(typeof lat === 'undefined')
		lat = 0.0;
	
	this.latitude = lat;
};

Device.prototype.getLatitude = function() {
	 return this.latitude;
};

Device.prototype.setLongitude = function(lng) {
	if(typeof lng === 'undefined')
		lng = 0.0;
	
	this.longitude = lng;
};

Device.prototype.getLongitude = function() {
	 return this.longitude;
};

Device.prototype.getEnabler = function() {
	 return this.enabler;
};

Device.prototype.setEnabler = function() {
	if(typeof enabler === 'undefined')
		enabler = "";
	
	 this.enabler = enabler;
};




Device.prototype.setPosition = function(position){
	this.setLatitude(position.lat());
	this.setLongitude(position.lng());
}

Device.prototype.getAttributes = function() {
	return this.attributes;
};

Device.prototype.addAttribute = function(attr) {
    if(!(attr instanceof DeviceAttribute)){
    	throw new Error("[Type mismatch] attr: "+attr+" is not a DeviceAttribute");
    }
    
	if(typeof this.attributes === 'undefined'){
    	this.attributes = new Object();
    }
	
    this.attributes[attr.getName()] = attr.getType();
};


Device.prototype.getMashups = function() {
	return this.mashups;
};

Device.prototype.addMashup = function(mash) {
    if(!(mash instanceof DeviceMashup)){
    	throw new Error("[Type mismatch] mash: "+mash+" is not a DeviceMashup");
    }
    
	if(typeof this.mashups === 'undefined'){
    	this.mashups = new Object();
    }
	
    this.mashups[mash.getId()] = mash;
};

Device.prototype.getCommands = function() {
	return this.commands;
};

Device.prototype.addCommand = function(command) {
    if(!(command instanceof DeviceCommand)){
    	throw new Error("[Type mismatch] command: "+command+" is not a DeviceCommand");
    }
    
	if(typeof this.commands === 'undefined'){
    	this.commands = new Object();
    }
	
    this.commands[command.getName()] = command;
};


Device.prototype.setOrganization = function(organization) {
	if(typeof organization === 'undefined')
		organization = "";
	
	 this.organization = organization;
};

Device.prototype.getOrganization = function() {
	 return this.organization;
};


Device.prototype.setDeviceOwner = function(device_owner) {
	if(typeof device_owner === 'undefined')
		device_owner = "";
	
	 this.device_owner = device_owner;
};

Device.prototype.getDeviceOwner = function() {
	 return this.device_owner;
};


Device.prototype.setScreenId = function(screenId) {
	if(typeof screenId === 'undefined')
		screenId = "";
	
	 this.screenId = screenId;
};

Device.prototype.getScreenId = function() {
	 return this.screenId;
};


Device.prototype.setMobileDevice = function(mobile_device) {
	if(typeof mobile_device === 'undefined')
		mobile_device = "";
	
	 this.mobile_device = mobile_device;
};

Device.prototype.getMobileDevice = function() {
	 return this.mobile_device;
};

//
Device.prototype.setTransport = function(transport) {
	if(typeof this.transport === 'undefined')
		this.transport = "";
	
	 this.transport = transport;
};

Device.prototype.getTransport = function() {
	 return this.transport;
};

Device.prototype.setDataformatProtocol = function(protocol) {
	if(typeof this.protocol === 'undefined')
		this.protocol = "";
	
	 this.protocol = protocol;
};

Device.prototype.getDataformatProtocol = function() {
	 return this.protocol;
};

Device.prototype.setEndpoint = function(endpoint) {
	if(typeof this.endpoint === 'undefined')
		this.endpoint = "";
	
	 this.endpoint = endpoint;
};

Device.prototype.getEndpoint = function() {
	 return this.endpoint;
};


Device.prototype.setAuthMethod = function(auth_method) {
	if(typeof this.auth_method === 'undefined')
		this.auth_method = "";
	
	 this.auth_method = auth_method;
};

Device.prototype.getAuthMethod = function() {
	 return this.auth_method;
};

Device.prototype.setCredential = function(credential) {
	if(typeof this.credential === 'undefined')
		this.credential = "";
	
	 this.credential = credential;
};

Device.prototype.getCredential = function() {
	 return this.credential;
};


Device.prototype.addCredential = function(credential) {
    if(!(credential instanceof DeviceCredential)){
    	throw new Error("[Type mismatch] credential: "+credential+" is not a DeviceCredential");
    }
    
	if(typeof this.credentials === 'undefined'){
    	this.credentials = new Object();
    }
	
    this.credentials[credential] = credential;
};


Device.prototype.setStreamingURL = function(endpoint) {
	if(typeof this.streamingURL === 'undefined')
		this.streamingURL = "";
	
	 this.streamingURL = endpoint;
};

Device.prototype.getStreamingURL = function() {
	 return this.streamingURL;
};

Device.prototype.getRunningsimulation = function() {
	 return this.runningSimulation;
};

Device.prototype.setRunningSimulation = function() {
	if(typeof runningSimulation === 'undefined')
		runningSimulation = "";
	
	 this.runningSimulation = runningSimulation;
};


Device.prototype.getLoraDeviceProfileId = function() {
	 return this.loraDeviceProfileId;
};

Device.prototype.setLoraDeviceProfileId = function() {
	if(typeof loraDeviceProfileId === 'undefined')
		loraDeviceProfileId = "";
	
	 this.loraDeviceProfileId = loraDeviceProfileId;
};

Device.prototype.getHistoricizing = function() {
	 return this.historicizing;
};

Device.prototype.setHistoricizing = function() {
	if(typeof historicizing === 'undefined')
		historicizing = "";
	
	 this.historicizing = historicizing;
};


