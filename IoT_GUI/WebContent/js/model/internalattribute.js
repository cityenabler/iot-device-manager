/*
    "internal_attributes": {
			"lorawan": {
				"application_server": {
					"host": "eu.thethings.network",
					"username": "j1obdxm9gxwlc0sgg",
            		"password": "ttn-account-v2.pwd",
					"provider": "TTN"
				},
				"app_eui": "70B3D57ED001B837",
				"application_id": "j1obdxm9gxwlc0sgg",
				"application_key": "1A33DA63FB6394816CF56AD619133AB3",
				"data_model": "cayennelpp",
				"dev_eui": "17446f057c1054a1"
			}
		}
*/

function InternalAttribute(lorawan){
	
	this.setLorawan(lorawan);
}

InternalAttribute.create = function(e){
	var intAttribute = new InternalAttribute();
	intAttribute.setLorawan(e.lorawan);
	
	return intAttribute;
}



InternalAttribute.prototype.getLorawan = function(){
	return this.lorawan;
}

InternalAttribute.prototype.setLorawan = function(lorawan){
	if(typeof lorawan === 'undefined')
		lorawan = "";
	
	this.lorawan = lorawan;
}
