/**
 * 
 */

function DeviceCredential(username, password, apikey){
	this.setUsername(username);
	this.setPassword(password);
	this.setApikey(apikey);
	
	
}

DeviceCredential.create = function(e){
	var retCredential = new DeviceCredential();
	retCredential.setUsername(e.username);
	retCredential.setPassword(e.password);
	retCredential.setApikey(e.apikey);
	
	return retCredential;
}


DeviceCredential.prototype.getUsername = function(){
	return this.username;
}

DeviceCredential.prototype.setUsername = function(username){
	if(typeof username === 'undefined')
		username = "";
	
	this.username = username;
}


DeviceCredential.prototype.getPassword = function(){
	return this.password;
}

DeviceCredential.prototype.setPassword = function(password){
	if(typeof password === 'undefined')
		password = "";
	this.password = password.toLowerCase();
}

DeviceCredential.prototype.getApikey = function(){
	return this.apikey;
}

DeviceCredential.prototype.setApikey = function(apikey){
	if(typeof apikey === 'undefined')
		apikey = "";
	this.apikey = apikey.toLowerCase();
}
