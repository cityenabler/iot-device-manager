function NewDevice (id, topcategory, category, lat, lng, organization, device_owner, screenId, mobile_device, transport, protocol, endpoint, loraDeviceProfileId, historicizing) {
	this.setDevice_id(id);
	this.setEntity_name(id);
	this.setEntity_type("Device");
	this.setStatic_attributes(new Array());
	this.setMobileDevice(mobile_device);
	this.setPosition(lat, lng);
	this.setAttributes(new Array());
	this.setMashups(new Array());
	this.setOrganization(organization);
	this.setDeviceOwner(device_owner);
	this.setScreenId(screenId);
	this.setTransport(transport);
	this.setDataformatProtocol(protocol);
	this.setEndpoint(endpoint);
	this.setCommands(new Array());
	this.setLoraDeviceProfileId(loraDeviceProfileId);
	this.setCredentials(new Array());
	
}


NewDevice.prototype.setDevice_id = function(id) {
	if(typeof id === 'undefined')
		id = "";
	
	 this.device_id = id;
};

NewDevice.prototype.getDevice_id = function() {
	 return this.device_id;
};

NewDevice.prototype.setEntity_name = function(name) {
	if(typeof name === 'undefined')
		this.entity_name = "";	
	    
	this.entity_name = name;
};

NewDevice.prototype.getEntity_name = function() {
	 return this.entity_name;
};

NewDevice.prototype.setEntity_type = function(category) {
	if(typeof category === 'undefined')
		category = "";
	
	 this.entity_type = category;
};

NewDevice.prototype.getEntity_type = function() {
	 return this.entity_type;
};

NewDevice.prototype.setStatic_attributes = function(arr) {
	if(typeof arr === 'undefined')
		arr = new Array();
	
	this.static_attributes = arr;
};

NewDevice.prototype.getStatic_attributes = function() {
	 return this.static_attributes;
};


NewDevice.prototype.setMashups = function(arr) {
	if(typeof arr === 'undefined')
		arr = new Array();
	
	this.static_mashups = arr;
};

NewDevice.prototype.getMashups = function() {
	 return this.static_mashups;
};



NewDevice.prototype.setPosition = function(lat, lng){
	
	var loc = new Object();
	    loc['name'] = "location";
	    loc['type'] = "geo:point";
	    loc['value'] = lat +","+lng;
	// Attention here
	    if ((lat!=0 && lng!=0) && this.mobile_device!="true"){
	    	this.addStaticAttribute(loc);
	    }
    
};

/*Attributes*/
NewDevice.prototype.setAttributes = function(attrs) {
	this.attributes = attrs;
};

NewDevice.prototype.getAttributes = function() {
	return this.attributes;
};

NewDevice.prototype.addAttribute = function(attr) {
	if(typeof this.attributes === 'undefined'){
    	this.attributes = new Array();
    }
	
    this.attributes.push(attr);
};

NewDevice.prototype.addStaticAttribute = function(attr) {
	if(typeof this.static_attributes === 'undefined'){
    	this.static_attributes = new Array();
    }
	
    this.static_attributes.push(attr);
};


NewDevice.prototype.getInternal_attributes = function() {
	return this.internal_attributes;
};


NewDevice.prototype.setInternal_attributes = function(iAttrs) {
	if(typeof iAttrs === 'undefined')
		iAttrs = new InternalAttribute();
	
	this.internal_attributes = iAttrs;
};


/*Mashups*/
NewDevice.prototype.setMashups = function(mashs) {
	this.mashups = mashs;
};

NewDevice.prototype.getMashups = function() {
	return this.mashups;
};

NewDevice.prototype.addMashup = function(mashs) {
	if(typeof this.mashups === 'undefined'){
    	this.mashups = new Array();
    }
	
    this.mashups.push(mashs);
};

/* anna */
NewDevice.prototype.addStaticMashup = function(mashs) {
	if(typeof this.static_mashups === 'undefined'){
    	this.static_mashups = new Array();
    }
	
    this.static_mashups.push(mashs);
};
/*fine*/

NewDevice.prototype.setOrganization = function(organization) {
	if(typeof this.organization === 'undefined')
		this.organization = "";
	
	 this.organization = organization;
};

NewDevice.prototype.getOrganization = function() {
	 return this.organization;
};

NewDevice.prototype.setDeviceOwner = function(device_owner) {
	if(typeof this.device_owner === 'undefined')
		this.device_owner = "";
	
	 this.device_owner = device_owner;
};

NewDevice.prototype.getDeviceOwner = function() {
	 return this.device_owner;
};

NewDevice.prototype.setScreenId = function(screenId) {
	if(typeof this.screenId === 'undefined')
		this.screenId = "";
	
	 this.screenId = screenId;
};

NewDevice.prototype.getScreenId = function() {
	 return this.screenId;
};


NewDevice.prototype.setMobileDevice = function(mobile_device) {
	if(typeof this.mobile_device === 'undefined')
		this.mobile_device = "";
	
	 this.mobile_device = mobile_device;
};

NewDevice.prototype.getMobileDevice = function() {
	 return this.mobile_device;
};

//
NewDevice.prototype.setTransport = function(transport) {
	if(typeof this.transport === 'undefined')
		this.transport = "";
	
	 this.transport = transport;
};

NewDevice.prototype.getTransport = function() {
	 return this.transport;
};

NewDevice.prototype.setDataformatProtocol = function(protocol) {
	if(typeof this.protocol === 'undefined')
		this.protocol = "";
	
	 this.protocol = protocol;
};

NewDevice.prototype.getDataformatProtocol = function() {
	 return this.protocol;
};

NewDevice.prototype.setEndpoint = function(endpoint) {
	if(typeof this.endpoint === 'undefined')
		this.endpoint = "";
	
	 this.endpoint = endpoint;
};

NewDevice.prototype.getEndpoint = function() {
	 return this.endpoint;
};

NewDevice.prototype.setLoraDeviceProfileId = function(loraDeviceProfileId) {
	if(typeof this.loraDeviceProfileId === 'undefined')
		this.loraDeviceProfileId = "";
	
	 this.loraDeviceProfileId = loraDeviceProfileId;
};

NewDevice.prototype.getLoraDeviceProfileId = function() {
	 return this.loraDeviceProfileId;
};


//
NewDevice.prototype.setCommands = function(arr) {
	if(typeof arr === 'undefined')
		arr = new Array();
	
	this.commands = arr;
};

NewDevice.prototype.getCommands = function() {
	 return this.commands;
};

NewDevice.prototype.addCommand = function(command) {
	if(typeof this.commands === 'undefined'){
    	this.commands = new Array();
    }
	
    this.commands.push(command);
};


// Credentials
NewDevice.prototype.setCredentials = function(arr) {
	if(typeof arr === 'undefined')
		arr = new Array();
	
	this.credentials = arr;
};

NewDevice.prototype.getCredentials = function() {
	 return this.credentials;
};

NewDevice.prototype.addCredential = function(credential) {
	if(typeof this.credentials === 'undefined'){
    	this.credentials = new Array();
    }
	
    this.credentials.push(credential);
};
