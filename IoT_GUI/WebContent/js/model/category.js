function Category (id, name) {
    this.setId(id);
    this.setName(name);
    this.setParentCategory();
    this.setParentCategoryId();
}

Category.create = function(e){
	var retCat = new Category(e.id, e.name);
	retCat.setParentCategory(e.parentCategory);
	retCat.setParentCategoryId(e.parentCategoryId);
	return retCat;
}

Category.prototype.setId = function(id) {
	if(typeof id === 'undefined')
		id = "";
	
	 this.id = id;
};

Category.prototype.getId = function() {
	 return this.id;
};

Category.prototype.setName = function(name) {
	if(typeof name === 'undefined')
		name = "";
	
	 this.name = name;
};

Category.prototype.setParentCategory = function(parentCategory) {
	if(typeof parentCategory === 'undefined')
		parentCategory = "";
	
	 this.parentCategory = parentCategory;
};

Category.prototype.setParentCategoryId = function(parentCategoryId) {
	if(typeof parentCategoryId === 'undefined')
		parentCategoryId = "";
	
	 this.parentCategoryId = parentCategoryId;
};

Category.prototype.getId = function() {
	 return this.id;
};

Category.prototype.getName = function() {
	 return this.name;
};

Category.prototype.getParentCategory = function() {
	 return this.parentCategory;
};

Category.prototype.getParentCategoryId = function() {
	 return this.parentCategoryId;
};

