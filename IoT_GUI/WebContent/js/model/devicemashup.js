/**
 * 
 */

function DeviceMashup(id, name, description, type, status,subscriptionId ){
	this.setId(id);
	this.setName(name);
	this.setDescription(description);
	this.setType(type);
	this.setStatus(status);
	this.setSubscriptionId(subscriptionId);
}

DeviceMashup.create = function(e){
	var retMashup = new DeviceMashup();
	retMashup.setId(e.id);
	retMashup.setName(e.name);
	retMashup.setDescription(e.description);
	retMashup.setType(e.type);
	retMashup.setStatus(e.status);
	retMashup.setSubscriptionId(e.subscriptionId);
	
	return retMashup;
}

DeviceMashup.prototype.getId = function(){
	return this.id;
}
DeviceMashup.prototype.setId = function(id){
	if(typeof id === 'undefined')
		id = "";
	this.id = id;
}

DeviceMashup.prototype.getName = function(){
	return this.name;
}

DeviceMashup.prototype.setName = function(name){
	if(typeof name === 'undefined')
		name = "";
	
	this.name = name;
}

DeviceMashup.prototype.getDescription = function(){
	return this.description;
}
DeviceMashup.prototype.setDescription = function(description){
	if(typeof description === 'undefined')
		description = "";
	this.description = description;
}

DeviceMashup.prototype.getType = function(){
	return this.type;
}

DeviceMashup.prototype.setType = function(type){
	if(typeof type === 'undefined')
		type = "";
	this.type = type.toLowerCase();
}


DeviceMashup.prototype.getStatus = function(){
	return this.status;
}

DeviceMashup.prototype.setStatus = function(status){
	if(typeof status === 'undefined')
		status = "";
	this.status = status;
}

DeviceMashup.prototype.getSubscriptionId = function(){
	return this.subscriptionId;
}

DeviceMashup.prototype.setSubscriptionId = function(subscriptionId){
	if(typeof subscriptionId === 'undefined')
		subscriptionId = "";
	this.subscriptionId = subscriptionId;
}