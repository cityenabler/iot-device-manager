function NewCamera (id, topcategory, category, lat, lng, organization, device_owner, screenId, endpoint) {
	this.setDevice_id(id);
	this.setEntity_name(id);
	this.setEntity_type("Device");
	this.setStatic_attributes(new Array());
	this.setMobileDevice(false);
	this.setPosition(lat, lng);
	this.setAttributes(new Array());
	this.setOrganization(organization);
	this.setDeviceOwner(device_owner);
	this.setScreenId(screenId);
	
	this.setEndpoint(endpoint);	
}


NewCamera.prototype.setDevice_id = function(id) {
	if(typeof id === 'undefined')
		id = "";
	
	 this.device_id = id;
};

NewCamera.prototype.getDevice_id = function() {
	 return this.device_id;
};

NewCamera.prototype.setEntity_name = function(name) {
	if(typeof name === 'undefined')	
	 this.entity_name = "";
	 this.entity_name = name;
};

NewCamera.prototype.getEntity_name = function() {
	 return this.entity_name;
};

NewCamera.prototype.setEntity_type = function(category) {
	if(typeof category === 'undefined')
		category = "";
	
	 this.entity_type = category;
};

NewCamera.prototype.getEntity_type = function() {
	 return this.entity_type;
};

NewCamera.prototype.setStatic_attributes = function(arr) {
	if(typeof arr === 'undefined')
		arr = new Array();
	
	this.static_attributes = arr;
};

NewCamera.prototype.getStatic_attributes = function() {
	 return this.static_attributes;
};

NewCamera.prototype.setPosition = function(lat, lng){
	
	var loc = new Object();
	    loc['name'] = "location";
	    loc['type'] = "geo:point";
	    loc['value'] = lat +","+lng;
	// Attention here
	    if ((lat!=0 && lng!=0) && this.mobile_device!="true"){
	    	this.addStaticAttribute(loc);
	    }
    
};

/*Attributes*/
NewCamera.prototype.setAttributes = function(attrs) {
	this.attributes = attrs;
};

NewCamera.prototype.getAttributes = function() {
	return this.attributes;
};

NewCamera.prototype.addStaticAttribute = function(attr) {
	if(typeof this.static_attributes === 'undefined'){
    	this.static_attributes = new Array();
    }
	
    this.static_attributes.push(attr);
};


NewCamera.prototype.setOrganization = function(organization) {
	if(typeof this.organization === 'undefined')
		this.organization = "";
	
	 this.organization = organization;
};

NewCamera.prototype.getOrganization = function() {
	 return this.organization;
};

NewCamera.prototype.setDeviceOwner = function(device_owner) {
	if(typeof this.device_owner === 'undefined')
		this.device_owner = "";
	
	 this.device_owner = device_owner;
};

NewCamera.prototype.getDeviceOwner = function() {
	 return this.device_owner;
};

NewCamera.prototype.setScreenId = function(screenId) {
	if(typeof this.screenId === 'undefined')
		this.screenId = "";
	
	 this.screenId = screenId;
};

NewCamera.prototype.getScreenId = function() {
	 return this.screenId;
};

NewCamera.prototype.setMobileDevice = function(mobile) {
	 return this.mobile_device = mobile;
};

NewCamera.prototype.getMobileDevice = function() {
	 return this.mobile_device;
};

NewCamera.prototype.setEndpoint = function(endpoint) {
	if(typeof this.endpoint === 'undefined')
		this.endpoint = "";
	
	 this.endpoint = endpoint;
};

NewCamera.prototype.getEndpoint = function() {
	 return this.endpoint;
};