function Camera (id, category, lat, lng, organization, device_owner, screenId, streamingURL) {
    this.setId(id);
    this.setName(screenId);
    this.setCategory(category);
	this.setLatitude(lat);
    this.setLongitude(lng);
    this.setOrganization(organization);
    this.setDeviceOwner(device_owner);
    this.setScreenId(screenId);
    this.setMobileDevice(false);
    
    this.setStreamingURL(streamingURL);
}

Camera.create = function(object){
	var retDev = new Device();
	$.each(object, function(i, e){
		if('category' == i){ 
			retDev.setCategory(Category.create(e)); 
		}
		else{ retDev[i] = e; }
	});
	
	return retDev;
}

Camera.prototype.setId = function(id) {
	if(typeof id === 'undefined')
		id = "";
	
	 this.id = id;
};

Camera.prototype.getId = function() {
	 return this.id;
};

Camera.prototype.setName = function(name) {
	if(typeof name === 'undefined')
		name = "";
	
	 this.name = name;
};

Camera.prototype.getName = function() {
	 return this.name;
};

Camera.prototype.setCategory = function(category) {
	if(typeof category === 'undefined')
		category = "";
	
	 this.category = category;
};

Camera.prototype.getCategory = function() {
	 return this.category;
};

Camera.prototype.setLatitude = function(lat) {
	if(typeof lat === 'undefined')
		lat = 0.0;
	
	this.latitude = lat;
};

Camera.prototype.getLatitude = function() {
	 return this.latitude;
};

Camera.prototype.setLongitude = function(lng) {
	if(typeof lng === 'undefined')
		lng = 0.0;
	
	this.longitude = lng;
};

Camera.prototype.getLongitude = function() {
	 return this.longitude;
};

Camera.prototype.setPosition = function(position){
	this.setLatitude(position.lat());
	this.setLongitude(position.lng());
}

Camera.prototype.setOrganization = function(organization) {
	if(typeof organization === 'undefined')
		organization = "";
	
	 this.organization = organization;
};

Camera.prototype.getOrganization = function() {
	 return this.organization;
};


Camera.prototype.setDeviceOwner = function(device_owner) {
	if(typeof device_owner === 'undefined')
		device_owner = "";
	
	 this.device_owner = device_owner;
};

Camera.prototype.getDeviceOwner = function() {
	 return this.device_owner;
};


Camera.prototype.setScreenId = function(screenId) {
	if(typeof screenId === 'undefined')
		screenId = "";
	
	 this.screenId = screenId;
};

Camera.prototype.getScreenId = function() {
	 return this.screenId;
};

Camera.prototype.setMobileDevice = function(mobile) {
	 return this.mobile_device = mobile;
};

Camera.prototype.getMobileDevice = function() {
	 return this.mobile_device;
};


Camera.prototype.setStreamingURL = function(endpoint) {
	if(typeof this.streamingURL === 'undefined')
		this.streamingURL = "";
	
	 this.streamingURL = endpoint;
};

Camera.prototype.getStreamingURL = function() {
	 return this.streamingURL;
};
