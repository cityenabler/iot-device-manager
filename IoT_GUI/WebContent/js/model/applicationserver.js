/*
		{
			"host": "eu.thethings.network",
			"username": "j1obdxm9gxwlc0sgg",
        	"password": "ttn-account-v2.pwd",
			"provider": "TTN"
		}
		
*/

function Application_server(host, username, password, provider ){
	
	this.setHost(host);
	this.setUsername(username);
	this.setPassword(password);
	this.setProvider(provider);
}

Application_server.create = function(e){
	var appServAttr = new Application_server();
	appServAttr.setHost(e.host);
	appServAttr.setUsername(e.username);
	appServAttr.setPassword(e.password);
	appServAttr.setProvider(e.provider);
	
	return appServAttr;
}



Application_server.prototype.getHost = function(){
	return this.host;
}

Application_server.prototype.setHost = function(host){
	if(typeof host === 'undefined')
		host = "";
	
	this.host = host;
}


Application_server.prototype.getUsername = function(){
	return this.username;
}

Application_server.prototype.setUsername = function(username){
	if(typeof username === 'undefined')
		username = "";
	
	this.username = username;
}


Application_server.prototype.getPassword = function(){
	return this.password;
}

Application_server.prototype.setPassword = function(password){
	if(typeof password === 'undefined')
		password = "";
	this.password = password;
}

Application_server.prototype.setProvider = function(provider){
	if(typeof provider === 'undefined')
		provider = "";
	
	this.provider = provider;
}

Application_server.prototype.getProvider = function(){
	return this.provider;
}


