function DeviceAttribute(name, type, jsonschema){
	this.setName(name);
	this.type = type;
	this.jsonschema = jsonschema;
}

DeviceAttribute.create = function(e){
	var retAttr = new DeviceAttribute();
		retAttr.setName(e.name);
		retAttr.setType(e.type);
		retAttr.setJsonschema(e.jsonschema);
	
	return retAttr;
}

DeviceAttribute.prototype.getName = function(){
	return this.name;
}

DeviceAttribute.prototype.setName = function(name){
	if(typeof name === 'undefined')
		name = "";
	
	this.name = name;
}

DeviceAttribute.prototype.getType = function(){
	return this.type;
}

DeviceAttribute.prototype.setType = function(type){
	if(typeof type === 'undefined')
		type = "";
	this.type = type;
}

DeviceAttribute.prototype.getJsonschema = function(){
	return this.jsonschema;
}

DeviceAttribute.prototype.setJsonschema = function(jsonschema){
	if(typeof jsonschema === 'undefined')
		jsonschema = "";
	this.jsonschema = jsonschema;
}