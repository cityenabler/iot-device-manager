/**
 * 
 */

function DeviceCommand(name, type){
	this.setName(name);
	this.setType(type);
}

DeviceCommand.create = function(e){
	var retCommand = new DeviceCommand();
	retCommand.setName(e.name);
	retCommand.setType(e.type);
	
	return retCommand;
}


DeviceCommand.prototype.getName = function(){
	return this.name;
}

DeviceCommand.prototype.setName = function(name){
	if(typeof name === 'undefined')
		name = "";
	
	this.name = name;
}


DeviceCommand.prototype.getType = function(){
	return this.type;
}

DeviceCommand.prototype.setType = function(type){
	if(typeof type === 'undefined')
		type = "";
	this.type = type.toLowerCase();
}

