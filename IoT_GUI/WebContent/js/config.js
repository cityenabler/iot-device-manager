/*
 * IoT Device Manager
 * Configuration file
 */

/* CONFIGURE IN CASE THE APPLICATION IS DEPLOYED ON THE SERVER PROD or TEST ENV */
//var productionEnv = false;

/* Configure landing */
//PROD ENV 			
//var redirect_uri = "http://cedus.eu/landing";
//TEST ENV 			var redirect_uri = "http://217.172.12.224/landing";
//LOCALHOST ENV		var redirect_uri = "http://217.172.12.224/landing";

/* Configure origin_page */
//PROD ENV			
//var origin_page = "http://localhost:8080/dema/login";
//TEST ENV 			var origin_page = "http://217.172.12.224/dema/login";
//LOCALHOST ENV		var origin_page = "http://localhost:8080/IoT_GUI_new/login";

/* Configure contextRoot */
//var contextRoot = "/dema";

/* Configure client_id */
//const client_id = "ee93798c13844cd684376ffaa800181d";

/** IoT Simulator URL */
//const iotsimulatorurl = "http://localhost:8080/iotsim";
/* **************************************** */
/* IDM Configuration 						*/
/* **************************************** */
//var auth_logout = "/logout";

/* Configure idmURL */
// PROD ENV			
//var idmURL = "http://idm.cedus.eu";
//var idmProfileURL = "/";
// TEST ENV			var idmURL = "http://217.172.12.224:8000";
// LOCALHOST ENV 	var idmURL = "http://217.172.12.224:8000";


/* **************************************** */
/* Context Broker Configuration 			*/
/* *************************************** */
/* PROD ENV */
//var cbHost = "http://217.172.12.177:1026";
//var cbHostRemote = "http://217.172.12.177:1026";
//var cbNotifyHost = "http://192.168.150.29:5050/notify";
/* TEST ENV */
/*
var cbHost = "http://192.168.111.204:1026";
var cbHostRemote = "http://192.168.111.204:1026";
var cbNotifyHost = "http://192.168.111.204:5050/notify";
/* **************************************** */
/* IDAS Configuration */
/* **************************************** */

//var iotAgentHost = "http://217.172.12.177:8084";

//var iotService = "/iot/services";

/* Business configuration */
var uslist = ["Parking", "Waste", "Mobility", "Illumination", "Environment", "Tourism", "Water", "KPI", "Issues", "Manufacturing", "Energy"];

//Keep the LowerCase !
var uslistcolor = {parking:"blue", waste:"blue-grey", mobility:"purple", illumination:"amber", 
				   environment:"light-green", tourism:"orange", water:"cyan", kpi:"pink", 
				   issues:"red", taxes:"brown", controlroom:"teal", manufacturing:"lime",
				   energy:"yellow", industry:"deep-purple", building:"grey", 
				   animals:"deep-orange", harvest:"light-blue", finance:"indigo"
				   };

/* Help links*/
var helpLink = "https://github.com/telefonicaid/iotagent-node-lib/blob/2.7.0/README.md"


var data4enablers = {
		"city": {
			"ctx":["antwerp", "helsinki", "milano", "la_plata", "hamburg","iot_devices", "test", "lucidat", "name", "saferstreets", "aq4h", "athletes", "c4t", "cultoura"],
			"cats": ["environment", "mobility", "parking", "waste", "illumination", "tourism", "water", "kpi", "issues", "taxes", "controlroom","finance"]
		},
		"facility": {
			"ctx":["pontsaintmartin", "polimi", "arnaldo_castro"],
			"cats": ["manufacturing", "energy", "industry", "building", "kpi", "controlroom"]
		},
		"farm":{
			"ctx":["safra"],
			"cats": ["animals", "harvest", "kpi", "controlroom", "water" ]
		}
}
