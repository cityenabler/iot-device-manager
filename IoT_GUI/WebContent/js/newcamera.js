var newdevice = new Camera();
var mapclicklistener;
var presentmarkers = [];
var map;
var operationType = "create_camera";
var mapcenter = {};
var connectedUserOrgId;
var dataClickListener;
var marker;

$(document).ready(function(){
	
	onDocReady();
	
	$("#organizationId").attr("selected", true);
	connectedUserOrgId=$("#organizationId").val();
	// re-initialize material-select
    $('select').formSelect();
    
    $('.contextCategorySelMain').on('contentCategoriesMainChanged', function() {
		updateMap();
    });
    
});


function updateMap(){
	
	var myDropdownSelContextMain =  $('#myDropdownSelContextMain').val();
	var myDropdownSelCategoryMain =  $('#myDropdownSelCategoryMain').val();
	
	if (myDropdownSelCategoryMain != null){
	
		var fiwareService = myDropdownSelContextMain;
		var fiwareServicepath = myDropdownSelContextMain+"_"+myDropdownSelCategoryMain.toLowerCase();
	
		
		var headersAll = new Object();
		headersAll['fiware-service'] = fiwareService;
		headersAll['fiware-servicepath'] = fiwareServicepath;
	
		var input = new Object();
		input['action'] = 'getMapCenter';
	
		$.ajax({
			url:  'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {
				mapcenter = data;
				initNewDeviceMap(mapcenter);
				
			},
			error : function(xhr, status, error) {
				console.log(status);
			}
		});
	}
}


$(document).on('change', '#organizationId', function(){
	connectedUserOrgId=$(this).val();
});


function onDocReady(){
	mainOnDocReady();
	
	var scope = get('scope');
	var urbanservice = get('urbanservice');
	
	var headersAll = new Object();
	headersAll['fiware-service'] = scope;
	headersAll['fiware-servicepath'] = urbanservice;
	
	var input = new Object();
	input['action'] = 'getMapCenter';
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : headersAll,
		success : function(data) {
			mapcenter = data;
			initNewDeviceMap();
			
		
			
			
			if (typeof currentDevice !== 'undefined' 
				&& currentDevice != null 
				&& currentDevice.trim().length > 0) {
				
				
				try{
					prefillDeviceForm();
					
					if (actionmenu=="copyDevice"){
						$('#edit_device_title').addClass('hide');
						$('#new_device_title').removeClass('hide');
						$('#filterSelect').removeClass('hide'); //context_menu 
						
					}else{
						operationType = "modifyDevice";
						$('#name').attr("disabled", true); 
						$('#edit_device_title').removeClass('hide');
						$('#edit_mobile_device_title').removeClass('hide');
						$('#filterSelect').addClass('hide'); //context_menu 
						
					}
					
				}
				catch(error){
					alert($('#msg_invalid_device').text());
				}
			}
			else{ 
				$('#new_device_title').removeClass('hide'); 
				$('#new_mobile_device_title').removeClass('hide');
			}
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});
	
};

$(document).on('change', '.deviceselectfield', function(){
	newdevice[$(this).attr('name')] = $(this).val();
});

$(document).on('input', '.devicetextfield', function(){
	newdevice[$(this).attr('name')] = $(this).val();
});



// Register device
$(document).on('submit', '#newdeviceform', function(){
	
	var service;
	var path;
	if (operationType == "modifyDevice"){
		
		 service = get("scope");
		 path = get("urbanservice");
		
	}else{
		var myDropdownSelCategoryMain =  $('#myDropdownSelCategoryMain').val();
		
		//create device
		service =  $('#myDropdownSelContextMain').val();
		
		path=""
		if (typeof myDropdownSelCategoryMain !== 'undefined' && myDropdownSelCategoryMain != null	&& myDropdownSelCategoryMain.trim().length > 0)		
			path = service+"_"+myDropdownSelCategoryMain.toLowerCase();
		
		if ( typeof service == 'undefined' || service == null || service.trim().length == 0 || 
				typeof myDropdownSelCategoryMain == 'undefined' || myDropdownSelCategoryMain == null || myDropdownSelCategoryMain.trim().length == 0  ){
			
			M.toast({html:$('#msg_context_category_mandatory').text(), classes:'red'});
			return false;
			
		}
			
		
		
	}
	
	var _headers = new Object();
		_headers['fiware-service'] = service;
		_headers['fiware-servicepath'] = "/" + path;
		
		// device mobile
		var isMobile = true;
		newdevice.setMobileDevice(isMobile);
		
		// Compose new device
		var nd = new NewCamera(newdevice.getId(), service, path, newdevice.getLatitude(), newdevice.getLongitude(), null, null, null, null);
		
		
	    
		var optype = new Object();
		optype['name'] = "opType";
		optype['type'] = "Text";
		optype['value'] = "ready";

		nd.addStaticAttribute(optype);
		

	var organization = new Object();
	organization['name'] = "organization";
	organization['type'] = "Text";
	organization['value'] = $('#organizationId').val(); 
	nd.addStaticAttribute(organization);
	
	var device_owner = new Object();
	device_owner['name'] = "device_owner";
	device_owner['type'] = "Text";
	device_owner['value'] = connectedUserId; // username id of connected user
	nd.addStaticAttribute(device_owner);
	
	var screenId = new Object();
	screenId['name'] = "screenId";
	screenId['type'] = "Text";
	screenId['value'] = newdevice.getName();
	nd.addStaticAttribute(screenId);
	
	var streaming_url = new Object();
	streaming_url['name'] = "streaming_url";
	streaming_url['type'] = "url";
	streaming_url['value'] = $('#streamingurl').val().trim(); 
	nd.addStaticAttribute(streaming_url);

	nd.setEndpoint(streaming_url.value);
		
	var devices = new Array();
		devices.push(nd);
	
	var indevlist = new Object();
		indevlist['devices'] = devices;
		
	var input = new Object();
		input['action'] = operationType;
		input['payload'] = JSON.stringify(indevlist);
		
	$.ajax({
		url: 'camerahandler', 
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : _headers,
		success : function(data) {
			
			var result = JSON.parse(data.response);
			
			var queryparams = new Object();
				queryparams['success'] = true;
				queryparams['msg'] = $('#msg_device_published').text();
				queryparams['scope'] = service;
				queryparams['urbanservice'] = path;
				queryparams['urbanserviceName'] = myDropdownSelCategoryMain;
				//Chiamata a modale di conferma submit: all'OK modale fare "location.href";
				$('#confirm_modal').modal({
					onOpenEnd: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
					    	 // Call backend and fill in the modal with data coming from the device jet created
					    	 fillConfirmModal(result);
					},
					onCloseEnd: function() { 
					    	 location.href="devices?"+$.param(queryparams);
					} // Callback for Modal close
				});
					
				$('#confirm_modal').modal('open');
				
		},
		error : function(xhr, status, error) {
			
			var error = JSON.parse(xhr.responseText);
			
			if(error.forbidden == true){
				
				var forbiddenChars = error.forbiddenChars;
				
				$("#streamingurl").removeAttr("validate").addClass("invalid");
				M.toast({html:$('#msg_camera_stream_url_is_not_valid').text() + forbiddenChars, classes:'red'});
				
			}else{
				M.toast({html:$('#msg_unable_to_publish').text(), classes:'red'});
				
			}
			
		}
	});

	return false;
})




function prefillDeviceForm(){

	// Get values from current device
	var _device = JSON.parse(currentDevice);
	
	$('#name').val(_device.screenId).trigger('input');
	$('#deviceid').val(_device.id).trigger('input');
	
	if (actionmenu!="copyDevice"){
		$('#name').attr("disabled", true); 
		$('#deviceid').attr("disabled", true); 
	}

	$("#organizationId option[value='" + _device.organization + "']").attr("selected", true);
	$('#organizationId').formSelect();
	
	$("#dataformat_protocol option[value='" + _device.protocol + "']").attr("selected", true);
	$('#dataformat_protocol').attr("disabled", true);
	
	$("#transport_protocol option[value='" + _device.transport + "']").attr("selected", true);
	$('#transport_protocol').attr("disabled", true);
	
	$('#streamingurl').val(_device.streamingURL);
	
	
	// Configured Endpoint
	$('#device_endpoint').val(_device.endpoint);
	if (_device.retrieveDataMode=="pull" && _device.transport=="MQTT"){
		$('#device_endpoint').addClass('hide');
	}
		
	// Configured retrievedatamode // DEVICE PUSH/PULL
	var retrieveDataModeConf = _device.retrieveDataMode;
	

	var latitude = _device.latitude;
	var longitude = _device.longitude;
	var center = {lat: latitude, lng: longitude};
	initNewDeviceMap(center);
	
	// re-initialize material-select
    $('select').formSelect();
	
}// END PREFILL
	


// REVIEW FUNCTION fillConfirmModal
/**   
 * Fill the data received from backend into the confirm modal 
 */
function fillConfirmModal(data){

			var apikey = data.apikey;
			var dataformatProtocol = data.dataformat;
			var endpoint = data.endpoint;
			var name = data.name;
			var organization = data.organization;
			var transportProtocolValue = data.transport; // HTTP/MQTT
			var retrieveDataMode = data.retrieveDataMode; // PUSH, PULL


			// FILL IN THE MODAL
			$('#deviceName').html(name);
			$('#transportProtocolValue').html(transportProtocolValue.toUpperCase());
			$('#dataformatProtocolValue').html(dataformatProtocol.toUpperCase());

			
			if(retrieveDataMode.toLowerCase()=='push'){
				$('#pullline').addClass('hide');
				$('#pushline').removeClass('hide');

				$('#pushhttpdeviceEndpoint').html(endpoint) ;
				$('#pushmqttbrokerEndpoint').html(endpoint) ;
				$('#apikeyValue').html(apikey) ;
				
				if(transportProtocolValue.toLowerCase()=='http'){
					$('.mqtt_topic').addClass('hide');
					$('#push_http_device_endpoint').removeClass('hide');
					$('#push_mqtt_broker_endpoint').addClass('hide');
				}else if (transportProtocolValue.toLowerCase()=='mqtt'){// mqtt
					$('.mqtt_topic').removeClass('hide');
					// ["/+/+/attrs/+","/+/+/attrs","/+/+/configuration/commands","/+/+/cmdexe"]
					$('#attributeTopic').html(data.topic);
					$('#push_http_device_endpoint').addClass('hide');
					$('#push_mqtt_broker_endpoint').removeClass('hide');
				}
				
			}
}




// forbidden chars

const regex = /[A-Za-z0-9_-]+/g
var toastPresent = false;

var checkForbiddenChars = function(string, oKeyEvent){
	
	//console.log(oKeyEvent.key);
    var isvalid = false;
    if(oKeyEvent.key === 'Delete'){
     isvalid = true;
    }
    else if(oKeyEvent.key === 'Backspace'){
      isvalid = true;
    }
    else if (/[\\|!"£\$%&\/\(\)=\?'\^ìèé\+\*\]\}òç@à°#ù§,;\.:<>€]/gi.test(oKeyEvent.key)){
      isvalid = false;
       string.value=string.value.substring(0,string.value.length-1)
       if (!toastPresent){
			toastPresent = true;
			M.toast({html:$('#msg_forbidden_characters').text(), classes:'red', completeCallback: function(){ toastPresent = false;}});
		}
   }
   else if(/[A-Za-z0-9_-]/gi.test(oKeyEvent.key)){
   	   isvalid = true;
   }
   
   return isvalid;
}



var checkName = function(string, oKeyEvent){
	
	var isValid = checkForbiddenChars(string, oKeyEvent);
	if (isValid){
		$('.devicetextfield#deviceid').val(string.value).trigger('input');
	}
	return isValid;     
}

$('#name').focusout(function(){
	
	var input = new Object();
	input['action'] = 'check_name';
	input['devicename'] = $("#name").val();
	
	var _headers = new Object();
	_headers['fiware-service'] = getNewDevService();
	_headers['fiware-servicepath'] = getNewDevServicePath();
	
	$.ajax({
		url : 'camerahandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : _headers,
		success : function(data) {

			if(data == false){
				M.toast({
					html : $('#msg_camera_with_this_name_exists').text(),
					classes : 'red'
				});
				$("#submit_camera").attr("disabled","disabled");
				$("#name").removeAttr("validate").addClass("invalid");
			}else{
				$("#submit_camera").removeAttr("disabled");
				$("#name").removeAttr("validate").addClass("valid");
			}
		}
	});
	return false;
});

function getNewDevService() {

	var service = $('#myDropdownSelContextMain').val();
	return service;
}

function getNewDevServicePath() {

	var myDropdownSelCategoryMain = $('#myDropdownSelCategoryMain').val();
	var service = $('#myDropdownSelContextMain').val();
	var path = ""
	if (typeof myDropdownSelCategoryMain !== 'undefined'
			&& myDropdownSelCategoryMain != null
			&& myDropdownSelCategoryMain.trim().length > 0)
		path = service + "_" + myDropdownSelCategoryMain.toLowerCase();

	return path;
}
