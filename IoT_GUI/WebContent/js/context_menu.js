var hubMapType = {};
var hubMapUrl = {};
var gatewayMap = {};

var firstLoadingHub = true;
var firstLoadingDF= true
var firstLoadingPP=true;

/* contexts retrieve */
$(document).ready(function(){
	
	if (isMultienabler == false){
		$('.enablerSelectionMain').remove();
		populateContextMain(defaultEnabler, false);
	
	}else{
		
		populateEnablerMain();
		
	}
    
	if($("#edit_device_title").hasClass('hide')) {
			populateHubsMain();
	}


	//to reinitialize material select
	$('.contextSelMain').on('contentContextMainChanged', function() {
	    $(this).formSelect();
	    
	 });
	
	$('.contextCategorySelMain').on('contentCategoriesMainChanged', function() {
	    $(this).formSelect();
	    
	 });
	
	$('.contextHubSelMain').on('contentHubsMainChanged', function() {
		var selectedHub = $('#myDropdownSelHubMain').val();
		populateProtocol(hubMapType[selectedHub]);
		populateDataFormat(hubMapType[selectedHub]);
		
		
		if (selectedHub !="Idas")
			$("#retrievedatamodeCheck").attr('disabled', true);
		
		if (actionmenu != "copyDevice" && firstLoadingHub){
			removeAttributes();
			firstLoadingHub=false;
		}
		
		//Read hub type from hubs in dropdownlist and show Gateway filter only for type = "kapua"
		for (const name in hubMapType) {
			if (selectedHub == name) {
				if (hubMapType[name] == "kapua"){
					document.getElementById('gatewaySelDivMainRow').style.display="block";
					populateGatewaysKapua();
					resetEndpointElements();
					//setPullSectionKapuaHub(hubMapUrl[selectedHub]);
					setPullSectionKapuaHub("");
					$('.commandsection').removeClass('hide');
					$('.attributesection').addClass('hide');
					$('.kapuaassetsection').removeClass('hide');
				}
			}
		}
		$(this).formSelect();
	 });
	
	$('.contextGatewaySelMain').on('contentGatewaysMainChanged', function() {
	    $(this).formSelect();
	 });
	
	$('.contextProcolSelMain').on('contentProcolMainChanged', function() {
		$('#txt_kapua_device_endpoint').addClass('hide');
	    $(this).formSelect();
	 });
	
	$('.contextDataFormatProcolSelMain').on('contentDataFormatProcolMainChanged', function() {
	    $(this).formSelect();
	 });
	
	/*changing  context*/
	$(".singleContextSelection").on('change', function() {
			 
		  var inputEl = $(this).parent().find('input.select-dropdown');
		  var parentOk = inputEl.parent();
		  if   (parentOk.length > 0){
		           
		           var curtain = inputEl.parent().parent().attr("id");

		           if (curtain == "categorySelDivMain"  ){
			        	   console.log("categorySelDivMain");
		           }else if (curtain == "contextSelDivMain"){
		        	   populateCategoriesMain();
		          } 
		           else if (curtain == "enablerSelDivMain"){
		        	   
		        	   var myDropdownSelE =  $('#myDropdownSelEnablerMain').val();
		        	   populateContextMain(myDropdownSelE, true);
		           }
		  }
	});
	
	/*changing  hub*/
	$(".singleHubSelection").on('change', function() {
		var selectedHub = $('#myDropdownSelHubMain').val();
		$("#retrievedatamodeCheck").attr('disabled', true);
		
		//Read hub type from hubs in dropdownlist and show Gateway filter only for type = "kapua"
		for (const name in hubMapType) {
			if (selectedHub == name) {
				populateProtocol(hubMapType[selectedHub]);
				populateDataFormat(hubMapType[selectedHub]);
				removeAttributes();
				
				switch(hubMapType[name]) {
					case "kapua": {
						document.getElementById('gatewaySelDivMainRow').style.display="block";
						populateGatewaysKapua();
						resetEndpointElements();
						//setPullSectionKapuaHub(hubMapUrl[selectedHub]);
						setPullSectionKapuaHub("");
						$('.commandsection').removeClass('hide');
						$('.attributesection').addClass('hide');
						$('.kapuaassetsection').removeClass('hide');
						break;
					}
					case "dive": {
						document.getElementById('gatewaySelDivMainRow').style.display="none";
						
						//TODO: add dive gateway management when Dive will be integrated (if provided)
						//populateGatewaysDive();
						resetEndpointElements();
						break;
					}
					case "idas": {
						$('#device_endpoint').val("http://217.172.12.145:8100");
						$('.attributesection').removeClass('hide');
						$('.kapuaassetsection').addClass('hide');
					}
					default: {
						document.getElementById('gatewaySelDivMainRow').style.display="none";
					}
				}
			}
		}
	});
	
});


function populateContextMain(currentEnabler, isMultienabler){
	$('#myDropdownSelContextMain').empty();
	$("#myDropdownSelContextMain").trigger('contentContextMainChanged');
	
	$('#myDropdownSelCategoryMain').empty();
	$("#myDropdownSelCategoryMain").trigger('contentCategoriesMainChanged');
	
	
	var curEnab = currentEnabler;
	if (isMultienabler) curEnab=""+currentEnabler;
	
	
	try{ 
		var input = new Object();
		input['action'] = 'getServices';
		input['enabler'] = curEnab;

		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				
				$('#myDropdownSelContextMain').append($('<option disabled selected>', {value:"", text:""}));
				
				var numContext = data.length;
                $.each(data, function(i, e){
                	var elEnabler = e.refScope;
                	
                	if (numContext == 1){//if it is just one, I select it and start the categories population
                		$('#myDropdownSelContextMain').append($('<option>', {value:e.id, text:e.name.value, selected:true}));
                		populateCategoriesMain();
                	}else{
                		$('#myDropdownSelContextMain').append($('<option>', {value:e.id, text:e.name.value}));
                	}
                	
                	
               		
				});
                $("#myDropdownSelContextMain").trigger('contentContextMainChanged');
               
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
	}
	catch(error){
		console.log(error);
	}
}

function populateCategoriesMain(){
	
	var myDropdownSelV =  $('#myDropdownSelContextMain').val();
	$('#myDropdownSelCategoryMain').empty();
	$("#myDropdownSelCategoryMain").trigger('contentCategoriesMainChanged');
	
	var input = new Object();
	input['action'] = 'getCategories';
	
	input['services'] = ''+myDropdownSelV;
	
	
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				
				$('#myDropdownSelCategoryMain').append($('<option disabled>', {value:"", text:""}));
				
				$.each(data, function(i, e){
                		$('#myDropdownSelCategoryMain').append($('<option>', {value:e, text:e}));
				});
				$("#myDropdownSelCategoryMain").trigger('contentCategoriesMainChanged');
				
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
	
}

function populateEnablerMain(){
	
	var currentEnabler = get('enabler');
	
	if (currentEnabler != null){
		$('#myDropdownSelEnablerMain').prepend($('<option disabled>', {value:"", text:""}));
		$('#myDropdownSelEnablerMain').find('option[value="'+currentEnabler+'"]').prop('selected', true);
		$("#myDropdownSelEnablerMain").formSelect();
	}else{
		currentEnabler =$('#myDropdownSelEnablerMain').val();
	}
	
	
	populateContextMain(currentEnabler, true);
}

function populateHubsMain(){
	var input = new Object();
	input['action'] = 'getHubs';
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			
			$('#myDropdownSelHubMain').append($('<option disabled>', {value:"", text:""}));			
			$.each(data, function(i, e){
				
				//Save all hubs couple name/type
				hubMapType[e.name] = e.type;
				hubMapUrl[e.name] = e.endpoint;
				$('#myDropdownSelHubMain').append($('<option>', {value:e.name, text:e.name, selected: true}));
			});                                                          			
			$("#myDropdownSelHubMain").trigger('contentHubsMainChanged');
		},
		error : function(xhr, status, error) {
			toggleLoader(false, "populateHubsMain error");
			console.log(error);
		}
	});
}

function populateGatewaysKapua(){
	clearGatewaySelectsFilter(true);
	
	var input = new Object();
	input['action'] = 'getGatewaysKapua';
	input['hubName'] = $('#myDropdownSelHubMain').val();
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			
			$('#myDropdownSelGatewayMain').append($('<option disabled>', {value:"", text:""}));
			
			$.each(data, function(i, e){
				
				//Save all gateways couple displayName/clientId
				gatewayMap[e.displayName] = e.clientId;
    			$('#myDropdownSelGatewayMain').append($('<option>', {value:e.displayName, text:e.displayName, selected: true}));
			});                                                          			
			$("#myDropdownSelGatewayMain").trigger('contentGatewaysMainChanged');
		},
		error : function(xhr, status, error) {
			toggleLoader(false, "populateGatewaysMain error");
			console.log(error);
		}
	});
}

function clearGatewaySelectsFilter(gateway){
	if(gateway){
		$('#myDropdownSelGatewayMain').empty();
		$("#myDropdownSelGatewayMain").trigger('contentGatewaysMainChanged');
		$('#gatewaySelDivMain').attr("data-tooltip", $('#msg_no_filters').text());
	}
}

function populateProtocol(hubType){
	if(hubType == "kapua"){
		$('#transport_protocol').empty();
		$('#transport_protocol').append($('<option>', {value:"MQTT", text:"MQTT", selected: true}));
		$('#transport_protocol').attr("disabled", true);
	} else {
		$('#transport_protocol').empty();
		$('#transport_protocol').append($('<option>', {value:"HTTP", text:"HTTP", selected: true}));
		$('#transport_protocol').append($('<option>', {value:"MQTT", text:"MQTT", selected: false}));
		
		if (agentSigfoxEnabled)
			$('#transport_protocol').append($('<option>', {value:"SIGFOX", text:"SIGFOX", selected: false}));
		
		if (agentLwm2mEnabled)	
			$('#transport_protocol').append($('<option>', {value:"COAP", text:"COAP", selected: false}));
			
			
		if (agenOpcuaEnabled)	
			$('#transport_protocol').append($('<option>', {value:"OPCUA", text:"OPCUA", selected: false}));
		
		
		if (agentLoraEnabled)	
			$('#transport_protocol').append($('<option>', {value:"LORA", text:"LORA", selected: false}));
		
		
		$('#transport_protocol').attr("disabled", false);
	}
	
	if (actionmenu == "copyDevice" && firstLoadingPP){
		
		var _device = JSON.parse(currentDevice);
		
		$("#transport_protocol option[value='" + _device.transport + "']").attr("selected", true);
		firstLoadingPP=false;
	}
	
	
	
	$("#transport_protocol").trigger('contentProcolMainChanged');
}

function populateDataFormat(hubType){
	var transp_prot = $("#transport_protocol");
	if(hubType == "kapua"){
		$('#dataformat_protocol').find('option:not(:first)').remove();
		$('#dataformat_protocol').append($('<option>', {value:"JSON", text:"JSON", selected: true}));
		$('#dataformat_protocol').attr("disabled", true);
		
		console.log("populateDataFormat name vuoto")
		//Reset Device Name textfield (if previous selection of Transport Protocol was OPCUA, input was set with a fixed name and disabled)
		
		if (actionmenu != "copyDevice" && firstLoadingDF){
			$('#name').val("");
			resetEndpointElements();
		}
		
	} else {
		
		//for Idas hub type leave existing behaviour
		$('#dataformat_protocol').find('option:not(:first)').remove();
		changeTransportProtocol(transp_prot.val());
		$('#transport_protocol').attr("disabled", false);
	}
	//TODO: add management for hub type "dive" when it will be introduced
	
		if (actionmenu == "copyDevice" && firstLoadingDF){
		
		var _device = JSON.parse(currentDevice);
		
		$("#dataformat_protocol option[value='" + _device.dataformat_protocol + "']").attr("selected", true);
		firstLoadingDF=false;
	}
	
	firstLoadingDF=false;
	
	$("#dataformat_protocol").trigger('contentDataFormatProcolMainChanged');
}

function resetEndpointElements(){
	$('.deviceEndpoint').addClass('hide');
	$('.devicepullsection').addClass('hide');
	$("#isMobileDevice").attr('disabled', false);
	$('.devicepullsection').addClass('hide');
	$('#txt_opcua_device_endpoint').addClass('hide');
	$('#txt_http_device_endpoint').addClass('hide');
	$('#txt_mqtt_broker_endpoint').addClass('hide');
	$('#listDeviceOpcua').addClass('hide');
	$('#name').prop('disabled', false);
	$('.loraDetails').addClass('hide');
	$('.loraServerIoDeviceProfileDiv').addClass('hide');
	$('.loraDetailsDiv').addClass('hide');
}

//Kapua Data Plane communication: PUSH mode -> leave switch on "push"
//Kapua Control Plane communication: PULL Mode -> make "pull section" visible to specify endpoint and commands
function setPullSectionKapuaHub(endPoint){
	var switchStatus = false;
	setDevicePullPush(switchStatus);
	$('#pushonlyalert').removeClass('hide');
	$("#retrievedatamodeCheck").attr('disabled', true);
	$('.deviceEndpoint').removeClass('hide');
	
	if (actionmenu != "copyDevice")
		$('#device_endpoint').val(endPoint);
	
	$('#device_endpoint').removeClass('hide');

	$("#isMobileDevice").prop('checked', false);
	$("#isMobileDevice").attr('disabled', true);
	$("#mobile_device").val(false);
	
	//Set Kapua endpoint label
	$('#txt_opcua_device_endpoint').addClass('hide');
	$('#txt_kapua_device_endpoint').removeClass('hide');
}
