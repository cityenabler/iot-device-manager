var newdevice = new Device();
var newmashup = new DeviceMashup();


$(document).ready(function(){
	
    $('#listeners_modal').modal({
    	
    	preventScrolling: true,
    	dismissible: false,
    	startingTop: '4%',
    	endingTop: '4%',
    	onOpenStart: function(){
    		$('#listeners_modal').css({"height": "90%", "max-height": "90%", "width":"80%"});
    		
    		var deviceId = $('#devicedetails').attr('data-deviceid');
    		$('#listenerDeviceId').html(deviceId);
    		
    		var fiwareservice = $('#devicedetails').attr('data-fiwareservice');
    		var fiwareservicepath = $('#devicedetails').attr('data-fiwareservicepath');
    		
    		//clean old mashup
    		var domitems = $('#mashappender').find('li:not(.template)');
    		$.each(domitems, function(i, e) {
				e.remove();

			});
    		
    		$('#nomashupMsg').removeClass("hide");
    		//re-attach mashup
    		getDeviceMashup(fiwareservice, fiwareservicepath, deviceId);
    		
    		//clean old endpoint
    		var domitemsEP = $('#endpointappender').find('li:not(.template)');
    		$.each(domitemsEP, function(i, e) {
				e.remove();

			});
    		
    		$('#noendpointMsg').removeClass("hide");
    		//re-attach endpoints
    		getDeviceListenerEndpoints(fiwareservice, fiwareservicepath, deviceId); 
    		
    	}
    });

    $('#endpoint_protocol').formSelect({
    	dropdownOptions:{
    		alignment: 'left',
        	constrainWidth: false
    	}
    	
    });

});




// Mashup

$(document).on('input', '#mashupId', function() {
	
	 var fiwareservice = $('#devicedetails').attr('data-fiwareservice');
	 var fiwareservicepath = $('#devicedetails').attr('data-fiwareservicepath');
	 var deviceId = $('#devicedetails').attr('data-deviceid');
	
	var mashupkeyword = $("#mashupId").val();
	// Get all the mashups 
	getMashupList(fiwareservicepath, fiwareservicepath, mashupkeyword, deviceId);
});

/* Get the mashup list */
function getMashupList(fiwareservice, fiwareservicepath, mashupkeyword, deviceId) {
	var input = new Object();
	input['action'] = 'getMashupList';
	input['service'] = fiwareservice;
	input['servicepath'] = fiwareservicepath;
	input['mashupkeyword'] = mashupkeyword;
	input['deviceId'] = deviceId;
	
	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			if (data != null && typeof data !== "undefined") {
				var allMashups = {};
				allMashups = data;
				
				var tmpData = {};
				for (var i = 0; i < allMashups.length; i++) {
					tmpData[allMashups[i].name] = null;
					allMashups[allMashups[i].name] = allMashups[i];
					delete allMashups[i];
				}

				$('input.autocomplete').autocomplete({
					data : tmpData,
					limit : 4,
					minLength : 2,
					onAutocomplete : function(sel) {
						// retrieve values
						$("#mashupId").val(allMashups[sel].name);
						newmashup.setId(allMashups[sel].mashupId);
						newmashup.setName(allMashups[sel].name);
						newmashup.setDescription(allMashups[sel].description);
						newmashup.setType(allMashups[sel].type);
					}

				});

				$("#mashupId").click();

			}
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});

};

/**
 * Gets the Device Mashup all info related to the specific deviceId mashupId
 * Review multiple mushup
 */
function getDeviceMashup(serv, path, deviceid) {

	var input = new Object();
	input['action'] = 'getDeviceMashup';
	input['device_id'] = deviceid;

	var headersAll = new Object();
	headersAll['fiware-service'] = serv;
	headersAll['fiware-servicepath'] = path;
	
//	$("#contentloader").fadeIn();
	toggleLoader(true, "getDeviceMashup");

	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : headersAll,
		success : function(data) {
			
			
			newdevice.mashups = new Object();//clean 
			
			var mashup = data;
			if (mashup.length > 0) { // if the mashup has been yet configured, it's not possible to modify it
				$.each(mashup, function(i, e) {
					var dm = new DeviceMashup(e.mashupId,e.name, e.description, e.type, e.status, e.subscriptionId  );
					newdevice.addMashup(dm);
					appendMashup(dm);
				});
			}
		},
		error : function(xhr, status, error) {
			console.error(error);
		}
	}).always(function() {
//		$("#contentloader").fadeOut();
		toggleLoader(false, "getDeviceMashup");
	});

}


function getDeviceListenerEndpoints(serv, path, deviceid) {

	var input = new Object();
	input['action'] = 'getDeviceListenerEndpoints';
	input['deviceid'] = deviceid;

	var headersAll = new Object();
	headersAll['fiware-service'] = serv;
	headersAll['fiware-servicepath'] = path;
	
//	$("#contentloader").fadeIn();
	toggleLoader(true, "getDeviceListenerEndpoints");

	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : headersAll,
		success : function(data) {
			
			var endpoints = data;
			if (endpoints.length >= 0) { 
				
				$.each(endpoints, function(i, e) {
					addEndpoint(e)

				});
			}
			
		},
		error : function(xhr, status, error) {
			console.error(error);
		}
	}).always(function() {
//		$("#contentloader").fadeOut();
		toggleLoader(false, "getDeviceListenerEndpoints");
	});

}

function appendMashup(newmashup) {

	// Hide "No attribute" message
	$('#nomashupMsg').addClass("hide");

	var appender = $('#mashappender').removeClass('hide');
	var template = appender.find('.newmashup.template');
	var item = template.clone();

	item.removeClass('template hide');
	item.find('.mashupTitle').text(newmashup.getName());
	item.find('.mashupDesc').text(newmashup.getDescription());
	item.find('a.deletemashup').attr('data-id', newmashup.getId());
	item.find('a.deletemashup').attr('id', "mashup_"+newmashup.getId());
	
	var isSubActive = false
	if (newmashup.getStatus() == "active")
		isSubActive = true;
	
	item.find('.listenerActivation').attr('checked', isSubActive);
	item.find('.subscriptionId').val(newmashup.getSubscriptionId())
	
	$("#mashupId").val("");

	appender.append(item);
}

$(document).on('click', '#addMashRecord', function() {
	var mashId = newmashup.getId();
	var existingmashs = newdevice.getMashups();

	if (mashId != "" && mashId != null && mashId != undefined) {
		if (typeof existingmashs[mashId] !== 'undefined') {
			M.toast({
				html : $('#msg_mashup_exists').text(),
				classes : 'red'
			});
			return false;
		}
		
		var input = new Object();
		input['action'] = 'addMashup';
		
		var headersAll = new Object();
		headersAll['fiware-service'] = $('#devicedetails').attr('data-fiwareservice');
		headersAll['fiware-servicepath'] = $('#devicedetails').attr('data-fiwareservicepath');
		headersAll['deviceid'] = $('#devicedetails').attr('data-deviceid');
		headersAll['mashupid'] = mashId;
		
			
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers: headersAll,
			success : function(data) {

				var elem = $("#mashup_"+mashId).closest('.newmashup');
				
				//set the subscriptionId in order to on/off it
				 $(elem).find('.subscriptionId').val(data.subscriptionId);
				 $(elem).find('.listenerActivation').attr('checked', true);
				
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
		
		
		newdevice.addMashup(newmashup);
		appendMashup(newmashup);
		newmashup = new DeviceMashup();//clean
		return false;
	} else {
		M.toast({
				html :	$("#select_a_mashup").text()		
		})
	}

})

$(document).on( 'click', '.deletemashup', function() {

	var input = new Object();
	input['action'] = 'removeSubscription';
	
	var mashid = $(this).attr('data-id');
	
	let elem = $(this).closest(".newmashup");
	
	var headersAll = new Object();
	headersAll['fiware-service'] = $('#devicedetails').attr('data-fiwareservice');
	headersAll['fiware-servicepath'] = $('#devicedetails').attr('data-fiwareservicepath');
	headersAll['deviceid'] = $('#devicedetails').attr('data-deviceid');
	//headersAll['mashupid'] = mashid;
	headersAll['subscriptionId'] = elem.find('.mashupLever').find('.subscriptionId').val();
	

	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : function(data) {
//			console.log("Subscriptio removed")
		},
		error : function(xhr, status, error) {
			console.log(error);
		}
	});
	
	var devicemashups = newdevice.getMashups();
	delete devicemashups[mashid];

	$(this).closest('li').remove();
	var domitems = $(this).closest('ul#mashappender').find('li:not(.template)');
	var modelitems = newdevice.getMashups();
	if (domitems.length == 0 && (Object.keys(modelitems).length == 0)) {
		$('#nomashupMsg').removeClass("hide");
		$('ul#mashappender').addClass("hide");
	}

	M.toast({
		html : $('#msg_mashup_deleted').text()
	});
	return false;

});



// ENDPOINT

//Event click to add a new empty Header
$(document).on("click", "#addHeaderEndpoint", function(){
	addNewHeader();
});

//Event click to delete an Header
$(document).on("click", ".deleteHeaderEndpoint", function(){
	deleteHeader($(this));
});

//Event click to change icon expand into collapsible-header
$(document).on('click', '#endpointHeaders .collapsible-header', function(){
		$('i.more').toggleClass('hide');
		$('i.less').toggleClass('hide');
});

//Event to add an Endpoint
$(document).on("click", "#addEndpoint", function(){
	
	let endpointUrl = $(this).parent().parent().find("input#endpointUrl").val();
	
	
	var isUrl = validURL(endpointUrl);
	
	if (isUrl && endpointUrl != "" && endpointUrl != " " && endpointUrl != null && endpointUrl != undefined) {
		
		
		var input = new Object();
		input['action'] = 'addListenerEndpoint';
		
		var headersAll = new Object();
		headersAll['fiware-service'] = $('#devicedetails').attr('data-fiwareservice');
		headersAll['fiware-servicepath'] = $('#devicedetails').attr('data-fiwareservicepath');
		headersAll['deviceid'] = $('#devicedetails').attr('data-deviceid');
		headersAll['endpointUrl'] = $("#endpointUrl").val();
		
		let endpoint_num = $('.newendpoint').length - 1; //to start from 0
		var nextId = 'e'+endpoint_num
		
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers: headersAll,
			success : function(data) {
				
				var elem = $("#"+nextId);
				
				//set the subscriptionId in order to on/off it
				$(elem).find('.subscriptionId').val(data.subscriptionId);
				$(elem).find('.listenerActivation').attr('checked', true);
				
				addEndpoint($("#endpointUrl").val());
			},
			error : function(xhr, status, error) {
				var error = JSON.parse(xhr.responseText);
				
				if(error.forbidden == true){
					
					var forbiddenChars = error.forbiddenChars;
					
					$("#endpointUrl").removeAttr("validate").addClass("invalid");
					M.toast({html:$('#msg_endpoint_is_not_valid').text() + forbiddenChars, classes:'red'});
					
				}else{
					$("#endpointUrl").removeAttr("validate").addClass("invalid");
					M.toast({html:$('#msg_unable_to_add_endpoint').text(), classes:'red'});
					
				}
			}
		});
		
		return false;
	} else {
		M.toast({
				html :	$("#insert_a_valid_endpoint").text()
		})
	}
});

// Event to delete an Endpoint
$(document).on("click", ".deleteendpoint", function(){
	deleteEndpoint($(this));
});

//Event to edit an Endpoint
$(document).on("click", ".editendpoint", function(){

	editEndpoint($(this));
	if(!$('#endpointHeaders li').hasClass('active'))
		$('#endpointHeaders .collapsible-header').trigger('click')
		
	deleteEndpoint($(this));
});


function calcHeadersNum(){
	let numH = 0;
	
	let headInput = $("#headersList #h0.headerRow #header_name_0").val();
	let headValue = $("#headersList #h0.headerRow #header_value_0").val();
	
	if(headInput != "" && headInput != " " && headInput != null && headInput != undefined){
		numH = $("#headersList .headerRow").length;
	}
	else {
		return numH;
	}
	
	return numH;
}

//Add a new empty Header row
function addNewHeader(){
	
	let row_cloned = $("#h0.headerRow").clone().removeClass('template');
	row_cloned.find('input').val('');
	let hr_num = $('.headerRow').length;
	
	row_cloned.find('#header_name_0').attr('id','header_name_'+hr_num).next('label').attr('for','header_value_'+hr_num)
	row_cloned.find('#header_value_0').attr('id','header_value_'+hr_num).next('label').attr('for','header_value_'+hr_num)
	
	row_cloned.attr("id",'h'+hr_num);
	row_cloned.appendTo("#headersList");
}

// Delete an existing Header row
function deleteHeader(rh){
	
	let elem = rh.parent().parent();
	
	if(!elem.hasClass('template')){
		elem.remove();
	}else{
		elem.find('input').val('');
	}
}

// Add a new Endpoint as listener
function addEndpoint(endpoint){
	$('#endpointappender').removeClass("hide");
	$('#noendpointMsg').addClass('hide');
	
	let endpoint_cloned = $(".newendpoint.template").clone().removeClass('template hide');
	let endpoint_num = $('.newendpoint').length - 1; //to start from 0
	let headers_num = calcHeadersNum();
	
	endpoint_cloned.attr("id",'e'+endpoint_num);
	
	if	(typeof endpoint === 'string' || endpoint instanceof String){//it arrives from GUI (new endpoint click)
		endpoint_cloned.find(".endpointURL").text(endpoint);
	}else{//it arrives from server (old endpoint)
		
		endpoint_cloned.find(".endpointURL").text(endpoint.endpointURL.value);
		endpoint_cloned.find('.subscriptionId').val(endpoint.subscriptionId.value);
		
		var isSubActive = false
		if (endpoint.status.value == "active" )
			isSubActive = true;
		
		endpoint_cloned.find('.listenerActivation').attr('checked', isSubActive);
		
	}
	
	endpoint_cloned.find(".numHeaders").text(headers_num+" headers");
	endpoint_cloned.appendTo("#endpointappender");
	
	//here it will called a function to retrieve all Endpoint's values
	cleanEndpoint(); //clean endpoint value
	cleanAllHeaders(); //clean endpoint's headers values
}

// Clean current endpoint values
function cleanEndpoint(){
	
	$('#endpoint_protocol').val('HTTP').change();
	$('#endpoint_protocol').parent().find('input').val("HTTP")
	$('#endpointUrl').val("");
}

// Clean current headers for the current endpoint
function cleanAllHeaders(){
	
	let headersList = $("#headersList");
	let template = headersList.find('.headerRow.template').detach();
	template.find('input').val('')
	headersList.empty();
	
	template.appendTo("#headersList");
}

// Remove a listener Endpoint
function deleteEndpoint(re){
	
	let elem = re.closest(".newendpoint");
	
	var input = new Object();
	input['action'] = 'removeSubscription';
	
	var headersAll = new Object();
	headersAll['fiware-service'] = $('#devicedetails').attr('data-fiwareservice');
	headersAll['fiware-servicepath'] = $('#devicedetails').attr('data-fiwareservicepath');
	headersAll['deviceid'] = $('#devicedetails').attr('data-deviceid');
	//headersAll['endpointUrl'] = $(re).siblings('p').text();
	headersAll['subscriptionId'] = elem.find('.endpointLever').find('.subscriptionId').val();
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : function(data) {
			
			console.log("endpoint deleted")
			console.log(data)
		},
		error : function(xhr, status, error) {
			console.log(error);
		}
	});
	
	

//	let elem = re.closest(".newendpoint");
	let numElem = $(".newendpoint").length;

	if(!elem.hasClass('template') && numElem > 2){
		elem.remove();
	}else{
		elem.remove();
		$('#endpointappender').addClass("hide");
		$('#noendpointMsg').removeClass("hide");
	}
}

function editEndpoint(ee){
	console.log("Edit Endpoint")
	let elem = ee.closest(".newendpoint");
	
	let url = elem.find('.endpointURL').text();
	let protocol = "HTTPS";
	let headers = [
				{
					"name":"headers1",
					"value":"value1"
				},
				{
					"name":"headers2",
					"value":"value2"
				}
			];
	
	$('#endpointUrl').val(url);
	$('#endpoint_protocol').val(protocol).change();
	$('#endpoint_protocol').parent().find('input').val(protocol);
	
	for (let i=0; i<headers.length; i+=1) {
		
		$("#h"+i+" #header_name_"+i).val(headers[i].name).siblings().addClass("active");
		$("#h"+i+" #header_value_"+i).val(headers[i].value).siblings().addClass("active");
		
		if(i<(headers.length - 1)) addNewHeader();
	}
	
}

/** Active/Deactive subscription */
$(document).on('change', '.listenerActivation', function(){
	
	var fiwareservice = $('#devicedetails').attr('data-fiwareservice');
	var fiwareservicepath = $('#devicedetails').attr('data-fiwareservicepath');

	var currentVal = $(this).is(":checked");
	var subscriptionId = $(this).closest('.switchListener').siblings('.subscriptionId').val();
	
	var subscriptionStatus = "inactive"
	if (currentVal == true)
		subscriptionStatus= "active";

	changeSubscriptionStatus(fiwareservice, fiwareservicepath, subscriptionId, subscriptionStatus);
});



/** Change the Subscription Status: inactive, active */
function changeSubscriptionStatus(service, servicepath, subscriptionId, subscriptionStatus){
		
	var input = new Object();
		input['action'] = 'changeSubscriptionStatus';
		input['service'] = service;
		input['servicepath'] = servicepath;
		input['subscriptionId'] = subscriptionId;
		input['subscriptionStatus'] = subscriptionStatus;

	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		data : input,
		success : function(data) {
//			console.log("changeSubscriptionStatus done ");
//			console.log(data);
		},
		error : function(xhr, status, error) {
			console.log(error);
		}
	});

}



