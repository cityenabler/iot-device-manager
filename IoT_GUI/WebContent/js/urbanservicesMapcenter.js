
var mapcenter = {};

function getMapCenter(){
	
	var scope = get('scope');
	var urbanservice = get('urbanservice');
	
	var headersAll = new Object();
	headersAll['fiware-service'] = scope;
	headersAll['fiware-servicepath'] = urbanservice;
	
	var input = new Object();
	input['action'] = 'getMapCenter';
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : headersAll,
		success : function(data) {
			mapcenter = data;
			prefillMapcenterForm();
			var queryparams = new Object();
				queryparams['scope'] = scope;
				queryparams['urbanservice'] = urbanservice;
				/*
				if (isSeller){
					$('#urbanservicemapcenter').removeClass('hide');
				}
				*/
	
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});
	
	
};


function prefillMapcenterForm(){
	// Get values from current mapcenter
	$('#latitudefield').val(mapcenter.lat); 
	$('#longitudefield').val(mapcenter.lng); 
	$('#zoomfield').val(mapcenter.zoom); 
}


//Notifica a orion
$(document).on('change', '.mapcentervalues', function(){
	var newLat = $('#latitudefield').val();
	var keyLat = $('#latitudefield').attr('id');
	
	var newLng = $('#longitudefield').val();
	var keyLng = $('#longitudefield').attr('id');
	
	var newZoom = $('#zoomfield').val();
	var keyZoom = $('#zoomfield').attr('id');
	
	if (checkCentermapVal(keyLat, newLat) && checkCentermapVal(keyLng, newLng) && checkCentermapVal(keyZoom, newZoom)) {
	
		mapcenter.lat = newLat;
		mapcenter.lng = newLng;
		mapcenter.zoom = newZoom;
		
		console.log(mapcenter);
		modifyMapCenter(mapcenter);
	}
	else {
		return false;
	}
	return true;
	
	});


function modifyMapCenter(mapcenter){
	console.log(mapcenter);
	
	var scope = get('scope');
	
	var headersAll = new Object();
	headersAll['fiware-service'] = scope;
	
	var input = new Object();
	input['action'] = 'modifyMapCenter';
	input['payload'] = JSON.stringify(mapcenter);
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : headersAll,
		success : function(data) {
			console.log(data);
			var queryparams = new Object();
				queryparams['scope'] = scope;
				queryparams['success'] = true;
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});
	
	
};


function checkCentermapVal(key, val){
	if (key == "zoomfield") {
		if (val==null || typeof val==="undefined" || val.trim()=="" ){ 
			alert($('#msg_invalid_value').text() + " " + val);
			return false;
		}
		var valN = Number(val.trim());
		if (!Number.isInteger(valN) || val<0 || val > 21){
			alert($('#msg_invalid_value').text() + " " + val);
			return false;	
		} 
		return true;
	}
		
	if (key != "zoomfield") {
		if (val==null || typeof val==="undefined" || val.trim()=="" || isNaN(val.trim()) ){
			alert($('#msg_invalid_value').text() + " " + val);
			return false;	
		}  
		
		
		if (key = "latitude") {
			var valN = Number(val.trim());
			if (val<-90 || val>90){
				alert($('#msg_invalid_value').text() + " " + val);
				return false;	
			} 
		}
		if (key = "longitude") {
			var valN = Number(val.trim());
			if (val<-180 || val>180){
				alert($('#msg_invalid_value').text() + " " + val);
				return false;	
			} 
		}
		return true;
	}
	
			
}

