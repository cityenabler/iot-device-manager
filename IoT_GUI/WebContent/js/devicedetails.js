/*This JS file is included in filterResult.jsp & devices.jsp */

var range = [];
var activeSimulation = [];
var stoppedSimulation = [];

//$(document).ready(function(){
$( window ).on( "load", function(){
	
	/*Insert into Range array new number range */
	$('#measure_range_from').on('change', function(e, chip){
		range[0] = $("#measure_range_from").val();
	});
	
	/*Insert into Range array new chip added */
	$('#measure_range_to').on('change', function(e, chip){
		range[1] = $("#measure_range_to").val();
	});
	

    
});


function _deleteDevice(serv, path, deviceid, dataformat_protocol, devToBeDeleted, success_callback){
	var devs = new Object();
		devs['device_id'] = deviceid;
		devs['dataformat_protocol'] = dataformat_protocol; 
		devs['device'] = devToBeDeleted; 
	var arr = new Array();
		arr.push(devs);

	var payload = new Object();
	payload['devices'] = arr;

	var input = new Object();
	input['action'] = 'deleteDevices';
	input['payload'] = JSON.stringify(payload);

	var headersAll = new Object();
	headersAll['fiware-service'] = serv;
	headersAll['fiware-servicepath'] = path;
	
//	$("#contentloader").fadeIn();
	toggleLoader(true, "_deleteDevice");

	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : success_callback,
		error : function(xhr, status, error) {
			console.error(error);
		}
	}).always(function() {
//		$("#contentloader").fadeOut();
		toggleLoader(false, "_deleteDevice");
	});

}


function getInfopointCurrentDevice (deviceid,fiwareService, fiwareServicePath  ){
	
	try{ 
		var headersAll = new Object();
			headersAll['fiware-service'] = fiwareService;
			headersAll['fiware-servicepath'] = fiwareServicePath;
			headersAll['device_id'] = deviceid;
			
		var input = new Object();
		input['action'] = 'getDeviceInfopoint';
		resetInfopoint(); //To reset the infopoint sentences visible
//		$("#contentloader").fadeIn();
		toggleLoader(true, "getInfopointCurrentDevice");
	
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {
				$('#infopoint_modal').modal({
					onOpenEnd: function(modal, trigger) {

				    	  $('#infoitemlist').find('.infoitem').not('.template').remove();
				    	  
				    	  let tProtocol = '', dProtocol='';
				    	  
				    	  $.each(data, function(i, e){
				    		  console.log("Name: "+e.name+" Value: "+e.value);
				    		  
				    		  if (e.name=="deviceId"){
				    			  $(".deviceId").text(e.value);
				    			  
				    		  }else if (e.name=="context"){
				    			  $("#contextName").text(e.value);
				    			  
				    		  }else if (e.name=="category"){
				    			  $("#categoryName").text(e.value);				    		
				    		  }else if (e.name=="fiware-service"){
				    			  $(".fiwareservice").text(e.value);		
				    		  }else if (e.name=="fiware-servicepath"){
				    			  $(".fiwareservicepath").text(e.value);			  
				    		  }else if(e.name == "dataformatProtocol"){
			    				  $("#d_protocol").text(e.value);
			    				  
			    				  if (e.value == 'APPLICATION_SERVER')
			    					  $("#protocol_lora_appserver").removeClass("hide");
			    				  else if (e.value == 'OPCUA')
			    					  $("#d_protocol").text("OPC UA");
				    		  
				    		  }else if(e.name == "transportProtocol"){
			    				  $("#t_protocol").text(e.value);
			    				  tProtocol = e.value.toLowerCase();
			    				  
			    				  if (e.value == 'OPCUA')
			    					  $("#t_protocol").text("OPC UA");
			    				  			    				  
				    		  }else if(e.name == "liveDataEndpoint"){
			    				  $(".liveDataEndpoint").text(e.value);
				    		  }else if(e.name == "liveDataEndpointDoc"){
			    				  $(".liveDataEndpointDoc").attr("href",e.value);  
				    		  }else if (e.name=="payload"){
						    	  $("#payload pre").text(JSON.stringify(JSON.parse(e.value), null, 2));
				    			  
				    		  }else if (e.name=="RetrieveDataMode"){
				    			  $("#data_mode").text(e.value);
				    			  dProtocol = e.value.toLowerCase();
				    			  
				    			  if(dProtocol == 'push'){
				    				  $("#pullEndpoint").addClass('hide');
				    				  $("#retrieveData_3_push").removeClass('hide');
				    				  $("#retrieveData_3_pull").addClass('hide');
				    			  }else{
				    				  $("#pullEndpoint").removeClass('hide');
				    				  $("#retrieveData_3_push").addClass('hide');
				    				  $("#retrieveData_3_pull").removeClass('hide');
				    			  }
				    				 
				    			  
				    		  }else if (e.name=="measuresEndpoint"){
						    	  $("#measuresEndpoint").removeClass('hide');
						    	  $(".measuresEndpointVal").text(e.value);  
				    			  
				    		  }else if (e.name=="authUsername"){
						    	  $("#pullCredentials").removeClass('hide');
						    	  $(".pullCredentialsUsername").text(e.value);
						    	  
				    		  }else if (e.name=="authPassword"){
						    	  $("#pullCredentials").removeClass('hide');
						    	  $(".pullCredentialsPassword").text(e.value);	
						    	  
				    		  }else if (e.name=="authApikey"){
						    	  $("#pullCredentialsApikey").removeClass('hide');
						    	  $(".pullCredentialsApikeyVal").text(e.value);  
				    			  
				    		  }else if (e.name=="endpoint"){
				    			  $(".endpoint").text(e.value);
				    			  
				    		  }else if(e.name=="mqttPublishMeasureTopic"){ 
				    			  $("#mqtt_pubmeasure_topic").text(e.value);
				    			  $("#mqtt_sendcommand_topic").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="mqttSendCommandTopic"){  
				    			  $("#pull_mqtt_sendcommand_topic").text(e.value);
				    			  $("#pull_mqtt_sendcommand_topic").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="MQTTBrokerUsername"){ 
				    			  $("#mqtt_broker_user").text(e.value);
				    			  $("#mqtt_broker_user").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="MQTTBrokerPassword"){  
				    			  $("#mqtt_broker_password").text(e.value);
				    			  $("#mqtt_broker_password").parent().removeClass('hide');	  
				    			  
				    		  }else if(e.name=="MQTTBrokerSSLCertificate"){
				    			  $("#mqtt_broker_ssl_cert_link").attr("href", e.value);
				    			  $("#mqtt_broker_ssl_cert_link").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="Server Endpoint"){
				    			  $("#opcua_endpoint_val").text(e.value);
				    			  $("#opcua_endpoint").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="Lora network"){
				    			  $("#lora_net").text(e.value);
				    			  $("#lora_net").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="DevEUI"){
				    			  $("#lora_EUI").text(e.value);
				    			  $("#lora_EUI").parent().removeClass('hide');
				    			  
				    		  }else if(e.name=="Live data endpoint"){
				    			  $("#lora_endpoint").text(e.value);
				    			  $("#lora_endpoint").parent().removeClass('hide');
				    			  
				    		  }
				    		  
				     		});
				    	  
				    	  	checkProtocols(tProtocol, dProtocol);
				    		$('.collapsible').collapsible();
				      },
				      onCloseEnd: function() { 
				    	  	
				      } // Callback for Modal close
				});
				
				$('#infopoint_modal').modal('open');
				
			},
			error : function(xhr, status, error) {
				console.log(error);
				M.toast({html:$('#msg_infopoint_not_available').text(),classes:'red'});
			}
		}).always(function() {
//			$("#contentloader").fadeOut();
			toggleLoader(false, "getInfopointCurrentDevice");
		});
		
	} catch(error){
		console.log(error);
		M.toast({html:$('#msg_infopoint_not_available').text(),classes:'red'});
	}
	
	return false;
}

//Check transport Protocol type and activates relative text description for Infopoint.
function checkProtocols(tProtocol, dProtocol){
	console.log("tProtocol, dProtocol: "+tProtocol+" "+dProtocol)
	switch(tProtocol) {
	  case "http":
		  		$("#endpointPush").removeClass('hide');
		   	break;
	  case "mqtt":
			  $("#mqtt_publish").removeClass('hide');
			  $("#mqtt_broker_endpoint").removeClass('hide');
			  $("#pullEndpoint").addClass('hide');
		  	break;
	  case "sigfox":
		   	break;
	  case "lwm2m":
		   	break;
	  case "opcua":
		  		$("p[id^='opcua']").removeClass('hide');
		   	break;
	  case "lora":
		  		$("p[id^='lora']").removeClass('hide');
		   	break;
	  default:
	    // code block
	} 
}
//To reset the infopoint sentences visible
function resetInfopoint(){
	
	$("p[id^='http']").addClass('hide');
	$("p[id^='mqtt']").addClass('hide');
	$("p[id^='sigfox']").addClass('hide');
	$("p[id^='lwm2m']").addClass('hide');
	$("p[id^='opcua']").addClass('hide');
	$("p[id^='lora']").addClass('hide');
	$("p[id^='pull']").addClass('hide');
	$("#endpointPush").addClass("hide");
	$("#pull_mqtt_subscribe").addClass("hide");
	$("#mqtt_broker_credentials").addClass("hide");
	$("#mqtt_broker_ssl_certificate").addClass("hide");
	$("#measuresEndpoint").addClass("hide");
	$("#mqtt_broker_endpoint").addClass("hide");
	$("#protocol_lora_appserver").addClass("hide");
	
}

function getCommandinfopoint (deviceid,fiwareService, fiwareServicePath, dataformat  ){
	
	try{ 
		var headersAll = new Object();
			headersAll['fiware-service'] = fiwareService;
			headersAll['fiware-servicepath'] = fiwareServicePath;
			headersAll['device_id'] = deviceid;
			headersAll['dataformat'] = dataformat;
			
		var input = new Object();
		input['action'] = 'getDeviceCmdInfopoint';
		
//		$("#contentloader").fadeIn();
		toggleLoader(true, "getCommandinfopoint");
	
		$.ajax({
			url: 'cmdhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {
				
				$('#infopoint_cmd_modal').modal({
					onOpenEnd: function(modal, trigger) {

				    	  $('#cmditem').formSelect();
				    	  
				    	  $('#cmdtemlist').find('.cmditem').not('.template').remove();
				    	  var template = $('#cmditemlist').find('.cmditem.template');
				    	  var appender = $('#cmditemlist .cmdselect');
				    	  appender.html("");
				    	  var cmdtxtarea = $('#cmd_message_textarea').val("");
				    	
				    	  $.each(data, function(i, e){
				    		  if (e.name=="deviceId"){
				    			  $("#cmd_deviceId").text(e.value);
				    		  }
				    		  else if (e.name=="commands"){
				    			  var cmds = JSON.parse(e.value);
				    			  $.each(cmds, function(i,e){
				    				 if (e.indexOf('measure')==-1){
				    					  var item = template.clone();
							    		  item.removeClass('template hide');
							    		  item.val(e);
							    		  item.text(e.toUpperCase());
							    		  appender.append(item);
				    				 }
				    				});
				    		  }
				     		})
				      },
				      onCloseEnd: function() { 
				    	  	
				      } // Callback for Modal close
				});
				
				$('#infopoint_cmd_modal').modal('open');
				
			},
			error : function(xhr, status, error) {
				console.log(error);
				M.toast({html:$('#msg_cmd_not_available').text(),classes:'red'});
			}
		}).always(function() {
//			$("#contentloader").fadeOut();
			toggleLoader(false, "getCommandinfopoint");
		});
		
	} catch(error){
		console.log(error);
		M.toast({html:$('#msg_cmd_not_available').text(),classes:'red'});
		}
	return false;
}


function sendCommandToDevice(service, servicepath, deviceid, cmdname, cmdMessage, dataformat, transportprotocol, retrievedatamode ){

	try{ 
		var headersAll = new Object();
			headersAll['fiware-service'] = service;
			headersAll['fiware-servicepath'] = servicepath;
			headersAll['device_id'] = deviceid;
			headersAll['cmd_name'] = cmdname;
			headersAll['cmd_message'] = cmdMessage;
			headersAll['dataformat'] = dataformat;
			headersAll['transportprotocol'] = transportprotocol;
			headersAll['retrievedatamode'] = retrievedatamode;
			
			
//		$("#contentloader").fadeIn();	
		toggleLoader(true, "sendCommandToDevice");
			
		var input = new Object();
		input['action'] = 'sendCommandToDevice';
		$.ajax({
			url: 'cmdhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {

				M.toast({html:$('#msg_sendcommand_success').text(), classes:'green'});
			},
			error : function(xhr, status, error) {
				console.log(error);
				M.toast({html:$('#msg_sendcommand_failure').text(),classes:'red'});
			}
		}).always(function() {
//			$("#contentloader").fadeOut();
			toggleLoader(false, "sendCommandToDevice");
		});
		
	} catch(error){
		console.log(error);
		M.toast({html:$('#msg_sendcommand_failure').text(),classes:'red'});
		}

}



/** Active/Deactive liveStream */
$(document).on('click', '.switch.devicedetailsCamera', function(){
	
	
		if ($("#livestreamCheck").is(':checked')) {
			
			var iframe = $('<iframe>');
				iframe.attr('width', '100%');
				iframe.attr('height', '315');
				iframe.attr('frameborder', '0');
				iframe.attr('allow', 'autoplay; encrypted-media');
				iframe.prop('allowfullscreen', true);
				
				iframe.attr('src', $('#camerastreamingurl').find('a').attr('href'));
			
			$('#cameralivestreaming').html(iframe);
			
			$('#cameralivestreaming').parent().removeClass('hide');
		} else {
			$('#cameralivestreaming').parent().addClass('hide');
			
		}
		return true;
	
	
	return false;
});



/**
 * Removes sendcommand feature 
 * if a device has the retrieve data mode push
 * if a device has only command about send measure 
 */
function removeCommandinfopoint(device, modal) {

	if (device.retrieveDataMode=="push") {
		modal.find('#commandinfopoint, #commandinfopoint2').addClass('hide');
	} else if (device.retrieveDataMode=="pull") {
		
		modal.find('#commandinfopoint, #commandinfopoint2').removeClass('hide');
		
		var commands = device.getCommands();
		var commandsSize = Object.keys(commands).length;
		$.each(commands, function(i,e){
			var cmdname = e.getName();
			if (cmdname.indexOf("measure") > -1 && commandsSize === 1) {
				modal.find('#commandinfopoint, #commandinfopoint2').addClass('hide');
			} 
		});
	}

}

/************************************************************** 
 *********************** SIMULATION  **************************
 **************************************************************/

/** Starts simulator for all attributes of a device*/
function startSimulatorFullDevice(device_id, fiwareService, fiwareServicePath){
	
	
	var input = new Object();
	input['action'] = 'start';
	
	input['service'] = fiwareService;
	input['servicepath'] = fiwareServicePath;
	
	var headersAll = new Object();
	headersAll['fiware-service'] = fiwareService;
	headersAll['fiware-servicepath'] = fiwareServicePath;

	headersAll['deviceid'] = device_id;
	
	$.ajax({
		url: 'iotsimhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : function(data) {
			 
			$('.startsimdev_allAttrs').addClass("hide");
			$('.stopsimdev_allAttrs').removeClass("hide").addClass("pulse");
			
			$('.startsimdev_default').addClass("hide");
			$('.stopsimdev_default').removeClass("hide");
			
		},
		error : function(xhr, status, error) {
			console.log(error);
		}
	});
	
	return true;     
}


/** Stops simulator for all attributes of a device*/
function stopSimulatorFullDevice(device_id, fiwareService, fiwareServicePath){
	
	
	var input = new Object();
	input['action'] = 'stop';
	
	input['service'] = fiwareService;
	input['servicepath'] = fiwareServicePath;
	
	var headersAll = new Object();
	headersAll['fiware-service'] = fiwareService;
	headersAll['fiware-servicepath'] = fiwareServicePath;

	headersAll['deviceid'] = device_id;
	
	$.ajax({
		url: 'iotsimhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : function(data) {
			
			$('.startsimdev_allAttrs').removeClass("hide");
			$('.stopsimdev_allAttrs').addClass("hide").removeClass("pulse");
			
			$('.startsimdev_default').removeClass("hide");
			$('.stopsimdev_default').addClass("hide");
			
			
		},
		error : function(xhr, status, error) {
			console.log(error);
		}
	});
	
	return true;     
}


/** Stops simulator */
function stopSimulator(simulationLevel,device_id, measure_id, fiwareService, fiwareServicePath ){

	var input = new Object();
	input['action'] = 'stop';
	
		input['service'] = fiwareService;
		input['servicepath'] = fiwareServicePath;
		
		var headersAll = new Object();
		headersAll['fiware-service'] = fiwareService;
		headersAll['fiware-servicepath'] = fiwareServicePath;
		headersAll['measure_id'] = measure_id;
		simdeviceid = device_id;
		headersAll['deviceid'] = simdeviceid;
		
		$.ajax({
			url: 'iotsimhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers: headersAll,
			success : function(data) {
				
				$('#startsim').removeClass("hide");
				$('#stopsimdev').addClass("hide");
				$('#startsimdev').removeClass("hide");
				$('#startsimdev_i').removeClass("hide");
					
				$('#play_'+measure_id).removeClass("hide");
				$('#stop_'+measure_id).addClass("hide");
				
				$('.startsimdev_allAttrs').removeClass("hide");
				$('.stopsimdev_allAttrs').addClass("hide");
							
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
	
	return true;     
}


/** checks  Device Status Simulator */
function checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath){
	

	var input = new Object();
	input['action'] = 'status';
	
	var headersAll = new Object();
	headersAll['fiware-service'] = fiwareService;
	headersAll['fiware-servicepath'] = fiwareServicePath;
	// Simulation config for a device
	if (simulationLevel == "device"){
		headersAll['measure_id'] = simattributeid;//$("#measure_id").val();
	}
	headersAll['deviceid'] = simdeviceid;
	
	$.ajax({
		url: 'iotsimhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		
		success : function(data) {
			
			if(data.isRunning){
				if (simulationLevel == "all"){
					$('#startsim').addClass("hide");
					
				} else if (simulationLevel == "device"){
					$('#startsimdev').addClass("hide");
					$('#stopsimdev').removeClass("hide");
					$('#stopsimdev_i').removeClass("hide");
					
					$('#play_'+simattributeid).addClass("hide");
					$('#stop_'+simattributeid).removeClass("hide");
				}
				
			} else if(!data.isRunning) {// if device is not running
				
				if (simulationLevel == "all"){
					$('#startsim').removeClass("hide");
				//	$('#stopsim').addClass("hide");
				} else if (simulationLevel == "device"){
					$('#startsimdev').removeClass("hide");
					$('#stopsimdev').addClass("hide");
					$('#stopsimdev_i').addClass("hide");
					
					$('#play_'+simattributeid).removeClass("hide");
					$('#stop_'+simattributeid).addClass("hide");
				}
			
			}
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
		});
		
		return true;     
}


/** Get Default simulator configuration */
function getSimulationConfig(deviceid, attributeid, attributetype, fiwareService, fiwareServicePath){

var configuration = {};

try{ 
	var headersAll = new Object();
		headersAll['fiware-service'] = fiwareService;
		headersAll['fiware-servicepath'] = fiwareServicePath;
		headersAll['measure_id'] = attributeid;
		headersAll['measure_type'] = attributetype;
		
		var input = new Object();
		input['action'] = 'getconfig';

			$.ajax({
				url: 'iotsimhandler',
				type : 'GET',
				dataType : 'json',
				data : input,
				headers : headersAll,
				success : function(data) {
					$('#simulationconfig_modal').modal({
						onOpenEnd: function(modal, trigger) {

					    	  		  $("#device_id").val(deviceid);
					    	  		  $("#device_id").html(deviceid);
					    	  		  $("#measure_id").val(attributeid);
					    	  		  $("#measure_id").html(attributeid);
					    	  		  $("#measure_type").val(attributetype);
					    	  		  $("#measure_type").html(attributetype);
						    	  	  $("#measure_delay_seconds").val(data.delaySeconds);
						    	  	  
					        		  checkAttributeType(data, attributetype)
					      },
					      onCloseEnd: function() { 
					    	  $("#startsimdev").removeClass("disabled");
					      } // Callback for Modal close
					});
					$('#simulationconfig_modal').modal('open');
				},
				error : function(xhr, status, error) {
					console.log(error);
					M.toast({html:$('#msg_conf_not_available').text(), classes:'red'});
				}
			});	
	
} catch(error){
	console.log(error);
	M.toast({html:$('#msg_cmd_not_available').text(), classes:'red'});
	}
	console.log("RETURNING...");
	console.log(configuration);
	return configuration;
};

/* Transform a materialize Chips json object into an array of its values */
function chipsToArray(chips){
	var r = [];
	for(x=0; x < chips.length; x++){
		r[x]=chips[x].tag;
	}
	return r;
}

/**Check the device attribute type to display and inizialize a relative input into the simulation modal with default values**/ 
function checkAttributeType(values, type){
	
	  var _range = values.valueRange;
	  
	  var vals = $.trim(_range).split("-");
	  var jsonStr = {};
	  var defaultChips = [];
	  range = []; //clean the range
	  
	  if(vals.length>0){
		  for(x=0; x<vals.length; x++){
			  range[x] = vals[x];
			  defaultChips[x] = {"tag":range[x]};
		  }
	  }
	  
	  
	  //Check if Number and set default values
	if(isAttributeTypeNumberOrSimilar(type) || isBinary(type) || isHexadecimal(type)){
		
		$(".attribute_number_type").removeClass("hide");
		$(".attribute_text_type").addClass("hide");
		$(".attribute_date_type").addClass("hide");
		$(".attribute_boolean_type").addClass("hide");
		$(".attribute_geolocation_type").addClass("hide");
		
		$("#measure_range_from").val(range[0]);
		$("#measure_range_to").val(range[1]);
		
		
		if (isAttributeTypeIntegerOrSimilar(type)){
			
			$("#measure_range_from").attr("step", 1);
			$("#measure_range_to").attr("step", 1);
		}else{
			$("#measure_range_from").attr("step", 0.01);
			$("#measure_range_to").attr("step", 0.01);
			
		}
		
		
	  //Check if Text and set default values
	}else if(isAttributeTypeTextOrSimilar(type)){
		
		$(".attribute_number_type").addClass("hide");
		$(".attribute_text_type").removeClass("hide");
		$(".attribute_date_type").addClass("hide");
		$(".attribute_boolean_type").addClass("hide");
		$(".attribute_geolocation_type").addClass("hide");
		
		
		//Inizialize and insert the default chip values  
		$('.chips-initial').chips({
	  		placeholder: $("#msg_chip_placeholder").text(),
	  		secondaryPlaceholder: $("#msg_chip_sec_placeholder").text(),
		    data: defaultChips,
		    onChipAdd: function(elem){
				var instance = M.Chips.getInstance(elem[0]);
				range=[];
				range=chipsToArray(instance.chipsData);
				if(range.length > 0) {
					$("#startsimdev").removeClass("disabled");
				}
			},
			onChipDelete: function(elem){
				var instance = M.Chips.getInstance(elem[0]);
				range=[];
				range=chipsToArray(instance.chipsData);
				if(range.length <= 0) {
					$("#startsimdev").addClass("disabled");
				}
			}
		});
		  
	}else if(isAttributeTypeDateOrSimilar(type)){
		
		$(".attribute_number_type").addClass("hide");
		$(".attribute_text_type").addClass("hide");
		$(".attribute_date_type").removeClass("hide");
		$(".attribute_boolean_type").addClass("hide");
		$(".attribute_geolocation_type").addClass("hide");
		
	}else if(isAttributeTypeBooleanOrSimilar(type)){
		
		$(".attribute_number_type").addClass("hide");
		$(".attribute_text_type").addClass("hide");
		$(".attribute_date_type").addClass("hide");
		$(".attribute_boolean_type").removeClass("hide");
		$(".attribute_geolocation_type").addClass("hide");
		
	}else if(isAttributeTypeGeolocationOrSimilar(type)){
		
		$(".attribute_number_type").addClass("hide");
		$(".attribute_text_type").addClass("hide");
		$(".attribute_date_type").addClass("hide");
		$(".attribute_boolean_type").addClass("hide");
		$(".attribute_geolocation_type").removeClass("hide");
		
		
		//Check other types not permitted
	}else{
		$(".attribute_number_type").addClass("hide");
		$(".attribute_text_type").addClass("hide");
	}

	_range =[];

}

/** checks  Device Status Simulator */
function checksDeviceStatusSimulatorAtLeastOne( deviceid, fiwareservice, fiwareservicepath, callback){
	
	
	var input = new Object();
	input['action'] = 'status';
	
	var headersAll = new Object();
	headersAll['fiware-service'] = fiwareservice;
	headersAll['fiware-servicepath'] = fiwareservicepath;

	headersAll['deviceid'] = deviceid;
	
	$.ajax({
		url: 'iotsimhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		
		success : function(data) {
			
			fiwareservicepath=	fiwareservicepath.substring(1, fiwareservicepath.length);//removing the /
			var itemId = fiwareservice+"_"+fiwareservicepath+"_"+deviceid;
			
			if(data.isRunning){
				
				$('#'+itemId).attr('data-runningsimulation', "true");
				$('#'+itemId).find(".runningicon").removeClass("hide");
				
				$('.startsimdev_allAttrs').addClass("hide");
				$('.stopsimdev_allAttrs').removeClass("hide");
				
				activeSimulation.push({ 
			        key: itemId,
			        deviceId:deviceid,
			        fiwareService: fiwareservice,
			        fiwareServicePath: fiwareservicepath
			    });
				stoppedSimulation = removeItemFromArray(stoppedSimulation,itemId );
				
				
			}else{
				$('#'+itemId).attr('data-runningsimulation',"false");
				$('#'+itemId).find(".runningicon").addClass("hide");
				
				$('.startsimdev_allAttrs').removeClass("hide");
				$('.stopsimdev_allAttrs').addClass("hide");
				
				activeSimulation = removeItemFromArray(activeSimulation,itemId );
				stoppedSimulation.push({ 
			        key: itemId,
			        deviceId:deviceid,
			        fiwareService: fiwareservice,
			        fiwareServicePath: fiwareservicepath
			    });
				
			}
			
			//callback only for details, it manages the general selection/simulation buttons
			 if(typeof callback == "function")
		          callback();
			
			
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
		});
		
		return true;     
}




function checkSimulationAttribute(deviceid, device_attribute, fiwareService, fiwareServicePath ){
	
	var result = false;
	
	var input = new Object();
	input['action'] = 'checkattr';
	
	var headersAll = new Object();
	
	headersAll['fiware-service'] = fiwareService;
	headersAll['fiware-servicepath'] = fiwareServicePath;
	headersAll['deviceid'] = deviceid;
	
	headersAll['measure_id'] = device_attribute;
	
	$.ajax({
		async: false,
		url: 'iotsimhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers: headersAll,
		success : function(data) {
			result = true;
		},
		error : function(xhr, status, error) {
			M.toast({html:$('#msg_invalid_attribute').text(), classes:'red'});
			console.log(error);
			result = false;
		}
	});

	return result;     
}


/** Starts simulator */
function startSimulator(simulationLevel, device_id, measure_id, fiwareService, fiwareServicePath ){
	
		var input = new Object();
		input['action'] = 'start';
		
		// Simulation config for a device

			input['service'] = fiwareService;
			input['servicepath'] = fiwareServicePath;
			
			var headersAll = new Object();
			headersAll['fiware-service'] = fiwareService;
			headersAll['fiware-servicepath'] = fiwareServicePath;
			
			headersAll['measure_id'] = $("#measure_id").val();
			headersAll['measure_type'] = $("#measure_type").val();
			headersAll['measure_delay_seconds'] = $("#measure_delay_seconds").val();			
			headersAll['measure_range'] =range;
			
			simdeviceid = $("#device_id").val();
			headersAll['deviceid'] = simdeviceid;
			
			$.ajax({
				url: 'iotsimhandler',
				type : 'GET',
				dataType : 'json',
				data : input,
				headers: headersAll,
				success : function(data) {
					 
						$('#startsimdev').addClass("hide");
						$('#stopsimdev').removeClass("hide");
						$('#stopsimdev_i').removeClass("hide");
						
						$('#play_'+$("#measure_id").val()).addClass("hide");
						$('#stop_'+$("#measure_id").val()).removeClass("hide");
						
						$('.startsimdev_allAttrs').addClass("hide");
						$('.stopsimdev_allAttrs').removeClass("hide");
				},
				error : function(xhr, status, error) {
					console.log(error);
				}
			});
			
		
	return true;     
}

/** Retrieve default values for the device attribute*/
function getDefaultValue(simulationLevel, simdeviceid, simattributeid, attributeType, fiwareService, fiwareServicePath){
	
	var headersAll = new Object();
	headersAll['fiware-service'] = fiwareService;
	headersAll['fiware-servicepath'] = fiwareServicePath;
	headersAll['measure_id'] = simattributeid;
	headersAll['measure_type'] = attributeType;
	
	var input = new Object();
	input['action'] = 'getconfig';
	
	var defaultValues = {}
	
		$.ajax({
			url: 'iotsimhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {
				defaultValues = data;
				startSimulatorWithDefault(simulationLevel, simdeviceid, simattributeid, data, fiwareService, fiwareServicePath);
			},
			error : function(xhr, status, error) {
				console.log(error);
				M.toast({html:$('#msg_conf_not_available').text(), classes:'red'});
			}
		});
		
}
/** Starts simulator with default values*/
function startSimulatorWithDefault(simulationLevel, simdeviceid, simattributeid, data, fiwareService, fiwareServicePath){

		
		var input = new Object();
		input['service'] = fiwareService;
		input['servicepath'] = fiwareServicePath;
		input['action'] = 'start';
		
		var headersAll = new Object();
		headersAll['fiware-service'] = fiwareService;
		headersAll['fiware-servicepath'] = fiwareServicePath;
		
		// Simulation config for a device
		if (simulationLevel == "device"){
			headersAll['measure_id'] = data.id;
			headersAll['measure_type'] = data.type;
			headersAll['measure_range'] = data.valueRange;	
			headersAll['measure_delay_seconds'] = data.delaySeconds;
			headersAll['mapcenterLat'] = data.mapcenterLat;
			headersAll['mapcenterLng'] = data.mapcenterLng;
		}
		headersAll['deviceid'] = simdeviceid;
		
		
		$.ajax({
			url:  'iotsimhandler',
			type : 'GET',
	        
			dataType : 'json',
			data : input,
			headers: headersAll,
			success : function(data) {
				
				if (simulationLevel == "all"){
					$('#startsim').addClass("hide");
					$('#stopsim_i').removeClass("hide");
					
					$('#play_'+simattributeid).addClass("hide");
					$('#stop_'+simattributeid).removeClass("hide");
					
				} else if (simulationLevel == "device"){
					
					$('#startsimdev').addClass("hide");
					$('#stopsimdev').removeClass("hide");
					$('#stopsimdev_i').removeClass("hide");
					
					$('#play_'+simattributeid).addClass("hide");
					$('#stop_'+simattributeid).removeClass("hide");
					
					$('.startsimdev_allAttrs').addClass("hide");
					$('.stopsimdev_allAttrs').removeClass("hide");
					
				}
				
			},
			error : function(xhr, status, error) {
				console.log("!!! ERROR !!!");
				console.log(error);
			}
		});
		
	return true;        
}





/*Show the selected second as tooltip (custon simulation) */
$('#measure_delay_seconds').on('change', function(e, chip){
	
	var numb = $(this).val();
	
	$(this).attr('data-tooltip',numb)
	
	$('.tooltipped').tooltip();
});

/*Show the selected radius as tooltip (custon simulation) */
$('#radius_distance').on('change', function(e, chip){
	
	var numb = $(this).val();
	
	$(this).attr('data-tooltip',numb)
	
	$('.tooltipped').tooltip();
});



function removeItemFromArray (myArray, key){
	
	for (var i = myArray.length - 1; i >= 0; --i) {
	    if (myArray[i].key == key) {
	        myArray.splice(i,1);
	    }
	}
	
	return myArray;
}

/* 
 * 
 * WebSocket Connections 
 * 
 * */


/* 
 * 
 * MEASURE NOTITIFICATIONS
 * 
 * */



/* 
 * input 2019-10-16T18:41:47.710Z
 * output 16-10-2019 20:41:47.710

 */
function msToTime(s) {

	  var d = new Date(s); 
	  var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
	    d.getFullYear() + " " 
	    + ("0" + d.getHours()).slice(-2) 
	    + ":" + ("0" + d.getMinutes()).slice(-2)
	    + ":" + ("0" + d.getSeconds()).slice(-2)
	    +"."+d.getMilliseconds();
	  
	  return datestring;
	  
}

function roundLocation (lat, long){
	
	var lat5 = parseFloat(lat.toFixed(5));
	var long5 = parseFloat(long.toFixed(5));
	
	return lat5+","+ long5;
}	 


function updateMapPositionOnPong (latWS, longWS, deviceName, zoom ) {
	
	if (typeof marker !== 'undefined' && marker != null ){
	    map.removeLayer(marker); //remove previous marker
	}
	marker = L.marker([latWS, longWS]).bindPopup(deviceName).addTo(map);	
}




var stompMeasuresClient = null;

//Stomp webSocket connection to notify new measure 
function connectMeasureNotification(service, servicepath, devid){
	
	try{
		var socket = new SockJS(measureNotificationWebsocketEndpoint);
		
		stompMeasuresClient = Stomp.over(socket);
		
		$("#column-running-attributes").addClass("hide");
		let wHeight = $(document).height();
		$("#column-running-attributes .card .card-content").css('max-height',wHeight*0.8)
		
		$(".runningAttr").addClass("hide");
		
		stompMeasuresClient.connect({}, 
				function(frame) {//connect Callback
					stompMeasuresClient.subscribe('/topic/'+service+servicepath+'/'+devid, function(messageOutput) {
						
						$("#column-running-attributes").removeClass("hide");
						$(".runningAttr").not(".template").removeClass("hide");
	
						//console.log(messageOutput);
						
						var jsonRetString = messageOutput.body
						var jsonRet = JSON.parse(jsonRetString);
						
						//console.log(jsonRet);
						
						
						for (var key in jsonRet) {
							  if (jsonRet.hasOwnProperty(key)){
								  
								  var currentValue = jsonRet[key];
	
								  if (key == "location"){
									  
									  var locArr = jsonRet[key].split(',');
										
									  var latWS = parseFloat(locArr[0]);
									  var longWS =	parseFloat(locArr[1]);
									  
									  currentValue = roundLocation(latWS, longWS);
									  
									  if (latWS != device.getLatitude() ||   longWS != device.getLongitude()  ){
										  updateMapPositionOnPong(latWS, longWS, device.name, 14 );
									  }
								  }
									  
								  else if (key == "ObservationDateTime")
									  currentValue = msToTime(jsonRet[key]);
								  							  
							  	 $("#measure_"+key).find(".measureValue").removeClass("hide").text(currentValue); 
							  } 
						}
					});
				},
				function(message) { //error callback
					console.log("Error on connection with socket on connectMeasureNotification method");
				});
		
	}catch(err) {
		console.log("Error on stompMeasuresClient connection");
	}
		
		
		
}

//measureNotification Stomp WebSocket closing
function disconnectMeasureNotification() {
	if(stompMeasuresClient != null) {
		try {
			stompMeasuresClient.disconnect();
		    console.log("stompMeasuresClient Disconnected");
		}catch(err) {
			console.log("Error on stompMeasuresClient disconnection");
		}
	}
	
}

/* 
 * 
 * STATUS CONTROLLER
 * 
 * */
var stompClient = null;

//statusController Stomp WebSocket
function connect(tenant, path, devid){
	try {
		var socket = new SockJS(statusControllerWebsocketEndpoint);
		stompClient = Stomp.over(socket);
	
		stompClient.connect({}, 
				function(frame) {//connect Callback
					stompClient.subscribe('/topic/'+tenant+'/'+path+'/'+devid, function(messageOutput) {
						showMessageOutput(JSON.parse(messageOutput.body));
					});
				},
				function(message) {//error callback
					$('#detailsloader').addClass('hide');
					$('.light').addClass('hide');
					$('#unreachable').removeClass('hide');
				});
	
	}catch(err) {
		console.log("Error on stompClient connection");
	}
}

//statusController Stomp WebSocket closing
function disconnect() {
	if(stompClient != null) {
		try {
			stompClient.disconnect();
			console.log("Disconnected");
		}catch(err) {
			console.log("Error on stompClient disconnection");
		}
	}
}

function showMessageOutput(messageOutput) {//called by connect function
//	console.log(messageOutput.deviceid);
	var d = new Date(messageOutput.lastupdate);
	var red = $('#redlight');
	var green = $('#greenlight');
	var loader = $('#detailsloader');
	var toShow = null;
	var toHide = null;
	
	loader.addClass('hide');
	
	if(messageOutput.status === 'ONLINE'){
		toHide = red
		toShow = green;
	}
	else{
		toHide = green
		toShow = red;
	}
	toHide.addClass('hide');
	
	toShow.removeClass('hide');
	toShow.attr('title', messageOutput.status + ' - ' 
						+ ('0'+d.getDate()).slice(-2) + "/" + ('0'+(1+(d.getMonth()))).slice(-2) + "/" + d.getFullYear() 
						+ " " +('0'+d.getHours()).slice(-2) + ":" + ('0'+d.getMinutes()).slice(-2) + ":" + ('0'+d.getSeconds()).slice(-2));
	
}

function resetLights(){
	$('#detailsloader').removeClass('hide');
	$('#unreachable').addClass('hide');
	$('.light').addClass('hide');
	
	$('sup#statuslights').addClass('hide');
	
}




/*Actions executed when we show the device details modal */
function deviceDetailsAction (device, trigger, deviceid, fiwareService, fiwareServicePath, dataformat, transportprotocol, retrievedatamode){
	
	
	$('#cameralivestreaming').parent().addClass('hide');
	$('#iscamera').val(false);
	
	var iscamera = Object.keys(device.getAttributes()).length == 0;
	
	var isOPCUA =  ((device.protocol == "OPCUA") ? true : false);
	var isLora =  ((device.transport == "LORA") ? true : false);
	
	trigger.parent().find('.preloader-wrapper').toggleClass('hide');
	
	var modalselector = trigger.attr('href');
	var modal = $(modalselector);
	
	// Removes sendcommand feature
	removeCommandinfopoint(device, modal);
	
	modal.attr('data-deviceid', deviceid);
	modal.attr('data-fiwareService', fiwareService);
	modal.attr('data-fiwareServicePath', fiwareServicePath);
	modal.attr('data-dataformat', dataformat);
	modal.attr('data-transportprotocol', transportprotocol);
	modal.attr('data-retrievedatamode', retrievedatamode);
	modal.find('.devicedetails ._name').text(device.screenId);
	modal.attr('data-hubtype', device.hubType);
	modal.attr('data-devicename', device.name);
	modal.attr('data-retrievedatamode', retrievedatamode);
	
	var c = device.getCategory();
	modal.find('.devicedetails._category .cat').text(c.getParentCategory());
	modal.find('.devicedetails._category .subcat').text(c.getName().replace("/", ""));
	

	
	
	if(iscamera){
		modal.find('.attrlist').addClass('hide');
		modal.find('.titleAttributeList').addClass('hide');
		modal.find('#sectionSimulationAttrAll').addClass('hide');
		modal.find('.labelTitleSwitch').addClass('hide');
		modal.find('#cameraLever').removeClass('hide');
		modal.find('#additionalFunctionPanel').addClass('hide');
		$('#livestreamCheck').prop('checked', false);
		
		
	}else{
		
		modal.find('#cameraLever').addClass('hide');
		
		modal.find('#sectionSimulationAttrAll').removeClass('hide');
		
		if (isOPCUA)
			$('#sectionSimulationAttrAll').addClass('hide');
		
		var attrappender = modal.find('.attrlist').removeClass('hide');
		modal.find('.titleAttributeList').removeClass('hide');
		modal.find('.labelTitleSwitch').removeClass('hide');
		modal.find('#additionalFunctionPanel').removeClass('hide');

		var attrtemplate = modal.find('.attr.template');
		
		var runningAttrsAppender = modal.find('#running_attributes_appender');
		var runningAttrTemplate = runningAttrsAppender.find('.runningAttr.template');
		
		//clean in case of re-open
		runningAttrsAppender.empty();
		runningAttrsAppender.append(runningAttrTemplate);
		

		$.each(device.getAttributes(), function(index, value){
			var attritem = attrtemplate.clone();
										
			attritem.removeClass("template hide");
			attritem.find('.name').text(index+" [ " + value + " ]");
			attritem.find('.attribName').attr("data-tooltip", index+" [ " + value + " ]");
			
			// Here appends the attribute values
			attritem.find('.simulationconfig').attr('data-attributeid', index);
			attritem.find('.simulationconfig').attr('data-attributetype', value);
			
			attritem.find('.startsimdev_default').attr('id', "play_"+index);
			attritem.find('.startsimdev_default').attr('data-attributetype', value).attr('data-attributeid', index);
			
			attritem.find('.stopsimdev_default').attr('id', "stop_"+index);
			attritem.find('.stopsimdev_default').attr('data-attributetype', value).attr('data-attributeid', index);
			
			if (isOPCUA)
				attritem.find('.simulationsButtons').addClass('hide');
			
			attrappender.append(attritem);
			
			
			//running attributes panel
			var runningAttr = runningAttrTemplate.clone();
			runningAttr.removeClass("template hide");
			runningAttr.attr('id', "measure_"+index);
			
			var measureValue = runningAttr.find('.measureValue').clone();
			
			runningAttr.find('.measureName').text(index).append(measureValue);
			runningAttrsAppender.append(runningAttr);
			
			
			if (! (isOPCUA) )
				checksDeviceStatusSimulator('device', deviceid, index, fiwareService, fiwareServicePath);//in order to update the icons
			
		}); 
		
		checksDeviceStatusSimulatorAtLeastOne(deviceid, fiwareService, fiwareServicePath);
		
		
	}
	
	var deviceOwner=device.getDeviceOwner();  
	var organizationDevice = device.getOrganization();
	var permitEdit = device.permitEdit;
	var permitDuplicate = device.permitDuplicate;
	var permitDelete = device.permitDelete;
	
	var menu = modal.find('#deviceactionmenu');
	
	//Hide/Show Edit and Delete fix
	if (permitEdit==true) {
		
		if (!isOPCUA){
			menu.find('#editcurrentdevice').removeClass('hide');
			$('.editActions').removeClass('hide');
		}
		
		$('.addListenersDiv').removeClass('hide');
		$('.showListenersDiv').removeClass('s12').addClass('s6');
		$('.showListenersDiv').parent().removeClass('singleMainRow');
		$('.onoffListener').attr('disabled', false);
		
	}else{
		
		$('.editActions').addClass('hide');
		$('.addListenersDiv').addClass('hide');
		$('.showListenersDiv').removeClass('s6').addClass('s12');
		$('.showListenersDiv').parent().addClass('singleMainRow');
		$('.onoffListener').attr('disabled', true);
	}
	
	if (permitDuplicate==true && duplicateEnabled) 
		menu.find('#copycurrentdevice').removeClass('hide');
	else
		menu.find('#copycurrentdevice').addClass('hide');

	
	if (permitDelete==true) 
		menu.find('#deletecurrentdevice').removeClass('hide');
	else
		menu.find('#deletecurrentdevice').addClass('hide');
		
	
	
	
	//Hide/Show infopoint and commands
	if (iscamera) {
		menu.find('.infopointcurrentdevice').parent().addClass('hide');
		menu.find('.commandinfopoint').parent().addClass('hide');
		menu.find('.listenerscurrentdevice').parent().addClass('hide');
		 
	}
	else {
		menu.find('.infopointcurrentdevice').parent().removeClass('hide');
		menu.find('.listenerscurrentdevice').parent().removeClass('hide');
		
		if(device.retrieveDataMode.toLocaleLowerCase() === 'pull') menu.find('.commandinfopoint').parent().removeClass('hide');
		else menu.find('.commandinfopoint').parent().addClass('hide');
	}
	
	if(iscamera){
		modal.find('#camerastreamingurl').removeClass('hide');
		modal.find('#camerastreamingurl').find('a').text(device.endpoint);
		modal.find('#camerastreamingurl').find('a').attr('href', device.endpoint);
	}
	else{
		modal.find('#camerastreamingurl').addClass('hide');
	}
	
	trigger.parent().find('.preloader-wrapper').toggleClass('hide');
	
	if(iscamera)
		$('#iscamera').val(true);
	else
		$('#iscamera').val(false);
	
	if(!iscamera){
		//socket connect to show measure notification
		connectMeasureNotification(fiwareService, fiwareServicePath, deviceid);
	}
	
	if(device.hasOwnProperty('commands') && device.commands.hasOwnProperty('ping')){
		$('sup#statuslights').removeClass('hide');
		try{ 
			connect(fiwareservice, fiwareservicepath, deviceid); 
		}
		catch(error){
			alert("Unable to connect");
			console.log(error);
		}
	}
	
	$('.tooltipped').tooltip();
	
}

function initMapDeviceDetails(flat, flng, deviceName, zoom){
	
	var center = {lat: flat, lng: flng};
	
	if (typeof zoom === 'undefined')
		zoom = 16;
	
	if(center.lat < 1 && center.lng < 1)//near 0,0
		zoom = 2;
	
	var map = initMap(document.getElementById('map'), center, zoom, deviceName);
	
	marker = L.marker([flat, flng]).bindPopup(deviceName).addTo(map);	
	
	//addMarker(center, map, deviceName);
	
}









