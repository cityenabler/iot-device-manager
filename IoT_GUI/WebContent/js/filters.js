var device;

$(document).ready(function(){
	onDocReady();
	
	// re-initialize material-select
    $('select').formSelect();
    
	$('#cmd_message_textarea').trigger('autoresize');
	
});



function onDocReady(){
	mainOnDocReady();
	
	$('#NoDeviceToShowFilter').addClass("hide");
	$.each(filteredEntities, function (i,e){
		appendItems(e);
	})
	if (filteredEntities==null || filteredEntities=="") {
		
		// NO DATA FOUND
		
		$('#NoDeviceToShowFilter').removeClass("hide");
		
		M.toast({html:$('#msg_nodatafound').text(), classes:'red'});
		
	}
};



$(document).on('click', '.filterSearch', function(){
	$('#filterForm').submit();
	return false;
});

function appendItems(value){
	
	
	var filteredEntityType = value.type;
	var filteredEntityId = value.id;
	var filteredEntityName = value.name;
	var filteredEntityScope = value.scopeId;
	var filteredEntityScopeName = value.scopeName;
	var filteredEntityUrbanservice = value.urbanserviceId;
	var filteredEntityUrbanserviceName = value.urbanserviceName;
	var filteredEntityDataformat = value.dataformat;
	var filteredEntityTransportProtocol = value.transportProtocol;
	var filteredEntityRetrieveDataMode = value.retrieveDataMode;
	
	
	var url;
	var icon;
	var color;
	
	var template = $('#resultslist').find('.filtereditem.template');
	var item = template.clone();
	
	if (filteredEntityId!=null && filteredEntityId!="") {
		
		item.find(".filteredType").text(filteredEntityType);
		item.find(".filteredId").text(filteredEntityId);
		item.find(".filteredName").text(filteredEntityName);
	
		if (filteredEntityType == "scope" ){
			
//			icon = "business";
//			url = "urbanservices?scope=" + filteredEntityId;
//			color = "primary accent-4";
			
//			var typeToShow = $('#mgs_'+filteredEntityType).text();
//			item.find(".filteredType").text(typeToShow);
			
//			appendItem(filteredEntityType, item, icon, color, url);		
			
		} else if (filteredEntityType == "urbanservice"){
			
//			icon = "settings_remote";
//			url = "devices?scope=" + filteredEntityScope + "&urbanservice=" + filteredEntityId;
//			color = "primary accent-3";
			
//			item.find(".filteredScope").text(filteredEntityScopeName);
//			
//			var typeToShow = $('#mgs_'+filteredEntityType).text();
//			item.find(".filteredType").text(typeToShow);
			
//			appendItem(filteredEntityType, item, icon, color, url);		
			
		} else if (filteredEntityType == "Device"){
			icon = "settings_input_antenna";
			url = "#devicedetails";
			color = "primary accent-2";
		
			var a = item.find('a');
			a.addClass("modal-trigger");
			a.addClass("trigger-filterResult");
			
			a.attr('data-fiwareservice', filteredEntityScope);
			a.attr('data-fiwareservicepath', filteredEntityUrbanservice);
			a.attr('data-dataformat', filteredEntityDataformat);
			a.attr('data-transportProtocol', filteredEntityTransportProtocol);
			a.attr('data-retrieveDataMode', filteredEntityRetrieveDataMode);
			
			item.find(".filteredScope").text(filteredEntityScopeName);
			item.find(".filteredUrbanservice").text(filteredEntityUrbanserviceName);
			
			appendItem(filteredEntityType, item, icon, color, url);		
		}
	
																			
	
	} 
}

function appendItem(filteredEntityType, item, icon, color, url){
	
	item.find(".material-icons").text(icon).addClass(color);
	item.find('a').attr("href", url);
	
	var appender = $('#resultslist');
	appender.append(item);
	item.removeClass('template hide').addClass(filteredEntityType.toLowerCase());
}

// Switch
$(document).on('change', '.scopecheckbox', function(){
	if($('.scopecheckbox').attr('checked')) {
	    $('#resultslist').find('.filtereditem.scope').hide();
	    $('.scopecheckbox').attr('checked', false);
	} else {
		 $('#resultslist').find('.filtereditem.scope').show();
		 $('.scopecheckbox').attr('checked', true);
	}
	return false;
});

$(document).on('change', '.uscheckbox', function(){
	if($('.uscheckbox').attr('checked')) {
	    $('#resultslist').find('.filtereditem.urbanservice').hide();
	    $('.uscheckbox').attr('checked', false);
	} else {
		 $('#resultslist').find('.filtereditem.urbanservice').show();
		 $('.uscheckbox').attr('checked', true);
	}
	return false;
});

$(document).on('change', '.devicecheckbox', function(){
	if($('.devicecheckbox').attr('checked')) {
	    $('#resultslist').find('.filtereditem.device').hide();
	    $('.devicecheckbox').attr('checked', false);
	} else {
		 $('#resultslist').find('.filtereditem.device').show();
		 $('.devicecheckbox').attr('checked', true);
	}
	return false;
});

/** DEVICE DETAILS **/
$(document).on('click', '.trigger-filterResult', function(){
	var trigger = $(this);
	var deviceid = trigger.find('.filteredId').text();
	
	// Clean modal
	var modalselector = trigger.attr('href');
	var modal = $(modalselector);
	cleanupmodal(modal);
	
	// getDevice
	try{ 
		
		var fiwareservice = trigger.attr('data-fiwareservice');
		var fiwareservicepath = trigger.attr('data-fiwareservicepath');
		var dataformat = trigger.attr('data-dataformat');
		var transportprotocol = trigger.attr('data-transportprotocol');
		var retrievedatamode = trigger.attr('data-retrievedatamode');
		
		
		var headersAll = new Object();
			headersAll['fiware-service'] = fiwareservice;
			headersAll['fiware-servicepath'] = fiwareservicepath;
			
		var input = new Object();
			input['action'] = 'getDevice';
			input['deviceid'] = deviceid; 
			input['dataformat'] = dataformat;
			
//		$("#contentloader").fadeIn();
		toggleLoader(true, "trigger-filterResult");

		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data, textStatus, xhr) {
				
				device = Device.create(data);
				device.streamingURL = device.streaming_url;
				
				M.Modal.getInstance($("#devicedetails")).open();
				
				//on devicedetails.js
				deviceDetailsAction(device, trigger, deviceid, fiwareservice, "/"+fiwareservicepath, dataformat, transportprotocol, retrievedatamode );
				
				initMapDeviceDetails(device.getLatitude(), device.getLongitude(), device.name );
				
				return true;
			},
			error : function(xhr, status, error) {
				console.log(status);
				console.log(error);
				console.log(xhr);
				M.toast({html:$('#msg_iota_not_available').text(), classes:'red'});
			}
		}).always(function() {
//			$("#contentloader").fadeOut();
			toggleLoader(false, "trigger-filterResult");
		});
		
		
		
		
	} catch(error){
		console.log(error);
	}
	// end
	
	return true;

});



$('.modal').modal({
	onCloseEnd: cleanupmodal,
	inDuration: 500,
	outDuration: 500
});




function cleanupmodal(modal){
	var mapdom = $('#map');
	mapdom.html("");

	var attrappender = modal.find('.attrlist');
	attrappender.find('.attr').not('.template').remove();
	
	//Close WebSocket
	disconnect();

	//Close measureNotification WebSocket
	disconnectMeasureNotification();
}


///// DELETE

$(document).on('click', '.deletecurrentdevice', function(){
	var modal = $(this).closest('.modal');
	var deviceid = modal.attr('data-deviceid');
	
	deleteDevice(deviceid, function(data){
		console.log(data);
		
		var serv = device.category.parentCategory; 
		var path = device.category.id; 
		
		var queryparams = new Object();
			queryparams['success'] = true;
			queryparams['msg'] = $('#msg_device_deleted').text();
			queryparams['scope'] = serv; //get('scope');
			queryparams['urbanservice'] = path; //get('urbanservice');
		
		var querystring = $.param(queryparams);
		
		location.href="devices?"+querystring;
		
	});	
});

function deleteDevice(deviceid, callback){
	var devToBeDeleted = device;
	
	var serv = devToBeDeleted.category.parentCategory; 
	var path = devToBeDeleted.category.id; 
	var dataformat_protocol = devToBeDeleted.protocol;
	
	_deleteDevice(serv, path, deviceid, dataformat_protocol, devToBeDeleted, callback);
}



//editcurrentdevice
$(document).on('click', '.editcurrentdevice', function(){
	var modal = $(this).closest('.modal');
	var deviceid = modal.attr('data-deviceid');
	var deviceserial = JSON.stringify(device);
	$("#editdevice").attr("value", deviceserial); 
	
	$('#editForm').submit();
	return false;
});

$(document).on('submit', '#editForm', function(){
	//Add param to the action of the page newdevice in edit mode (editForm)
	var queryparams = new Object();
	queryparams['scope'] = device.category.parentCategory;
	queryparams['urbanservice'] = device.category.id; 
	
	var isCamera = $('#iscamera').val();
	
	var action = 'newdevice?'+$.param(queryparams);
	
	if (isCamera=="true")
		action = 'newcamera?'+$.param(queryparams);
	
	$("#editForm").attr("action", action);
	return true;
	
});

//copy currentdevice
$(document).on('click', '.copycurrentdevice', function(){
	var modal = $(this).closest('.modal');
	var deviceid = modal.attr('data-deviceid');
	
	var deviceserial = JSON.stringify(device);
	
	$("#copydevice").attr("value", deviceserial); 
	$('#copyForm').submit();
	
	return false;
});

$(document).on('submit', '#copyForm', function(){
	//Add param to the action of the page newdevice in edit mode (editForm)
	var queryparams = new Object();
	queryparams['scope'] = device.category.parentCategory;
	queryparams['urbanservice'] = device.category.id.replace("/", "");
	
	var isCamera = $('#iscamera').val();
	var action = 'newdevice?'+$.param(queryparams);
	
	if (isCamera=="true")
		action = 'newcamera?'+$.param(queryparams);
		
	$("#copyForm").attr("action", action);
	
	return true;
	
});


/* Device infopoint */
$(document).on('click', '.infopointcurrentdevice', function(){
	
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	
	return getInfopointCurrentDevice (deviceid,fiwareService, fiwareServicePath  );
		
	return false;
});



/* Get Scope Name */
function getServiceName(id){
	
	var scope = id;
	var input = new Object();
	input['action'] = 'getServiceName';
	input['id'] = scope; 

	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			$('.devicedetails._category .cat').text(capitalize(data));
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
		
	});
	
};

/** Sends Command to Device */
$(document).on('click', '.sendcommand', function(){
	
	var deviceid = $('#cmd_deviceId').text();
	var cmdname = $('#cmdselect :selected').val();
	var cmdMessage = $('#cmd_message_textarea').val();
	
	var modalinfo = $('#devicedetails');
	
	var dataformat = modalinfo.attr('data-dataformat');
	var transportprotocol = modalinfo.attr('data-transportprotocol');
	var retrievedatamode = modalinfo.attr('data-retrievedatamode');
	
	var scope = device.category.parentCategory; 
	var urbanservice = device.category.id; 
	
	sendCommandToDevice(scope, urbanservice, deviceid, cmdname, cmdMessage, dataformat, transportprotocol, retrievedatamode );

	
});

/* Device Command Infopoint */ 
$(document).on('click', '.commandinfopoint', function(){
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	var dataformat = modalinfo.attr('data-dataformat');
	
    $('select').formSelect();
   
    var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	return getCommandinfopoint (deviceid,fiwareService, fiwareServicePath, dataformat  );
	
	
});

/************************************************************** 
 *********************** SIMULATION  **************************
 **************************************************************/

/** Start simulator for all attributes of the device */
$(document).on('click', '.startsimdev_allAttrs', function(){
	
	var modalinfo = $(this).closest('.modal');
	var simdeviceid = modalinfo.attr('data-deviceid');
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	startSimulatorFullDevice(simdeviceid, fiwareService, fiwareServicePath);
});

/** Start simulator for all attributes of the device */
$(document).on('click', '.stopsimdev_allAttrs', function(){
	
	var modalinfo = $(this).closest('.modal');
	var simdeviceid =  modalinfo.attr('data-deviceid');
	
    var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	
	stopSimulatorFullDevice(simdeviceid, fiwareService, fiwareServicePath);
});


$(document).on('click', '#stopsimdev', function(){
	
	var simdeviceid = $("#device_id").val();
	var simattributeid = $("#measure_id").val();
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	var simulationLevel = "device";
	
	stopSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
	
	/** Check status simulator for all device */
	checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
});	

$(document).on('click', '.stopsimdev_default', function(){
	
	var trigger = $(this);
	var simattributeid = trigger.attr('data-attributeid');
	var modalinfo = $(this).closest('.modal');
	var simdeviceid =  modalinfo.attr('data-deviceid');
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	var simulationLevel = "device";
			
	stopSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
	
	/** Check status simulator for all device */
	checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
});



/*  configure simulation */ 
/* Device Simulation show configuration */ 
$(document).on('click', '.simulationconfig', function(){
	
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	
	// Open modal for configuring simulation
	var trigger = $(this);
	var attributeid = trigger.attr('data-attributeid');
	var attributetype = trigger.attr('data-attributetype');
	
	/* Check the simulation status at level of device attribute:
	 * Verify the device simulation status and change the image */
	simulationLevel = "device";
	checksDeviceStatusSimulator(simulationLevel, deviceid, attributeid, fiwareService, fiwareServicePath);
	/***************************************************/
	
		
    $('select').formSelect();
   
    /* Get default configuration */
    if(attributetype!="object"){
    	getSimulationConfig(deviceid, attributeid, attributetype, fiwareService, fiwareServicePath);
    }else{
    	M.toast({html:$('#msg_simulation_not_possible').text(), classes:'red'});
    }
    
      
	return false;
});


/** Start simulator for just one device attribute */
$(document).on('click', '#startsimdev', function(){
	
	var device_attribute = $("#measure_id").val();
	var simdeviceid =  $('#devicedetails').attr("data-deviceid");
	
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	var isValid = checkSimulationAttribute(simdeviceid, device_attribute, fiwareService, fiwareServicePath);
	
	simulationLevel = "device";
	simattributeid = device_attribute;
	
	if (isValid){
		startSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);	
		/** Check status simulator for single device */
		checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);

	}
	
});

/** Start simulator for just one device attribute with default values*/
$(document).on('click', '.startsimdev_default', function(){
	
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	var trigger = $(this);
	var attributeName = trigger.attr('data-attributeid');
	var attributeType = trigger.attr('data-attributetype');
	
	var simdevId =  $('#devicedetails').attr("data-deviceid");
	var isValid = checkSimulationAttribute(simdevId, attributeName, fiwareService, fiwareServicePath);
	var simulationLevel = "device";
	var simattributeid = attributeName;
	
	if (isValid){
		getDefaultValue(simulationLevel, simdevId, simattributeid,attributeType, fiwareService, fiwareServicePath );

		/** Check status simulator for single device */
		checksDeviceStatusSimulator(simulationLevel, simdevId, simattributeid, fiwareService, fiwareServicePath);
	}
});


$(document).on('click', '.stopsimdev_default', function(){
	
	var fiwareService = device.category.parentCategory; 
	var fiwareServicePath = device.category.id; 
	
	var trigger = $(this);
	var simattributeid = trigger.attr('data-attributeid');
	
	simdeviceid = device.id;
	simulationLevel = "device";
			
	stopSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
	
	/** Check status simulator for all device */
	checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, fiwareService, fiwareServicePath);
});



/*some called functions are on the file devicedetails.js that is included in the same page (filterResult.jsp & devices.jsp )*/