var device = {};
var checkedall = false;
var deviceperpage = 6;  //Default 6 per page;
var pages = 1;
var currpage = 1;
var devicelist = new Object();
var showdevice = new Device();
var numDevices = 0;
var mapcenter = {};
var scope = '';
var urbanservice = '';
var simdeviceid = "";
var simulationLevel = "";

var currentIdasDevice ;
var currentIdasFiwareService ;
var currentIdasFiwareServicePath;
var currentEnabler;
var checkedBadgeDeviceId = [];
var checkedBadgeDeviceIdOLD = [];
var checkedDevice = [];
var checkedDeviceOLD = [];
var foundRunning = false;
var foundNotRunning = false;

var myDropdownSelE;
var selContexts;
var selCategories;




$( window ).on( "load", function(){
	
	//Inizialize main nav-bar
	$("#slide-out-main").sidenav({
      //edge: 'right', // Choose the horizontal origin
      draggable: true // Choose whether you can drag to open on touch screens
    });

    
	$('#cmd_message_textarea').trigger('autoresize');
	
	/* initialize the modal to confirm the delete of a device*/
    $('.confirmDeleteDevice').modal();
    
    /* initialize the modal to confirm the delete of the selected devices (multiple)*/
    $('.confirmDeleteDevices').modal();
	
	/*inizialize Tooltip*/
    $('.tooltipped').tooltip();
    
    inizializeSelectsFilter()
    createSelectClearAllFilterBtns();
    loadCloseEventSelect();
    
});

$(document).ready(function(){
	onDocReady();
});


function appendItems(devicelist, page){
	
	if(page==0) return false;
	//console.log("page: "+page)
	$('#devicelist').find('.deviceitem').not('.template').remove();
	
	var sublist = devicelist;
	
	$.each( sublist, function( key, value ) {
		var template = $('#devicelist').find('.deviceitem.template');
		var item = template.clone();
		item.removeClass('template hide');
		item.attr('data-id', value.getId());
		
		var fiwareservice = value.getCategory().getParentCategoryId();
		var fiwareservicepath = value.getCategory().getId();
		
		var bigId = fiwareservice+"_"+fiwareservicepath +"_"+value.getId();
		
		item.attr('id', bigId );
		item.attr('data-name', value.getScreenId());
		item.attr('data-service', fiwareservice);
		item.attr('data-servicepath', fiwareservicepath);
		item.attr('data-dataformat', value.getDataformatProtocol());
		item.attr('data-transportprotocol', value.getTransport());
		item.attr('data-retrievedatamode', value.retrieveDataMode);
		item.attr('data-runningsimulation', value.runningSimulation);
		item.attr('data-isdeletable', value.permitDelete);
		item.attr('data-hubType', value.hubType);
		
		var isInActiveSimulation =false;
		if (activeSimulation.some(e => e.key === bigId)){
			item.attr('data-runningsimulation', true);
			item.find(".runningicon").removeClass("hide");
			
			isInActiveSimulation=true;//it is in array
		}
		
		var isInStopped = stoppedSimulation.some(e => e.key === bigId);
		
		if (value.runningSimulation == true){	
			item.find(".runningicon").removeClass("hide");
			
			//check if it is in array, in case it isn't then I add it
			if (!isInActiveSimulation && !isInStopped){
				
				activeSimulation.push({ 
			        key: bigId,
			        deviceId:name,
			        fiwareService: fiwareservice,
			        fiwareServicePath: fiwareservicepath
			    });
			}
		}	
		if (isInStopped){
			item.attr('data-runningsimulation', false);
			item.find(".runningicon").addClass("hide");
		}
		
		if (value.streaming_url != "")
			item.find(".cameraicon").removeClass("hide");
		
		item.find(".devicename").text(value.getScreenId());
		var checkbox = item.find('.selector .filled-in');
		var checkboxid = checkbox.attr('id');
		checkbox.attr('id', checkboxid + "_" + bigId);

		var label = item.find('.selector label');
		var labelfor = label.attr('for');
		label.attr('for', labelfor + "_" + bigId);
		
		var checkedChanged = false;
		if (checkedBadgeDeviceId.some(e => e.key === bigId)){
			
			item.find('.selector .filled-in').prop('checked', "true");
			checkedChanged=true;
		}	
		
		var cate = value.getCategory().getName().toLowerCase();
		item.find('.card-image').addClass(uslistcolor[cate]);
		item.find('img').attr('src', 'imgs/icons/dev_'+value.getCategory().getName().toLowerCase()+'.svg');
		item.find(".devContext").text(value.getCategory().getParentCategory());
		
		item.find(".devCategory").text(value.getCategory().getName());
		
		if (isMultienabler){
			var enab = value.getEnabler();
			enab = enab.charAt(0).toUpperCase() + enab.slice(1);
			item.find(".devEnabler").text(enab);
		}
		else
			item.find(".devEnabler").addClass("hide");
		
		
		// show delete device button if owner
		var deviceOwner=value.device_owner;  
		var permitDelete = value.permitDelete;
		if (permitDelete==true) 
			item.find(".deletesingledevice").removeClass('hide');
		else
			item.find(".filled-in").remove();//remove the possibility of selection for delete

		
		
		var appender = $('#devicelist .row');
		appender.append(item);
		
		if (checkedChanged)
			changeSelectorFilledIn();

	});
	
	/*inizialize Tooltip*/
    $('.tooltipped').tooltip();
    
    //close loader
    toggleLoader(false, "appendItems");
}



function setupPagination(){
//	var keys = Object.keys( devicelist );
	var nitems = numDevices;//keys.length;

	var npages = Math.ceil(nitems/deviceperpage);
	
	//console.log("npages "+npages )
	
	var template = $('.pagination .pageanchor.template');
	var prepender = $('.navigation.next').parent();

	if(npages==0){
		var prevbutton = $('.pagination a.navigation.prev').parent().addClass('disabled').removeClass('waves-effect');
		var nextbutton = $('.pagination a.navigation.next').parent().addClass('disabled').removeClass('waves-effect');
	}

	for(var i=0; i<npages; i++){
		var pageanchor = template.clone();
		pageanchor.removeClass('template hide');
		var a = pageanchor.find('a');
		a.attr('data-n', (i+1));
		a.text((i+1));

		if(i==0){
			pageanchor.addClass('firstpage active').removeClass('waves-effect');
		}

		if(i==npages-1){
			pageanchor.addClass('lastpage');
		}

		prepender.before(pageanchor);
	}

	$('.pagination .pageanchor.active > a').click();
}


function onDocReady(){
//	var myDropdownSelE;
//	var selContexts;
//	var selCategories;
	
	if(sessionStorage.getItem("myDropdownSelEnabler") !== null){
		toggleLoader(true, "onDocReady")
		myDropdownSelE = JSON.parse(sessionStorage.getItem("myDropdownSelEnabler"));
		$("#enablerSelDiv").find('input').val(myDropdownSelE);
	}
	currentEnabler = get('enabler');
	if (isMultienabler == false){
		$('.enablerSelection').remove();
		populateContext(defaultEnabler, false);
		
	}else{
		populateEnabler(myDropdownSelE);
	}
	
	mainOnDocReady();
	//TODO
	
	/** Start simulator for all selected devices */
	$(document).on('click', '#startsim', function(){
		startSimulatorAllDevices();
	});
	
	/** Start simulator for all attributes of the device */
	$(document).on('click', '.startsimdev_allAttrs', function(){
		var simdeviceid =  $('#devicedetails').attr("data-deviceid");
		startSimulatorFullDevice(simdeviceid, currentIdasFiwareService, currentIdasFiwareServicePath);
	});
	
	/** Start simulator for all attributes of the device */
	$(document).on('click', '.stopsimdev_allAttrs', function(){
		var simdeviceid =  $('#devicedetails').attr("data-deviceid");
		stopSimulatorFullDevice(simdeviceid, currentIdasFiwareService, currentIdasFiwareServicePath);
	});
	
	
	/** Start simulator for just one device attribute */
	$(document).on('click', '#startsimdev', function(){
		
		var device_attribute = $("#measure_id").val();
		var simdeviceid =  $('#devicedetails').attr("data-deviceid");
		
		var isValid = checkSimulationAttribute(simdeviceid, device_attribute, currentIdasFiwareService, currentIdasFiwareServicePath);
		
		simulationLevel = "device";
		simattributeid = device_attribute;
		
		if (isValid){
			startSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);	
			/** Check status simulator for single device */
			checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);

		}
		
	});
	
	/** Start simulator for just one device attribute with default values*/
	$(document).on('click', '.startsimdev_default', function(){
		
		var trigger = $(this);
		var attributeName = trigger.attr('data-attributeid');
		var attributeType = trigger.attr('data-attributetype');
		
		var simdevId =  $('#devicedetails').attr("data-deviceid");
		var isValid = checkSimulationAttribute(simdevId, attributeName, currentIdasFiwareService, currentIdasFiwareServicePath);
		var simulationLevel = "device";
		var simattributeid = attributeName;
		
		if (isValid){
			getDefaultValue(simulationLevel, simdevId, simattributeid,attributeType, currentIdasFiwareService, currentIdasFiwareServicePath );
	
			/** Check status simulator for single device */
			checksDeviceStatusSimulator(simulationLevel, simdevId, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
		}
	});

	
	/** Stop simulator for all device */
	$(document).on('click', '#stopsim', function(){
		stopSimulatorAllDevices();
	});
	
	/** Stops simulator for all selected devices */
	function stopSimulatorAllDevices(){

		var input = new Object();
		input['action'] = 'stop';
		
		
		// checkedBadgeDeviceId has all selected badge
		$.each(checkedBadgeDeviceId, function(index, value){
			
			var elem = $('#'+value.key);
			
			var deviceid = value.deviceId;
			var deviceservice = value.fiwareService;
			var deviceservicepath = value.fiwareServicePath;
						
			input['service'] = deviceservice;
			input['servicepath'] = deviceservicepath;
			
			var headersAll = new Object();
			headersAll['fiware-service'] = deviceservice;
			headersAll['fiware-servicepath'] = deviceservicepath;
			headersAll['deviceid'] = deviceid;
						
						
			$.ajax({
					url: 'iotsimhandler',
					type : 'GET',
					dataType : 'json',
					data : input,
					headers: headersAll,
					success : function(data) {
							
						elem.attr('data-runningsimulation', "false");
						elem.find(".runningicon").addClass("hide");
						
						activeSimulation = removeItemFromArray(activeSimulation,value.key );
						stoppedSimulation.push(value);
						checkedBadgeDeviceId = removeItemFromArray(checkedBadgeDeviceId,value.key );
						checkedBadgeDeviceIdOLD = removeItemFromArray(checkedBadgeDeviceIdOLD,value.key );
						
						delete checkedDevice[value.fiwareServicePath+value.deviceId];
						delete checkedDeviceOLD[value.fiwareServicePath+value.deviceId];
						
						resetGeneralSimulationButton ();
					},
						error : function(xhr, status, error) {
							console.log(error);
						}
				});
						
		});
				
		return true;     
	}
	
	//used when the user click on the alert, after he/she created a device in a context different than the selected on the filters.
	$(document).on('click', '#updateDeviceFilters', function() {
		
			var service = get('scope');
			var servicepathName = get('urbanserviceName');
			
			sessionStorage.setItem("myDropdownSelCategory","[\""+servicepathName+"\"]");
			sessionStorage.setItem("myDropdownSelContext","[\""+service+"\"]");
			
			var i = 0;
			$("#myDropdownSelEnabler").find("option").each(function() {
				
					if (i!=0)//the first is the disabled option
						$(this).prop('selected', true);
					
					i++;
			});
			
			var enablerUpdateDev =$('#myDropdownSelEnabler').val();
			sessionStorage.setItem("myDropdownSelEnabler", JSON.stringify($('#myDropdownSelEnabler').val()));
			
			//start the flow to populate enabler/contexts/categories that will read from the SessionStorage and customize the result
			populateEnabler(enablerUpdateDev);
			
			//inizializeSelectsFilter();
			
			toggleLoader(true, "updateDeviceFilters");
		
		return true;
	});
	
	/** Stop simulator for just one attribute of a device CUSTOM */
	$(document).on('click', '#stopsimdev', function(){
		simulationLevel = "device";
		
		var simdeviceid = $("#device_id").val();
		var simattributeid = $("#measure_id").val();
		
		stopSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
		
		/** Check status simulator for all device */
		checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
	});	
	
	$(document).on('click', '.stopsimdev_default', function(){
		
		var trigger = $(this);
		var simattributeid = trigger.attr('data-attributeid');
		
		simdeviceid = device.id;
		simulationLevel = "device";
				
		stopSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
		
		/** Check status simulator for all device */
		checksDeviceStatusSimulator(simulationLevel, simdeviceid, simattributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
	});
	/**************************************************/

	/*Select/unselect all badges management */
	$(document).on('click', '#selectalltrigger', function(){
		
		//in this case it's unselect all, then I clean the selected array
		if ($(this).hasClass("checked")){
			 checkedBadgeDeviceId = [];
		     checkedBadgeDeviceIdOLD = [];
		     checkedDevice = [];
		     checkedDeviceOLD = [];
		     
		     $('.selector .filled-in').not( "#filled-in-box").prop('checked', false).change();
		     
		}else{
			$('.selector .filled-in').not( "#filled-in-box").prop('checked', true).change();
		}
	
	});
	
	
	/*Checked badges management - UI trigger */
	$(document).on('change', '.selector .filled-in', function(){
		changeSelectorFilledIn();
	});

	
	/** DEVICE DETAILS **/
	$(document).on('click', '.modaltrigger', function(){
		var trigger = $(this);
		var deviceid = trigger.closest('.deviceitem').attr('data-id');
		var categoryid = trigger.closest('.deviceitem').attr('data-servicepath');
		var dataformat = trigger.closest('.deviceitem').attr('data-dataformat');
		var transportprotocol = trigger.closest('.deviceitem').attr('data-transportprotocol');
		var retrievedatamode = trigger.closest('.deviceitem').attr('data-retrievedatamode');
		device = devicelist[categoryid+deviceid];//OCD Device
		
		var streaming_url = device.streaming_url;
		
		var fiwareService = device.getCategory().getParentCategoryId();
		var fiwareServicePath = "/"+device.getCategory().getId()  ;
		
		currentIdasFiwareService = fiwareService;
		currentIdasFiwareServicePath = fiwareServicePath;
		
		
		try{ 
			var headersAll = new Object();
				headersAll['fiware-service'] = fiwareService;
				headersAll['fiware-servicepath'] = fiwareServicePath;
			
			var input = new Object();
				input['action'] = 'getDevice';
				input['deviceid'] = deviceid;
				input['dataformat'] = dataformat;
				
			toggleLoader(true, "DEVICE DETAILS modaltrigger");
		
			$.ajax({
				url:  'ajaxhandler',
				type : 'GET',
				dataType : 'json',
				data : input,
				headers : headersAll,
				success : function(data, textStatus, xhr) {
					
					device = Device.create(data);
					currentIdasDevice = device;
					currentIdasDevice.streamingURL=streaming_url;
															
					
					//on devicedetails.js
					deviceDetailsAction(device, trigger, deviceid, fiwareService, fiwareServicePath, dataformat, transportprotocol, retrievedatamode);
					
					var zoom = mapcenter.zoom;
					initMapDeviceDetails(device.getLatitude(), device.getLongitude(), device.name,zoom );
					
				},
				error : function(xhr, status, error) {
					console.log(status);
					console.log(error);
					console.log(xhr);
					M.toast({html:$('#msg_iota_not_available').text(), classes:'red'});
				}
				
			}).always(function() {
//				$("#contentloader").fadeOut();
				toggleLoader(false, "DEVICE DETAILS modaltrigger");
			});
		
	
			
		}
		catch(error){
			console.log(error);
		}
		
		return true;
	});

	//Initialization modals materialize
	$('#newdevicemassiveload_modal').modal();
	
	$('#devicedetails.modal').modal({
		preventScrolling: true,
		onCloseEnd: onModalClose,
		startingTop: '0%',
		endingTop: '0%'
	});

	
	$(document).on('click', '.pagination a', function(){
		
		
		var prevbutton = $('#pagination-long .pagination li:first');
		var nextbutton = $('#pagination-long .pagination li:last');
		
		var currentClick = ($(this));
		
		if(currentClick.parent().attr('data-page') == 1){
			prevbutton.addClass('disabled hide').removeClass('waves-effect');
		}else{
			prevbutton.removeClass('disabled hide').addClass('waves-effect');
		}
		
		if(currentClick.parent().attr('data-page') == pages){
			nextbutton.addClass('disabled hide').removeClass('waves-effect');
		}else{
			nextbutton.removeClass('disabled hide').addClass('waves-effect');
		}

		currpage = currentClick.parent().attr('data-page');
		appendItems(devicelist, currpage);

		$('.pagination li.active').toggleClass('active').toggleClass('waves-effect');
		currentClick.parent().toggleClass('active').toggleClass('waves-effect');

	});

	$(document).on('click', '.pagination li[data-page="prev"], .pagination li[data-page="next"]', function(){

			if($(this).hasClass('disabled')) return false;
	
			if($(this).attr('data-page')=='prev')
				currpage--;
			else if($(this).attr('data-page')=='next')
				currpage++;
	
			$('.pagination li.active').toggleClass('active').toggleClass('waves-effect');
	
			$('.pagination li[data-page="' + currpage + '"]').toggleClass('active').toggleClass('waves-effect');
	
			var prevbutton = $('.pagination li[data-page="prev"]');
			var nextbutton = $('.pagination li[data-page="next"]');
	
			var curractive = $('.pagination li.active');
			if(curractive.attr('data-page')==1)
				prevbutton.addClass('disabled hide').removeClass('waves-effect');
			else
				prevbutton.removeClass('disabled hide').addClass('waves-effect');
	
			if(curractive.attr('data-page')==pages)
				nextbutton.addClass('disabled hide').removeClass('waves-effect');
			else
				nextbutton.removeClass('disabled hide').addClass('waves-effect');
	
	});
	
	//to reinitialize material select
	$('.contextSel').on('contentContextChanged', function() {
	    $(this).formSelect({classes:"filterDevice"});
//	    createSelectClearAllFilterBtns()
	 });
	
	$('.contextCategorySel').on('contentCategoriesChanged', function() {
	    $(this).formSelect({classes:"filterDevice"});
//	    createSelectClearAllFilterBtns()
	 });
	
	//MB: add hubs
	$('.contextHubSel').on('contentHubsChanged', function() {
	    $(this).formSelect({classes:"filterDevice"});
	 });

	
	//to unselect all devices after that an user click on back button of the browser (CENABLER-873)
	$('.selector .filled-in').prop('checked',false);
	
	$('#pageloader').fadeOut(); //fade out page loader 
	
	//ROB 
//	toggleLoader(false, "onDocReady");
	
};// on doc ready


function changeSelectorFilledIn(){
	
	
	var checked = []
	checked = $('.selector .filled-in:checked');
	
	
	var notchecked = $('.selector .filled-in:not(:checked)');
	var notchecked2 = []
	notchecked2 = notchecked.slice();//copy the array to loop over it
	
	
	var notCheckedIdAr = []
	
	//remove the template item from array
	$.each(notchecked2, function(index, value){
		
		var isTemplate = $(value.closest('.deviceitem')).hasClass("template");
		
		if (isTemplate == true){
			notchecked.splice( index, 1 );
		}else{
			
			var closeItem= value.closest('.deviceitem');
			
			var notCkedId = $(closeItem).attr('id');
			var deviceIdB =closeItem.getAttribute("data-id");
			var fiwareServiceB = closeItem.getAttribute("data-service");
			var fiwareServicePathB = closeItem.getAttribute("data-servicepath");
			
			//useful to retrieve checked badge with pagination
			notCheckedIdAr.push({
		        key: notCkedId,
		        deviceId:deviceIdB,
		        fiwareService: fiwareServiceB,
		        fiwareServicePath: fiwareServicePathB
		    });
			
		}
	});
	
	checkedall = (notchecked.length == 0);//here notchecked doesn't have template

	var deletetrigger = $('#slide-out-main .deletetrigger');
	
	
	var checked2 = []
	checked2 = checked.slice();//copy the array to loop over it
	
	//remove the template item from array
	$.each(checked2, function(index, value){
		
		var isTemplate = $(value.closest('.deviceitem')).hasClass("template");
		
		if (isTemplate == true){
			checked.splice( index, 1 );
		}
	});
	
	checkedBadgeDeviceId = [];
	checkedDevice = [];
	
	//here checked doesn't have template
	$.each(checked, function(index, value){
		
		var closeItem= value.closest('.deviceitem');
		
		var badgeDeviceId = closeItem.getAttribute("id");
		var deviceIdB =closeItem.getAttribute("data-id");
		var fiwareServiceB = closeItem.getAttribute("data-service");
		var fiwareServicePathB = closeItem.getAttribute("data-servicepath");
		//var deleteDevEnabled = closeItem.getAttribute("data-isdeletable");
		
		checkedBadgeDeviceId.push({
	        key: badgeDeviceId,
	        deviceId: deviceIdB,
	        fiwareService: fiwareServiceB,
	        fiwareServicePath: fiwareServicePathB
	    });
		
		var currentDevice = devicelist[fiwareServicePathB+deviceIdB];//OCD Device
		 checkedDevice[fiwareServicePathB+deviceIdB]=currentDevice;
		 
	});
	
	
	
	
	//manage the other page checked
	$.each(checkedBadgeDeviceIdOLD, function(indexC, valueC){
			
			//	if it's in an other page, it isn't on notchecked array
			if ( !( notCheckedIdAr.some(e => e.key === valueC.key)     ||  checkedBadgeDeviceId.some(e => e.key === valueC.key) )  ){
			 	
				checkedBadgeDeviceId.push(valueC);
			 	
			 	var oldDevice = checkedDeviceOLD[valueC.fiwareServicePath+valueC.deviceId];
			 	checkedDevice[valueC.fiwareServicePath+valueC.deviceId]=oldDevice;
			} 	
	});
	
	
	//update the old array for next iteration
	checkedBadgeDeviceIdOLD = checkedBadgeDeviceId;
	checkedDeviceOLD = checkedDevice;
	
	$('.numberOfSelectedDevice').html(checkedBadgeDeviceId.length)		
	
	
	checkFoundRunningAndNot();
	manageFireSimulationButton();
	
	
	if (checkedBadgeDeviceId.length > 0){
		
		deletetrigger.removeClass('disabled');
		
		if (checkedall == true)
			$('#selectalltrigger').addClass('checked');
		
	}else{
		
		deletetrigger.addClass('disabled');
		$('#selectalltrigger').removeClass('checked');
	}
	
	
	
	
	
}

/* check if any selected device is running on notrunning about simulation */
function checkFoundRunningAndNot(){
	
	foundRunning = false;
	foundNotRunning = false;
	
	$.each(checkedBadgeDeviceId, function(indexC, valueC){
		
		 if (activeSimulation.some(e => e.key === valueC.key))
			  foundRunning = true;
		 else
			 foundNotRunning = true;
			 
		if (foundRunning && foundNotRunning)//break the loop for optimization
			return false;
	});
}



//Initialize filters Selects by inserting "Select All" and "Clear All" buttons
function createSelectClearAllFilterBtns(){
	
	$('#filterSelect ul.dropdown-content').each(function(i) {
		var sel = $(this);
		var btns = $('#selBtnsTemplate').html();
			
//		sel.find(' li.disabled').html(btns).removeClass('disabled').addClass('firstSel');
		sel.find(' li.disabled').html(btns).addClass('firstSel');
		
		sel.find('li.firstSel .selAll').click(function(){
			sel.find('li:not(.firstSel)').each(function(index) {
					var record = $(this);
					if(!record.hasClass("selected")) record.click();  
		  });
		});
		sel.find('li.firstSel .clrAll').click(function(){
			sel.find('li:not(.firstSel)').each(function(index) {
					var record = $(this);
					if(record.hasClass("selected")) record.click();  
		  }); 
		});
		
	});
}

/*manage the select action */
function loadCloseEventSelect(){
	
	$(".filterDevice").on('change', function() {
        var inputEl = $(this).parent().find('input.select-dropdown');
        inputEl.addClass("openSel")
    });
}

// Re-initialize filter select after population
function inizializeSelectsFilter(){
	$('select.filterDevice').formSelect({
		classes:"filterDevice",
		dropdownOptions: {
	        onCloseEnd: function(el) {
	            eventOfSelectFilter(el);
	        }
	    }
	});
	createSelectClearAllFilterBtns();
}

function clearSelectsFilters(context, category){
	
	if(context){
		$('#myDropdownSelContext').empty();
		$("#myDropdownSelContext").trigger('contentContextChanged');
		$('#contextSelDiv').attr("data-tooltip", $('#msg_no_filters').text());
	}
	if(category){
		$('#myDropdownSelCategory').empty();
		$("#myDropdownSelCategory").trigger('contentCategoriesChanged');
		$('#categorySelDiv').attr("data-tooltip", $('#msg_no_filters').text());
	}
}

function clearHubSelectsFilter(hub){
	
	if(hub){
		$('#myDropdownSelHub').empty();
		$("#myDropdownSelHub").trigger('contentHubChanged');
		$('#hubSelDiv').attr("data-tooltip", $('#msg_no_filters').text());
	}
}

//Creates a select tooltip with the selected values grouped by 3
function createTooltipSelect(selectDiv){
	
	var parentTooltip = selectDiv;
	var inputValue = selectDiv.find("input.select-dropdown.dropdown-trigger").val();
	var newString = '';
	
	if(inputValue!=''){
		
		for(i=l=0;i<inputValue.length;i++){
			if(inputValue[i] == ',') l++;
			if(inputValue[i] == ',' && l%3==0) {
				newString += "<br>";
			}else{
				newString += inputValue[i]
			}
		}
		parentTooltip.attr("data-tooltip", newString)
		
	}else{
		newString = '';
		parentTooltip.attr("data-tooltip", $('#msg_no_filters').text() )
	}
}

//Choose what function calls to fill next Filter Select
function eventOfSelectFilter(el){
	
   var inputSelect = $(el);
		
   if (inputSelect.val()  !== null && inputSelect.val()  !=="" ){
		
	   toggleLoader(true, "filter");
		
       if(inputSelect.hasClass("openSel")){
    	   inputSelect.removeClass("openSel")
           var curtain = inputSelect.parent().parent().attr("id");
    	   
    	   createTooltipSelect(inputSelect.parent().parent())
           
    	   if (curtain == "hubSelDiv"){
           	  var myDropdownSelH;
         	  if(sessionStorage.getItem("myDropdownSelHub") !== null && $('#myDropdownSelHub').val()== null){
         		    myDropdownSelH = JSON.parse(sessionStorage.getItem("myDropdownSelHub"));
         	  }
         	  else {
         		    myDropdownSelH =  $('#myDropdownSelHub').val();
         	  }
 			  sessionStorage.setItem("myDropdownSelHub", JSON.stringify(myDropdownSelH));
 			  
         	  populateDevices(0, deviceperpage, true, true);
 	          
            }else if (curtain == "categorySelDiv"){
          	  var myDropdownSelDiv;
        	  if(sessionStorage.getItem("myDropdownSelCategory") !== null && $('#myDropdownSelCategory').val()== null){
        		    myDropdownSelDiv = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
        	  }
        	  else {
        		    myDropdownSelDiv =  $('#myDropdownSelCategory').val();
        	  }
			  sessionStorage.setItem("myDropdownSelCategory", JSON.stringify(myDropdownSelDiv));
			  
        	  let bol = populateDevices(0, deviceperpage, true, true);
	          
           }else if (curtain == "contextSelDiv"){

        	  var myDropdownSelC;
        	  if(sessionStorage.getItem("myDropdownSelContext") !== null && $('#myDropdownSelContext').val()== null){
        		   myDropdownSelC = JSON.parse(sessionStorage.getItem("myDropdownSelContext"));
        	  	}
        	  else {
        		   myDropdownSelC =  $('#myDropdownSelContext').val();
        	  }
        	  sessionStorage.setItem("myDropdownSelContext", JSON.stringify(myDropdownSelC));
        	  
        	  let bol = populateCategories();
        	  
    	   	  $('#categorySelDiv').attr("data-tooltip", $('#msg_no_filters').text() );
    	   	  
          } else if (curtain == "enablerSelDiv"){
//				var myDropdownSelE;
				if(sessionStorage.getItem("myDropdownSelEnabler") !== null && $('#myDropdownSelEnabler').val()== null){
					myDropdownSelE = JSON.parse(sessionStorage.getItem("myDropdownSelEnabler"));
				}
				else {
					myDropdownSelE =  $('#myDropdownSelEnabler').val();
				}
				sessionStorage.setItem("myDropdownSelEnabler", JSON.stringify(myDropdownSelE));
				
				if(myDropdownSelE != undefined && myDropdownSelE.length > 0) {
					let bol = populateContext(myDropdownSelE, true, el);
				}else{
					clearSelectsFilters(true, true);
					inizializeSelectsFilter();
					noResultAction();
					toggleLoader(false, "filter");
				}
				
				$('#contextSelDiv').attr("data-tooltip", $('#msg_no_filters').text());
           }
       }else{
    	   toggleLoader(false, "filter");
       }
	}
}

function populateEnabler(currentEnabler){
	$('#myDropdownSelEnabler').prepend($('<option disabled>', {value:"", text:""}));
	
	if(currentEnabler !== undefined && currentEnabler.length > 0 ){
		jQuery.each(currentEnabler, function(index, item) {
			$('#myDropdownSelEnabler').find('option[value="'+item+'"]').prop('selected', true);
		}.bind(this));
		
		populateContext(currentEnabler, true); 
		createTooltipSelect($("#enablerSelDiv"));
	}else{
		clearSelectsFilters(true, true);
		inizializeSelectsFilter();
		toggleLoader(false, "populateEnabler");
	}
	
	noResultAction();
}



function populateContext(currentEnabler, isMultienabler, el){
	
	$('#devicelist').find('.deviceitem').not('.template').remove();
	devicelist = new Object();
	
	clearSelectsFilters(true, true); //clear context and category selects
	
	hideNavigationButton();
	
	var curEnab = currentEnabler;
	if (isMultienabler) curEnab=""+currentEnabler;
	
	try{ 
		var input = new Object();
		input['action'] = 'getServices';
		input['enabler'] = curEnab;
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				noResultAction();
				
				$('#myDropdownSelContext').append($('<option disabled>', {value:"", text:""}));
				
				var numContext = data.length;
				
				var contextSelected = false;
				
				if (numContext == 1){
            		//$('#myDropdownSelContext').append($('<option>', {value:e.id, text:e.name.value, selected:true})); // for service entity from Orion
                	$('#myDropdownSelContext').append($('<option>', {value:data[0].service.value, text:data[0].name.value, selected:true}));// for service entity from Tenant Manager
                		
            		contextSelected =true;
            	}else{
				
		                $.each(data, function(i, e){
		                	var elEnabler = e.refScope;
		                	//$('#myDropdownSelContext').append($('<option>', {value:e.id, text:e.name.value })); // for service entity from Orion
                		$('#myDropdownSelContext').append($('<option>', {value:e.service.value, text:e.name.value })); // for service entity from Tenant Manager
						});
		                
		                
		                var currentContext;
	            		  if(sessionStorage.getItem("myDropdownSelContext") !== null){
	            			  currentContext = JSON.parse(sessionStorage.getItem("myDropdownSelContext"));
	            			  contextSelected =true;
	            		  	}
	            		   
	            		if(currentContext !== undefined){
	            			jQuery.each(currentContext, function(index, item) {
	            				$('#myDropdownSelContext').find('option[value="'+item+'"]').attr('selected', true);
	            			}.bind(this));
	            		}
                
            	}
                
                
                $("#myDropdownSelContext").trigger('contentContextChanged');
                inizializeSelectsFilter()

                if (numContext >= 1) {  
                	populateHubs();
                	populateCategories();	//It also populates the devices despite the hubs
                }
                
                createTooltipSelect($("#contextSelDiv"));
			},
			error : function(xhr, status, error) {
				toggleLoader(false, "populateContext error");
				console.log(error);
				return false;
			}
		});
	}
	catch(error){
		console.log(error);
	}
	return false;
}

function populateCategories(){
		
	$('#devicelist').find('.deviceitem').not('.template').remove();
	devicelist = new Object();
	
	var myDropdownSelV =  $('#myDropdownSelContext').val();
	
	clearSelectsFilters(false, true); //clear category select
	
	hideNavigationButton();
	
	var input = new Object();
	input['action'] = 'getCategories';
	input['services'] = ''+myDropdownSelV;
	
		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				
				noResultAction();
				$('#myDropdownSelCategory').append($('<option disabled>', {value:"", text:""}));
				
				var currentCategories;
				if(sessionStorage.getItem("myDropdownSelCategory") !== null){
					currentCategories = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
				}
				$.each(data, function(i, e){

        		if(currentCategories !== undefined){
        			if(currentCategories.includes(e))
        				$('#myDropdownSelCategory').append($('<option>', {value:e, text:e, selected: true}));
        			else
            			$('#myDropdownSelCategory').append($('<option>', {value:e, text:e, selected: false}));
        		}
        		else
        			$('#myDropdownSelCategory').append($('<option>', {value:e, text:e, selected: true}));
				});                                                          
				
				$("#myDropdownSelCategory").trigger('contentCategoriesChanged');
				inizializeSelectsFilter()
				 
				/*start population of devices */
				populateDevices(0, deviceperpage, true, true);
				
				createTooltipSelect($("#categorySelDiv"));
			},
			error : function(xhr, status, error) {
				toggleLoader(false, "populateCategories error");
				console.log(error);
				return false;
			}
		});
	
		return false;
}

function populateHubs(){
	
	$('#devicelist').find('.deviceitem').not('.template').remove();
	devicelist = new Object();
	
	var myDropdownSelV =  $('#myDropdownSelCategory').val();
	
	clearHubSelectsFilter(true); //clear hub select
	
	hideNavigationButton();
	
	var input = new Object();
	input['action'] = 'getHubs';
	input['categories'] = ''+myDropdownSelV;
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			
			noResultAction();
			$('#myDropdownSelHub').append($('<option disabled>', {value:"", text:""}));
			
			var currentHubs;
			if(sessionStorage.getItem("myDropdownSelHub") !== null){
				currentHubs = JSON.parse(sessionStorage.getItem("myDropdownSelHub"));
			}
			$.each(data, function(i, e){
    		if(currentHubs !== undefined){
    			if(currentHubs.includes(e))
    				$('#myDropdownSelHub').append($('<option>', {value:e.name, text:e.name, selected: true}));
    			else
        			$('#myDropdownSelHub').append($('<option>', {value:e.name, text:e.name, selected: false}));
    		}
    		else
    			$('#myDropdownSelHub').append($('<option>', {value:e.name, text:e.name, selected: true}));
			});                                                          
			
			$("#myDropdownSelHub").trigger('contentHubsChanged');
			inizializeSelectsFilter()
			 
			/*start population of devices */
			//populateDevices(0, deviceperpage, true, true);
			//MB: add hubs - don't change previous device population from categories, to be changed only if hubs selection changes
			
			createTooltipSelect($("#hubSelDiv"));
		},
		error : function(xhr, status, error) {
			toggleLoader(false, "populateHubs error");
			console.log(error);
			return false;
		}
	});

	return false;
}

function populateDevices(start, size, setupPagination, forceReloadingDevices){
	
	$('#devicelist').find('.deviceitem').not('.template').remove();
	devicelist = new Object();
	selContexts =  $('#myDropdownSelContext').val();

	  if(sessionStorage.getItem("myDropdownSelCategory") !== null && $('#myDropdownSelCategory').val()== null){
		  selCategories = JSON.parse(sessionStorage.getItem("myDropdownSelCategory"));
	  	}
	  else {
		  selCategories =  $('#myDropdownSelCategory').val();
	  }
	 
	  if (typeof selCategories === 'undefined' || selCategories==""){
		  
		  toggleLoader(false, "populateDevices");
		  noResultAction();
		  return false;
	  }else{
		  sessionStorage.setItem("myDropdownSelCategory", JSON.stringify(selCategories));
	  }	  
	  
	//TODO: check session before, like for myDropdownSelCategory
	  selHubs = $('#myDropdownSelHub').val();
	  
	hideNavigationButton();

	//Populate the devicelist through REST API invocation
	try{ 
		var input = new Object();
		input['action'] = 'getDeviceListMulticontext';
		input['selContexts'] = ""+selContexts;
		input['selCategories'] = ""+selCategories;
		input['selHubs'] = "" +selHubs;
		input['start'] = ""+start;
		input['size'] = ""+size;
		input['reloadDevices'] = ""+forceReloadingDevices;
		
		$.ajax({
			url:  'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data, textStatus, xhr) {
				
				var total = xhr.getResponseHeader("totalCount");
				numDevices = total;
				
				noResultAction();
				
				if (total == 0)
					$('#NoDeviceToShow').removeClass("hide");
				
				$.each(data, function(i, e){
					
					var cd = Device.create(e);
					devicelist[cd.getCategory().getId()+cd.getId()] = cd;
				});
				
				deviceResultAction(data);

				if(setupPagination) paginationSetup(devicelist, total); 
				
				appendItems(devicelist, currpage);
				
				manageFireSimulationButton();
				
				
				
				manageSuccessMessage(true);
				
				
				
			},
			error : function(xhr, status, error) {
				toggleLoader(false, "populateDevices error");
				console.log(status);
				console.log(error);
				console.log(xhr);
				
				M.toast({html:$('#msg_iota_not_available').text(), classes:'red'});
			}
		});
		
	}
	catch(error){
		console.log(error);
	}
	return false;
}





var openedInfoWindow = null;

function initDeviceFullMap(){
 document.getElementById('context-map').innerHTML = "<div id='mapDevices' style='width: 100%; height: 100%;'></div>";

	var mapTitleH = $('#mapTitle').outerHeight(true);
	var h = window.innerHeight;
	var mapOffset = $("#map-container").offset();
	var hColums = h - mapOffset.top - mapTitleH - $("footer").outerHeight(true);
	
	$("#context-map").css("height", hColums-mapTitleH + 50 + "px");
	$("#catlist").css("margin", "2px 0px 0px 0px");
	$(".catitem .block").css("max-height", (hColums/3)-150+"px")
	
	var mbAttr = '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors ',
	mbUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	
	var streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr, minZoom: 2}); //base layer
	var map = new L.Map('mapDevices', {
		zoom: 2, 
		center: new L.latLng([37.49, 13.26]),
		layers: streets
	});
	map.setView([37.49, 13.26], 2);

	//Populate the map with devicelist
	var markerArray = [];
	
		for (var key in devicelist) {
	           var device = devicelist[key];
	           
	           var lat,lng,deviceName,enabler,runningSimulation;
	           try  {
	        	   //TODO show simulation status on map
//	       			var fiwareService = device.getCategory().getParentCategoryId();
	       			var fiwareservicepath = device.getCategory().getId()
//	       			var cardId = fiwareservice+"_"+fiwareservicepath +"_"+device.getId()
	        	   
					lat = device.latitude;
					lng = device.longitude;
					deviceName  =  device.name;
					deviceId = device.id;
					runningSimulation = device.runningSimulation;
					
					
					if((lat != 0 && lat != undefined) && (lng != 0 && lat != undefined)) {
						var marker  = [lat,lng];
						markerArray.push(marker);
						L.marker(marker)
						.bindPopup('<div class="deviceitem" data-servicepath="'+fiwareservicepath+'" data-id="' + deviceId + '">' + deviceName + '<br /><a class="modaltrigger modal-trigger" href="#devicedetails" data-deviceid="' + deviceId + '">Show Details</a></div>')
						.addTo(map);
						
					}
				} catch(e){}
	        }

		map.fitBounds(markerArray);
}

function onModalClose(modal){
	//Close WebSocket
	disconnect();

	//Close measureNotification WebSocket
	disconnectMeasureNotification();
	
	//Clean map
	cleanMap(modal);
	
	//Empty attributes list
	emptyattrs(modal);
	
	//ResetLights
	resetLights();
	
	var modalId = $(modal).attr("id");
	
	//newdevicemassiveload_modal id other modal
	
	if (modalId == "devicedetails"){
		
		var isCamera = $('#iscamera').val();
		
		if (isCamera == "false"){
			var deviceid = $('#devicedetails').attr("data-deviceid");
			var fiwareservice = $('#devicedetails').attr("data-fiwareservice");
			var fiwareservicepath = $('#devicedetails').attr("data-fiwareservicepath");
			
			checksDeviceStatusSimulatorAtLeastOne(deviceid, fiwareservice, fiwareservicepath, function(){
				changeSelectorFilledIn();
			});
			
			
			
		}
		
	}	
}





function emptyattrs(modal){
	var attrappender = $(modal).find('.attrlist');
	attrappender.find('.attr').not('.template').remove();
}

function cleanMap(modal){
	
	var mapdom = $('#map');
	mapdom.html("");
	
}

$(document).on('click', '#visualizemap', function(){
	
	$('#listvisualization').addClass('hide');
	$('#mapvisualization').removeClass('hide');
	
	$('#visualizemap').addClass('hide');
	$('#visualizebadges').removeClass('hide');
	
	initDeviceFullMap();
});



$(document).on('click', '#visualizebadges', function(){
	
	actionVisualizeList();
	
});



$(document).on('click', '.multipledeletedevice', function(){

	//list of selected devices to delete in order to show in alert
	var appender = $('#appenderDevicesToDelete');
	$(appender).empty();
	var kapuaPresent = false;
	$.each(checkedBadgeDeviceId, function(index, value){
		
		var deviceid = value.deviceId;
		var category = value.fiwareServicePath;
		
		var devKey = category+deviceid;
		devToBeDeleted = checkedDevice[devKey];
		
		var serv = devToBeDeleted.category.parentCategory 
		var path = devToBeDeleted.category.name; 
		var htmlApp = "<p><b>"+deviceid+"</b> ("+serv+", "+path+")</p>"
		appender.append(htmlApp);
		
		var hubType = devToBeDeleted.hubType;
		if(hubType === 'kapua') {
			kapuaPresent = true;
		}
	});
	if(kapuaPresent) {
		$('#modalMultipleDeleteConfirmText').addClass("hide");
		$('#modalMultipleDeleteConfirmTextKapua').removeClass("hide");
	} else {
		$('#modalMultipleDeleteConfirmTextKapua').addClass("hide");
		$('#modalMultipleDeleteConfirmText').removeClass("hide");
	}
	
	$('#confirmDeleteDevices').modal('open');
	
});


//Multiple delete
$(document).on('click', '.devicesToDeleteModalYes', function(){
	
//	var selItems = $('.deviceitem:not(.template) input[type="checkbox"]:checked');
	
	var devToBeDeleted ;
	// checkedBadgeDeviceId has all selected badge
	$.each(checkedBadgeDeviceId, function(index, value){
		//var elem = $(this).closest('.deviceitem');
		
		var deviceid = value.deviceId;
		var deviceservice = value.fiwareService;
		var category = value.fiwareServicePath;
		
		
//		var deviceid = elem.attr('data-id');
//		var category = elem.attr('data-servicepath');
		
		var devKey = category+deviceid;
		
		
		//devToBeDeleted = devicelist[devKey];
		
		devToBeDeleted = checkedDevice[devKey];
		
		deleteDevice(devKey, function(data){
			delete devicelist[category+deviceid];
			delete checkedDevice[category+deviceid];
			
			var elem = $('#'+value.key);
			
			if (elem !=undefined)
				elem.remove();
		});
	});
	
	
	var serv = devToBeDeleted.category.parentCategoryId; 
	var path = devToBeDeleted.category.id; 
	
	var queryparams = new Object();
		queryparams['success'] = true;
		queryparams['msg'] = $('#msg_devices_deleted').text();
		queryparams['scope'] = serv;
		queryparams['urbanservice'] = path;
	
	var querystring = $.param(queryparams);

	location.href="devices?"+querystring;

});


//Single delete, to open the modal
$(document).on('click', '.deletedevice', function(){
	
	$('#confirmDeleteDevice').modal('open');
	
	var item = $(this).closest('.deviceitem');
	var deviceid = item.attr('data-id');
	
	var devicename = item.attr('data-name');
	var service = item.attr('data-service');
	var servicepath = item.attr('data-servicepath');
	var hubType = item.attr('data-hubType');
	
	if(hubType === 'kapua') {
		$('#modalDeleteConfirmText').addClass("hide");
		$('#modalDeleteConfirmTextKapua').removeClass("hide");
	} else {
		$('#modalDeleteConfirmTextKapua').addClass("hide");
		$('#modalDeleteConfirmText').removeClass("hide");
	}
	
	$('#deviceToDeleteModal').html(devicename);
	$('#deviceToDeleteModal').attr('data-id', deviceid);
	$('#deviceToDeleteModal').attr('data-service', service);
	$('#deviceToDeleteModal').attr('data-servicepath', servicepath);
	
});

//Single delete WITH CONFIRM	
$(document).on('click', '.deviceToDeleteModalYes', function(){
	
	var deviceid= $('#deviceToDeleteModal').attr('data-id');
	var service= $('#deviceToDeleteModal').attr('data-service');
	var servicepath= $('#deviceToDeleteModal').attr('data-servicepath').replace("/", "");
	
	var devKey = servicepath+deviceid;
	
	deleteDevice(devKey, function(data){
		
		var queryparams = new Object();
			queryparams['success'] = true;
			queryparams['msg'] = $('#msg_device_deleted').text();
			queryparams['scope'] = service;
			queryparams['urbanservice'] = servicepath;
		
		var querystring = $.param(queryparams);
		location.href="devices?"+querystring;
	});
});



//delete from deviceDetail
$(document).on('click', '.deletecurrentdevice', function(){
	
	$('#confirmDeleteDevice').modal('open');
	
	var modalDevice = $('#devicedetails');
	
			
	var deviceid = modalDevice.attr('data-deviceid');
	var devicename = modalDevice.attr('data-devicename');

	var service = modalDevice.attr('data-fiwareservice');
	var servicepath = modalDevice.attr('data-fiwareservicepath');
	
	var hubType = modalDevice.attr('data-hubType');
	
	if(hubType === 'kapua') {
		$('#modalDeleteConfirmText').addClass("hide");
		$('#modalDeleteConfirmTextKapua').removeClass("hide");
	} else {
		$('#modalDeleteConfirmTextKapua').addClass("hide");
		$('#modalDeleteConfirmText').removeClass("hide");
	}
	
	$('#deviceToDeleteModal').html(devicename);
	$('#deviceToDeleteModal').attr('data-id', deviceid);
	$('#deviceToDeleteModal').attr('data-service', service);
	$('#deviceToDeleteModal').attr('data-servicepath', servicepath);
	
});



/*Delete the device */
function deleteDevice(deviceKey, callback){
	
	var devToBeDeleted = devicelist[deviceKey];
	
	if (typeof devToBeDeleted === "undefined")
		devToBeDeleted = checkedDevice[deviceKey];
	
	var serv = devToBeDeleted.category.parentCategoryId;
	var path = devToBeDeleted.category.id; 
	var dataformat_protocol = devToBeDeleted.protocol;
	var device_owner = devToBeDeleted.device_owner;
	_deleteDevice(serv, path, devToBeDeleted.id, dataformat_protocol, devToBeDeleted, callback);
	
}



//editcurrentdevice
$(document).on('click', '.editcurrentdevice', function(){
	var modal = $(this).closest('.modal');
	var deviceid = modal.attr('data-deviceid');
	currentIdasFiwareService = modal.attr('data-fiwareService');
	currentIdasFiwareServicePath = modal.attr('data-fiwareServicePath');
	
	var deviceserial = JSON.stringify(currentIdasDevice);
	
	$("#editdevice").attr("value", deviceserial); 
	
	$('#editForm').submit();
	return false;
});

$(document).on('submit', '#editForm', function(){
	//Add param to the action of the page newdevice in edit mode (editForm)
	var queryparams = new Object();
	queryparams['scope'] = currentIdasFiwareService;
	queryparams['urbanservice'] = currentIdasFiwareServicePath.replace("/", "");
	
	
	var isCamera = $('#iscamera').val();
	
	var action = 'newdevice?'+$.param(queryparams);
	
	if (isCamera=="true")
		action = 'newcamera?'+$.param(queryparams);
		
	$("#editForm").attr("action", action);
	return true;
	
});

//copy currentdevice
$(document).on('click', '.copycurrentdevice', function(){
	var modal = $(this).closest('.modal');
	var deviceid = modal.attr('data-deviceid');
	var category = modal.attr('data-fiwareservicepath').slice(1);
	var deviceOCB = devicelist[category+deviceid];
	var fiwareService = deviceOCB.getCategory().getParentCategory().toLowerCase().replace(" ","_");
	var fiwareServicePath = "/"+deviceOCB.getCategory().getId();
	
	
	var deviceserial = JSON.stringify(currentIdasDevice);
	$("#copydevice").attr("value", deviceserial); 
	$('#copyForm').submit();
	
	return false;
});

$(document).on('submit', '#copyForm', function(){
	//Add param to the action of the page newdevice in edit mode (editForm)
	var queryparams = new Object();
	queryparams['scope'] = currentIdasFiwareService;
	queryparams['urbanservice'] = currentIdasFiwareServicePath.replace("/", "");
	
	var isCamera = $('#iscamera').val();
	var action = 'newdevice?'+$.param(queryparams);
	
	if (isCamera=="true")
		action = 'newcamera?'+$.param(queryparams);
		
	$("#copyForm").attr("action", action);
	
	return true;
	
});


/* Device infopoint */
$(document).on('click', '.infopointcurrentdevice', function(){
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	var fiwareService = modalinfo.attr('data-fiwareService');
	var fiwareServicePath = modalinfo.attr('data-fiwareServicePath');
	return getInfopointCurrentDevice (deviceid,fiwareService, fiwareServicePath  );
		
});

/* Device Command Infopoint */ 
$(document).on('click', '.commandinfopoint', function(){
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	var dataformat = modalinfo.attr('data-dataformat');
	
    $('select').formSelect();
   
    return getCommandinfopoint (deviceid,currentIdasFiwareService, currentIdasFiwareServicePath, dataformat );
	
});


/** Sends Command to Device */
$(document).on('click', '.sendcommand', function(){
	
	
	var deviceid = $('#cmd_deviceId').text();
	var cmdname = $('#cmdselect :selected').val();
	var cmdMessage = $('#cmd_message_textarea').val();
	
	var modalinfo = $('#devicedetails');
	
	var dataformat = modalinfo.attr('data-dataformat');
	var transportprotocol = modalinfo.attr('data-transportprotocol');
	var retrievedatamode = modalinfo.attr('data-retrievedatamode');
	
	sendCommandToDevice(currentIdasFiwareService, currentIdasFiwareServicePath, deviceid, cmdname, cmdMessage, dataformat, transportprotocol, retrievedatamode  );
	
});


//Massive device loading - open modal to select file
$(document).on('click', '.fileupload_open', function(){
	
	var userid = connectedUserId;
	
	$('#userid').val(userid);
	$('#newdevicemassiveload_modal').modal('open');
		
});


function uploadFile(_file)
{
	var files = _file.files;
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });
    
    postFilesData(data); 
 }

function postFilesData(data,headersAll){

	var input = new Object();
	input['file'] = data;
		
	
 $.ajax({
    url: 'massiveloadhandler',
    type: 'POST',
    data: input,
    headers : headersAll,
    cache: false,
    dataType: 'json',
    processData: false, 
    contentType: false, 
    success: function(data, textStatus, jqXHR)
    {
		M.toast({html:$('#msg_fileupload_success').text(), classes:'green'});
    },
    error: function(jqXHR, textStatus, errorThrown)
    {
    	console.log(error);
		M.toast({html:$('#msg_fileupload_failure').text(), classes:'red'});
    }
    });
}




function startSimulatorAllDevices(){
	
	var input = new Object();
	input['action'] = 'start';
	
	
	// checkedBadgeDeviceId has all selected badge
	$.each(checkedBadgeDeviceId, function(index, value){
		
		var elem = $('#'+value.key);
		
		var deviceid = value.deviceId;
		var deviceservice = value.fiwareService;
		var deviceservicepath = value.fiwareServicePath;
		
		input['service'] = deviceservice;
		input['servicepath'] = deviceservicepath;
		
		var headersAll = new Object();
		headersAll['fiware-service'] = deviceservice;
		headersAll['fiware-servicepath'] = deviceservicepath;
		headersAll['deviceid'] = deviceid;
		
		$.ajax({
			url: 'iotsimhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers: headersAll,
			success : function(data) {
				
				elem.attr('data-runningsimulation', "true");
				elem.find(".runningicon").removeClass("hide");
				
				
				activeSimulation.push(value);
				checkedBadgeDeviceId = removeItemFromArray(checkedBadgeDeviceId,value.key );
				checkedBadgeDeviceIdOLD = removeItemFromArray(checkedBadgeDeviceIdOLD,value.key );
				
				delete checkedDevice[value.fiwareServicePath+value.deviceId];
				delete checkedDeviceOLD[value.fiwareServicePath+value.deviceId];
				
				
				resetGeneralSimulationButton ();
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
		
	});
	
	
}





function resetGeneralSimulationButton (){
	
	$('#startsim').addClass("disabled");
	$('#stopsim').addClass("disabled");
	$('.selector .filled-in').prop('checked',false);
	
	$('#selectalltrigger').removeClass('checked');
	checkedall = false;
	
	$('.numberOfSelectedDevice').html("0");
	$('#slide-out-main .deletetrigger').addClass('disabled');
	
}

function manageFireSimulationButton(){
	
	if (checkedBadgeDeviceId.length > 0){
		
		
		if (foundRunning)
			$('#stopsim').removeClass('disabled');
		else
			$('#stopsim').addClass('disabled');
			 
		if	 (foundNotRunning)
			$('#startsim').removeClass('disabled');
		else
			$('#startsim').addClass('disabled');
		
		
	}else{
		
		$('#startsim').addClass('disabled');
		$('#stopsim').addClass('disabled');
	}
	
	return true;
	
}



/*  configure simulation */ 
/* Device Simulation show configuration */ 
$(document).on('click', '.simulationconfig', function(){
	
	
	var modalinfo = $(this).closest('.modal');
	var deviceid = modalinfo.attr('data-deviceid');
	
	// Open modal for configuring simulation
	var trigger = $(this);
	var attributeid = trigger.attr('data-attributeid');
	var attributetype = trigger.attr('data-attributetype');
	
	/* Check the simulation status at level of device attribute:
	 * Verify the device simulation status and change the image */
	simulationLevel = "device";
	checksDeviceStatusSimulator(simulationLevel, deviceid, attributeid, currentIdasFiwareService, currentIdasFiwareServicePath);
	/***************************************************/
	
		
    $('select').formSelect();
   
    /* Get default configuration */
    if(attributetype!="object"){
    	getSimulationConfig(deviceid, attributeid, attributetype, currentIdasFiwareService, currentIdasFiwareServicePath);
    }else{
    	M.toast({html:$('#msg_simulation_not_possible').text(), classes:'red'});
    }
    
      
	return false;
});



/** Check if attribute is valid */
$("#measure_id").change(function(){
	var device_attribute = $("#measure_id").val();
	checkSimulationAttribute(device.id, device_attribute, currentIdasFiwareService, currentIdasFiwareServicePath);
});



/** Materialize Pagination **/

function paginationSetup(devlist, numDevices){
//	numDevices = Object.keys(devlist).length;
	pages = Math.ceil(numDevices/deviceperpage);
	//console.log("paginationSetup Pages: "+pages)

	$('#pagination-long').empty();
	$('#pagination-long').materializePagination({
	    align: 'left',
	    lastPage: pages,
	    firstPage: 1,
	    useUrlParameter: false,
	    onClickCallback: function(requestedPage) {
	       
	       var start = deviceperpage*(requestedPage-1);
	       populateDevices(start, deviceperpage, false, false)
	    }
	});
	
	var curractive = $('.pagination li.active');
	
	var curractive = $('.pagination li.active');
	
	if(curractive.attr('data-page')=="1"){
		$('.pagination li[data-page="prev"]').addClass('disabled hide').removeClass('waves-effect');
	}
	if(pages<="1"){
		$('.pagination').addClass( 'hide');
		$('.pagination li[data-page="prev"]').addClass('disabled hide').removeClass('waves-effect');
		$('.pagination li[data-page="next"]').addClass('disabled hide').removeClass('waves-effect');
	}
		
}

function positionCurrentpage(olddevperpages, newdevxpages){
	var position = 0;
	var delta = 0;
	var p = parseInt(pages),  c = parseInt(currpage);
	
	if(olddevperpages > newdevxpages) {
		delta = parseInt(p - c - 1);
		
	}else if(olddevperpages < newdevxpages) {
		delta = parseInt(p - c);
	}
	//console.log("delta: "+delta+" Sign: "+Math.sign(delta))
	
	if(Math.sign(delta)==-1) position = parseInt(c + delta);
	else if(Math.sign(delta)==1) position = parseInt(c - delta)
	
	if(delta==0 || position==0) position=1;
	return position;
}

$(document).on('click', '.deviceperpage', function(){
	$("#deviceperpage").children().removeClass("active"); //inizialize
	$(this).addClass("active");
	var OLDdeviceperpage = deviceperpage;
	deviceperpage = $(this).html();
	paginationSetup(devicelist, numDevices);
	currpage = positionCurrentpage(OLDdeviceperpage, deviceperpage);
	
	var start = deviceperpage * ( currpage - 1); 
	populateDevices(start, deviceperpage, true, false);
	
	$('.pagination li.active').toggleClass('active').toggleClass('waves-effect');
	
	$('.pagination li[data-page="' + currpage + '"]').toggleClass('active').toggleClass('waves-effect');

	var prevbutton = $('.pagination li[data-page="prev"]');
	var nextbutton = $('.pagination li[data-page="next"]');

	var curractive = $('.pagination li.active');
	if(curractive.attr('data-page')==1)
		prevbutton.addClass('disabled hide').removeClass('waves-effect');
	else
		prevbutton.removeClass('disabled hide').addClass('waves-effect');

	if(curractive.attr('data-page')==pages)
		nextbutton.addClass('disabled hide').removeClass('waves-effect');
	else
		nextbutton.removeClass('disabled hide').addClass('waves-effect');
});


function noResultAction(){
	
	$('#noresults').removeClass("hide");
	$('#startsim').addClass('disabled');
	$('#selectalltrigger').addClass('disabled');
	$('#visualizemap').addClass('disabled');
	$('#totalNUmberOfDevice').text("0");
	$('#NoDeviceToShow').addClass("hide");//to reinitialize
	
	hideNavigationButton ();
	
	actionVisualizeList();
	
	$('#devicelist').find('.deviceitem').not('.template').remove();
	devicelist = new Object();
}

function deviceResultAction(data){
	

	$('#NoDeviceToShow').removeClass("hide");
	
	showNavigationButton ();
	
	$('#deviceperpage').removeClass("hide");
	$('#totalNUmberOfDevice').text(numDevices);
	
	if (numDevices <= 6)
		$('#deviceperpage').addClass("hide");
	else if (numDevices>6 &&  numDevices <= 15){

		document.getElementById("dev15").innerHTML = numDevices;
		$('#dev24').addClass("hide");
		$('#dev24Bullet').addClass("hide");

	}else if(numDevices>15 &&  numDevices <= 24){
		document.getElementById("dev15").innerHTML = 15;
		document.getElementById("dev24").innerHTML = numDevices;
		$('#dev24').removeClass("hide");
		$('#dev24Bullet').removeClass("hide");
	}else{
		document.getElementById("dev15").innerHTML = 15;
		document.getElementById("dev24").innerHTML = 24;
		$('#dev24').removeClass("hide");
		$('#dev24Bullet').removeClass("hide");
		
	}
	
	if (data.length >0){
		$('#noresults').addClass("hide");
		$('#visualizemap').removeClass("disabled");
		$('#selectalltrigger').removeClass("disabled");
	}
		
}


function actionVisualizeList(){
	
	$('#mapvisualization').addClass('hide');
	$('#listvisualization').removeClass('hide');
	
	$('#visualizebadges').addClass('hide');
	$('#visualizemap').removeClass('hide');
	
	var mapdom = $('#fullmap');
	mapdom.html("");
	
}

/* massive load */
$('.contextCategorySelMain').on('contentCategoriesMainChanged', function() {
	
	checkLoadMassimeFields();
});

$('#file').on('change', function() {
	checkLoadMassimeFields();
});


/*if Context, Category and file are selected, then enabled the upload button */
function checkLoadMassimeFields(){
	
	var myDropdownSelCategoryMain =  $('#myDropdownSelCategoryMain').val();
	var fileVal= $("#file").val();
	
	if ( typeof myDropdownSelCategoryMain !== 'undefined' && myDropdownSelCategoryMain != null && myDropdownSelCategoryMain.trim().length > 0 &&
		 typeof fileVal !== 'undefined' && fileVal != null && fileVal.trim().length > 0 	){
	
		var fiwareService =  $('#myDropdownSelContextMain').val();
		var fiwareServicepath = fiwareService+"_"+myDropdownSelCategoryMain.toLowerCase();
		
		$("#serviceMassimeLoad").val(fiwareService);
		$("#servicepathMassiveLoad").val(fiwareServicepath);
		
		$(".buttonModalMassiveLoad").removeClass("disabled");
	}else{
		
		$(".buttonModalMassiveLoad").addClass("disabled");
	}
	
	
}

/*hide navigation button on the left 1,2...last and on the right show 6,15,24 of totalNumber */
function hideNavigationButton (){
	$('#pagination-long').addClass("hide");
	$('#deviceperpage').addClass("hide");
}
/*show navigation button on the left 1,2...last and on the right show 6,15,24 of totalNumber */
function showNavigationButton (){
	$('#pagination-long').removeClass("hide");
	$('#deviceperpage').removeClass("hide");
}




/* Prototype to remove a random value from an array */
Array.prototype.remove = function(v) { this.splice(this.indexOf(v) == -1 ? this.length : this.indexOf(v), 1); }

/*some called functions are on the file devicedetails.js that is included in the same page (filterResult.jsp & devices.jsp )*/

