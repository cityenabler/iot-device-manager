var newdevice = new Device();
var newattr = new DeviceAttribute();
var newattrJsonSchema = new Array();
var newmashup = new DeviceMashup();
var newcommand = new DeviceCommand();
var newcredential = new DeviceCredential();
var mapclicklistener;
var presentmarkers = [];
var map;
var marker;
var operationType = "create_device";
var mapcenter = {};
var connectedUserOrgId;
var dataClickListener;
var alreadyHistoricizing =false;
var orgs = [];
var assetTypeMap = {};
var assetAttrMap = {};
var assetAttrListMap = {};
var assetDriverMap = {};
var assetSelectedMap = {};
var hubTypeDeviceToModify;
var gatewayDeviceToModify;
var assetDeviceToModify;
var kapuaDriver;

$(document).ready(function() {
	
	onDocReady();

	$("#organizationId").attr("selected", true);
	connectedUserOrgId = $("#organizationId").val();
	// re-initialize material-select
	$('select').formSelect();
	$('a.helpLink').attr('href', helpLink);

	$('.contextCategorySelMain').on('contentCategoriesMainChanged', function() {
		updateMap();
		
		//check if the ttnDeviceID is already used
		var transportProt = $('#transport_protocol').val();
		if (transportProt == 'LORA'){
			var ttnDeviceID = $('#ttnDeviceID').val();
			if (ttnDeviceID != "")
				$('#ttnDeviceID').focusout();
		}
			
		
		
	});

	$('.tooltipped').tooltip();//initialize tooltip
	
	$("#blank_space_error").attr("data-error", $("#space_warning").html());
	
});

function updateMap() {

	var myDropdownSelContextMain = $('#myDropdownSelContextMain').val();
	var myDropdownSelCategoryMain = $('#myDropdownSelCategoryMain').val();

	if (myDropdownSelCategoryMain != null) {

		var fiwareService = myDropdownSelContextMain;
		var fiwareServicepath = myDropdownSelContextMain + "_"
				+ myDropdownSelCategoryMain.toLowerCase();

		var headersAll = new Object();
		headersAll['fiware-service'] = fiwareService;
		headersAll['fiware-servicepath'] = fiwareServicepath;

		var input = new Object();
		input['action'] = 'getMapCenter';

		$.ajax({
			url : 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			headers : headersAll,
			success : function(data) {
				mapcenter = data;
				initCenterMap(mapcenter);

			},
			error : function(xhr, status, error) {
				console.log(status);
			}
		});
	}
}

/*Management of map coordinates  */
document.getElementById("find-coordinates").addEventListener("click", initNewDeviceMapFromInputFields);

var inputLat = document.getElementById("pac-input-lat");
var inputLng =  document.getElementById("pac-input-lng");

inputLat.addEventListener("keydown", function(e){
	if(e.key === 'Enter' || e.keyCode === 13 ){
		e.preventDefault();
		return false;
	}
});

inputLng.addEventListener("keydown", function(e){
	if(e.key === 'Enter' || e.keyCode === 13 ){
		e.preventDefault();
		return false;
	}
});

$(document).on('change', '#organizationId', function() {
	connectedUserOrgId = $(this).val();
});

function appendAttribute(newattr) {

	// Hide "No attribute" message
	$('#noattrs-msg').addClass("hide");

	var appender = $('#attrappender').removeClass('hide');
	var template = appender.find('.newattr.template');

	var item = template.clone();
	item.removeClass('template hide');

	item.find('.attrname').text(newattr.getName());
	item.find('.attrtype .typevalue').text(newattr.getType());

	/*
	 * if (newattr.getJsonschema()!=undefined){
	 * item.find('.attrobjschema').removeClass("hide");
	 * item.find('.attrobjschema
	 * .objschemavalue').text(newattr.getJsonschema()); }
	 */
	item.find('a').attr('data-id', newattr.getName());

	appender.append(item);
}

function assetappendAttribute(newattr, assetname) {
		// Hide "No attribute" message
		$('#noassetattrs-msg').addClass("hide");

		var appender = $('#assetattrappender').removeClass('hide');
		var template = appender.find('.newattr.template');
		
		var item = template.clone();
		item.removeClass('template hide');

		//For Kapua asset disable delete of single attributes from the list
		$('.deleteattr').addClass('hide');
		
		//For Kapua asset there is a 'li' with asset name to show
		$('.assettitle').removeClass('hide');
		
		appender.find('.assetname').text(assetname);
		item.find('.attrname').text(newattr.getName());
		item.find('.attrtype .typevalue').text(newattr.getType());

		appender.append(item);
	}

// append command
function appendCommand(newcommand) {

	// Hide "No attribute" message
	$('#nocommands-msg').addClass("hide");

	var appender = $('#commandappender').removeClass('hide');
	var template = appender.find('.newcommand.template');

	var item = template.clone();
	item.removeClass('template hide');

	item.find('.commandname').text(newcommand.getName());
	item.find('.commandtype .typevalue').text(newcommand.getType());
	item.find('a').attr('data-id', newcommand.getName());

	appender.append(item);
}

function onDocReady() {
	mainOnDocReady();

	var center = {
		lat : 0,
		lng : 0
	};
	initCenterMap(center);

	if (typeof currentDevice !== 'undefined' && currentDevice != null
			&& currentDevice.trim().length > 0) {
		try {
			prefillDeviceForm();

//			var transProt = $('#transport_protocol')
			var _device = JSON.parse(currentDevice);

			changeTransportProtocol(_device.transport);
			if (actionmenu == "copyDevice") {
				$('#new_device_title').removeClass('hide');
				$('#new_mobile_device_title').removeClass('hide');

				if (_device.transport != "OPCUA")
					$('#name').attr("disabled", false);

				$('#deviceid').attr("disabled", false);

				

				// fix
				var select = $("#dataformat_protocol");
				select.val(_device.protocol);
				select.attr("disabled", false);
				select.formSelect();

				var selectTp = $("#transport_protocol");
				selectTp.attr("disabled", false);
				selectTp.formSelect();

				operationType = "create_device";
			} else {

				operationType = "modifyDevice";
				$('#name').attr("disabled", true);
				$('#edit_device_title').removeClass('hide');
				$('#edit_mobile_device_title').removeClass('hide');
				
				//Changes about add hubs for DeMa version 2
//				$('#filterSelect').addClass('hide'); // context_menu
				$('#contextMenuMainRows').addClass('hide');	//hide only contextMenu row with enabler, context, category, not the whole div with new hub and gateway
				$('#myDropdownSelHubMain').append($('<option>', {value:_device.hubName, text:_device.hubName, selected: true}));
				$('#myDropdownSelHubMain').attr("disabled", true);
				if(_device.hubType !== "Idas") {	//Show Gateway only for hub not Idas
					$("#gatewaySelDivMainRow").removeClass('hide');
					$('#myDropdownSelGatewayMain').append($('<option>', {value:_device.gateway, text:_device.gateway, selected: true}));
					$('#myDropdownSelGatewayMain').attr("disabled", true);
				}
				hubTypeDeviceToModify = _device.hubType;
				gatewayDeviceToModify = _device.gateway;
				assetDeviceToModify = _device.asset;
				
				//Populate Transport protocol (not populated statically anymore) adding option of current device
				$("#transport_protocol").append($('<option>', {value:_device.transport, text:_device.transport, selected:true}));
				
				//Set the section about pull/push
				if(_device.hubType === "kapua") {
					setPullSectionKapuaHub(_device.endpoint);
					$('.attributesection').addClass('hide');
					$('.kapuaassetsection').removeClass('hide');
				}
				//END changes about add hubs for DeMa version 2
				
				// fix
				var select = $("#dataformat_protocol");
				select.val(_device.protocol);
				select.attr("disabled", true);
				select.formSelect();

			}
		} catch (error) {
			console.log(error);
			alert($('#msg_invalid_device').text());
		}
	} else {
		$('#new_device_title').removeClass('hide');
		$('#new_mobile_device_title').removeClass('hide');
	}

	/*LoRa Attributes follow a schema for name and type*/
	$(document).on('change', '#attrNameSel', function() {
		
		//based on https://github.com/myDevicesIoT/cayenne-docs/blob/master/docs/LORA.md
		
		var loraAttrName = $(this).val();
		
		if(loraAttrName == "digital_in"  ||
		   loraAttrName == "digital_out"  ||
		   loraAttrName == "luminosity"  ||	
		   loraAttrName == "presence"  	
		
		){
			$('#newattr_type').val("number");
			
		}else if (loraAttrName == "analog_in"  ||
				loraAttrName == "analog_out"  ||
				loraAttrName == "temperature"  ||
				loraAttrName == "relative_humidity"  ||
				loraAttrName == "accelerometer"  ||
				loraAttrName == "barometric_pressure"  || //
				loraAttrName == "gyrometer"  ||
				loraAttrName == "gps" 
					
			){
			
			$('#newattr_type').val("decimal");
			
		}
		checkLoraAttrName();
		
	});
	
	$(document).on('change', '#newattr_nameChannel', function() {
		checkLoraAttrName();
	});
	
	
	
	function checkLoraAttrName(){
		
		var loraAttrName = $('#attrNameSel').val();
		
		if (loraAttrName!= ""){
			$('#prevewAttrName').removeClass("hide");
			var fullNameAttr = loraAttrName+"_"+  $('#newattr_nameChannel').val();
			$('#namePreview').html(fullNameAttr);
			$('.addattributebtn ').removeClass('disabled', 'disabled');
			
				
			var attrType = $('#newattr_type').val();
			
			newattr["name"] = fullNameAttr;
			newattr["type"] = attrType;
				
		}else{
			
			$('.addattributebtn ').addClass('disabled', 'disabled');
			$('#prevewAttrName').addClass("hide");
			
		}
		
	}
	
	
	
	
	$('.modalnewattr').modal({
		onOpenEnd : function(modal) {
			$(modal).find('#newattr_name').focus();

			var _df = $('#dataformat_protocol').val();
			
			var attrName ="";
			if (_df.toLowerCase() =="cayennelpp"){ //LORA case
				
				
				$('#attrNameSel').val("");
				
				$('.attrNameSelect').removeClass('hide');
				$('.attrNameChannel').removeClass('hide');
				$('#newattr_nameChannel').prop('required',true);
				$('#newattr_nameChannel').val("0");
				$('.attrNameText').addClass('hide');
				$('#newattr_name').removeAttr("required");
				$('.attrTypeDiv').addClass('m4');
				$('.attrTypeDiv').removeClass('m6');
				$('#newattr_type').prop('disabled',true);	
				$('#newAttributeLoraHelp').removeClass("hide");
				
				attrName = $('#attrNameSel').val();//select text
				
			}else{
				$('.attrNameSelect').addClass('hide');
				$('.attrNameChannel').addClass('hide');
				$('#newattr_nameChannel').removeAttr("required");
				$('.object_type').removeClass('hide');
				$('.attrNameText').removeClass('hide');
				$('#newattr_name').prop('required',true);
				$('#newattr_type').prop('disabled',false);				
				$('.attrTypeDiv').addClass('m6');
				$('.attrTypeDiv').removeClass('m4');
				$('#newAttributeLoraHelp').addClass("hide");
				
				
				if (_df.toLowerCase() == "json") {
					$('#object_type').removeClass('hide');
				} else {
					$('#object_type').addClass('hide');
				}
				
				attrName = $('#newattr_name').val();//input text
				
			}
			
			$('#prevewAttrName').addClass("hide");
			
			var attrType = $('#newattr_type').val();
			

			if ((attrName != "") && (attrType != "")) {
				$('.addattributebtn ').removeClass('disabled', 'disabled');
			} else {
				$('.addattributebtn').addClass('disabled', 'disabled');
			}
		},
		onCloseEnd : function(modal) {
			$(modal).find('.newattrfield').val("");
			$('.newattr_objschema_textarea').addClass('hide');
		}
	});
	
	$('.modalnewasset').modal({
		onOpenEnd : function(modal) {
			populateAssetlist();
			
			$('#newasset_type').prop('disabled',false);				
			$('.assetTypeDiv').addClass('m6');
			$('.assetTypeDiv').removeClass('m4');

			$('#prevewAssetName').addClass("hide");
			
			var assetType = $('#newasset_type').val();

			if (assetType != "") {
				$('.addassetbtn ').removeClass('disabled', 'disabled');
			} else {
				$('.addassetbtn').addClass('disabled', 'disabled');
			}
		},
		onCloseEnd : function(modal) {
			$(modal).find('.newassetfield').val("");
			$('.newasset_objschema_textarea').addClass('hide');
		}
	});

	$('.modalcommand').modal({
		onOpenEnd : function(modal) {
			$(modal).find('#newcommand_name').focus();
		},
		onCloseEnd : function(modal) {
			$(modal).find('.newcommandfield').val("");
		}
	});

};

$(document).on('change', '.deviceselectfield', function() {
	newdevice[$(this).attr('name')] = $(this).val();
});

// checkForbiddenChars trigger

$(document).on('input', '.devicetextfield', function() {
	newdevice[$(this).attr('name')] = $(this).val();
});

$(document).on('change', '.newattrfield',function() {
	
					var val = $(this).val().trim();
					
					var attrName = $('#newattr_name').val();
					var attrType = $('#newattr_type').val();
					var attrObjSchema = $('#newattr_objschema').val();
					// complexjsonattribute
					var _df = $('#dataformat_protocol').val();
					
					
					
					if (_df.toLowerCase() == "cayennelpp") {
						return false;
					}else if (_df.toLowerCase() == "json") {
						if (attrType != "" && typeof attrType !== 'undefined' && attrType != null) {
							if (attrType == "object") {
								$('.newattr_objschema_textarea').removeClass('hide');
								$('.addattributebtn').addClass('disabled','disabled');
							} else {
								$('.newattr_objschema_textarea').addClass('hide');
								$('.addattributebtn ').removeClass('disabled','disabled');
							}
						}
						// Attribute name cannot be named "type"
						 if(attrName == "type" ){
							 $('.addattributebtn').addClass('disabled', 'disabled');
						 }
						else if ((attrName != "")	&& ((attrType != "") && attrType != "object")) {
							$('.addattributebtn ').removeClass('disabled','disabled');
						} else if ((attrName != "")	&& ((attrType != "") && attrType == "object" && attrObjSchema != "")) {
							$('.addattributebtn ').removeClass('disabled','disabled');
						} else {
							$('.addattributebtn').addClass('disabled','disabled');
						}

					} else { // not json
						if ((attrName != "") && (attrType != "")) {
							$('.addattributebtn ').removeClass('disabled','disabled');
						} else {
							$('.addattributebtn').addClass('disabled','disabled');
						}
					}
					newattr[$(this).attr('name')] = val;
					
				});

$(document).on('change', '.newassetfield',function() {
	var val = $(this).val().trim();
	var assetName = document.getElementById('name').value;	//The same of the name chosen for the device in the main page
	var assetType = $('#newasset_type').val();	//The sensor name taken from the list in MongoDB
	assetSelectedMap["name"] = assetName;
	assetSelectedMap["type"] = assetType;
	
	if (assetType != "" && typeof assetType !== 'undefined' && assetType != null) {
		for (const name in assetAttrListMap) {
			if(assetType == name) {
				$('#newasset_objschema').val("Attr: " + assetAttrListMap[name]);				
			}
		}
		$('.newasset_objschema_textarea').removeClass('hide');
	}
	else $('.newasset_objschema_textarea').addClass('hide');
	
	if(assetType == "" || typeof assetType === 'undefined' || assetType == null){
		$('.addassetbtn').addClass('disabled', 'disabled');
	}
	else $('.addassetbtn ').removeClass('disabled','disabled');
});

$(document).on('input', '.newcommandfield', function() {
	var val = $(this).val().trim();
	var cmdName = $('#newcommand_name').val();
	var cmdType = $('#newcommand_type').val();

	if ((cmdName != "") && (cmdType != "")) {
		$('.addcommandbtn').removeClass('disabled', 'disabled');
	} else {
		$('.addcommandbtn').addClass('disabled', 'disabled');
	}
	newcommand[$(this).attr('name')] = val;
});

$(document).on('input', '.seldeviceauthmethod', function() {
	var val = $(this).val().trim();
	newdevice.setAuthMethod(val);
});

$(document).on('input', '.newusernamefield', function() {
	var val = $(this).val().trim();
	newcredential.setUsername(val);
	newdevice.setCredential(newcredential);
});

$(document).on('input', '.newpwdfield', function() {
	var val = $(this).val().trim();
	newcredential.setPassword(val);
	newdevice.setCredential(newcredential);
});

$(document).on('input', '.newapikeyfield', function() {
	var val = $(this).val().trim();
	newcredential.setApikey(val);
	newdevice.setCredential(newcredential);
});

// changed according to the new iota
$(document).on('change', '#listDeviceOpcuaSel', function() {

	setDeviceOpcuaName();

});

// changed according to the new iota
$(document).on('change', '#transport_protocol', function() {
	if($('#name').val() == $('#listDeviceOpcuaSel option:selected').text()){
		$('#name').val("");		
	}
	changeTransportProtocol($(this));
	removeAttributes();
});

function setDeviceOpcuaName() {
	var deviceOpcuaName = $('#listDeviceOpcuaSel option:selected').text();

	var deviceOpcuaId = $('#listDeviceOpcuaSel').val();

	$('#name').val(deviceOpcuaName);
	$('.devicetextfield#deviceid').val(deviceOpcuaId).trigger('input');
	$('#name').prop('disabled', true);

	newdevice.setName(deviceOpcuaName);

	var eneS = deviceOpcuaId.substring(0, 3);
	isMes = true;
	if (eneS == "ene") {
		isMes = false
	}
	// fill the attributes for mes/energy
	setOPCUAAttr(isMes);
}

function changeTransportProtocol(elem) {

	$('.deviceEndpoint').addClass('hide');
	$('.devicepullsection').addClass('hide');
	$("#isMobileDevice").attr('disabled', false);
	$('.devicepullsection').addClass('hide');
	$('#txt_opcua_device_endpoint').addClass('hide');
	$('#txt_http_device_endpoint').addClass('hide');
	$('#txt_mqtt_broker_endpoint').addClass('hide');
	$('#listDeviceOpcua').addClass('hide');
	$('#name').prop('disabled', false);
	$('.loraDetails').addClass('hide');
	$('.loraServerIoDeviceProfileDiv').addClass('hide');
	$('.loraDetailsDiv').addClass('hide');

	switchRetrieveMode(".switchRetrieveMode");
	
	
	var isPushpPull = $("#retrievedatamode").val();

	if (isPushpPull == "pull") {

		setDevicePullPush(true);
	}

	var label_insert_device_endpoint = $('#label_insert_device_endpoint')
			.text();
	var label_insert_device_endpoint_opcua = $(	'#label_insert_device_endpoint_opcua').text();

	var tip_insert_device_endpoint = $('#tip_insert_device_endpoint').text();
	var tip_insert_device_endpoint_opcua = $('#tip_insert_device_endpoint_opcua').text();

	$('#device_endpoint').attr("placeholder", label_insert_device_endpoint);
	$('#device_endpoint').attr("data-tooltip", tip_insert_device_endpoint);
	$('.tooltipped').tooltip();// re-initialize the tooltip framework

	var transportProt = elem;
	
	//bugfix CENABLER-1504
	if (!(typeof transportProt === 'string' || transportProt instanceof String))
		transportProt = elem.val();
	
	
	var select = $("#dataformat_protocol");
	$(select).find('option:not(:first)').remove();
	select.attr("disabled", false);
	
	
	if (transportProt == "HTTP" || transportProt == "MQTT") {
		$('#pushonlyalert').addClass('hide');
		$("#retrievedatamodeCheck").attr('disabled', false);


	if (agentUL20Enabled)
		$(select).append($('<option>', {value:'UL2_0', text:'UL2.0' }));
		
	if (agentJSONEnabled)
		$(select).append($('<option>', {value:'JSON', text:'JSON', selected:true }));
		

	} else if (transportProt == "SIGFOX") {
		// TRUE=PULL, FALSE=PUSH
		var switchStatus = false;
		setDevicePullPush(switchStatus);
		$('#pushonlyalert').removeClass('hide');
		$("#retrievedatamodeCheck").attr('disabled', true);

		$(select).append($('<option>', {value:'SIGFOX', text:'SIGFOX', selected:true }));
		select.attr("disabled", true);

	} else if (transportProt.toUpperCase() == "COAP") {
		var switchStatus = false;
		setDevicePullPush(switchStatus);

		$('#pushonlyalert').addClass('hide');
		$("#retrievedatamodeCheck").attr('disabled', true);

		$(select).append($('<option>', {value:'LWM2M', text:'LWM2M', selected:true }));
		select.attr("disabled", true);

	} else if (transportProt == "OPCUA") {
		// TRUE=PULL, FALSE=PUSH
		var switchStatus = false;
		setDevicePullPush(switchStatus);
		$('#pushonlyalert').removeClass('hide');
		$("#retrievedatamodeCheck").attr('disabled', true);
		$('.deviceEndpoint').removeClass('hide');
		$('#device_endpoint').removeClass('hide');

		$("#isMobileDevice").prop('checked', false);
		$("#isMobileDevice").attr('disabled', true);
		$("#mobile_device").val(false);
		$('#txt_opcua_device_endpoint').removeClass('hide');
		$(".devicegeolocation").removeClass('hide');
		$('#listDeviceOpcua').removeClass('hide');

		$(select).append($('<option>', {value:'OPCUA', text:'OPC UA', selected:true }));
		select.attr("disabled", true);

		$('#device_endpoint').attr("placeholder",label_insert_device_endpoint_opcua);
		$('#device_endpoint').attr("data-tooltip",	tip_insert_device_endpoint_opcua);

		setDeviceOpcuaName();

		$('.tooltipped').tooltip();// re-initialize the tooltip framework

		select.attr("disabled", true);
		
	} else if (transportProt.toUpperCase() == "LORA") {
		var switchStatus = false;
		setDevicePullPush(switchStatus);

		$('#pushonlyalert').addClass('hide');
		$("#retrievedatamodeCheck").attr('disabled', true);

		
		$(select).append($('<option>', {value:'CAYENNELPP', text:'CayenneLpp', selected:true }));
		$(select).append($('<option>', {value:'CBOR', text:'CBOR'}));
		$(select).append($('<option>', {value:'APPLICATION_SERVER', text:'custom decoded by the LoRaWAN application server'}));
		
		$('.loraDetails').removeClass('hide');
		$('.loraDetailsDiv').removeClass('hide');

		$('#loraStack').change();

	}

	select.formSelect();
	
}

/*name becomes the id key on Orion, so we must check if it is already used */
$('#name').focusout(function(){
	
	var input = new Object();
	input['action'] = 'check_name';
	input['devicename'] = $("#name").val();
	
	var _headers = new Object();
	_headers['fiware-service'] = getNewDevService();
	_headers['fiware-servicepath'] = getNewDevServicePath();
	
	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : _headers,
		success : function(data) {

			if(data == false){
				M.toast({
					html : $('#msg_device_with_this_name_exists').text(),
					classes : 'red'
				});
				$("#submit_device").attr("disabled","disabled");
				$("#name").removeAttr("validate").addClass("invalid");
			}else{
				$("#submit_device").removeAttr("disabled");
				$("#name").removeAttr("validate").addClass("valid");
			}
		}
	});
	return false;
});


/*DeviceID on LoRa device becomes the deviceId on Orion, so we must check if it is already used */
$('#ttnDeviceID').focusout(function(){
	
	var newDeviceId = $("#ttnDeviceID").val();
	
	if (operationType == "modifyDevice"){
		
		var _device = JSON.parse(currentDevice);
		var oldDeviceId = _device.id; //useful in edit mode
		
		if (oldDeviceId == newDeviceId){
			$("#submit_device").removeAttr("disabled");
			$("#ttnDeviceID").removeAttr("validate").addClass("valid");
			return false;
		}
	}	
		
	var input = new Object();
	input['action'] = 'checkDeviceId';
	input['deviceid'] = newDeviceId;
	
	var _headers = new Object();
	_headers['fiware-service'] = getNewDevService();
	_headers['fiware-servicepath'] = getNewDevServicePath();
	
	$.ajax({
		url : 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		headers : _headers,
		success : function(data) {

			if(data == false){
				M.toast({
					html : $('#msg_device_with_this_deviceID_exists').text(),
					classes : 'red'
				});
				$("#submit_device").attr("disabled","disabled");
				$("#ttnDeviceID").removeAttr("validate").addClass("invalid");
			}else{
				$("#submit_device").removeAttr("disabled");
				$("#ttnDeviceID").removeAttr("validate").addClass("valid");
			}
		}
	});
	return false;
});


$(document).on('change', '#ttnDeviceID', function() {

	var loraDeviceID = $('#ttnDeviceID').val();
	$('.devicetextfield#deviceid').val(loraDeviceID).trigger('input');

});

// changed according to the new iota
$(document).on('change', '#loraStack', function() {

	var loraStack = $('#loraStack').val();

	if (loraStack == "LORASERVER") {

		$('.loraTTNDetails').addClass('hide');
		$('.loraServerIoDeviceProfileDiv').removeClass('hide');
		$('#loraIoDetailsHelp').removeClass('hide');
		$('#loraTTNDetailsHelp').addClass('hide');
		populateDeviceProfiles();

	} else if (loraStack == "TTN") {

		$('.loraTTNDetails').removeClass('hide');
		$('.loraServerIoDeviceProfileDiv').addClass('hide');
		$('#loraIoDetailsHelp').addClass('hide');
		$('#loraTTNDetailsHelp').removeClass('hide');
	}

});

function populateDeviceProfiles() {
	try {

		var service = getNewDevService();
		var servicePath = getNewDevServicePath();

		var input = new Object();
		input['action'] = 'getLoraDeviceProfiles';
		input['service'] = service;
		input['servicepath'] = servicePath;

			$.ajax({
					url : 'ajaxhandler',
					type : 'GET',
					dataType : 'json',
					data : input,
					success : function(data) {


						// $('#loraDeviceProfile').append($('<option disabled>',
						// {value:"", text:""}));

						var numProfile = data.length;

						// clean the old value
						$('#loraDeviceProfile').empty();
						$('#loraDeviceProfile').trigger('loraDeviceProfileChanged');

						$.each(data,function(i, e) {

								if (numProfile == 1)
									$('#loraDeviceProfile').append($('<option>',{value : e.id, text : e.name, selected : true}));
								else {

										if (currentDevice != "") {// we are in edit mode

											var loraDeviceProfileId = JSON.parse(currentDevice).loraDeviceProfileId;

											if (e.id == loraDeviceProfileId)
													$('#loraDeviceProfile').append($('<option>',{value : e.id, text : e.name, selected : true	}));
											else
												$('#loraDeviceProfile').append($('<option>',{value : e.id, text : e.name	}));

										} else {
													$('#loraDeviceProfile').append($('<option>',{value : e.id ,text : e.name	}));
										}

								}
						});

						$("#loraDeviceProfile").trigger('loraDeviceProfileChanged');

					},
					error : function(xhr, status, error) {
						console.log(error);
					}
				});
	} catch (error) {
		console.log(error);
	}
}

// to reinitialize material select
$('.loraDeviceProfileC').on('loraDeviceProfileChanged', function() {
	// $(this).formSelect({classes:"loraDeviceProfileC"});
	$(this).formSelect();
});

// Set pull device pull/passive - push/active
var setDevicePullPush = function(switchStatus) {
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	// TRUE=PULL, FALSE=PUSH
	if (switchStatus == false) {
		// PUSH/ACTIVE DEVICE
		$('.switchRetrieveMode').prop('checked', false);
		var retrievedatamode = "push";
		$("#retrievedatamode").val(retrievedatamode);

		$('.devicepullsection').addClass('hide');
		$('.deviceEndpoint').addClass('hide');

		$('.commandsection').addClass('hide');
		$('.deviceauthsection').addClass('hide');
	} else {

		// PULL/ACTIVE DEVICE
		$('.switchRetrieveMode').prop('checked', true);
		var retrievedatamode = "pull";
		$("#retrievedatamode").val(retrievedatamode);

		$('.devicepullsection').removeClass('hide');
		$('.deviceEndpoint').removeClass('hide');

		$('.commandsection').removeClass('hide');
		$('.deviceauthsection').removeClass('hide');

	}
};

// VERIFIY the coherence between dataformat and transport
$(document)
		.on(
				'change',
				'#dataformat_protocol',
				function() {
					var transportProt = $('#transport_protocol').val();
					var dataformatProt = $('#dataformat_protocol').val();

					// Check Coherence between Transport and Dataformat
					var isCoherentTransportDataformat = checkCoherenceTransportDataformat(transportProt, dataformatProt)
					if (!isCoherentTransportDataformat) {
						var select = $("#dataformat_protocol");
						select.val('JSON');
						select.formSelect('destroy');
						select.formSelect();

						M.toast({
							html : $('#msg_invalid_dataformat').text(),
							classes : 'red'
						});
					}
				});

// switchmobiledevice
$(document).on(
		'change',
		'.switchmobiledevice',
		function() {
			var isMobileDevice = $("#isMobileDevice").val();
			var mobile_device = $("#mobile_device").val();

			if (typeof mobile_device == undefined || mobile_device.trim() == ""
					|| mobile_device == null) {
				mobile_device = "false";
			} else {
				if ($("#isMobileDevice").val() == "on") {// was mobile (on),
															// becomes fixed
															// (off)
					mobile_device = "false";
					$("#isMobileDevice").val("off");
					$("#isMobileDevice").attr('checked', false);
					$('.devicegeolocation').removeClass('hide');

					// re init map
					initCenterMap(mapcenter);


				} else if ($("#isMobileDevice").val() == "off") {// was off,
																	// becomes
																	// mobile
					mobile_device = "true";
					$("#isMobileDevice").val("on");
					$("#isMobileDevice").attr('checked', true);
					$('.devicegeolocation').addClass('hide')
				}

				var mapLat = $('#pac-input-lat').val();
				var mapLng = $('#pac-input-lng').val();

				newdevice.setLatitude(mapLat);
				newdevice.setLongitude(mapLng);

				$("#mobile_device").val(mobile_device);
			}

			newdevice.setMobileDevice(mobile_device);

});

//switchHistoricizing
$(document).on(
		'change',
		'.switchHistoricizing',
		function() {
			var isHistory = $("#isHistory").val();
			var historicizing = $("#historicizing").val();

			if (typeof historicizing == undefined || historicizing.trim() == ""
					|| historicizing == null) {
				historicizing = "false";
			} else {
				if (isHistory == "on") {// was on, becomes off
					historicizing = "false";
					$("#isHistory").val("off");
					$("#isHistory").attr('checked', false);
					
					
					if (operationType == "modifyDevice"){
						if (alreadyHistoricizing)
							$("#removeOldSection").removeClass("hide");
					}
					
					
				} else if (isHistory == "off") {// was off, becomes on
					historicizing = "true";
					$("#isHistory").val("on");
					$("#isHistory").attr('checked', true);
					$("#removeOldSection").addClass("hide");
					
				}

				$("#historicizing").val(historicizing);
			}

});

//switchRemoveOld
$(document).on(
		'change',
		'.switchRemoveOld',
		function() {
			var isRemoveOld = $("#isRemoveOld").val();
			var removeOld = $("#removeOld").val();

			if (typeof removeOld == undefined || removeOld.trim() == ""
					|| removeOld == null) {
				removeOld = "false";
			} else {
				if (isRemoveOld == "on") {// was on, becomes off
					removeOld = "false";
					$("#isRemoveOld").val("off");
					$("#isRemoveOld").attr('checked', false);
					
				} else if (isRemoveOld == "off") {// was off, becomes on
					removeOld = "true";
					$("#isRemoveOld").val("on");
					$("#isRemoveOld").attr('checked', true);
				}
				$("#removeOld").val(removeOld);
			}
});



// Function to verify the coherence between transport protocol and dataformat
// protocol
var checkCoherenceTransportDataformat = function(transportProt, dataformatProt) {
	var isValid = false;
	if (transportProt == "HTTP" || transportProt == "MQTT") {
		if (dataformatProt == 'JSON' || dataformatProt == 'UL2_0') {
			isValid = true;
		}
	} else if (transportProt == "SIGFOX") {
		if (dataformatProt == 'SIGFOX') {
			isValid = true;
		}
	} else if (transportProt == "COAP") {
		if (dataformatProt == 'LWM2M') {
			isValid = true;
		}
	} else if (transportProt == "OPCUA") {
		if (dataformatProt == 'OPCUA') {
			isValid = true;
		}
	} else if (transportProt == "LORA") {
		if (dataformatProt == 'APPLICATION_SERVER' || dataformatProt == 'CAYENNELPP'
				|| dataformatProt == 'CBOR') {
			isValid = true;
		}
	}
	return isValid;
}

function getNewDevService() {

	var service = $('#myDropdownSelContextMain').val();
	
	if (operationType == "modifyDevice")	
		service = get("scope");
	
	
	return service;
}

function getNewDevServicePath() {

	var myDropdownSelCategoryMain = $('#myDropdownSelCategoryMain').val();
	var service = $('#myDropdownSelContextMain').val();
	var path = ""
	if (typeof myDropdownSelCategoryMain !== 'undefined'
			&& myDropdownSelCategoryMain != null
			&& myDropdownSelCategoryMain.trim().length > 0)
		path = service + "_" + myDropdownSelCategoryMain.toLowerCase();

	
	if (operationType == "modifyDevice")	
		path = get("urbanservice");
	
	return path;
}

// Register device
$(document)
		.on(
				'submit',
				'#newdeviceform',
				function() {

					if (Object.keys(newdevice.getAttributes()).length <= 0) {
						M.toast({
							html : $('#msg_attribute_missing').text(),
							classes : 'red'
						});
						return false;
					}

					// Check Coherence between Transport and Dataformat
					var transportProt = $('#transport_protocol').val();
					var dataformatProt = $('#dataformat_protocol').val();
					var isCoherentTransportDataformat = checkCoherenceTransportDataformat(
							transportProt, dataformatProt);
					if (!isCoherentTransportDataformat) {
						M.toast({
							html : $('#msg_invalid_dataformat').text(),
							classes : 'red'
						});
						$('#dataformat_protocol').focus();
						return isCoherentTransportDataformat;
					}

					var service;
					var path;
					var myDropdownSelCategoryMain = $('#myDropdownSelCategoryMain').val();
					var servicepathName = myDropdownSelCategoryMain;
					
					if (operationType == "create_device") {

						// create device
						service = $('#myDropdownSelContextMain').val();

						path = ""
						if (typeof myDropdownSelCategoryMain !== 'undefined'
								&& myDropdownSelCategoryMain != null
								&& myDropdownSelCategoryMain.trim().length > 0)
							path = service + "_"
									+ myDropdownSelCategoryMain.toLowerCase();

						if (typeof service == 'undefined'
								|| service == null
								|| service.trim().length == 0
								|| typeof myDropdownSelCategoryMain == 'undefined'
								|| myDropdownSelCategoryMain == null
								|| myDropdownSelCategoryMain.trim().length == 0) {

							M.toast({
								html : $('#msg_context_category_mandatory')
										.text(),
								classes : 'red'
							});
							return false;

						}
					}

					if (operationType == "modifyDevice") {

						var service = get("scope");
						var path = get("urbanservice");
					}

					var _headers = new Object();
					_headers['fiware-service'] = service;
					_headers['fiware-servicepath'] = "/" + path;


					// device mobile
					var isMobile = $('#mobile_device').val();
					newdevice.setMobileDevice(isMobile);
					

					
					
					// dynamic transport_protocol MQTT/HTTP
					var transport_protocol = $('#transport_protocol').val();
					newdevice.setTransport(transport_protocol);

					// dynamic dataformat_protocol UL2.0/JSON
					var dataformat_protocol = $('#dataformat_protocol').val();
					newdevice.setDataformatProtocol(dataformat_protocol);

					// Compose new device
					var nd = new NewDevice(newdevice.getId(), service, path,
							newdevice.getLatitude(), newdevice.getLongitude(),
							null, null, null, newdevice.getMobileDevice(),
							newdevice.getTransport(), newdevice
									.getDataformatProtocol(), null);

					// fix: add transport_protocol also in the static attributes
					// for orion
					var _transport_protocol = new Object();
					_transport_protocol['name'] = "transport_protocol";
					_transport_protocol['type'] = "Text";
					_transport_protocol['value'] = transport_protocol;
					nd.addStaticAttribute(_transport_protocol);
					// fix: add dataformat_protocol also in the static
					// attributes for orion
					var _dataformat_protocol = new Object();
					_dataformat_protocol['name'] = "dataformat_protocol";
					_dataformat_protocol['type'] = "Text";
					_dataformat_protocol['value'] = dataformat_protocol;
					nd.addStaticAttribute(_dataformat_protocol);

					// DeviceMobile
					if (isMobile == "true") {
						
						//it could exist if we are copying a mobile device
						var existLoc = existLocation();
						
						if (!existLoc){
						
								// device mobile
								var loc = new Object();
								loc['name'] = "location";
								loc['type'] = "geo:point";
								loc['value'] = newdevice.getLatitude() + ","
										+ newdevice.getLongitude();// is it unuseful ?
								nd.addAttribute(loc);
						
						}
					}
					
					var history = $('#historicizing').val();					
					var _history = new Object();
					_history['name'] = "historicizing";
					_history['type'] = "Text";
					_history['value'] = history;
					nd.addStaticAttribute(_history);
					
					var removeOld = $('#removeOld').val();					
					var _removeOld = new Object();
					_removeOld['name'] = "removeOldHistoricalData";
					_removeOld['type'] = "Text";
					_removeOld['value'] = removeOld;
					nd.addStaticAttribute(_removeOld);
					

					var optype = new Object();
					optype['name'] = "opType";
					optype['type'] = "Text";
					optype['value'] = "ready";

					nd.addStaticAttribute(optype);

					/*
					 * Add new static attributes: Owner = organization
					 * device_owner = user The device is part of a unique
					 * organization
					 */
					var organization = new Object();
					organization['name'] = "organization";
					organization['type'] = "Text";
					organization['value'] = $('#organizationId').val();
					nd.addStaticAttribute(organization);

					var device_owner = new Object();
					device_owner['name'] = "device_owner";
					device_owner['type'] = "Text";
					device_owner['value'] = connectedUserId; // username id of connected user
					nd.addStaticAttribute(device_owner);

					var screenId = new Object();
					screenId['name'] = "screenId";
					screenId['type'] = "Text";
					screenId['value'] = newdevice.getName();
					nd.addStaticAttribute(screenId);

					var mobile_device = new Object();
					mobile_device['name'] = "mobile_device";
					mobile_device['type'] = "Text";
					mobile_device['value'] = $('#mobile_device').val();
					nd.addStaticAttribute(mobile_device);
					
					// Add static attributes for hubs for DeMa version 2
					var hub_name = new Object();
					hub_name['name'] = "hub_name";
					hub_name['type'] = "Text";
					hub_name['value'] = $('#myDropdownSelHubMain').val();
					nd.addStaticAttribute(hub_name);
					
					var hub_type = new Object();
					hub_type['name'] = "hub_type";
					hub_type['type'] = "Text";
					if (operationType == "modifyDevice") {
						hub_type['value'] = hubTypeDeviceToModify;
					} else {
						hub_type['value'] = hubMapType[$('#myDropdownSelHubMain').val()];
					}
					nd.addStaticAttribute(hub_type);
					
					if (hubMapType[$('#myDropdownSelHubMain').val()] !== 'idas') {	/*Not idas -> Kapua (and future others)*/
						var gateway = new Object();
						gateway['name'] = "gateway";
						gateway['type'] = "Text";
						if (operationType == "modifyDevice") {
							gateway['value'] = gatewayDeviceToModify;
						} else {
							gateway['value'] = gatewayMap[$('#myDropdownSelGatewayMain').val()];	/*get clientId by key = displayName*/
						}
						nd.addStaticAttribute(gateway);
						
						var asset = new Object();
						asset['name'] = "asset";
						asset['type'] = "Text";
						if (operationType == "modifyDevice") {
							asset['value'] = assetDeviceToModify;
						} else {
							asset['value'] = assetSelectedMap["type"];	/*save the name of the sensor as DeMa device static attribute*/
						}
						nd.addStaticAttribute(asset);
						
						if (kapuaDriver == "modbus") {
							var modbusAddr = new Object();
							modbusAddr['name']="modbus_addr";
							modbusAddr['type']="Text";
							modbusAddr['value']=$('#modbus_addr').val();
							nd.addStaticAttribute(modbusAddr);
						}
					}
					
					$.each(newdevice.getAttributes(), function(i, e) {
						var o = new Object();
						o['name'] = i;
						o['type'] = e;
						nd.addAttribute(o);
					});

					// PUSH/PULL Retrieve data mode
					var retrieveDataModeObj = new Object();
					retrieveDataModeObj['name'] = "retrieve_data_mode";
					retrieveDataModeObj['type'] = "Text";

					if (newdevice.getTransport() == "OPCUA") {
						endpointToPull = $('#device_endpoint').val();
						nd.setEndpoint(endpointToPull);
					}

					var switchStatus = $('.switchRetrieveMode').prop('checked'); // TRUE=PULL,
																					// FALSE=PUSH
					if (switchStatus == true) {
						var retrievedatamode = "pull";

						// Endpoint in case of pull
						endpointToPull = $('#device_endpoint').val();
						nd.setEndpoint(endpointToPull);
						// Load configured commands
						if (Object.keys(newdevice.getCommands()).length >= 0) {
							$.each(newdevice.getCommands(), function(i, e) {
								nd.addCommand(e);
							});
						}
						
						// Load device authentication_method
						var auth_method = $('#device_auth').val();
						var device_auth = new Object();
						device_auth['name'] = "auth_method";
						device_auth['type'] = "Text";
						device_auth['value'] = auth_method;
						nd.addStaticAttribute(device_auth);

						if (auth_method == 'basic') {
							var username = $('#deviceauth_basic_user').val();
							var auth_username = new Object();
							auth_username['name'] = "auth_user";
							auth_username['type'] = "Text";
							auth_username['value'] = username;
							nd.addStaticAttribute(auth_username);

							var pwd = $('#deviceauth_basic_pwd').val();
							var auth_pwd = new Object();
							auth_pwd['name'] = "auth_pwd";
							auth_pwd['type'] = "Text";
							auth_pwd['value'] = pwd;
							nd.addStaticAttribute(auth_pwd);
							newdevice.setCredential(newcredential);

						} else if (auth_method == 'apikey') {
							var apikey = $('#deviceauth_apikey').val();
							var auth_apikey = new Object();
							auth_apikey['name'] = "auth_apikey";
							auth_apikey['type'] = "Text";
							auth_apikey['value'] = apikey;
							nd.addStaticAttribute(auth_apikey);
							newdevice.setCredential(newcredential);

						} else {
							console.log("Device not protected");

						}
					} else {
						retrievedatamode = "push";
						
						//Only for Kapua devices save endpoint in push mode
						if ((operationType == "modifyDevice" && hubTypeDeviceToModify == 'kapua') || (hubMapType[$('#myDropdownSelHubMain').val()] == 'kapua')) {
							endpointToPush = $('#device_endpoint').val();
							const pattern1 = new RegExp('[a-zA-Z]+://\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+');
							//const pattern2 = new RegExp('\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+');
							//const pattern3 = new RegExp('\\d+\\.\\d+\\.\\d+\\.\\d+');
							
							//if (!pattern1.test(endpoint) || !pattern2.test(endpoint) || !pattern3.test(endpoint)) {
							//To consider patterns 2 and 3 add logic to build final address with missing parts and save it in device attribute
							if (!pattern1.test(endpointToPush)) {
								M.toast({
									html : $('#msg_error_kapua_endpoint').text(),
									classes : 'red'
								});
								$('#device_endpoint').focus();
								return false;
							}
							nd.setEndpoint(endpointToPush);
						}
					}
					retrieveDataModeObj['value'] = retrievedatamode;
					nd.addStaticAttribute(retrieveDataModeObj);

					/* CHECK MANDATORRY FIELDS */
					var switchStatus = $('.switchRetrieveMode').prop('checked'); // TRUE=PULL,
																					// FALSE=PUSH
					if (switchStatus == true) {
						// If device is passive/pull: Check if any command has
						// been added
						if (Object.keys(newdevice.getCommands()).length <= 0) {
							M.toast({
								html : $('#msg_command_missing').text(),
								classes : 'red'
							});
							return false;
						}
						// If device is passive/pull: Check if endpoint is
						// inserted
						var endpointToPull = $('#device_endpoint').val();
						if (endpointToPull.trim().length <= 0) {
							M.toast({
								html : $('#msg_endpoint_missing').text(),
								classes : 'red'
							});
							$('#device_endpoint').focus();
							return false;
						}
						// If device is passive/pull: Check if auth method is
						// selected
						var auth_method = $('#device_auth').val();
						newdevice.setAuthMethod(auth_method);
						if (newdevice.getAuthMethod().trim().length <= 0) {
							M.toast({
								html : $('#msg_authmethod_missing').text(),
								classes : 'red'
							});
							return false;
						} else {
							// If device is passive/pull and the config method
							// is selected then check if credential are inserted
							if (auth_method == 'basic') {
								if ($('#deviceauth_basic_user').val().trim().length <= 0
										|| $('#deviceauth_basic_pwd').val()
												.trim().length <= 0) {
									M.toast({
										html : $('#msg_credential_missing')
												.text(),
										classes : 'red'
									});
									return false;
								}
							} else if (auth_method == 'apikey') {
								if ($('#deviceauth_apikey').val().trim().length <= 0) {
									M.toast({
										html : $('#msg_credential_missing')
												.text(),
										classes : 'red'
									});
									return false;
								}
							}
						}
					}

					if (transport_protocol == "OPCUA") {

						var endpointToPull = $('#device_endpoint').val();
						if (endpointToPull.trim().length <= 0) {
							M
									.toast({
										html : $('#msg_server_endpoint_missing')
												.text(),
										classes : 'red'
									});
							$('#device_endpoint').focus();
							return false;
						}

					}

					if (transport_protocol == "LORA") {

						var loraStack = $('#loraStack').val();

						var loraStackStatAttr = new Object();
						loraStackStatAttr['name'] = "loraStack";
						loraStackStatAttr['type'] = "Text";
						loraStackStatAttr['value'] = loraStack;
						nd.addStaticAttribute(loraStackStatAttr);

						var ttnDevEUI = $('#ttnDevEUI').val();
						var ttnAppKey = $('#ttnAppKey').val();
						var ttnDeviceID = $('#ttnDeviceID').val();
						
						
						
						if (ttnDeviceID.trim().length <= 0) {
							M.toast({
								html : $('#insert_ttn_device_id_missing').text(),
								classes : 'red'
							});
							$('#ttnDeviceID').focus();
							return false;
						}											
						

						if (ttnDevEUI.trim().length <= 0) {
							M.toast({
								html : $('#insert_device_eui_missing').text(),
								classes : 'red'
							});
							$('#ttnDevEUI').focus();
							return false;
						}

						var isDevEui64bitExadecimal = is64bitExadecimal(ttnDevEUI);

						if (!isDevEui64bitExadecimal) {
							M.toast({
								html : "Device EUI "
										+ $('#msg_must_be_64bit_exad').text(),
								classes : 'red'
							});
							$('#ttnDevEUI').focus();
							return false;
						}
						
						var isAppKey128bitExadecimal = is128bitExadecimal(ttnAppKey);
						
						
						if (!isAppKey128bitExadecimal) {
							M.toast({
								html : "App Key "
										+ $('#msg_must_be_128bit_exad').text(),
								classes : 'red'
							});
							$('#ttnAppKey').focus();
							return false;
						}
						
						
						

						var ttnAppEUI = $('#ttnAppEUI').val();
						
						var ttnAppId = $('#ttnAppId').val();
						var ttnAccessKey = $('#ttnAccessKey').val();

						if (loraStack == "LORASERVER") {
							var loraDeviceProfileId = $('#loraDeviceProfile')
									.val();

							if (loraDeviceProfileId.trim().length <= 0) {
								M
										.toast({
											html : $(
													'#msg_lora_device_profile_missing')
													.text(),
											classes : 'red'
										});
								$('#loraDeviceProfile').focus();
								return false;
							}

							nd.setLoraDeviceProfileId(loraDeviceProfileId);

						} else if (loraStack == "TTN") {
							
							if (ttnAppId.trim().length <= 0) {
								M.toast({
									html : $('#insert_application_id_missing')
											.text(),
									classes : 'red'
								});
								$('#ttnAppId').focus();
								return false;
							}

							// check mandatory fields
							if (ttnAppEUI.trim().length <= 0) {
								M.toast({
									html : $('#insert_application_eui_missing')
											.text(),
									classes : 'red'
								});
								$('#ttnAppEUI').focus();
								return false;
							}

							var isAppEui64bitExadecimal = is64bitExadecimal(ttnAppEUI);

							if (!isAppEui64bitExadecimal) {
								M.toast({
									html : "Application EUI "
											+ $('#msg_must_be_64bit_exad')
													.text(),
									classes : 'red'
								});
								$('#ttnAppEUI').focus();
								return false;
							}


							if (ttnAccessKey.trim().length <= 0) {
								M.toast({
									html : $('#insert_access_key_missing')
											.text(),
									classes : 'red'
								});
								$('#ttnAccessKey').focus();
								return false;
							}

						}// TTN

						var data_model = $("#dataformat_protocol").val().toLowerCase();

						var appServerAttrs = new Application_server("", "",ttnAccessKey, "");
						var lorawanAtt = new Lorawan(ttnAppEUI, ttnAppId,ttnAppKey, data_model, ttnDevEUI,appServerAttrs);
						var intAttrs = new InternalAttribute(lorawanAtt);

						nd.setInternal_attributes(intAttrs);

					}

					// CHECK MODBUS address for Kapua devices
					if (((operationType == "modifyDevice" && hubTypeDeviceToModify == 'kapua') || (hubMapType[$('#myDropdownSelHubMain').val()] == 'kapua')) && (kapuaDriver == "modbus")) {
						modbusAddress = $('#modbus_addr').val();
						if (modbusAddress.trim().length <= 0) {
							M.toast({
								html : $('#msg_insert_modbus_address').text(),
								classes : 'red'
							});
							$('#modbus_addr').focus();
							return false;
						}
					}

					/* END */

					var devices = new Array();
					devices.push(nd);

					var indevlist = new Object();
					indevlist['devices'] = devices;

					var input = new Object();
					input['action'] = operationType;
					input['payload'] = JSON.stringify(indevlist);

					// send jsonschema
					var attrjsonschema = new Object();
					attrjsonschema['attrjsonschema'] = newattrJsonSchema;
					input['attrjsonschema'] = JSON.stringify(attrjsonschema);
					// clear obj newattrJsonSchema
					newattrJsonSchema = new Array();
					
//					$("#contentloader").fadeIn();
//					$('body').css("overflow", 'hidden');
					toggleLoader(true, "New device");
					console.log("chiamo loader")

					$.ajax({
						url : 'ajaxhandler',
						type : 'GET',
						dataType : 'json',
						data : input,
						headers : _headers,
						success : function(data) {

							var queryparams = new Object();
							queryparams['success'] = true;
							queryparams['scope'] = service;
							queryparams['urbanservice'] = path;
							queryparams['urbanserviceName'] = servicepathName;
							queryparams['msg'] = $('#msg_device_published').text();

							// Chiamata a modale di conferma submit: all'OK
							// modale fare "location.href";
							$('#confirm_modal').modal(
									{
										onOpenEnd : function(modal, trigger) { // Callback
																				// for
																				// Modal
																				// open.
																				// Modal
																				// and
																				// trigger
																				// parameters
																				// available.
											/*
											 * ======= Call backend and fill in
											 * the modal =======
											 */
											// fillConfirmModal('mqtt',
											// 'push');//chiamata a funzione
											// fittizzia per simulare il recupro
											// dei valori della form da mostrare
											// nella modale;
											// Call backend and fill in the
											// modal with data coming from the
											// device jet created
											// var transport_protocol = "mqtt";
											// var device_type = "push"; //
											// ACTIVE/push or PASSIVE/pull
											fillConfirmModal(data);

										},
										onCloseEnd : function() {
											location.href = "devices?" + $.param(queryparams);
										} // Callback for Modal close
									});

							$('#confirm_modal').modal('open');

						},
						error : function(xhr, status, error) {
							var error_msg;
							if (xhr.status>=501) {
								error_msg = "Unable to publish the device: ".concat(xhr.responseJSON.message);
							} else {
								error_msg = $('#msg_unable_to_publish').text()
							}
							output = false;
							console.log(status);
							
							
							M.toast({
								html : error_msg,
								classes : 'red'
							});
						}
						
					}).always(function() {
//						$("#contentloader").fadeOut();
//						$('body').css("overflow", 'auto');
						toggleLoader(false, "New device");
					});

					return false;
				})

$(document).on('submit', '#newattrform', function() {

	var existingattrs = newdevice.getAttributes();
	var newattrname = newattr.getName();

	if (typeof existingattrs[newattrname] !== 'undefined') {
		M.toast({
			html : $('#msg_attribute_exists').text(),
			classes : 'red'
		});
		return false;
	}

	if (existingattrs[newattrname] == 'type') {
		M.toast({
			html : $('#msg_attribute_exists').text(),
			classes : 'red'
		});
		return false;
	}

	// add new attribute to the arraylist
	var _schema = newattr.getJsonschema();
	if (_schema != undefined) {
		newattrJsonSchema.push(newattr);
	}

	newdevice.addAttribute(newattr);
	appendAttribute(newattr);
	newattr = new DeviceAttribute();
	$('#newattr').modal('close');
	return false;

});

$(document).on('submit', '#newassetform', function() {
	
	for (const name in assetAttrMap) {
		if(assetSelectedMap["type"] == name) {
			for (const attrIdx in assetAttrMap[name]) {
				var n = assetAttrMap[name][attrIdx]["name"];
				var t = assetAttrMap[name][attrIdx]["type"];
				newattr = new DeviceAttribute();
				newattr.setName(n);
				newattr.setType(t);
				newdevice.addAttribute(newattr);
				assetappendAttribute(newattr, assetSelectedMap["type"]);
			}
		}
	}
	kapuaDriver = assetDriverMap[assetSelectedMap["type"]];
	if (kapuaDriver == "modbus") {
		$('.kapuadriversection').removeClass('hide');
	} else {
		//TODO: implement behaviour on section/input parameters to show for other drivers 
	}
	$('#addasset').addClass('disabled', 'disabled');
	newattr = new DeviceAttribute();
	$('#newasset').modal('close');
	return false;
});

$(document).on(
		'click',
		'.deleteattr',
		function() {

			var attrid = $(this).attr('data-id');

			if (currentDevice != ""
					&& JSON.parse(currentDevice).mobile_device == "true"
					&& attrid == 'location') {
				M.toast({
					html : $('#msg_not_delete_location').text(),
					classes : 'red'
				});
			} else {
				var deviceattributes = newdevice.getAttributes();
				delete deviceattributes[attrid];

				$(this).closest('li').remove();

				var domitems = $(this).closest('ul#attrappender').find(
						'li:not(.template)');
				var modelitems = newdevice.getAttributes();
				if (domitems.length == 0
						&& (Object.keys(modelitems).length == 0)) {
					$('#noattrs-msg').removeClass("hide");
					$('ul#attrappender').addClass("hide");
				}
			}

			return false;
		});

$(document).on(
		'click',
		'.deleteassetattr',
		function() {
				var deviceattributes = newdevice.getAttributes();
				for (devi in deviceattributes)
					delete deviceattributes[devi];
				//delete deviceattributes[attrid];
				
				$(this).closest('#assetattrappender').find('li:not(.template).newattr').remove();

				var domitems = $(this).closest('ul#assetattrappender').find(
						'li:not(.template)');
				var modelitems = newdevice.getAttributes();
				//check domitems.length == 1 and not 0 because the 'li' with asset name must not be considered
				if (domitems.length == 1
						&& (Object.keys(modelitems).length == 0)) {
					$('#noassetattrs-msg').removeClass("hide");
					$('ul#assetattrappender').addClass("hide");
					$('#addasset').removeClass('disabled', 'disabled');
				}
				$('.kapuadriversection').addClass('hide');
				$('#modbus_addr').val("");

			return false;
		});

// START NEW COMMAND FORM
$(document).on('submit', '#newcommandform', function() {

	var existingcommands = newdevice.getCommands();
	var newcommandname = newcommand.getName();

	if (typeof existingcommands[newcommandname] !== 'undefined') {
		M.toast({
			html : $('#msg_attribute_exists').text(),
			classes : 'red'
		});
		return false;
	}

	newdevice.addCommand(newcommand);
	appendCommand(newcommand);

	newcommand = new DeviceCommand();
	$('#newcommand').modal('close');
	return false;

});

// Remove command from the list
$(document).on(
		'click',
		'.deletecommand',
		function() {

			var commandid = $(this).attr('data-id');
			var devicecommands = newdevice.getCommands();
			delete devicecommands[commandid];

			$(this).closest('li').remove();

			var domitems = $(this).closest('ul#commandappender').find(
					'li:not(.template)');
			var modelitems = newdevice.getCommands();
			if (domitems.length == 0 && (Object.keys(modelitems).length == 0)) {
				$('#nocommands-msg').removeClass("hide");
				$('ul#commandappender').addClass("hide");
			}

			return false;
		});

// END

function prefillDeviceForm() {

	var scope = get('scope');
	var urbanservice = get('urbanservice');

	$('#myDropdownSelContextMain').val(scope);
	$('#myDropdownSelCategoryMain').val(urbanservice);

	// Get values from current device
	var _device = JSON.parse(currentDevice);

	$('#name').val(_device.screenId).trigger('input').attr("disabled", true);
	$('#deviceid').val(_device.id).trigger('input').attr("disabled", true);

	$("#organizationId option[value='" + _device.organization + "']").attr("selected", true);
	$('#organizationId').formSelect();

	$("#transport_protocol option[value='" + _device.transport + "']").attr("selected", true);
	$('#transport_protocol').attr("disabled", true);

	// fix
	var select = $("#dataformat_protocol");
	select.find('option#dataformatul').prop('disabled', false);
	select.find('option#dataformatjson').prop('disabled', false);
	select.find('option#dataformatsigfox').prop('disabled', false);
	select.find('option#dataformatlwm2m').prop('disabled', false);
	select.find('option#dataformatopcua').prop('disabled', false);

	select.val(_device.protocol);
	select.formSelect();

	$("#dataformat_protocol option[value='" + _device.protocol + "']").attr("selected", true);
	$('#dataformat_protocol').attr("disabled", true);

	if (_device.transport == 'COAP' || _device.transport == 'SIGFOX'
			|| _device.transport == 'OPCUA' || _device.transport == 'LORA') {
		$("#retrievedatamodeCheck").attr('disabled', true);
	}

	if (_device.transport == 'LORA') {

		var host = _device.internal_attributes.lorawan.application_server.host;

		var loraStack = getLoraStack(host);
		$('#loraStack').val(loraStack);

		
		$('#ttnDeviceID').val(_device.id);
		
		$('#ttnDevEUI').val(_device.internal_attributes.lorawan.dev_eui);
		$('#ttnAppKey').val(_device.internal_attributes.lorawan.application_key);

		if (loraStack == "TTN") {

			$('#ttnAppEUI').val(_device.internal_attributes.lorawan.app_eui);
			$('#ttnAppId').val(	_device.internal_attributes.lorawan.application_id);
			$('#ttnAccessKey').val(_device.internal_attributes.lorawan.application_server.password);

		}
		
		//prefill the deviceid that with Lora is the ttnDeviceID
		var loraDeviceID = $('#ttnDeviceID').val();
		$('.devicetextfield#deviceid').val(loraDeviceID).trigger('input');

	}


	// Mobile Device
	$("#mobile_device").val(_device.mobile_device);
	$("#isMobileDevice").attr('disabled', true);
	if (_device.mobile_device == "true") {
		$("#isMobileDevice").val("on");
		$("#isMobileDevice").attr('checked', true);
		// hide the map
		$('.devicegeolocation').addClass('hide');
	} else if (_device.mobile_device == "false") {

		$("#isMobileDevice").val("off");
		$("#isMobileDevice").attr('checked', false);
		// hide the map
		$('.devicegeolocation').removeClass('hide');

	}
	
	
	// Historicizing
	$("#historicizing").val(_device.historicizing);
	
	alreadyHistoricizing = _device.historicizing;
	
//	$("#isHistory").attr('disabled', true);
	if (_device.historicizing == true) {
		$("#isHistory").val("on");
		$("#isHistory").attr('checked', true);
		
	} else if (_device.historicizing == false) {

		$("#isHistory").val("off");
		$("#isHistory").attr('checked', false);

	}
	
	

	// Configured Endpoint
	$('#device_endpoint').val(_device.endpoint);
	if (_device.retrieveDataMode == "pull" && _device.transport == "MQTT") {
		$('#device_endpoint').addClass('hide');
	}

	// Configured retrievedatamode // DEVICE PUSH/PULL
	var retrieveDataModeConf = _device.retrieveDataMode;
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	if (retrieveDataModeConf == "pull") {
		$("#retrievedatamodeCheck").val("on");
		$("#retrievedatamodeCheck").attr('checked', true);

		$('.devicepullsection').removeClass('hide');
		$('.deviceEndpoint').removeClass('hide');

		$('.commandsection').removeClass('hide');

		setDevicePullPush(true);

		if (_device.transport == "mqtt") {
			$('#msg_mqtt_topic_communication').removeClass('hide');
		}
		// Authentication method
		var deviceAuthMethod = _device.auth_method;
		var deviceAuthUser = _device.auth_user;
		var deviceAuthPwd = _device.auth_pwd;
		var deviceAuthApikey = _device.auth_apikey;
		$('.deviceauthsection').removeClass('hide');
		
		$('#device_auth').val(deviceAuthMethod);
		
		if (deviceAuthMethod == "none") {
			$('.basic_user_section').addClass('hide');
			$('.basic_pwd_section').addClass('hide');
			$('.auth_apikey_section ').addClass('hide');
		} else if (deviceAuthMethod == "basic") {
			$('.basic_user_section').removeClass('hide');
			$('.basic_pwd_section').removeClass('hide');
			$('.auth_apikey_section ').addClass('hide');
			$("#deviceauth_basic_user").val(deviceAuthUser);
			$("#deviceauth_basic_pwd").val(deviceAuthPwd);
		} else if (deviceAuthMethod == "apikey") {
			$('.basic_user_section').addClass('hide');
			$('.basic_pwd_section').addClass('hide');
			$('.auth_apikey_section ').removeClass('hide');
			$("#deviceauth_apikey").val(deviceAuthApikey);
		}

	}
	// Commands configured
	$.each(_device.commands, function(i, e) {
		var newcommand = new DeviceCommand();
		newcommand.setName(e.name);
		newcommand.setType(e.type);

		newdevice.addCommand(newcommand);
		appendCommand(newcommand);

	});

	// Attribute configured
	$.each(_device.attributes, function(attr_name, attr_type) {
		var newattr_1 = new DeviceAttribute();
		newattr_1.setName(attr_name);
		newattr_1.setType(attr_type);

		newdevice.addAttribute(newattr_1);
		
	// Asset configured (DeMa version 2)
		if(_device.hubType === "kapua") 
			assetappendAttribute(newattr_1, _device.asset);
		else	
			appendAttribute(newattr_1);
	});
	$('#addasset').addClass('disabled', 'disabled');
	$('.deleteassetattr').addClass('hide');
	
	if(_device.hubType == "kapua" && _device.modbusAddr != null && _device.modbusAddr != "") {
		$('.kapuadriversection').removeClass('hide');
		$('#modbus_addr').val(_device.modbusAddr);
		
		/*Inputs that can be set only for a new device, but cannot be changed in edit mode*/
		
		if (actionmenu != "copyDevice") {
			$('#modbus_addr').prop('disabled', true);
			$('#device_endpoint').prop('disabled', true);
		}
		
		
	}
	
	

	var latitude = _device.latitude;
	var longitude = _device.longitude;
	var center = {
		lat : latitude,
		lng : longitude
	};
	initNewDeviceMap(center);

	// re-initialize material-select
	$('select').formSelect();

}// END PREFILL

function getLoraStack(host) {

	if (host == "eu.thethings.network")
		return "TTN";
	else
		return "LORASERVER";

}



// Switch device pull/passive - push/active
$(document).on('click',	'.switchRetrieveMode',function() {
						switchRetrieveMode($(this));
						}
);




function switchRetrieveMode(el){
	
	
	var switchStatus = $(el).prop('checked'); // TRUE=PULL,
	// FALSE=PUSH
	var retrievedatamode = $("#retrievedatamode").val();
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	if (switchStatus == true) {
	// PULL/PASSIVE DEVICE
	retrievedatamode = "pull";
	$("#retrievedatamode").val(retrievedatamode);
	
	$('.devicepullsection').removeClass('hide');
	$('.deviceEndpoint').removeClass('hide');
	//$('#device_endpoint').removeClass('hide');
	//$('#txt_http_device_endpoint').removeClass('hide');
	
	$('.commandsection').removeClass('hide');
	$('.deviceauthsection').removeClass('hide');
	
	var transportProt = $('#transport_protocol').val();
	if (transportProt == "HTTP") {
	$('#device_endpoint').removeClass('hide');
	$('#txt_http_device_endpoint').removeClass('hide');
	$('#txt_mqtt_broker_endpoint').addClass('hide');
	} else if (transportProt == "MQTT") {
	$('#device_endpoint').addClass('hide');
	$('#msg_mqtt_topic_communication').removeClass('hide');
	$('#txt_http_device_endpoint').addClass('hide');
	}
	} else {
	// PUSH/ACTIVE DEVICE
	retrievedatamode = "push";
	$("#retrievedatamode").val(retrievedatamode);
	
	$('.devicepullsection').addClass('hide');
	$('.deviceEndpoint').addClass('hide');
	
	$('.commandsection').addClass('hide');
	$('.deviceauthsection').addClass('hide');
	}
}




// REVIEW FUNCTION fillConfirmModal
/**
 * Fill the data received from backend into the confirm modal
 */
function fillConfirmModal(data) {

	var apikey = data.apikey;
	var dataformatProtocol = data.dataformat;
	var endpoint = data.endpoint;
	var measuresEndpoint = data.measuresEndpoint;
	
	var name = data.name;
	var organization = data.organization;
	var transportProtocolValue = data.transport; // HTTP/MQTT
	var retrieveDataMode = data.retrieveDataMode; // PUSH, PULL
	var serverEndpoint = data.serverEndpoint; // PUSH, PULL
	var deviceId = data.deviceId;

	var mqttpassword = data.mqttpassword;
	var mqttusername = data.mqttusername;
	var sslcertlink = data.sslcertlink;

	var loraStackV = data.loraStack;

	// FILL IN THE MODAL
	$('#deviceName').html(name);
	$('#deviceIdConfirm').html(deviceId);
	$('#transportProtocolValue').html(transportProtocolValue.toUpperCase());
	$('#dataformatProtocolValue').html(dataformatProtocol.toUpperCase());

	if (deviceId == undefined)
		$('#deviceIdConfirmNoEmpty').addClass('hide');

	if (retrieveDataMode.toLowerCase() == 'push') {
		$('#pullline').addClass('hide');
		$('#pushline').removeClass('hide');

		$('#pushhttpdeviceEndpoint').html(measuresEndpoint);
		$('#pushmqttbrokerEndpoint').html(endpoint);
		$('#apikeyValue').html(apikey);

		if (transportProtocolValue.toLowerCase() == 'http') {
			$('.mqtt_topic').addClass('hide');
			$('#push_http_device_endpoint').removeClass('hide');
			$('#push_mqtt_broker_endpoint').addClass('hide');
		} else if (transportProtocolValue.toLowerCase() == 'mqtt') {// mqtt
			$('.mqtt_topic').removeClass('hide');
			// ["/+/+/attrs/+","/+/+/attrs","/+/+/configuration/commands","/+/+/cmdexe"]
			$('#attributeTopic').html(data.topic);
			$('#push_http_device_endpoint').addClass('hide');
			$('#push_mqtt_broker_endpoint').removeClass('hide');

			if (
				(typeof mqttusername !== 'undefined' && mqttusername.trim().length > 0)
				||
				(typeof sslcertlink !== 'undefined' && sslcertlink.trim().length > 0)
				) {
				$('.mqtt_auth').removeClass('hide');

				if (typeof mqttusername !== 'undefined' && mqttusername.trim().length > 0) {
					$('#mqttUsernameT').html(mqttusername);
					$('#mqttPasswordT').html(mqttpassword);

					$('#mqttUsername').removeClass('hide');
					$('#mqttPassword').removeClass('hide');
				}

				if (typeof sslcertlink !== 'undefined' && sslcertlink.trim().length > 0) {
					$('#mqttCertLinkT').html(sslcertlink);
					$('#mqttCertLinkT').attr("href", sslcertlink)
					$('#mqttCertLink').removeClass('hide');
				}
			}

		} else if (transportProtocolValue.toLowerCase() == 'sigfox'
				|| transportProtocolValue.toLowerCase() == 'coap') {
			$('.mqtt_topic').addClass('hide');
			$('#push_http_device_endpoint').addClass('hide');
			$('#push_mqtt_broker_endpoint').addClass('hide');

		}
		if (transportProtocolValue.toLowerCase() == 'opcua') {
			$('.mqtt_topic').addClass('hide');
			$('#push_http_device_endpoint').removeClass('hide');
			$('#push_mqtt_broker_endpoint').addClass('hide');

			$('#opcuaServerEndpoint').html(serverEndpoint);
			$('#serverOpcuaEndpointLine').removeClass('hide');

		}
		if (transportProtocolValue.toLowerCase() == 'lora') {

			$('#loraStackConf').html(loraStackV);
			$('#loraStackLine').removeClass('hide');
			$('#apikeyLine').addClass('hide');

		}

	} else if (retrieveDataMode.toLowerCase() == 'pull') {// PULL
		$('#pullline').removeClass('hide');
		$('#pushline').addClass('hide');

		$('#pullhttpdeviceEndpoint').html(endpoint);
		$('#pullmqttbrokerEndpoint').html(endpoint);
		$('#deviceApikeyValue').html(apikey);

		if (transportProtocolValue.toLowerCase() == 'http') {
			$('.mqtt_topic').addClass('hide');
			$('#pull_http_device_endpoint').removeClass('hide');
			$('#pull_mqtt_broker_endpoint').addClass('hide');
		} else if (transportProtocolValue.toLowerCase() == 'mqtt') {// mqtt
			$('.mqtt_topic').removeClass('hide');
			// ["/+/+/attrs/+","/+/+/attrs","/+/+/configuration/commands","/+/+/cmdexe"]
			$('#attributeTopic').html(data.topic);
			
			var subTopic = data.topic;
			var pubTopic = subTopic.replace("cmd", "attrs");
			
			$('#attributeTopicPub').html(pubTopic);
			$('#mqttPullTopicPub').removeClass('hide');
			
			
			
			$('#pull_http_device_endpoint').addClass('hide');
			$('#pull_mqtt_broker_endpoint').removeClass('hide');
			
			
			if (
					(typeof mqttusername !== 'undefined' && mqttusername.trim().length > 0)
					||
					(typeof sslcertlink !== 'undefined' && sslcertlink.trim().length > 0)
					) {
					$('.mqtt_auth').removeClass('hide');

					if (typeof mqttusername !== 'undefined' && mqttusername.trim().length > 0) {
						$('#mqttUsernameT').html(mqttusername);
						$('#mqttPasswordT').html(mqttpassword);

						$('#mqttUsername').removeClass('hide');
						$('#mqttPassword').removeClass('hide');
					}

					if (typeof sslcertlink !== 'undefined' && sslcertlink.trim().length > 0) {
						$('#mqttCertLinkT').html(sslcertlink);
						$('#mqttCertLinkT').attr("href", sslcertlink)
						$('#mqttCertLink').removeClass('hide');
					}
				}
			
		}

	}
}

// Device authentication method
$(document).on('change', '#device_auth', function() {

	var selAuthMethod = $(this).val();

	if (selAuthMethod == 'basic') {
		$('.basic_user_section').removeClass('hide');
		$('.basic_pwd_section').removeClass('hide');
		$('.auth_apikey_section').addClass('hide');
	} else if (selAuthMethod == 'apikey') {
		$('.auth_apikey_section').removeClass('hide');
		$('.basic_user_section').addClass('hide');
		$('.basic_pwd_section').addClass('hide');
	} else {
		$('.basic_user_section').addClass('hide');
		$('.basic_pwd_section').addClass('hide');
		$('.auth_apikey_section').addClass('hide');
	}

});

// forbidden chars
const regex = /[A-Za-z0-9]+/g
var toastPresent = false;
//underscore (_) and dash (-) removed from allowed characters because in KapuaKura are not allowed in configuration names and we build them appending the device name to them
var checkForbiddenChars = function(string, oKeyEvent) {
	// console.log(oKeyEvent.key);
	var stringValue = string.value;
	var isvalid = false;
	if (oKeyEvent.key === 'Delete') {
		isvalid = true;
	} else if (oKeyEvent.key === 'Backspace') {
		isvalid = true;
	} else if (/[\\|!"£\$%&\/\(\)=\?'\^ìèé\+\*\]\}òç@à°#ù§,;\.:<>€_-]/gi
			.test(oKeyEvent.key)) {
		isvalid = false;
		string.value = string.value.substring(0, string.value.length - 1)
		if (!toastPresent) {
			toastPresent = true;
			M.toast({
				html : $('#msg_forbidden_characters').text(),
				classes : 'red',
				completeCallback : function() {
					toastPresent = false;
				}
			});
		}
	} else if (/[A-Za-z0-9]/gi.test(oKeyEvent.key)) {
		isvalid = true;
	}
	if (stringValue === "type") {
		isvalid = false;
		if (!toastPresent) {
			toastPresent = true;
			M.toast({
				html : $('#msg_forbidden_type').text(),
				classes : 'red',
				completeCallback : function() {
					toastPresent = false;
				}
			});
		}
	}

	return isvalid;
}

var checkName = function(string, oKeyEvent) {

	var isValid = checkForbiddenChars(string, oKeyEvent);
	var transport_protocol = $('#transport_protocol').val();
	
	if (isValid && transport_protocol != "lora") {
		$('.devicetextfield#deviceid').val(string.value).trigger('input');
	}
	return isValid;
}

var checkAttributeName = function(string, oKeyEvent) {
	return checkForbiddenChars(string, oKeyEvent);
}

var checkAttributeType = function(string, oKeyEvent) {
	return checkForbiddenChars(string, oKeyEvent);
}

var checkCmdName = function(string, oKeyEvent) {
	return checkForbiddenChars(string, oKeyEvent);
}

var checkCmdType = function(string, oKeyEvent) {
	return checkForbiddenChars(string, oKeyEvent);
}

var checkModbusType = function(string, oKeyEvent) {
	return checkForbiddenChars(string, oKeyEvent);
}

function setOPCUAAttr(isMes) {

	// search old attrs from data models and delete them
	var deviceattributes = newdevice.getAttributes();
	for (devi in deviceattributes)
		delete deviceattributes[devi];

	// search old attrs from DOM and delete them
	var domitems = $('ul#attrappender').find('li:not(.template)');
	for (i = 0; i < domitems.length; i++) {

		domitems[i].remove();
	}

	if (isMes == false) {

		var newattr_1 = new DeviceAttribute();
		newattr_1.setName("Pressure");
		newattr_1.setType("number");
		newdevice.addAttribute(newattr_1);
		appendAttribute(newattr_1);

		var newattr_2 = new DeviceAttribute();
		newattr_2.setName("Flow");
		newattr_2.setType("number");
		newdevice.addAttribute(newattr_2);
		appendAttribute(newattr_2);

		var newattr_3 = new DeviceAttribute();
		newattr_3.setName("ActivePowerL1");
		newattr_3.setType("number");
		newdevice.addAttribute(newattr_3);
		appendAttribute(newattr_3);

	} else {

		var newattr_1 = new DeviceAttribute();
		newattr_1.setName("Busy");
		newattr_1.setType("Boolean");
		newdevice.addAttribute(newattr_1);
		appendAttribute(newattr_1);

		var newattr_2 = new DeviceAttribute();
		newattr_2.setName("RFIDTagPresent");
		newattr_2.setType("Boolean");
		newdevice.addAttribute(newattr_2);
		appendAttribute(newattr_2);

		var newattr_3 = new DeviceAttribute();
		newattr_3.setName("Done");
		newattr_3.setType("Boolean");
		newdevice.addAttribute(newattr_3);
		appendAttribute(newattr_3);

		var newattr_4 = new DeviceAttribute();
		newattr_4.setName("StationEntryxBG5");
		newattr_4.setType("Boolean");
		newdevice.addAttribute(newattr_4);
		appendAttribute(newattr_4);

		var newattr_5 = new DeviceAttribute();
		newattr_5.setName("ReadyAtStationxBG1");
		newattr_5.setType("Boolean");
		newdevice.addAttribute(newattr_5);
		appendAttribute(newattr_5);

		var newattr_6 = new DeviceAttribute();
		newattr_6.setName("StationExitxBG6");
		newattr_6.setType("Boolean");
		newdevice.addAttribute(newattr_6);
		appendAttribute(newattr_6);

		var newattr_7 = new DeviceAttribute();
		newattr_7.setName("OrderNo");
		newattr_7.setType("number");
		newdevice.addAttribute(newattr_7);
		appendAttribute(newattr_7);

		var newattr_8 = new DeviceAttribute();
		newattr_8.setName("OrderPosition");
		newattr_8.setType("number");
		newdevice.addAttribute(newattr_8);
		appendAttribute(newattr_8);

		var newattr_9 = new DeviceAttribute();
		newattr_9.setName("CarrierID");
		newattr_9.setType("number");
		newdevice.addAttribute(newattr_9);
		appendAttribute(newattr_9);

		var newattr_10 = new DeviceAttribute();
		newattr_10.setName("WorkPlanNo");
		newattr_10.setType("number");
		newdevice.addAttribute(newattr_10);
		appendAttribute(newattr_10);

		var newattr_11 = new DeviceAttribute();
		newattr_11.setName("StepNo");
		newattr_11.setType("number");
		newdevice.addAttribute(newattr_11);
		appendAttribute(newattr_11);

		var newattr_12 = new DeviceAttribute();
		newattr_12.setName("OperationNo");
		newattr_12.setType("number");
		newdevice.addAttribute(newattr_12);
		appendAttribute(newattr_12);

	}
}

//Check if the attribute location already exists
function existLocation(){
	
	var attrName = $('.attrname');

	var existsLocation = false; 
	$(attrName).each(function(i, objEl) {

		var elemO = $( objEl );
		
		if (elemO.text() == "location")
			existsLocation = true;
		
	});
	return existsLocation;
}

/* to remove "Device attributes" elements and 
   related variables whenever the selection of 
   Transport Protocol element changes
*/
function removeAttributes(){
	var deviceattributes = newdevice.getAttributes();
	for (devi in deviceattributes)
		delete deviceattributes[devi];

	// search old attrs from DOM and delete them
	var domitems = $('ul#attrappender').find('li:not(.template)');
	for (i = 0; i < domitems.length; i++) {
		domitems[i].remove();
	}
}

function populateAssetlist(){	
	var input = new Object();
	input['action'] = 'getAssets';
	input['pagesize'] = '50';
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			$('#newasset_type').empty();
			$('#newasset_type').append($('<option>', {value:"", text:"Choose an asset", selected: true}));
			$.each(data, function(i, e){
				$('#newasset_type').append($('<option>', {value:e.name, text:e.name, selected: false}));
				assetTypeMap[e.name] = e.type;
				var attribs = JSON.parse(e.attributes);
				assetAttrMap[e.name] = attribs;
				assetAttrListMap[e.name] = e.attributes;
				assetDriverMap[e.name] = e.driverPid;
			});
		},
		error : function(xhr, status, error) {
			toggleLoader(false, "populateAssetlist error");
			console.log(error);
			return false;
		}
	});
	
	return false;
}
