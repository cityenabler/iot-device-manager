
// encodeBase64

jQuery.base64 = (function($) {
	var _PADCHAR = "=", _ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", _VERSION = "1.0";
	function _getbyte64(s, i) {
		var idx = _ALPHA.indexOf(s.charAt(i));
		if (idx === -1) {
			throw "Cannot decode base64"
		}
		return idx
	}
	function _decode(s) {
		var pads = 0, i, b10, imax = s.length, x = [];
		s = String(s);
		if (imax === 0) {
			return s
		}
		if (imax % 4 !== 0) {
			throw "Cannot decode base64"
		}
		if (s.charAt(imax - 1) === _PADCHAR) {
			pads = 1;
			if (s.charAt(imax - 2) === _PADCHAR) {
				pads = 2
			}
			imax -= 4
		}
		for (i = 0; i < imax; i += 4) {
			b10 = (_getbyte64(s, i) << 18) | (_getbyte64(s, i + 1) << 12)
					| (_getbyte64(s, i + 2) << 6) | _getbyte64(s, i + 3);
			x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 255, b10 & 255))
		}
		switch (pads) {
		case 1:
			b10 = (_getbyte64(s, i) << 18) | (_getbyte64(s, i + 1) << 12)
					| (_getbyte64(s, i + 2) << 6);
			x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 255));
			break;
		case 2:
			b10 = (_getbyte64(s, i) << 18) | (_getbyte64(s, i + 1) << 12);
			x.push(String.fromCharCode(b10 >> 16));
			break
		}
		return x.join("")
	}
	function _getbyte(s, i) {
		var x = s.charCodeAt(i);
		if (x > 255) {
			throw "INVALID_CHARACTER_ERR: DOM Exception 5"
		}
		return x
	}
	function _encode(s) {
		if (arguments.length !== 1) {
			throw "SyntaxError: exactly one argument required"
		}
		s = String(s);
		var i, b10, x = [], imax = s.length - s.length % 3;
		if (s.length === 0) {
			return s
		}
		for (i = 0; i < imax; i += 3) {
			b10 = (_getbyte(s, i) << 16) | (_getbyte(s, i + 1) << 8)
					| _getbyte(s, i + 2);
			x.push(_ALPHA.charAt(b10 >> 18));
			x.push(_ALPHA.charAt((b10 >> 12) & 63));
			x.push(_ALPHA.charAt((b10 >> 6) & 63));
			x.push(_ALPHA.charAt(b10 & 63))
		}
		switch (s.length - imax) {
		case 1:
			b10 = _getbyte(s, i) << 16;
			x.push(_ALPHA.charAt(b10 >> 18) + _ALPHA.charAt((b10 >> 12) & 63)
					+ _PADCHAR + _PADCHAR);
			break;
		case 2:
			b10 = (_getbyte(s, i) << 16) | (_getbyte(s, i + 1) << 8);
			x.push(_ALPHA.charAt(b10 >> 18) + _ALPHA.charAt((b10 >> 12) & 63)
					+ _ALPHA.charAt((b10 >> 6) & 63) + _PADCHAR);
			break
		}
		return x.join("")
	}
	return {
		decode : _decode,
		encode : _encode,
		VERSION : _VERSION
	}
}(jQuery));



function capitalize (s) {
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
  }



/* Get Scope Name */
function getServiceName(id){
	var scope = id;
	
	var input = new Object();
	input['action'] = 'getServiceName';
	input['id'] = scope; 
	
	
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			$('#scope_breadcrumb').text(capitalize(data));
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});
};


/* Get Urbanservice Name */
function getServicePathName(refScopeId, urbanserviceId){
	
	var input = new Object();
	input['action'] = 'getServicePathName';
	input['service'] = refScopeId; 
	input['servicepath'] = urbanserviceId; 
		
	$.ajax({
		url: 'ajaxhandler',
		type : 'GET',
		dataType : 'json',
		data : input,
		success : function(data) {
			$('#urbanservice_breadcrumb').text(capitalize(data));
		},
		error : function(xhr, status, error) {
			console.log(status);
		}
	});
};


function isAttributeTypeNumberOrSimilar(type){
	
	if (type=="number" || type=="Number" || 
	type=="float" || type=="Float"|| 
	type=="double" || type=="Double" ||
	type=="decimal" || type=="Decimal" ||
	type=="integer" || type=="Integer" ||
	type=="int" || type=="Int"){
		return true;
	}else{
		return false;
		
	}
}

function isAttributeTypeIntegerOrSimilar(type){
	
	if (type=="number" || type=="Number" || 
	type=="integer" || type=="Integer" ||
	type=="int" || type=="Int"){
		return true;
	}else{
		return false;
		
	}
}





function isAttributeTypeTextOrSimilar(type){
	
	if (type=="string" || type=="String" || 
	type=="text" || type=="Text"|| 
	type=="chars" || type=="Chars" ){
		return true;
	}else{
		return false;
		
	}
}

function isAttributeTypeDateOrSimilar(type){
	
	if (type=="date" || type=="Date" || 
	type=="dateTime" || type=="DateTime" || 
	type=="datetime" || type=="Datetime" || 
	type=="timestamp" || type=="Timestamp" ||
	type=="timeStamp" || type=="TimeStamp"	){
		return true;
	}else{
		return false;
		
	}
}

function isAttributeTypeBooleanOrSimilar(type){
	
	if (type=="boolean" || type=="Boolean")
		return true;
	else
		return false;
}


function isAttributeTypeGeolocationOrSimilar(type){
	
	if (type=="position" || type=="Position"|| 
		type=="geo:point" || type=="geo:Point" ||
		type=="geoLocation" || type=="geoLocation" )
		return true;
	else
		return false;
}

function isBinary(type){
	if(type=="binary"){
		return true;
	} else 
		return false;
}

function isHexadecimal(type) {
	if(type=="hex"){
		return true;
	} else
		return false;
}

function is64bitExadecimal(deveui){
	
	var re = /[0-9A-Fa-f]{6}/g;

	if (deveui.length != 16)
	    return false;
	    
	if(re.test(deveui))
		return true
	else
		return false
}
/*Check if an url is valid */
function validURL(str) {
	 var  regex =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
	  if(!regex .test(str))
	    return false;
	  else 
	    return true;
	}

function is128bitExadecimal(deveui){
	
	var re = /[0-9A-Fa-f]{6}/g;

	if (deveui.length != 32)
	    return false;
	    
	if(re.test(deveui))
		return true
	else
		return false
}

/**
* https://stackoverflow.com/a/57097715/4173912
* Removes URL parameters
* @param removeParams - param array
*/
function removeURLParameters(removeParams) {
  const deleteRegex = new RegExp(removeParams.join('=|') + '=')

  const params = location.search.slice(1).split('&')
  let search = []
  for (let i = 0; i < params.length; i++) if (deleteRegex.test(params[i]) === false) search.push(params[i])

  window.history.replaceState({}, document.title, location.pathname + (search.length ? '?' + search.join('&') : '') + location.hash)
}