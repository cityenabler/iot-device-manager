var cfeActive = tools["cfeTool"].active;
var cfeLink = tools["cfeTool"].link;

$(document).ready(function(){
	
	mainOnDocReady();
	
	$('.modal').modal({dismissible: false});
	
});


function doesFileExist(urlToFile, gradient, item){
	var ext=[".jpg"];
	
	console.log(cfeActive)
	if(cfeActive){
		var urlToFile = cfeLink+'/'+urlToFile;
	}
	
	for (var i = 0; i < ext.length; i++) {
		var file = urlToFile+ext[i];
	
	
	    $.ajax({
	        url: file,
	        type:'HEAD',
//	        username: 'user',
//	        password: 'pass',
	        crossDomain : true,
	        xhrFields: {
	            withCredentials: false
	    }
	    })
	        .done(function() {
	        	item.find(".catanchor").css('background', 'url('+file+') no-repeat center').css('background-size','cover');
	        })
	        .fail(function() {
		        console.log("File doesn't exist: "+gradient+" "+file);
	        })
	}
	
}

function appendItems(value, url){

	var template = $('#catlist').find('.catitem.template');
	var item = template.clone();
	item.removeClass('template hide');
	var bkgColor = "";
	if($(".catanchor.primary").length){
		bkgColor = $(".catanchor.primary").css("background-color").replace(')',', 0.5)');
		item.find(".catanchor").removeClass('primary');
	}else{
		bkgColor = $(".catanchor.secondary").css("background-color").replace(')',', 0.5)');
		item.find(".catanchor").removeClass('secondary');
	}
	
	
	item.find(".catname").text(value);
	

	
	var imgName = value.replace(new RegExp(" ", 'g'), "_").toLowerCase();
	
	var img = doesFileExist("context_images/"+imgName, bkgColor, item);
	console.log("IMG: "+img)
	item.find(".catanchor").css('background', img +' no-repeat center').css('background-size','cover');
	item.find(".catanchor").attr("href", url);
	
	var appender = $('#catlist .row #newcattrigger');
	
	appender.before(item);

}




