$(document).ready(function(){
		
	onDocReady();
	
});

var checkedall = false;
var deviceperpage = 6;
var currpage = 1;
var urbanserviceslist = new Object();
var usUsed = new Object();

function onDocReady(){
	mainOnDocReady();
	//get latitude, longitude and zoom
	getMapCenter();
	
	$('.modal').modal({dismissible:false});
	var scope = get('scope');
	
	// Get the refScope name
	getServiceName(scope);
	
	//Populate the scopelist through REST API invocation
	try{ 
		var input = new Object();
		input['action'] = 'getServicePaths';
		input['service'] = scope; 

		$.ajax({
			url: 'ajaxhandler',
			type : 'GET',
			dataType : 'json',
			data : input,
			success : function(data) {
				$.each(data, function(i, e){
				
					var queryparams = new Object();
						queryparams['scope'] = scope;
						queryparams['urbanservice'] = e.id;
				
					var querystring = $.param(queryparams);
					
					if ( !(e.name.value.toLowerCase().indexOf("kpi") > -1 || e.name.value.toLowerCase().indexOf("issues") > -1)) {
						console.log("*************" + e.id);
						appendItems(e, 'devices?'+querystring);
						usUsed[e] = true;
					} 
						
				});
				
				$('.delsubcat').removeClass("hide");
				$.each(uslist, function(i, e){
					if(typeof usUsed[e] === 'undefined' || !usUsed[e]){
						$('#newurbanservicetrigger').removeClass('hide');
					}
				});
				appendSelectOption(usUsed);
				
				
				$('#pageloader').fadeOut();
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
	}
	catch(error){
		console.log(error);
	}
	
};

function appendItems(value, url){

	if(typeof value.id === 'undefined' || value.id == null || value.id == "" || value.id == "/"){
		
	    return;
	}
	
		var template = $('#urbanserviceslist').find('.urbanserviceitem.template');
		var item = template.clone();
		item.removeClass('template hide');
		
		item.addClass(value.name.value);
		item.find('.block').css("background-image","url('imgs/icons/"+value.name.value.toLowerCase()+".svg')");
		item.find('.block').addClass(uslistcolor[value.name.value.toLowerCase()]);
		
		var replaceChars={"_":" " };
		var servicename = value.name.value;
		item.find(".urbanservicename").text(servicename.replace(/_/g,function(match) {return replaceChars[match];}));
		item.find(".serviceanchor").attr("href", url);
		item.append('<input type="hidden" name="us_id" value=' + value.id + ' />');
		
		var appender = $('#urbanserviceslist .row #newurbanservicetrigger');
		
		appender.before(item);

}

function appendSelectOption(usUsed){
	uslist.forEach(function(e){
		e=e.replace(" ","_");
		//e=e.replace(/_/g,function(match) {return replaceChars[match];})
		var us = usUsed[e];
		if(typeof us === "undefined" || us == null || !us){
			 e=e.replace("_"," ");
			 $('#subcategorynamefield').append($('<option>', {
			    value: e,
			    text: e
			 }));
		}
	});
	
	$('#subcategorynamefield').formSelect();
}










