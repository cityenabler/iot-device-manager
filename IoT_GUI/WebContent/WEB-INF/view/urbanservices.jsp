<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms">
	<c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
		<c:set var = "isSeller" scope = "session" value = "true"/>
	 </c:if>
</c:forEach>



<html class="iotmanager">
<head>
	<title><i18n:message value="apptitle"/></title>
	<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
	
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="css/materialize.css">
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/urbanservices.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="js/materialize.min.js"></script>
   
   	<!-- DE Tools file configuration from EnablerShowcase -->
	<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
		
    
</head>

<body class="materialize">
	<%@include file="../frames/config.jspf"%>
	<%@include file="../frames/loader.jspf"%>
	
	<header>
		<%@include file="../frames/navbar.jspf"%>
	</header>

	<div class="section">
		<div class="h-40">
		<div class="row  valign-wrapper">
			<div class="col valign">
					<a href="index" class="btn-floating waves-effect waves-light left" id="contex_breadcrumb">
					<i class="material-icons ">arrow_back</i>
				</a>
			</div>
			<div class="col breadcrumb_bar nav-wrapper valign">
		       <a href="index" id="scope_breadcrumb" class="breadcrumb"><i18n:message value="scope"/></a>
			</div>
		</div>
	</div>
	</div>

	<div class="section container">
		<div class="row hide" id="errorMessage"></div>
		
		<c:if test = "${fn:containsIgnoreCase(urboIntegration, 'false')}">
			<c:if test = "${isSeller}"> 
				<div>
					<%@include file="../frames/urbanservice_mapcenter.jspf" %>
				</div>
			</c:if>
		</c:if>	
		
		<div id="urbanserviceslist">
			<%@include file="../frames/urbanserviceitem.jspf" %>
			<div class="row">
					<div class="col s12 m4 l3 urbanserviceitem" id="newurbanservicetrigger">
					<!-- If urboIntegration is true, it's not possible create urbanservice -->
						<!-- If is SELLER can create a urban service -->
						<c:if test = "${fn:containsIgnoreCase(urboIntegration, 'false')}">
							<c:if test = "${isSeller}"> 
								<div class="hoverable z-depth-1 block nopadding" >
									<a href="#newsubcatmodal" class="serviceanchor center-align secondary-text urbanservicename">
										<i class="material-icons">add</i>
										<br/>
										<span><i18n:message value="new_urbanservice"/></span>
									</a>
								</div>
							</c:if>
						</c:if>
					</div>
				
			</div>
		</div>
		<div id="push"> </div>
	</div>

	<%@include file="../frames/footer.jspf"%>
	
	<%@include file="../frames/urbanservice_newmodal.jspf"%>
	
	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/urbanservices.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<script type="text/javascript" src="js/urbanservicesMapcenter.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<%@include file="../frames/messages.jspf"%>
	
</body>

</html>
