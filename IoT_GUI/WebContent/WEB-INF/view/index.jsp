<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms"> 
 	<c:if test = "${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
 		<c:set var = "isSeller" scope = "session" value = "true"/> 
 	 </c:if> 
 </c:forEach> 

<%--  <td><c:out value="${urboIntegration}"></c:out></td> --%>


<html class="iotmanager">
<head>
	<title><i18n:message value="apptitle"/></title>
	<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		
	<!-- Compiled and minified CSS -->
 	<link rel="stylesheet" href="css/materialize.css">
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/index.css"/>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<!-- Compiled and minified JavaScript -->
 	<script src="js/materialize.min.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
 		
	<!-- DE Tools file configuration from EnablerShowcase -->
		<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
</head>

<body class="materialize">
	<%@include file="../frames/config.jspf"%>
	<%@include file="../frames/loader.jspf"%>
	<div id="containerLayout">
		
		<div id="headerLayout">
			<header>
				<%@include file="../frames/navbar.jspf"%>
			</header>
		</div>
		<div id="bodyLayout">
			<div class="container">
				<div class="row hide" id="errorMessage"></div>
			
				<div id="nrcat" class="section center"><b><%-- <span>0</span>&nbsp;<i18n:message value="scopes"/>--%></b></div> 
		  
				<div id="catlist">
					<%@include file="../frames/categoryitem.jspf" %>
					
					<!-- isSeller and is integrated with urbo -->
						<div class="row">
								<div class="col s12 m6 l4 catitem " id="newcattrigger">
								<c:if test = "${fn:containsIgnoreCase(urboIntegration, 'false')}">
									<c:if test = "${isSeller}">
										<div class="hoverable z-depth-1 block nopadding">
											<a href="#newcatmodal"  class="catanchor center-align secondary-text catname">
												<i class="material-icons">add</i>
												<br/>
												<span><i18n:message value="new_scope"/></span>
											</a>
										</div>
										</c:if>
									</c:if>
								</div>
							</div>
				
					
				</div>
			</div>
		</div>
		<div id="footerLayout">
			<%@include file="../frames/footer.jspf"%>
		</div>
		
<!-- 	MODALI -->
		<%@include file="../frames/settings_newcatmodal.jspf"%>
	</div>

	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?"></script>
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/index.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/category.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
<%-- 	<%@include file="../frames/messages.jspf"%> --%>
	
	
</body>

</html>
