<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<html class="iotmanager">
<head>
<title><i18n:message value="apptitle" /></title>
<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- Compiled and minified CSS -->
 <link rel="stylesheet" href="css/materialize.css">

<link rel="stylesheet" href="css/main.css" />
<link rel="stylesheet" href="css/newdevice.css" />
<link rel="stylesheet" href="css/maps.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"  crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"  crossorigin=""></script>
							
	<link rel="stylesheet" href="css/leaflet.extra-markers.min.css">
	<link rel="stylesheet" href="css/leaflet-search.min.css">
	<link rel="stylesheet" href="css/leaflet-search.src.css">
	<script src="js/leaflet.extra-markers.min.js"></script>
	<script src="js/leaflet-search.src.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="js/materialize.min.js"></script>
	
<script type="text/javascript">
	var currentDevice = '${currentDevice}';
	var actionmenu = '${actionmenu}';
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

<!-- DE Tools file configuration from EnablerShowcase -->
<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 

</head>

<body class="materialize">
<%@include file="../frames/config.jspf"%>

	<header>
		<%@include file="../frames/navbar.jspf"%>
	</header>
	<div class="container">
	<br>
		<div class=" white">
			<div class="section">
				<div class="row">
					<div class="col s2 valign">
					</div>
					<div class="col s8 center">
						<h4 id="new_device_title" class="hide">
							<i18n:message value="new_camera" />
						</h4>
						<h4 id="edit_device_title" class="hide">
							<i18n:message value="edit_camera" />
						</h4>
					</div>
					<div class="col s2">
						<a href="home" id="backarrow" class="btn-floating waves-effect waves-light right" > 
							<i class="material-icons">close</i>
						</a>
					</div>
				</div>
			</div>

			<div class="row">
				<form action="#!" id="newdeviceform">
						
					<div class="row">
						<div id="filterSelect" class="col s10 offset-s1">
							<%@include file="../frames/context_menu.jspf"%>
						</div>
					</div>	
						
					<div class="row">
						<div class="col s12">
	
							<input id="deviceid" type="hidden" class="devicetextfield"  name="id">
	
							<!-- Name and Organization -->
							<div class="row">
								<!-- 			Organization name -->
								<script type="text/javascript">
									var connectedUserId = '${userId}';
									var connectedUserOrgId = '${organizationId}';
									var connectedUserOrgName = '${organizationName}';
								</script>
	
								<div class="input-field col s10 offset-s1 m5 push-m6">
									<select id="organizationId" class="devicetextfield">
										<c:forEach items="${requestScope.organization}"
											var="organization">
											<option value="${organization.id}">${organization.name}</option>
										</c:forEach>
									</select> <label for="organizationId"><i18n:message value="user_organization" /></label>
								</div>
								<!-- 			Device name -->
								<div class="input-field col s10 offset-s1 m5 pull-m5 offset-m1">
								    <!-- removed _ and - from allowed characters for checkName because not allowed in KapuaKura -->
									<input id="name" type="text" class="validate devicetextfield" 
										onkeyup="return checkName(this, event);"
										pattern="[A-Za-z0-9]+"
										required name="name"
										placeholder='<i18n:message value="insert_camera_name"/>'>
									<label for="name"><i18n:message value="camera_name" /></label>
								</div>
							</div>
							
							<div class="row">
								<!-- Camera sreaming url -->
								<div class="input-field col s10 offset-s1 m10 offset-m1">
									<input id="streamingurl" type="url" class="validate devicetextfield" 
										required name="streamingurl"
										placeholder='<i18n:message value="insert_streaming_url"/>'>
									<label for="streamingurl"><i18n:message value="streaming_url" /></label>
								</div>	
							</div>
	
							<div class="bottommargin"></div>
	
							<!-- Switch Geolocalization device -->
							<div class="row section">
								
								<div class="row bottommargin">
									<div class="col s10 offset-s1">
										<h5>
											<i18n:message value="camera_localization" />
										</h5>
									</div>
								</div>
	
								<!-- Geolocalization -->
								<div class="row devicegeolocation">
									<div class="col s12">&nbsp;</div>
									<div class="col s10 offset-s1 m4 offset-m1">
										<div class="input-field ">
											<input id="pac-input-lat" type="number" class="validate"
												required readonly
												placeholder='<i18n:message value="camera_latitude"/>'>
											<label for="pac-input"><i18n:message
													value="device_latitude" /></label>
										</div>
										<div class="input-field ">
											<input id="pac-input-lng" type="number" class="validate"
												required readonly
												placeholder='<i18n:message value="camera_longitude"/>'>
											<label for="pac-input"><i18n:message
													value="device_longitude" /></label>
										</div>
									</div>
									<div class="col s10 offset-s1 m6 ">
										<div class="tooltipped z-depth-1" id="map" data-position="top"
											data-delay="500"
											data-tooltip='<i18n:message value="locate_camera"/>'></div>
									</div>
									<div class="col s12">&nbsp;</div>
								</div>
							</div>
	
					
						</div>
					</div>
					
					<!-- Form Buttons -->
					<div class="divider row"></div>
					<div class="right">
						<a href="home" id="btnCancel" class="waves-effect waves-light btn" >
							<i18n:message value="cancel" />
						</a>
						<button type="submit" id="submit_camera" class="waves-effect waves-light btn">
							<i18n:message value="save" />
						</button>
						<!-- <a class="waves-effect waves-light btn red"><i18n:message value="delete"/></a> -->
					</div>
				</form>
			</div>
		</div>
		<br>
		<%-- 	<%@include file="../frames/newdevice_newattr.jspf"%> --%>
		<%-- 	<%@include file="../frames/newdevice_newcommand.jspf"%> --%>
		<%@include file="../frames/newcamera_confirmmodal.jspf"%>
	</div>



	<%@include file="../frames/footer.jspf"%>
	
	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/camera.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/newcamera.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/newcamera.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/context_menu.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<%@include file="../frames/messages.jspf"%>

</body>
</html>
