
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<html class="iotmanager">
<head>
	<title><i18n:message value="apptitle" /></title>
	<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
	
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="css/materialize.css"/>
	
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/newdevice.css" />
	<link rel="stylesheet" href="css/maps.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"  crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"  crossorigin=""></script>
					
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
				
	<link rel="stylesheet" href="css/leaflet.extra-markers.min.css">
	<link rel="stylesheet" href="css/leaflet-search.src.css">
	<script src="js/leaflet.extra-markers.min.js"></script>
	<script src="js/leaflet-search.src.js"></script>
	<!-- Compiled JavaScript -->
	<script src="js/materialize.js"></script>
	
	<script type="text/javascript">
		var currentDevice = '${currentDevice}';
		var actionmenu = '${actionmenu}';
	</script>
			
	<!-- DE Tools file configuration from EnablerShowcase -->
	<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 


</head>

<body class="materialize">
	<%@include file="../frames/config.jspf"%>
	<header>
		<%@include file="../frames/navbar.jspf"%>
	</header>
	
	<div class="container">
	<br>
		<div class=" white">
			<div class="section">
				<div class="row">
					<div class="col s2 valign">
					</div>
					<div class="col s8 center">
						<h4 id="new_device_title" class="hide">
							<i18n:message value="new_device" />
						</h4>
						<h4 id="edit_device_title" class="hide">
							<i18n:message value="edit_device" />
						</h4>
					</div>
					<div class="col s2">
						<a href="home" id="backarrow" class="btn-floating  waves-effect waves-light right" > 
							<i class="material-icons">close</i>
						</a>
					</div>
				</div>
			</div>
	
	
			<div id="contentloader" class="" style="display:none;">
					  <div class="progress">
					      <div class="indeterminate"></div>
					  </div>
			</div>
	
	
			<div class="row">
				<form action="#!" id="newdeviceform">
	
					<div class="row">
						<div id="filterSelect" class="col s10 offset-s1">
							<%@include file="../frames/context_menu.jspf"%>
						</div>
					</div>
	
					<div class="row">
						<div class="col s12">
	
							<input id="deviceid" type="hidden" class="devicetextfield"  name="id">
	
	
	
							<div class="row">
								
								<div class="row nomargin">
									<div class="col s12 m10 offset-m1">
										<h5>
											<i18n:message value="device_name_and_organization"/>
										</h5>
									</div>
								</div>
								<!-- 			Device name -->
								<div class="input-field col s12 m5 offset-m1">
								    <!-- removed _ and - from allowed characters for checkName because not allowed in KapuaKura -->
									<input id="name" type="text" class="validate devicetextfield" 
										onkeyup="return checkName(this, event);"
										pattern="[A-Za-z0-9]+"
										required name="name"
										placeholder='<i18n:message value="insert_device_name"/>'>
									<label for="name"><i18n:message value="device_name" /></label>
									<span id="blank_space_error" class="helper-text" data-error=""></span>
								</div>
								
								<div class="input-field col s12 m5">
									<select id="organizationId" class="devicetextfield">
		
									</select> 
									<label><i18n:message value="user_organization" /></label>
								</div>

							</div>
	
	
	
							<!-- 			Transport Protocol		HTTP/MQTT/SIGFOX/COAP				 -->
							<div class="row section">
								<div class="col s12 m5 offset-m1">
									<div class="row nomargin">
										<div class="">
											<h5>
												<i18n:message value="transport_protocol" />
												<a class="helpLink tooltipped" data-position="bottom" data-delay="50" data-tooltip="<i18n:message value="help_protocol" />" href="help#h_protocol" target="_blank">
													<i class="material-icons">help</i>
												</a>
											</h5>
										</div>
									</div>
									<div class="">
										<div class="input-field ">
											<select id="transport_protocol" class="contextProcolSelMain">
												<!-- disabled -->
												<option value="" disabled><i18n:message value="choose_protocol" /></option>
												<!-- Change for add hubs for DeMa version 2: add options dynamically -->
												<!-- <option selected value="HTTP">HTTP</option>
												<option value="MQTT">MQTT</option>
												<option value="SIGFOX">SIGFOX</option>
												<option value="COAP">CoAP</option>
												<option value="OPCUA">OPC UA</option>
												<option value="LORA">LoRa</option>
												-->
											</select>
										</div>
									</div>
								</div>
	
								<!-- 			Data format						 -->
								<div class="col s12 m5">
									<div class="row nomargin">
										<div class="">
											<h5>
												<i18n:message value="data_format" />
												<a class="helpLink tooltipped" data-position="bottom" data-delay="50" data-tooltip="<i18n:message value="help_format" />" href="help#h_format" target="_blank">
													<i class="material-icons">help</i>
												</a>
											</h5>
										</div>
									</div>
									<div class="">
										<div class="input-field ">
											<select id="dataformat_protocol" class="contextDataFormatProcolSelMain">
												<!-- selected -->
												<option value="" disabled><i18n:message	value="choose_data_format" /></option>
												<option value="UL2_0">UL2.0</option>
												<option value="JSON" selected>JSON</option>
<!-- 												following are attached via javascript function changeTransportProtocol -->
													<!-- Change for add hubs for DeMa version 2: add options dynamically -->
<!-- 												<option value="SIGFOX">SIGFOX</option> -->
<!-- 												<option value="LWM2M">LWM2M</option> -->
<!-- 												<option value="OPCUA">OPC UA</option> -->
<!-- 												<option value="CAYENNELPP">CayenneLpp</option> -->
<!-- 												<option value="CBOR">CBOR</option> -->
<!-- 												<option value="APPLICATION_SERVER">LoRA-JSON</option> -->
											</select>
										</div>
									</div>
								</div>
							</div>
	
	
							<!-- LORA -->
							<%@include file="../frames/newdevice_lora.jspf"%>
	
	
	
							<!-- 			Switch type of data or retrive_data_mode PUSH PULL -->
							<div class="section">
								<div class="row">
								
									<div class="col s12 m10 offset-m1">
										<h5>
											<i18n:message value="txt_retrieve_data" />
											<span class=" tooltipped" data-position="bottom" data-delay="50" data-tooltip="<i18n:message value="help_retrievemode" />" >
													<i class="material-icons">help</i>
											</span>
										</h5>
									</div>
									
									<div class="col s12 m10 offset-m1">
										<br>
										<div class="switch _isactive">
											<label>
												<i18n:message value="txt_push" /> 
												<input id="retrievedatamode" type="hidden"> 
												<input id="retrievedatamodeCheck" type="checkbox" class="switchRetrieveMode"> 
												<span class="lever "></span> 
												<i18n:message value="txt_pull" /> 
											</label>
										</div>
									</div>
							
								<!-- 	Device pull section -->
								
									<!-- Device endpoint -->
									<div class="col s12 m10 offset-m1 hide deviceEndpoint">
										<br>
										<div class="input-field">
											<input id="device_endpoint" type="url"
												class="validate devicetextfield required tooltipped"
												name="device_endpoint" value="http://217.172.12.145:8100"
												placeholder='<i18n:message value="insert_device_endpoint"/>'
												data-position="top" data-delay="500"
												data-tooltip='<i18n:message value="msg_mqtt_topic_communication"/>'>
											
											<label id="txt_opcua_device_endpoint" for="device_endpoint"
												class="hide"><i18n:message	value="txt_opcua_device_endpoint" />
											</label> 
											
											<label id="txt_http_device_endpoint" for="device_endpoint"		
												class="hide"><i18n:message	value="txt_http_device_endpoint" />
											</label> 
											
											<label
												id="txt_mqtt_broker_endpoint" for="device_endpoint"
												class="hide"><i18n:message
													value="txt_mqtt_broker_endpoint" />
											</label>
											<label
												id="txt_kapua_device_endpoint" for="device_endpoint"
												class="hide"><i18n:message value="txt_kapua_device_endpoint" />
											</label>
										</div>
										
										
										<div id="listDeviceOpcua" class="hide">
											<label  for="listDeviceOpcuaSel" >Devices managed by the current OPC Server</label>
												<select id="listDeviceOpcuaSel" >
												
													<option value="mes10_plcManual">Manual_mes</option>
													<option value="mes20_plcMagazineFront">MagazineFront_mes</option>
													<option value="mes30_plcDrillingCPS">DrillingCPS_mes</option>
													<option value="mes50_plcRobotAssembly">RobotAssembly_mes</option>
													<option value="mes60_plcCameraInspection">CameraInspection_mes</option>
													<option value="mes70_plcMagazineBack">MagazineBack_mes </option>
													<option value="mes80_plcPress">Press_mes</option>
													<option value="ene1">Manual_energy</option>
													<option value="ene2">MagazineFront_energy</option>
													<option value="ene3">DrillingCPS_energy</option>
													<option value="ene5">RobotAssembly_energy</option>
													<option value="ene6">CameraInspection_energy</option>
													<option value="ene7">MagazineBack_energy</option>
													<option value="ene8">Press_energy</option>
											    </select> 
										</div>
									</div>
								</div>	
									
									
									
								<div class="row devicepullsection hide nomargin">
		
									<!-- *** Device authentication section *** -->
									<div class=" col s12 m10 offset-m1 deviceauthsection hide">
										<!-- Auth method -->
										<div class="row authmethodsection nomargin">
											<div class="row nomargin">
												<div class="input-field ">
													<p>
														<b><i18n:message value="txt_deviceauth_method" /></b>
													</p>
												</div>
												<div class="">
													<select id="device_auth">
														<option value="none"><i18n:message value="txt_deviceauth_none" /></option>
														<option value="basic"><i18n:message value="txt_deviceauth_basic" /></option>
														<option value="apikey"><i18n:message value="txt_deviceauth_apikey" /></option>
													</select>
												</div>
											</div>
										</div>
										<!-- credentials -->
										<div class="row credentialsection nomargin">
											<div class="section">
												<div class="row bottommargin">
													<div class="input-field">
														<div class="input-field basic_user_section hide">
															<input id="deviceauth_basic_user" type="text"
																class="validate devicetextfield required tooltipped newusernamefield"
																name="deviceauth_basic_user"
																placeholder='<i18n:message value="txt_insert_user"/>'
																data-position="top" data-delay="500"
																data-tooltip='<i18n:message value="msg_deviceauth_basic_user"/>'>
															<label for="user">
																<i18n:message value="deviceauth_basic_user" /></label>
														</div>
														<div class="input-field basic_pwd_section hide">
															<input id="deviceauth_basic_pwd" type="text"
																class="validate devicetextfield required tooltipped newupwdfield"
																name="deviceauth_basic_pwd"
																placeholder='<i18n:message value="txt_insert_pwd"/>'
																data-position="top" data-delay="500"
																data-tooltip='<i18n:message value="msg_deviceauth_basic_pwd"/>'>
															<label for="deviceauth_basic_pwd">
																<i18n:message value="deviceauth_basic_pwd" /></label>
														</div>
														<div class="input-field auth_apikey_section hide">
															<input id="deviceauth_apikey" type="text"
																class="validate devicetextfield required tooltipped newapikeyfield"
																name="deviceauth_apikey"
																placeholder='<i18n:message value="txt_insert_apikey"/>'
																data-position="top" data-delay="500"
																data-tooltip='<i18n:message value="msg_deviceauth_apikey"/>'>
															<label for="deviceauth_apikey">
																<i18n:message value="deviceauth_apikey" /></label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!-- *** Device command section *** -->
								<div class="row section commandsection hide">
									<div class="switch _isactive">
									
										<div class="row nomargin">
											<div class="col s12 m5 offset-m1">
												<h5>
													<i18n:message value="device_commands" />
												</h5>
											</div>
											<div class="col s12 m5">
												<a href="#newcommand" class="modal-trigger btn-floating waves-effect waves-light right">
													<i class="material-icons">add</i>
												</a>
											</div>
										</div>
										<div class="col s12 m10 offset-m1">
											<p id="nocommands-msg" class="left-align text-italic blue-grey lighten-4">
												<i18n:message value="no_command" />
											</p>
										</div>
										<div class="col s12 m10 offset-m1">
											<ul class="collection with-header hide" id="commandappender">
												<li class="collection-header">
													<h6>
														<b><i18n:message value="command_list"/></b>
													</h6>
												</li>
												<li class="collection-item template newcommand avatar hide">
													<i class="material-icons circle blue-grey">lens</i> 
													<span class="col s11 left-align title commandname">{commandname}</span>
													<a href="#!" class="col s1 secondary-content deletecommand">
														<i class="material-icons secondary-text">delete</i>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							
	
							<div class="bottommargin"></div>
								
								<!-- 			Attributes -->
							<div class="row section attributesection">
								<div class="row nomargin">
									<div class="col s12 m5 offset-m1">
										<h5>
											<i18n:message value="device_attributes" />
										</h5>
									</div>
									<div class="col s12 m5">
										<a href="#newattr" class="btn-floating waves-effect waves-light right modal-trigger">
											<i class="material-icons">add</i>
										</a>
									</div>
								</div>
								<div class="col s12 m10 offset-m1">
									<p id="noattrs-msg" class="left-align text-italic blue-grey lighten-4">
										<i18n:message value="no_attribute" />
									</p>
								</div>
								<div class="col s12 m10 offset-m1">
									<ul class="collection with-header hide" id="attrappender">
										<li class="collection-header">
											<h6>
												<b><i18n:message value="attributes_list" /></b>
											</h6>
										</li>
										<li class="collection-item avatar template newattr hide">
											<i class="material-icons circle green">wifi</i> 
											<span class="attrname col s11 left-align title">{attrname}</span>
											<p class="attrtype col s11 left-align">
												type: <i class="typevalue">{attrtype}</i>
											</p> 
											<p class="attrobjschema col s11 left-align hide">
												schema: <i class="objschemavalue">{objschemavalue}</i>
											</p>
											<a href="#!" class="col s1 secondary-content deleteattr">
												<i class="material-icons secondary-text">delete</i>
											</a>
										</li>
									</ul>
								</div>
							</div>
					
							<!-- Kapua asset section, to show in alternative to Attributes -->
							<div class="row section kapuaassetsection hide">
								<div class="row nomargin">
									<div class="col s12 m5 offset-m1">
										<h5>
											<i18n:message value="sensor_connected" />
										</h5>
									</div>
									<div class="col s12 m5">
										<a href="#newasset" id="addasset" class="btn-floating waves-effect waves-light right modal-trigger">
											<i class="material-icons">add</i>
										</a>
									</div>
								</div>
								<div class="col s12 m10 offset-m1">
									<p id="noassetattrs-msg" class="left-align text-italic blue-grey lighten-4">
										<i18n:message value="no_attribute" />
									</p>
								</div>
								<div class="col s12 m10 offset-m1">
									<ul class="collection with-header hide" id="assetattrappender">
										<!--<li class="collection-header">
											<h6>
												<b><i18n:message value="attributes_list" /></b>
											</h6>
										</li> -->
										<li class="collection-header assettitle hide">
											<h6>
												<b>Asset: <i class="assetname">{assetname}</i></b>
												<a href="#!" class="col s1 secondary-content deleteassetattr">
													<i class="material-icons secondary-text">delete</i>
												</a>
											</h6>
										</li>
										<li class="collection-item avatar template newattr hide">
											<i class="material-icons circle green">wifi</i> 
											<span class="attrname col s11 left-align title">{attrname}</span>
											<p class="attrtype col s11 left-align">
												type: <i class="typevalue">{attrtype}</i>
											</p>
											<!-- <a href="#!" class="col s1 secondary-content deleteattr">
												<i class="material-icons secondary-text">delete</i>
											</a> -->
										</li>
									</ul>
								</div>
								<!-- Section for modbus address input -->
								<div class="col s12 m2 offset-m1 hide kapuadriversection">
									<br>
									<div class="input-field">
										<input id="modbus_addr" type="text"
													class="validate modbusaddresstextfield required tooltipped newapikeyfield"
													onkeyup="return checkModbusType(this, event);"
													pattern="[0-9]+"
													name="unitid" 
													placeholder='<i18n:message value="insert_modbus_address"/>'
													data-position="top" data-delay="500"
													data-tooltip='<i18n:message value="msg_modbus_address"/>'>
										<label
											id="modbus_address" for="modbus_addr"><i18n:message value="txt_device_modbus_address" />
										</label>
										<span id="blank_space_error" class="helper-text" data-error=""></span>
									</div>	
								</div>
							</div>
								
								
							<!-- 			Switch Geolocalization device -->
	
							<div class="row section">
								<div class="row bottommargin">
									<div class="col s12 m10 offset-m1">
										<h5>
											<i18n:message value="device_localization" />
										</h5>
									</div>
								</div>
								<div class="col s12 m10 offset-m1">
									<div class="switch _isactive ">
										<label>
										    <i18n:message value="fixed_device" /> 
										    <input id="mobile_device" type="hidden" value="false"> 
										    <input id="isMobileDevice" type="checkbox" class="switchmobiledevice" value="off">
											<span class="lever "></span> 
											<i18n:message	value="mobile_device" />
										</label>
									</div>
								</div>
							</div>
	
							<!-- 			Geolocalization -->
							<div class="row devicegeolocation">
								<div class="col s12">&nbsp;</div>
								<div class="col s12 m4 offset-m1">
									<div class="input-field ">
										<input id="pac-input-lat" type="number" class="validate" max="90" min="-90" step="any" required placeholder='<i18n:message value="device_latitude"/>'>
										<label for="pac-input"><i18n:message value="device_latitude" /></label>
									</div>
									<div class="input-field">
										<input id="pac-input-lng" type="number" class="validate" max="180" min="-180" step="any" required placeholder='<i18n:message value="device_longitude"/>'>
										<label for="pac-input"><i18n:message value="device_longitude" /></label>
									</div>
									<div class="right" >
										<button id="find-coordinates" type="button" class="waves-effect waves-light btn">
											<i18n:message value="find" />
										</button>	
									</div>
								</div>
								<div class="col s12 m6 ">
									<div class="tooltipped z-depth-1" id="map" data-position="top" data-delay="500" data-tooltip='<i18n:message value="locate_device"/>'></div>
								</div>
								<div class="col s12">&nbsp;</div>
							</div>	
								
							</div>
						</div>
					</div>
					
		<% if (Boolean.parseBoolean( ConfTools.getString("bigdataEnabled"))){ %>			
					
					<div class="row section">
								<div class="row bottommargin">
									<div class="col s12 m10 offset-m1">
										<h5>
											<i18n:message value="measures_historicizing" />
											<span class=" tooltipped" data-position="bottom" data-delay="50" data-tooltip="<i18n:message value="help_historicizing" />" >
													<i class="material-icons">help</i>
											</span>
										</h5>
									</div>
								</div>
								<div class="col s12 m10 offset-m1">
									<div class="switch _isactive ">
										<label> 
											<i18n:message value="off" /> 
											<input	id="historicizing" type="hidden" value="false"> 
											<input  id="isHistory" type="checkbox" class="switchHistoricizing" value="off">
											<span class="lever "></span> 
											<i18n:message	value="on" />
										</label>
									</div>
								</div>
								
								<br/><br/>
								
								<div id ="removeOldSection" class="row bottommargin hide">
									<div class="col s12 m10 offset-m1">
									
										<i18n:message value="remove_already_stored_data" />
									
										<div class="switch _isactive ">
											<label> 
												<i18n:message value="no" /> 
												<input	id="removeOld" type="hidden" value="false"> 
												<input  id="isRemoveOld" type="checkbox" class="switchRemoveOld" value="off">
												<span class="lever "></span> 
												<i18n:message	value="yes" />
											</label>
										</div>
									</div>
								</div>
					</div>
			<% } %>					
					
	
					<!-- 			Form Buttons -->
					<div class="divider row"></div>
					<div class="right">
							<a id="btnCancel" href="home" class="waves-effect waves-light btn">
								<i18n:message value="cancel" />
							</a>
							<button type="submit" id="submit_device" class="waves-effect waves-light btn">
								<i18n:message value="save" />
							</button>
							<!-- <a class="waves-effect waves-light btn "><i18n:message value="delete"/></a> -->
					</div>
				</form>
			</div>
		</div>
	<br>
	
		<%@include file="../frames/newdevice_newattr.jspf"%>
		<%@include file="../frames/newdevice_newasset.jspf"%>
		<%@include file="../frames/newdevice_newcommand.jspf"%>
		<%@include file="../frames/newdevice_confirmmodal.jspf"%>
	</div>



	<%@include file="../frames/footer.jspf"%>

	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/deviceattribute.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicemashup.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecommand.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecredential.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/device.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/newdevice.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/internalattribute.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/lorawan.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/applicationserver.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/newdevice.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/context_menu.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<script type="text/javascript">
		var connectedUserId = '${userId}';
		orgs = JSON.parse('${organization}');
		$('#organizationId').empty();
		$.each(orgs, function(i, org) {
			$('#organizationId').append(new Option(org,org));
		});
		$('.select').formSelect();
	</script>
	
	<%@include file="../frames/messages.jspf"%>

</body>
</html>
