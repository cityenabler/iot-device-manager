<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!-- OBJECT FROM BACKEND -->
<c:forEach items="${requestScope.userPerms}" var="userPerms">
	<c:if
		test="${fn:containsIgnoreCase(userPerms.applicationRole, 'Seller')}">
		<c:set var="isSeller" scope="session" value="true" />
	</c:if>
	<c:if test="${fn:containsIgnoreCase(userPerms.asset, 'scope')}">
		<c:set var="scopePermission" scope="session"
			value="${userPerms.permissionCRUD}" />
	</c:if>
	<c:if test="${fn:containsIgnoreCase(userPerms.asset, 'urbanservice')}">
		<c:set var="urbanServicePermission" scope="session"
			value="${userPerms.permissionCRUD}" />
	</c:if>
</c:forEach>


<html class="iotmanager">
	<head>
		<title>IoT Device Manager</title>
		<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/materialize.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		
		<link rel="stylesheet" href="css/main.css" />
		<link rel="stylesheet" href="css/home.css" />
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>

		<!-- DE Tools file configuration from EnablerShowcase -->
		<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	</head>
	
	<body class="materialize grey lighten-3">
	<%@include file="../frames/config.jspf"%>
	
		<div id="containerLayout">
		
			<div id="headerLayout">
				<header>
					<%@include file="../frames/navbar.jspf"%>
				</header>
			</div>
			
			<div id="bodyLayout">
				<div class="container">
					<div class="row hide" id="errorMessage"></div>
			
					<div id="catlist" class="row">
						<div class="col s12 m6 l4 catitem enabler" id="city">
							<div class="hoverable z-depth-1 block white nopadding">
								<a href="index?enabler=city" class="catanchor secondary-text">
									<span class="labelholder"><i18n:message value="enabler" /></span>
									<span class="catname">City</span>
								</a>
							</div>
						</div>
						
						<div class="col s12 m6 l4 catitem enabler" id="facility"">
							<div class="hoverable z-depth-1 block white nopadding">
								<a href="index?enabler=facility" class="catanchor secondary-text">
									<span class="labelholder"><i18n:message value="enabler" /></span>
									<span class="catname">Facility </span>
								</a>
							</div>
						</div>
						
						<div class="col s12 m6 l4 catitem enabler" id="farm">
							<div class="hoverable z-depth-1 block white nopadding">
								<a href="index?enabler=farm" class="catanchor secondary-text">
									<span class="labelholder"><i18n:message value="enabler" /></span>
									<span class="catname">Farm </span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footerLayout">
				<%@include file="../frames/footer.jspf"%>
			</div>
			
			<iframe id="knowage-loader" class="hide" src=""></iframe>
			<%@include file="../frames/messages.jspf"%>
		</div>
	</body>

</html>
