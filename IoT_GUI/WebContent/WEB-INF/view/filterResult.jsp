<%@page import="it.eng.iot.configuration.Conf"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!-- Check on function create , delete -->
		<c:forEach items="${requestScope.userPerms}" var="userPerms">
			<c:if test = "${ userPerms.asset == 'device'}">
				<!--  CHECK: IF THE CONNECTED USER can CREATE -->
				<c:if test = "${fn:contains(userPerms.permissionCRUD, 'C')}">  
					<c:set var = "allowCreate" scope = "session" value = "true"/>
				</c:if>
				<!--  CHECK: IF THE CONNECTED USER can DELETE -->
				<c:if test = "${fn:contains(userPerms.permissionCRUD, 'D')}">  
					<c:set var = "allowDelete" scope = "session" value = "true"/>
				</c:if>
				<!--  CHECK: IF THE CONNECTED USER can UPDATE -->
				<c:if test = "${fn:contains(userPerms.permissionCRUD, 'U')}">  
					<c:set var = "allowUpdate" scope = "session" value = "true" />
				</c:if>
			 </c:if>
		</c:forEach>
		

<html class="iotmanager">
<head>
	<title>IoT Device Manager</title>
	<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="css/materialize.css">
	
	<link rel="stylesheet" href="css/main.css"/>
<!-- 	<link rel="stylesheet" href="css/filters.css"/> -->
	<link rel="stylesheet" href="css/devices.css"/>
	
	<link rel="stylesheet" href="css/minimize.css"/>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"  crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"  crossorigin=""></script>
				
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"  crossorigin="anonymous">
			
	<link rel="stylesheet" href="css/leaflet.extra-markers.min.css">
	<script src="js/leaflet.extra-markers.min.js"></script>
	
	<!-- Compiled and minified JavaScript -->
 	<script src="js/materialize.min.js"></script>
 	
 	<script type="text/javascript" src="./js/websocket/sockjs.js"></script>
	<script type="text/javascript" src="./js/websocket/stomp.js"></script>
	
	<script type="text/javascript">
		var filteredEntities = JSON.parse('${filteredEntities}');
	</script>
	
	
	<!-- DE Tools file configuration from EnablerShowcase -->
	<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
		
	<%
		String  measureNotificationWebsocketEndpoint = ConfTools.getString("measure-notification.weboscket.endpoint");
		String  statusControllerWebsocketEndpoint = ConfTools.getString("status-controller.weboscket.endpoint");
	%>
	
	<script type="text/javascript">
		var measureNotificationWebsocketEndpoint = '<%=measureNotificationWebsocketEndpoint %>';
		var statusControllerWebsocketEndpoint = '<%=statusControllerWebsocketEndpoint %>';
		console.log("DeMa v_<%=Conf.getVersion()%>");
	</script>
	
</head>

<body class="materialize">
	<%@include file="../frames/config.jspf"%>
	<div id="containerLayout">
	
		<div id="headerLayout">
			<header>
				<%@include file="../frames/navbar.jspf"%>
			</header>
		</div>
			
		<div id="bodyLayout">	
	
			<div class="section">
				<div class="h-40">
					<div class="row  valign-wrapper">
						<div class="col valign">
							<a href="urbanservices?scope=" id="backlink" class="btn-floating waves-effect waves-light left">
								<i class="material-icons">arrow_back</i>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<div id="contentloader" class="" style="display:none;">
					  <div class="progress">
					      <div class="indeterminate"></div>
					  </div>
					</div>
			
			<div class="container white">
				<div class="row hide" id="errorMessage"></div>
				
				<div id="listvisualization" class="row">
					<div class="col l3 m6 s12 hide">
					
						<p><i18n:message value="scope"/></p>
						<div class="switch">
							<label>
							    <i18n:message value="off"/>
							    <input type="checkbox" class="scopecheckbox" >
							    <span class="lever"></span>
							    <i18n:message value="on"/>
						  	</label>
						</div>
						
						<p><i18n:message value="urbanservice"/></p>
						<div class="switch">
							<label>
							    <i18n:message value="off"/>
							    <input type="checkbox" class="uscheckbox" >
							    <span class="lever"></span>
							    <i18n:message value="on"/>
						  	</label>
						</div>
						
						<p><i18n:message value="device"/></p>
						<div class="switch">
							<label>
							    <i18n:message value="off"/>
							    <input type="checkbox" class="devicecheckbox" checked>
							    <span class="lever"></span>
							    <i18n:message value="on"/>
						  	</label>
						</div>
					
					</div>
					<div class="col s12 hide-on-med-and-up">&nbsp;</div>
					<ul id="resultslist" class="collection col l8 offset-l2 m6 offset-m3 s12">
					
						<li id="NoDeviceToShowFilter" class=" hide collection-item ">
							<i18n:message value="no-device-to-show"/>
						</li>
					
						<%@include file="../frames/filtereditem.jspf" %> 
					</ul>
				</div>
				
				
		 	</div>
			<%@include file="../frames/index_devicedetails.jspf" %>
		 	<%@include file="../frames/newdevice_infopointmodal.jspf"%>
		 	<%@include file="../frames/newdevice_infopointcmdmodal.jspf"%>
		 	<%@include file="../frames/newdevice_simconfmodal.jspf"%>
		 	<%@include file="../frames/newdevice_listeners.jspf"%>
		 </div>
		<div id="footerLayout">	
			<%@include file="../frames/footer.jspf"%>
		</div>
	</div>
		
	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/index.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<script type="text/javascript" src="js/model/device.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/deviceattribute.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecommand.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecredential.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/category.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<script type="text/javascript" src="js/filters.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/devicedetails.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicemashup.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/newlistener.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<%@include file="../frames/messages.jspf"%>

</body>

</html>
