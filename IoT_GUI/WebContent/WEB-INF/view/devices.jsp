<%@page import="it.eng.iot.configuration.Conf"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@page import="it.eng.iot.configuration.ConfTools"%>

<!-- JSTL -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
			

<html class="iotmanager">
<head>
	<title><i18n:message value="apptitle"/></title>
	<link rel="shortcut icon" href="favicon.ico?" type="image/x-icon" />
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
   	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="css/materialize.css">
	<link rel="stylesheet" href="css/nouislider.css">
	
	<link rel="stylesheet" href="css/main.css"/>
	<link rel="stylesheet" href="css/devices.css"/>
	
	<link rel="stylesheet" href="css/minimize.css"/>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
   	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
   	
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"  crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"  crossorigin=""></script>
					
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"  crossorigin="anonymous">
			
	<link rel="stylesheet" href="css/leaflet.extra-markers.min.css">
	<script src="js/leaflet.extra-markers.min.js"></script>
   	 		
	<!-- Compiled and minified JavaScript -->
	<script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/nouislider.min.js"></script>
    
    <script type="text/javascript" src="./js/websocket/sockjs.js"></script>
	<script type="text/javascript" src="./js/websocket/stomp.js"></script>

	<!-- DE Tools file configuration from EnablerShowcase -->
	<script src="<%= ConfTools.getString("configurationTools")%>?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script> 
						
	<script type="text/javascript">
		currentLanguage = '<%=request.getSession().getAttribute("lang")%>';
		userId = '<%=session.getAttribute("userId")%>';
		organizations = '<%=session.getAttribute("organization")%>';
	</script>
		
	<%
        boolean isMultienabler = Boolean.parseBoolean(Conf.getString("MultiEnabler.enabled"));
		String  measureNotificationWebsocketEndpoint = ConfTools.getString("measure-notification.weboscket.endpoint");
		String  statusControllerWebsocketEndpoint = ConfTools.getString("status-controller.weboscket.endpoint");
	%>	
	<script type="text/javascript">
		var isMultienabler = <%=isMultienabler %>;
		var defaultEnabler = '<%=Conf.getString("MultiEnabler.default") %>';
		var measureNotificationWebsocketEndpoint = '<%=measureNotificationWebsocketEndpoint %>';
		var statusControllerWebsocketEndpoint = '<%=statusControllerWebsocketEndpoint %>';
		console.log("DeMa v_<%=Conf.getVersion()%>");
	</script>
	
</head>

<body class="materialize">
	<%@include file="../frames/config.jspf"%>
	<%@include file="../frames/loader.jspf"%>
		
	<div id="container_layout">
	
		<div id="header_layout">
			<%@include file="../frames/navbar.jspf"%>
		</div><!-- header_layout -->	
		
		<div id="body_layout">
		
			<div>
				<%@include file="../frames/side-nav.jspf"%>
			</div>
			<div id="mainContainer" class="white z-depth-0">
			
				<%@include file="../frames/index_actionbar.jspf" %>
				
				<div class="row hide" id="errorMessage"></div>
				
				<div id="listvisualization">
				
					<div id="pagination-show" class="row">
						<div id="pagination-long"></div>
						<div id="deviceperpage" class="center"><span><i18n:message value="show"/>: </span>
							<a id="dev6" class="deviceperpage btn-floating btn-flat active" href="#!">6</a>&bull;
							<a id="dev15" class="deviceperpage btn-floating btn-flat" href="#!">15</a><span id="dev24Bullet" >&bull; </span> 
							<a id="dev24" class="deviceperpage btn-floating btn-flat" href="#!">24</a>
							<b><i18n:message value="of"/></b>&nbsp; &nbsp;<span id="totalNUmberOfDevice">0</span>
						</div>
					</div>
					
					
					<div id="contentloader" class="" style="display:none;">
					  <div class="progress">
					      <div class="indeterminate"></div>
					  </div>
					</div>
					
					<div id="devicelist">
						
						<%@include file="../frames/index_deviceitem.jspf" %>
						
						<div class="row">
							<div id="noresults" class="">
								<p id="NoDeviceToShow" class="hide"><i18n:message value="no-device-to-show"/></p>
								<p id="SelectFilters" class=""><i18n:message value="please-use-the-filters"/></p>
							</div>
						</div>
			
					</div>
				</div>
				
				<div id="mapvisualization" class="hide">
					<div id="map-container">
						<h5 id="mapTitle"></h5>
						<div id="context-map" ></div>
					</div>
				</div>
			</div>
		</div><!-- body_layout -->
		
		<div id="footer_layout">
			<%@include file="../frames/footer.jspf"%>
		</div><!-- footer_layout -->
	
	</div><!-- container_layout -->
	
	<%@include file="../frames/index_devicedetails.jspf" %>
	<%@include file="../frames/newdevice_infopointmodal.jspf"%>
	<%@include file="../frames/newdevice_infopointcmdmodal.jspf"%>
	<%@include file="../frames/newdevice_massiveloadmodal.jspf"%>
	<%@include file="../frames/newdevice_simconfmodal.jspf"%>
	<%@include file="../frames/newdevice_listeners.jspf"%>
	<%@include file="../frames/deletedevice_confirm.jspf"%>
	<%@include file="../frames/deletedevices_confirm.jspf"%>
	
	<script type="text/javascript" src="js/model/device.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/deviceattribute.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecommand.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicecredential.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/category.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/config.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/main.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/util.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/devices.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/materialize-pagination.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/devicedetails.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/model/devicemashup.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	<script type="text/javascript" src="js/newlistener.js?<%=GregorianCalendar.getInstance().getTimeInMillis()%>"></script>
	
	<script type="text/javascript">
		orgs = JSON.parse('${organization}');
		$('#organizationid').empty();
		$.each(orgs, function(i, org) {
			$('#organizationid').append(new Option(org,org));
		});
		$('.select').formSelect();
	</script>
	
	<%@include file="../frames/messages.jspf"%>
	

</body>

</html>