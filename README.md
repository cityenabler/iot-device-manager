![IoTDeviceManager logo](https://devices.digitalenabler.eng.it/imgs/logo.png)
# IoT Device Manager

This tool is part of the [Digital Enabler](https://digitalenabler.eng.it) ecosystem platform.
The tool is intended for easily manage devices and sensors.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This tool runs on [Tomcat >=8.5](https://tomcat.apache.org/download-80.cgi).
Another important pre-requisite is having an instance of a FIWARE IoT Stack up and running with at least one IoT Agent and one Orion Context Broker.
For more information on how to set-up an IoT Stack take a look at the [FIWARE Tutorials](https://fiware-tutorials.readthedocs.io/en/latest/index.html).

### Installing

Once you have an IoT Stack instance up and running you can checkout the code of the IoT Device Manager through:

```
git clone https://bitbucket.org/cityenabler/iot-device-manager.git
```

If you use an IDE like [Eclipse](https://www.eclipse.org/) you can perform the checkout directly form it and the checkout will result in a project in your workspace.
Once checked-out, open the project and edit the properties files located in the folder: 

```
src/it/eng/iot/configuration
```

In this folder you can find a properties file for each IoT Agent you want to configure.

In order to make an agent working you have only to configure the correct ip and port where the tool should try to contact the agent.
E.g. the json agent can be configured through the file:

```
src/it/eng/iot/configuration/configuration_iota_json.properties
```
in this file you have to configure the property: 

```
iota.json.host=127.0.0.1
```

Then repeat for each agent you want to configure.

Once configured the devices, you have to configure the Identity Manager and the Orion Context Broker.
Such a configuration can be done, respectively, through the files:

```
src/it/eng/iot/configuration/configuration_idm.properties
src/it/eng/iot/configuration/configuration_orion.properties
```

Once everything is properly configured you can start your tomcat application server and navigate to [http://localhost:8080/dema](http://localhost:8080/dema).

You will be immediately redirected to the FIWARE Identity Manager you've configured and, once authenticated, you will be able to see the homepage of the IoT Device Manager. 



## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

In case you need to add the support for a **new protocol and/or dataformat** you can refer to [this](docs/new-agent.md) documentation.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Antonino Sirchia** - *Development team Leader* - [antonino.sirchia@eng.it](mailto:antonino.sirchia@eng.it)
* **Filippo Giuffrida** - *Development team member* - [filippo.giuffrida@eng.it](mailto:filippo.giuffrida@eng.it)
